﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using DataAccess;
using Microsoft.Extensions.Configuration;
using MimeKit;
using Ozytis.Common.Core.Emails.Core;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using static Business.EmailContentsManager;

namespace Business
{
	public class EmailsManager
    {
        public EmailsManager(DataContext dataContext, IConfiguration configuration, EmailContentsManager emailContentsManager)
        {
            this.DataContext = dataContext;
            this.Configuration = configuration;
            this.EmailContentsManager = emailContentsManager;
        }

        public DataContext DataContext { get; }

        public IConfiguration Configuration { get; }

        public EmailContentsManager EmailContentsManager { get; }

        //MAIL ENVOIE CREATION DU MOT DE PASSE
        public async Task SendInvitationEmailAsync(string userFullName, string userEmail, string token, bool isParticipant)
        {
            //var emailTemplate = await this.EmailContentsManager.GetEmailTemplateAsync("NewAccount", "fr-FR");

            //adapter url en fonction rôle utilisateur (editor ou player)
            var baseUrl = isParticipant ? this.Configuration["Data:SituLearn.Player:applicationUrl"] : this.Configuration["Data:Network:BaseUrl"];

            var url = $"{baseUrl}activate-account?email={WebUtility.UrlEncode(userEmail)}&token={WebUtility.UrlEncode(token)}";

            List<EmailTemplateVar> variables = new List<EmailTemplateVar>()
            {
                new()  {
                    Id = "UserNameAndCivility",
                    Replacement = userFullName,
                    ReplacementType = EmailTemplateVarReplacementType.Text
                },
                new()  {
                    Id = "Link",
                    Replacement = url,
                    ReplacementType = EmailTemplateVarReplacementType.Button,
                    NewText = "Je finalise mon inscription"
                }
            };

            var emailTemplate = await this.EmailContentsManager.GetPopulatedEmailTemplateAsync(EmailContentsManager.NewAccountEmail, "fr-FR", variables);

            //string content = emailTemplate.Content.Replace("@Link", url);
            //content = content.Replace("@UserNameAndCivility", userFullName);
            MimeMessage email = new();
            email.From.Add(new MailboxAddress(this.Configuration["Data:Emails:DefaultSenderName"], this.Configuration["Data:Emails:DefaultSenderEmail"]));
            email.To.Add(new MailboxAddress(userFullName, userEmail));
            email.Subject = emailTemplate.Subject;
            //email.AddBody(null, content);
            email.AddBody(null, emailTemplate.Content);

            await email.SendWithSmtpAsync();
        }
    }
}
