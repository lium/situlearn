﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Business;
using Common;
using Common.Security;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Ozytis.Common.Core.Api;
using Ozytis.Common.Core.Storage;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Server.ApiTransform;

namespace Web.Server.Controllers
{
    [Authorize]
	[Route("api/games")]
	public class GamesController : ControllerBase
	{
		public GamesController(GamesManager gamesManager, UsersManager usersManager, IFileSystem fileSystem, IConfiguration configuration)
		{
			this.GamesManager = gamesManager;
			this.UsersManager = usersManager;
			this.FileSystem = fileSystem;
			this.Configuration = configuration;
			this.PictureFileUrlPrefix = this.Configuration["Data:Network:BaseUrl"] + "files?path=";
		}

		public GamesManager GamesManager { get; }
		public UsersManager UsersManager { get; }

		public IFileSystem FileSystem { get; }

		public IConfiguration Configuration { get; }

		public string PictureFileUrlPrefix { get; }

		[Authorize]
		[HttpGet("mygames/{lastUpdateDate}")]
		public async Task<IEnumerable<Game>> GetMyGamesAsync(DateTime lastUpdateDate)
		{
			ApplicationUser player = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

			if (player == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

            var games = await this.GamesManager.GetAllPlayerGames(player.Id)
				.Include(g => g.Players)
					//.ThenInclude(p => p.CurrentUnit)
				 .Include(g => g.Players)
					//.ThenInclude(p => p.Journey)
				//.Include(g => g.FieldTrip)
				.Include(g => g.Players)
					.ThenInclude(p => p.PlayedUnits)
					.ThenInclude(u => u.PlayedActivities)
					.ThenInclude(a => a.Files)
				.Include(g => g.Players)
					.ThenInclude(p => p.PlayedUnits)
					.ThenInclude(u => u.PlayedActivities)
					//.ThenInclude(a => a.Activity)
				.Include(g => g.Players)
					.ThenInclude(p => p.PlayedUnits)
				//.ThenInclude(u => u.Unit)
				.Where(g => g.LastUpdateDate >= lastUpdateDate)
				.ToArrayAsync();

			var files = games.SelectMany(g => g.Players)
				.SelectMany(p => p.PlayedUnits)
				.SelectMany(pu => pu.PlayedActivities)
				.SelectMany(pa => pa.Files);

			foreach (var file in files)
			{
				if (!string.IsNullOrEmpty(file.Url))
				{
                    try
                    {
                        byte[] data = await this.FileSystem.ReadAllBytesAsync(file.Url);

                        if (data != null && data.Length > 0)
                        {
                            file.Url = "data:" + file.MimeType + $";base64,{Convert.ToBase64String(data)}";

                        }
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }				
			}

			foreach (var game in games)
			{
				foreach (var playerGame in game.Players)
				{
					playerGame.User = null;
					foreach (var playedUnit in playerGame.PlayedUnits)
					{
						if (playedUnit.Unit != null)
						{
							playedUnit.Unit.InformationSheetPicture = this.PrefixPictureUrl(playedUnit.Unit.InformationSheetPicture);
						}
					}
				}
			}

			return games;
		}

		[Authorize]
		[HandleBusinessException]
		[HttpPut("mygames")]
		public async Task UpdateMyGamesAsync([FromBody] Game[] games)
		{
			ApplicationUser player = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

			if (player == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.GamesManager.UpdateGamesAsync(games, player);

		}

		[Authorize]
		[HandleBusinessException]
		[HttpPut]
		public async Task UpdateMyGameAsync([FromBody] Game game)
		{
			ApplicationUser player = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

			if (player == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.GamesManager.UpdateGameAsync(game, player);

		}

		[Authorize]
		[HandleBusinessException]
		[HttpPut("position")]
		public async Task UpdatePositionAsync([FromBody] PlayerPositionUpdateModel model)
		{
			await this.GamesManager.UpdatePlayerPositionAsync(model.PlayerId, model.Latitude, model.Longitude, model.BatteryLevel, model.LastUpdatedPositionDate);
		}


		[HttpGet("headers")]
		public async Task<IEnumerable<FieldTripMadeHeaderModel>> GetAllFieldTripsMadeHeadersAsync()
		{
			ApplicationUser player = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

			var fieldTripsMadeHeader = await this.GamesManager.SelectAll()
				.Include(g => g.FieldTrip)
					.ThenInclude(ft => ft.ExplorationMap)
                .Include(g => g.Players)
					.ThenInclude(p => p.PlayedUnits)
						.ThenInclude(pu => pu.Unit)
				.Include(g => g.Players)
					.ThenInclude(p => p.PlayedUnits)
						.ThenInclude(pu => pu.PlayedActivities)
							.ThenInclude(pa => pa.Activity)
				.Include(g => g.Players)
					.ThenInclude(p => p.PlayedUnits)
						.ThenInclude(pu => pu.PlayedActivities)
							.ThenInclude(pa => pa.Files)
				.Where(g => g.Players.Any(p => g.EndDate.HasValue && p.UserId == player.Id))
				.OrderByDescending(g => g.EndDate.Value)
				.Select(game => new FieldTripMadeHeaderModel
				{
					GameId = game.Id,
					FieldTrip = game.FieldTrip,
					PlayedUnits = game.Players.FirstOrDefault(p => p.UserId == player.Id).PlayedUnits
							.Select(pu => pu.Unit)
							.ToList(),
					PlayedReadingActivities = game.Players.FirstOrDefault(p => p.UserId == player.Id).PlayedUnits
							.Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
							.SelectMany(pu => pu.PlayedActivities)
							.Where(pa => pa.Activity.ActivityType == ActivityType.Statement && pa.Latitude.HasValue && pa.Longitude.HasValue)
							.OrderBy(pa => pa.AnswerDate)
							.ToList(),
                    TotalPoints = game.Players.FirstOrDefault(p => p.UserId == player.Id).PlayedUnits
                            .Sum(pu => pu.LocationPoints)
                            +
                            game.Players.FirstOrDefault(p => p.UserId == player.Id).PlayedUnits
                                .Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
                                .SelectMany(pu => pu.PlayedActivities)
                                .Sum(pa => pa.Points ?? 0),
                    RealizedTime = (int)(game.EndDate.Value - game.StartDate).TotalMinutes
				}
				)
				.ToArrayAsync();

			foreach (var fieldtripMadeHeader in fieldTripsMadeHeader)
			{
				foreach (var playedUnit in fieldtripMadeHeader.PlayedUnits)
				{
					playedUnit.InformationSheetPicture = this.PrefixPictureUrl(playedUnit.InformationSheetPicture);
				}
			}

			foreach (var fieldtripMadeHeader in fieldTripsMadeHeader)
			{
				foreach (var playedReadingActivity in fieldtripMadeHeader.PlayedReadingActivities)
				{
					foreach (var file in playedReadingActivity.Files)
					{
						file.Url = !string.IsNullOrEmpty(file.Url) ? this.PrefixPictureUrl(file.Url) : file.Url;
					}
				}
			}

			return fieldTripsMadeHeader;
		}

		[Authorize]
		[HttpGet("share-reading/{readingId:guid}")]
		[ValidateModel, HandleBusinessException]
		public async Task ShareReadingAsync(Guid readingId)
		{
			ApplicationUser player = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

			if (player == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.GamesManager.ShareReadingAsync(readingId, player);
        }

        [Authorize]
        [HttpGet("share-report/{gameId:guid}")]
        [ValidateModel, HandleBusinessException]
        public async Task ShareReportAsync(Guid gameId, string email = null)
        {
            ApplicationUser player = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

            if (player == null)
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            await this.GamesManager.ShareReportAsync(gameId, email, player);
        }

        [HttpPost("statements")]
		[HandleBusinessException]
		public async Task<Dictionary<Guid, bool>> CheckPlayedActivitiesForStatementsRemoval([FromBody] List<Guid> playedActivityIds)
		{
			return await this.GamesManager.GetPlayedActivityIdIsPurgeDictionary(playedActivityIds);
		}

		private string PrefixPictureUrl(string url)
		{
			if (string.IsNullOrEmpty(url))
			{
				return null;
			}

			return this.PictureFileUrlPrefix + url;
		}

		[Authorize]
		[HandleBusinessException]
		[HttpPut("unit")]
		public async Task UpdatePlayedUnitAsync([FromBody] PlayedUnitUpdateModel model)
		{
			ApplicationUser user = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

			PlayedUnit playedUnit = new PlayedUnit
			{
				Id = model.Id,
				HaveLocatedUnit = model.HaveLocatedUnit,
				PlayerId = model.PlayerId,
				UnitId = model.UnitId,
				LocalisationReachDate = model.LocalisationReachDate,
				LocationPoints = model.LocationPoints
			};

			await this.GamesManager.UpdatePlayedUnitAsync(playedUnit, model.GameId, model.FielTripId, model.JourneyId, model.StartCurrentUnitDate,
				model.CurrentUnitActivationHasBeenUnlocked, user);
		}

		[Authorize]
		[HandleBusinessException]
		[HttpPut("activity")]
		public async Task UpdatePlayedActivityAsync([FromBody] PlayedActivityUpdateModel model)
		{
			ApplicationUser user = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

			PlayedActivity playedActivity = new PlayedActivity
			{
				Id = model.Id,
				ActivityId = model.ActivityId,
				ActivityName = model.ActivityName,
				Answer = model.Answer,
				AnswerDate = model.AnswerDate,
				Latitude = model.Latitude,
				Longitude = model.Longitude,
				PlayedUnitId = model.PlayedUnitId,
				Points = model.Points,
				Files = model.Files
			};

			await this.GamesManager.UpdatePlayedActivityAsync(playedActivity, model.PlayerId, model.UnitId, model.GameId, model.FielTripId, model.JourneyId, user);
		}

		[Authorize]
		[HandleBusinessException]
		[HttpPut("end")]
		public async Task EndGameAsync([FromBody] EndGameUpdateModel model)
		{
			await this.GamesManager.EndGameAsync(model.GameId, model.PlayerId, model.EndGameDate);
        }

        [AllowAnonymous]
        [HttpGet("report")]
        [NoGeneration]
        public async Task<FileResultModel> DownloadReportAsync(string path)
        {
			try
			{
                string dateString = path.Substring(path.Length - 20).Substring(0, 10);
                var dateSplit = dateString.Split(' ').Select(d => Int32.Parse(d));
                var dateLink = new DateOnly(dateSplit.ElementAt(2), dateSplit.ElementAt(1), dateSplit.ElementAt(0));

				if (dateLink < DateOnly.FromDateTime(DateTime.UtcNow).AddDays(-7))
				{
                    throw new BusinessException("Le fichier n'est plus disponible (le lien n'est valide qu'une semaine)");
                }

                var result = await this.FileSystem.ReadAllBytesAsync(path);

				if (result == null)
				{
					throw new BusinessException("Le fichier n'a pas été trouvé");
				}

				string fileName = path.Replace("readings-to-share/", "");

                return new FileResultModel(Convert.ToBase64String(result), "application/zip", fileName);
            }
			catch (Exception e)
            {
                throw new BusinessException(new string[] { e.Message });
            }
        }

        [HttpDelete("{userId}")]
        [Authorize]
        [HandleBusinessException]
        public async Task DeleteMyGamesAsync(string userId)
        {
            var current = this.UsersManager.SelectAllUsers().FirstOrDefault(u => u.UserName == this.User.Identity.Name);

            var user = await this.UsersManager.SelectUserAsync(userId);

            if (user.Id != current.Id)
            {
                throw new BusinessException("Vous n'êtes pas autorisé à supprimer les données de participation d'un autre utilisateur");
            }

            await this.GamesManager.DeleteMyGamesAsync(userId);
		}

		[Authorize(Policy = Policy.UserCanCreateFieldTrips)]
		[HttpGet("monitor-players/{fieldTripId:guid}")]
		public async Task<IEnumerable<PlayerForMonitorModel>> GetAllPlayersForMonitorAsync(Guid fieldTripId, DateTime startTime)
		{
			IEnumerable<Player> players = await this.GamesManager.GetAllPlayersStartedGameAfterSupervisionStartTimeAsync(fieldTripId, startTime.ToUniversalTime());

			return players.Select(p => p.ToMonitorApiResult()).ToArray();
		}
	}
}
