﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.User
{
    [Authorize(Policy = Common.Security.Policy.UserIsAdministrator)]
    [Route(UserDetailsPage.Url)]
    public partial class UserDetailsPage : ComponentBase
    {
        public const string Url = "user/{userId}";

        public static string GetUrl(string userId)
        {
            return Url.Replace("{userId}", userId);
        }

        [Inject]
        public UsersRepository UsersRepository { get; set; }


        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Parameter]
        public string UserId { get; set; }

        public UserModel User { get; set; }


        protected override async Task OnInitializedAsync()
        {
            this.User = await this.UsersRepository.GetUserAsync(this.UserId);

            this.StateHasChanged();
        }


    }
}
