﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;

using ClientApi;

using Entities;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Ozytis.Common.Core.ClientApi.Storage;
using SituLearn.Player.Repositories.IndexedDbManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Player.Repositories
{
    public class FieldTripsRepository : BaseRepository
    {
        public string[] localPictures = new string[]
        {
            "images/avatar.svg",
            "images/bookmark.svg",
            "images/btn-map.svg",
            "images/check.svg",
            "images/close.svg",
            "images/compas.svg",
            "images/field.svg",
            "images/home-picture-logo.svg",
            "images/hourglass.svg",
            "images/icon-people.svg",
            "images/journey-texture-dark.svg",
            "images/journey-texture-light.svg",
            "images/leaflet-marker-information-sheet.svg",
            "images/leaflet-marker-myposition.svg",
            "images/leaflet-marker-reading.svg",
            "images/leaflet-marker-remaining-unit.svg",
            "images/leaflet-marker-unit.svg",
            "images/locked.svg",
            "images/logout.svg",
            "images/radar.svg",
            "images/scanner.svg",
            "images/share-nodes.svg",
            "images/starred-3x.svg",
            "images/stopwatch.svg",
            "images/texture.svg",
        };

        const string FieldTripsCacheKeyName = "fieldtrips";
        const string SavedFieldTripsCacheKeyName = "saved-fieldtrips";

        private readonly FieldTripsService fieldTripsService = new();

        public Dictionary<Guid, FieldTripHeaderModel> FieldTrips { get; set; } = new();

        public Dictionary<Guid, FieldTrip> SavedFieldTrips { get; set; } = new();

        public IConnectivityService ConnectivityService { get; }
        public IJSRuntime JSRuntime { get; }
        public event EventHandler OnCacheUnavailable;

        [Inject]
        public NavigationManager NavigationManager { get; set; }
        public FieldTripsManager FieldTripsManager { get; }
        public FieldTripHeadersManager FieldTripHeadersManager { get; }
        public bool HasAddedPicturestoCache { get; set; }

        public FieldTripsRepository(IClientStorageService storageService, IConnectivityService connectivityService, IJSRuntime jSRuntime, FieldTripsManager fieldTripsManager, FieldTripHeadersManager fieldTripHeadersManager) : base(storageService)
        {
            this.ConnectivityService = connectivityService;
            this.JSRuntime = jSRuntime;
            this.FieldTripsManager = fieldTripsManager;
            this.FieldTripHeadersManager = fieldTripHeadersManager;
        }

        public async Task InitializeAsync()
        {
            await this.FieldTripHeadersManager.InitializeAsync();
            var fieldTripHeaders = await this.FieldTripHeadersManager.SelectAll();
            this.FieldTrips = fieldTripHeaders.ToDictionary(f => f.Id, f => f);
            var savedFieldTrips = await this.FieldTripsManager.SelectAll();
            this.SavedFieldTrips = savedFieldTrips.ToDictionary(s => s.Id, s => s);

        }

        public async Task<IEnumerable<FieldTripHeaderModel>> GetAllFieldTripsHeadersAsync()
        {
            if (this.FieldTrips == null || !this.FieldTrips.Any() || this.ConnectivityService.IsOnline)
            {
                await this.LoadFieldTripsHeadersAsync();
            }

            return this.FieldTrips.Values;
        }

        public async Task LoadFieldTripsHeadersAsync()
        {
            var fieldTrips = await this.fieldTripsService.GetAllFieldTripsHeadersAsync();

            if (fieldTrips != null)
            {
                this.FieldTrips = fieldTrips.ToDictionary(x => x.Id, x => x);
                await this.FieldTripHeadersManager.BulkCreateOrUpdateById(this.FieldTrips);

            }
        }

        public async Task AddFieldTripsHeadersPicturesInCacheAsync()
        {
            if (this.FieldTrips != null)
            {
                try
                {
                    foreach ((_, FieldTripHeaderModel fieldTrip) in this.FieldTrips)
                    {
                        await this.AddPictureToCacheAsync(fieldTrip.Picture);
                    }
                }
                catch (Exception e)
                {
                    this.OnCacheUnavailable.Invoke(this, null);
                    Console.WriteLine("Unable to add header picture to cache");
                }
            }
        }

        public async Task LoadSavedFieldTripsAsync()
        {
            if (this.ConnectivityService.IsOnline)
            {
                IEnumerable<FieldTrip> fieldTrips = await this.fieldTripsService.GetAllSavedFieldTripsAsync();

                if (fieldTrips != null)
                {
                    foreach (FieldTrip fieldTrip in fieldTrips)
                    {
                        try
                        {

                            await this.AddFieldTripPicturesToCacheAsync(fieldTrip);
                        }
                        catch (Exception e)
                        {
                            this.OnCacheUnavailable?.Invoke(this, null);
                            Console.WriteLine("Unable to add fieldTripPictures to cache");
                        }
                        await this.FieldTripsManager.CreateOrUpdateById(fieldTrip.Id.ToString(), fieldTrip);
                    }

                    this.SavedFieldTrips = fieldTrips.ToDictionary(f => f.Id, f => f);
                }

            }
            else
            {
                var savedFieldTrips = await this.FieldTripsManager.SelectAll();
                this.SavedFieldTrips = savedFieldTrips.ToDictionary(s => s.Id, s => s);
            }
        }

        public async Task RemoveUselessPicturesFromCacheAsync()
        {
            try
            {
                var picturesToKeep = this.FieldTrips.Values.Select(f => f.Picture);

                picturesToKeep = picturesToKeep.Concat(this.SavedFieldTrips.Values.Select(f => f.HomePagePicture));
                picturesToKeep = picturesToKeep.Concat(this.SavedFieldTrips.Values.Select(f => f.EndPagePicture));
                picturesToKeep = picturesToKeep.Concat(this.SavedFieldTrips.Values.Select(f => f.ExplorationMap.CustomBackgroundMap));

                picturesToKeep = picturesToKeep.Concat(this.SavedFieldTrips.Values.SelectMany(f => f.LocatedGameUnits).Select(lgu => lgu.InformationSheetPicture));
                picturesToKeep = picturesToKeep.Concat(this.SavedFieldTrips.Values.SelectMany(f => f.LocatedGameUnits).Select(lgu => lgu.ConclusionPicture));
                picturesToKeep = picturesToKeep.Concat(this.SavedFieldTrips.Values.SelectMany(f => f.LocatedGameUnits).Select(lgu => lgu.LocationSupportPicture));

                foreach (var activity in this.SavedFieldTrips.Values.SelectMany(f => f.LocatedGameUnits).Concat(this.SavedFieldTrips.Values.SelectMany(f => f.Journeys.SelectMany(j => j.JourneyLocatedGameUnits.Select(jlu => jlu.LocatedGameUnit)))).SelectMany(lgu => lgu.Activities))
                {
                    if (activity.ActivityType == ActivityType.Question && activity is QuestionActivity question)
                    {
                        picturesToKeep = picturesToKeep.Append(question.Picture);

                        if (question.Clue != null)
                        {
                            picturesToKeep = picturesToKeep.Append(question.Clue.Picture);
                        }

                        if (question.QuestionActivityType == QuestionActivityType.PictureMcq)
                        {
                            foreach (var answer in question.Answers)
                            {
                                picturesToKeep = picturesToKeep.Append(answer.Content);
                            }
                        }

                    }
                    else if (activity is ReadingActivity statement)
                    {
                        picturesToKeep = picturesToKeep.Append(statement.Picture);
                    }
                }

                picturesToKeep = picturesToKeep.Except(new List<string> { null });

                await this.JSRuntime.InvokeVoidAsync("window.ozytis.cache.removeUselessPicturesFromCache", picturesToKeep);
            }
            catch (Exception e)
            {
                this.OnCacheUnavailable.Invoke(this, null);
                Console.WriteLine("Unable to remove useless fieldTripPictures to cache");
            }
        }

        public async Task<FieldTrip> GetFieldTripAsync(Guid fieldTripId)
        {
            FieldTrip fieldTrip;
            await this.ConnectivityService.RefreshStateAsync();
            if (this.ConnectivityService.IsOnline)
            {
                fieldTrip = await this.fieldTripsService.GetFieldTripAsync(fieldTripId, true, true);
            }
            else
            {
                fieldTrip = this.SavedFieldTrips.FirstOrDefault(ft => ft.Key == fieldTripId).Value;
            }

            return fieldTrip;
        }

        public async Task AddOrRemoveFromFavoritesAsync(Guid fieldTripId)
        {
            await this.fieldTripsService.AddOrRemoveFromFavoritesAsync(fieldTripId);

            //MAJ du cache
            await this.LoadFieldTripsHeadersAsync();
        }

        public async Task AddOrRemoveFromDownloadsAsync(Guid fieldTripId)
        {
            //add/delete from server
            var result = await this.fieldTripsService.AddOrRemoveFromDownloadsAsync(fieldTripId);
            result.CheckIfSuccess();

            bool downloaded = result.Data;
            var newFieldTrip = await this.fieldTripsService.GetFieldTripAsync(fieldTripId, true, true);

            if (!downloaded)
            {
                //delete from indexedDb
                await this.FieldTripsManager.DeleteById(fieldTripId.ToString());
                this.SavedFieldTrips.Remove(fieldTripId);
                await this.RemoveFieldTripPicturesFromCacheAsync(newFieldTrip);
            }
            else
            {
                this.SavedFieldTrips.Add(fieldTripId, newFieldTrip);
                //add to indexedDb
                await this.FieldTripsManager.CreateOrUpdateById(fieldTripId.ToString(), newFieldTrip);
                await this.AddFieldTripPicturesToCacheAsync(newFieldTrip);
            }

            //Update header in cache
            var header = await this.FieldTripHeadersManager.SelectById(fieldTripId.ToString());
            header.IsDownloaded = downloaded;
            await this.FieldTripHeadersManager.CreateOrUpdateById(header.Id.ToString(), header);
        }

        private async Task AddFieldTripPicturesToCacheAsync(FieldTrip fieldTrip)
        {
            List<Task> tasks = new List<Task>();
            tasks.Add(this.AddPictureToCacheAsync(fieldTrip.HomePagePicture));
            tasks.Add(this.AddPictureToCacheAsync(fieldTrip.EndPagePicture));
            tasks.Add(this.AddPictureToCacheAsync(fieldTrip.ExplorationMap.CustomBackgroundMap));
            foreach (var locatedGameUnit in fieldTrip.LocatedGameUnits)
            {
                tasks.Add(this.AddPictureToCacheAsync(locatedGameUnit.InformationSheetPicture));
                tasks.Add(this.AddPictureToCacheAsync(locatedGameUnit.ConclusionPicture));
                tasks.Add(this.AddPictureToCacheAsync(locatedGameUnit.LocationSupportPicture));

                foreach (var item in locatedGameUnit.Activities)
                {
                    if (item.ActivityType == ActivityType.Question && item is QuestionActivity question)
                    {
                        tasks.Add(this.AddPictureToCacheAsync(question.Picture));

                        if (question.Clue != null)
                        {
                            tasks.Add(this.AddPictureToCacheAsync(question.Clue.Picture));
                        }

                        if (question.QuestionActivityType == QuestionActivityType.PictureMcq)
                        {
                            foreach (var answer in question.Answers)
                            {
                                tasks.Add(this.AddPictureToCacheAsync(answer.Content));
                            }
                        }

                    }
                    else if (item is ReadingActivity statement)
                    {
                        tasks.Add(this.AddPictureToCacheAsync(statement.Picture));
                    }
                }
            }

            await Task.WhenAll(tasks);

        }

        private async Task RemoveFieldTripPicturesFromCacheAsync(FieldTrip fieldTrip)
        {
            List<Task> tasks = new List<Task>();
            tasks.Add(this.RemovePictureFromCacheAsync(fieldTrip.HomePagePicture));
            tasks.Add(this.RemovePictureFromCacheAsync(fieldTrip.EndPagePicture));
            tasks.Add(this.RemovePictureFromCacheAsync(fieldTrip.ExplorationMap.CustomBackgroundMap));
            foreach (var locatedGameUnit in fieldTrip.LocatedGameUnits)
            {
                tasks.Add(this.RemovePictureFromCacheAsync(locatedGameUnit.InformationSheetPicture));
                tasks.Add(this.RemovePictureFromCacheAsync(locatedGameUnit.ConclusionPicture));
                tasks.Add(this.RemovePictureFromCacheAsync(locatedGameUnit.LocationSupportPicture));

                foreach (var item in locatedGameUnit.Activities)
                {
                    if (item.ActivityType == ActivityType.Question && item is QuestionActivity question)
                    {
                        tasks.Add(this.RemovePictureFromCacheAsync(question.Picture));

                        if (question.Clue != null)
                        {
                            tasks.Add(this.RemovePictureFromCacheAsync(question.Clue.Picture));
                        }

                        if (question.QuestionActivityType == QuestionActivityType.PictureMcq)
                        {
                            foreach (var answer in question.Answers)
                            {
                                tasks.Add(this.RemovePictureFromCacheAsync(answer.Content));
                            }
                        }

                    }
                    else if (item is ReadingActivity statement)
                    {
                        tasks.Add(this.RemovePictureFromCacheAsync(statement.Picture));
                    }
                }
            }

            await Task.WhenAll(tasks);

        }

        public async Task AddPictureToCacheAsync(string url)
        {
            if (this.JSRuntime != null && !string.IsNullOrEmpty(url))
            {
                await this.JSRuntime.InvokeVoidAsync("window.ozytis.cache.addToCache", url);
            }
        }

        private async Task RemovePictureFromCacheAsync(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                await this.JSRuntime.InvokeVoidAsync("window.ozytis.cache.removeFromCache", url);
            }
        }
    }
}
