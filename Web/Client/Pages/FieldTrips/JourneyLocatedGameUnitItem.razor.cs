// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using System.Threading.Tasks;
using Web.Client.Repositories;
using Entities;

namespace Web.Client.Pages.FieldTrips
{
    public partial class JourneyLocatedGameUnitItem : ComponentBase
    {
        [Parameter]
        public JourneyLocatedGameUnit Link { get; set; }

        [Parameter]
        public Journey Journey { get; set; }

        [Parameter]
        public EventCallback OnRemove { get; set; }

        [Parameter]
        public EventCallback OnChange { get; set; }

        [Parameter]
        public bool CanEdit { get; set; }
        [Parameter]
        public int? Index { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        protected async Task RemoveFromJourneyAsync()
        {
            await this.FieldTripsRepository.RemoveUnitFromJourneyAsync(this.Journey.FieldTripId, this.Journey.Id, this.Link.Id);
            await this.OnRemove.InvokeAsync();
        }

        protected async Task OnItemDroppedAsync(object droppedItem)
        {
            var currentIndex = this.Link.Order;

            if (droppedItem is LocatedGameUnit gameUnit)
            {
                Console.WriteLine($"Located game unit added {gameUnit.Name}");

                if (this.Journey.JourneyLocatedGameUnits.Any(link => link.LocatedGameUnitId == gameUnit.Id))
                {
                    this.Notifier.Notify(Color.Danger, $"Cette unit� est d�j� pr�sente dans le parcours {this.Journey.Name.ToLowerInvariant()}");
                }

                await this.FieldTripsRepository.AddLocatedUnitToJourneyAsync(this.Journey.FieldTripId, this.Journey.Id, gameUnit.Id, currentIndex);

                await this.OnChange.InvokeAsync(droppedItem);
            }
            else if (droppedItem is JourneyLocatedGameUnit link)
            {
                await this.FieldTripsRepository.UpdateGameUnitOrderAsync(this.Journey.FieldTripId, this.Journey.Id, link.Id,
                  currentIndex);

                await this.OnChange.InvokeAsync(droppedItem);
            }
        }
    }
}