﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Common.Security;
using Entities;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.Users
{
    [Route(UsersPage.Url)]
    [Authorize(Policy = Policy.UserIsAdministrator)]
    public partial class UsersPage : ComponentBase
    {
        public const string Url = "/users";

        public static string Icon = FontAwesomeIcons.UsersCog;

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }
        [CascadingParameter]
        public IConfirmProvider Confirm { get; set; }

        public UsersPage()
        {

        }

        public OzSimpleRemoteTable<ApplicationUser, UserModel> Table { get; set; }

      
        public UserModel[] Users { get; set; }              

        

        public async Task ConnectAs(UserModel user)
        {
            if (!await Confirm.AskAsync($"Etes vous sûr de vouloir vous connecter avec le compte de {user.FirstName} {user.LastName} ?"))
            {
                return;
            }

            await this.UsersRepository.ConnectAs(user.Id);
            this.NavigationManager.NavigateTo(FieldTrips.FieldTripsPage.IndexUrl, true);
        }

        public async Task DeleteUser(string userId)
        {
            try
            {
                if (!await Confirm.AskAsync("Êtes-vous sûr de vouloir supprimer cet utilisateur ?"))
                {
                    return;
                }

                await this.UsersRepository.DeleteUserAsync(userId);

                var entity = this.Table.Data.Find(x => x.Id == userId);

                await this.Table.LoadAllDataAsync();
                this.StateHasChanged();

                this.Notifier.Notify(Color.Success, $"L'utilisateur {entity.Email} a bien été supprimé.");
            }
            catch (Exception e)
            {
                this.Notifier.Notify(Color.Danger, $"{e.Message}");
                throw;
            }
        }

        public async Task DoSendInvitational(string email)
        {
            try
            {
                await this.UsersRepository.ResendInvitationalMail(email);
                this.Notifier.Notify(Color.Info, "Le mail d'invitation est en cours d'envoi");
            }
            catch (Exception e)
            {
                this.Notifier.Notify(Color.Danger, "Une erreur est survenue : " + e.Message);
            }
        }
    }
}
