﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Common;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.Users
{
    [Route(Url)]
    [Authorize(Policy = Common.Security.Policy.UserIsAdministrator)]
    public partial class EditPage : FormComponent
    {
        public const string Url = "users/{userId}";

        public static string GetUrl(string userId, string backUrl = null)
        {
            var url = Url.Replace("{userId}", userId);

            if (!string.IsNullOrEmpty(backUrl))
            {
                url += $"?backUrl={Uri.EscapeDataString(backUrl)}";
            }

            return url;
        }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Parameter]
        public string UserId { get; set; }

        public UserUpdateModel Model { get; set; }

        public IEnumerable<string> Roles { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var existing = await this.UsersRepository.GetUserAsync(this.UserId);

            this.Model = new();
            this.Roles = new string[] { UserRoleNames.Administrator, UserRoleNames.Designer, UserRoleNames.Participant };

            if (existing != null)
            {
                this.Model.Id = existing.Id;
                this.Model.LastName = existing.LastName;
                this.Model.FirstName = existing.FirstName;
                this.Model.Email = existing.Email;
                this.Model.PhoneNumber = existing.PhoneNumber;
                this.Model.Establishment = existing.Establishment;
                this.Model.Role = existing.Role;
            }

            await base.OnInitializedAsync();
        }

        public override async Task OnProcessAsync()
        {
            try
            {
                await this.UsersRepository.UpdateUserAsync(this.Model);
                this.Notifier.Notify(Color.Primary, "Modification effectuée.", actionEnding: async () => { this.NavigationManager.NavigateTo(UsersPage.Url); await Task.CompletedTask; });
            }
            catch (Exception e)
            {
                this.Notifier.Notify(Color.Danger, "Une erreur est survenue. \n" + e.Message);
                throw;
            }
        }
    }
}
