﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Api.Activity;
using ClientApi;
using Entities;
using Ozytis.Common.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Web.Client.Repositories
{
	public class ActivitiesRepository
	{
		private readonly ActivitiesService activitiesServices = new();

		public async Task<LocatedActivity> GetActivity(Guid activityId)
		{
			return await this.activitiesServices.GetActivityAsync(activityId);
		}

		public async Task<IEnumerable<LocatedActivity>> GetAllLocatedActivitiesAsync(Expression<Func<IQueryable<LocatedActivity>, IQueryable<LocatedActivity>>> query)
		{
			return await this.activitiesServices.GetAllLocatedActivitiesAsync(query);
		}

		public async Task<LocatedActivity> CreateQuestionAsync(QuestionCreateModel model, Guid fieldTripId)
		{
			var result = await this.activitiesServices.CreateQuestionAsync(model, fieldTripId);

			if (!result.Success)
			{
				throw new BusinessException(result.Errors);
			}

			return result.Data;
		}

		public async Task DeleteAsync(LocatedActivity activity)
		{
			await this.activitiesServices.RemoveActivityAsync(activity.Id);
		}

		public async Task<LocatedActivity> GetQuestion(Guid activityId)
		{
			return await this.activitiesServices.GetQuestionAsync(activityId);
		}

		public async Task<LocatedActivity> GetStatement(Guid activityId)
		{
			return await this.activitiesServices.GetStatementAsync(activityId);
		}

		public async Task<LocatedActivity> UpdateQuestionAsync(QuestionUpdateModel questionUpdateModel)
		{
			var result = await this.activitiesServices.UpdateQuestionAsync(questionUpdateModel);

			if (!result.Success)
			{
				throw new BusinessException(result.Errors);
			}

			return result.Data;
		}

		public async Task<LocatedActivity> CreateStatementAsync(StatementCreateModel statementCreateModel, Guid fieldTripId)
		{
			var result = await this.activitiesServices.CreateStatementAsync(statementCreateModel, fieldTripId);

			if (!result.Success)
			{
				throw new BusinessException(result.Errors);
			}

			return result.Data;
		}

		public async Task<LocatedActivity> DuplicateActivityAsync(Guid activityId, Guid locatedGameUnitId)
		{
			var result = await this.activitiesServices.DuplicateActivityAsync(activityId, locatedGameUnitId);

			if (!result.Success)
			{
				throw new BusinessException(result.Errors);
			}

			return result.Data;
		}

		public async Task<LocatedActivity> UpdateStatementAsync(StatementUpdateModel statementUpdateModel)
		{
			var result = await this.activitiesServices.UpdateStatementAsync(statementUpdateModel);

			if (!result.Success)
			{
				throw new BusinessException(result.Errors);
			}

			return result.Data;
		}

		public async Task UpdateLocatedActivityOrderAsync(Guid fieldTripId, Guid linkId, int newOrder)
		{
			var result = await this.activitiesServices.UpdateLocatedActivityOrderAsync(fieldTripId, new DraggableObjectOrderUpdateModel
			{
				LinkId = linkId,
				NewOrder = newOrder
			});
			result.CheckIfSuccess();
		}

		public async Task<Guid> AddAnswer(Guid questionId, string content, bool isCorrect)
		{
			var result = await this.activitiesServices.AddAnswerToQuestionActivityAsync(questionId, isCorrect, content);

			if (result.Success && !result.Delayed)
			{
				return result.Data;
			}
			else
			{
				throw new BusinessException(result.Delayed ? "Réseau indisponible" : "Une erreur est survenue");
			}
		}

		public async Task UpdateAnswer(Guid answerId, bool isCorrect, string content)
		{
			var result = await this.activitiesServices.UpdateAnswerAsync(answerId, isCorrect, content);
			result.CheckIfSuccess();
		}

		public async Task DeleteAnswer(Guid answerId)
		{
			var result = await this.activitiesServices.RemoveAnswerAsync(answerId);
			result.CheckIfSuccess();
		}
	}
}
