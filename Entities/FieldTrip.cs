﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;
using Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    public class FieldTrip : SoftDelete
    {
        public Guid Id { get; set; }

        [MaxLength(60)]
        public string Name { get; set; }

        [MaxLength(40)]
        public string PedagogicalFieldName { get; set; }

        public FieldTripType FieldTripType { get; set; }

        public DateTime LastUpdateDate { get; set; }

        [MaxLength(300)]
        public string Description { get; set; }

        public bool IsVisible { get; set; }

        public bool IsPublished { get; set; }

        public bool TimeIsLimited { get; set; }

        public int AllottedTimeDuration { get; set; }

        public bool ShowElapsedTime { get; set; }

        /// <summary>
		/// Option supprimée dans l'interface mais conservée en BDD
		/// </summary>
        public bool ReadingsAreVisible { get; set; }

        /// <summary>
		/// Option supprimée dans l'interface mais conservée en BDD
		/// </summary>
        public DateTime? ReadingsVisibilityStartDate { get; set; }

        /// <summary>
		/// Option supprimée dans l'interface mais conservée en BDD
		/// </summary>
        public DateTime? ReadingsVisibilityEndDate { get; set; }

        public ExplorationMap ExplorationMap { get; set; }

        [ForeignKey("ExplorationMap")]
        public Guid ExplorationMapId { get; set; }

        [InverseProperty("FieldTrip")]
        public List<LocatedGameUnit> LocatedGameUnits { get; set; }

        [MaxLength(500)]
        public string HomePagePicture { get; set; }

        [MaxLength(500)]
        public string EndPagePicture { get; set; }

        public ApplicationUser Designer { get; set; }

        [ForeignKey("Designer")]
        [MaxLength(ApplicationUser.IdMaxLength)]
        public string DesignerId { get; set; }

        //[MaxLength(1000)]
        public string HomePageText { get; set; }

        [MaxLength(250)]
        public string HomePageTitle { get; set; }

        //[MaxLength(1000)]
        public string EndPageText { get; set; }

        [MaxLength(250)]
        public string EndPageTitle { get; set; }

        [MaxLength(100)]
        public string Establishment { get; set; }

        public ICollection<Journey> Journeys { get; set; }

        public ICollection<UserFieldTrip> UserFieldTrips { get; set; }

		public bool AllowTeamNames { get; set; }
	}
}