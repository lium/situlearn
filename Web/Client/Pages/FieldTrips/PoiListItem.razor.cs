// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System.Threading.Tasks;
using Web.Client.Repositories;
using Entities;
using Common;
using Ozytis.Common.Core.Utilities;

namespace Web.Client.Pages.FieldTrips
{
    public partial class PoiListItem : ComponentBase
    {
        [Parameter]
        public LocatedGameUnit Unit { get; set; }

        [Parameter]
        public EventCallback OnDeleted { get; set; }

        [Parameter]
        public EventCallback OnUpdate { get; set; }

        [Parameter]
        public EventCallback OnEdit { get; set; }

        [Parameter]
        public EventCallback OnEditCancelled { get; set; }

        [Parameter]
        public EventCallback<ActivationAreaType> OnActivationAreaTypeChange { get; set; }

        [Parameter]
        public bool CanEdit { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Parameter]
        public FieldTrip FieldTrip { get; set; }

        [Parameter]
        public bool IsEditing { get; set; }

        public bool IsDeleting { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [CascadingParameter]
        public IConfirmProvider Confirm { get; set; }

        [Parameter]
        public bool MapIsGeolocalized { get; set; }

        protected async Task DeleteAsync()
        {
            if (this.IsDeleting || !await Confirm.AskAsync($"Etes-vous s�r de vouloir supprimer le point {this.Unit.Name} ? Cette action est irreversible"))
            {
                return;
            }

            this.IsDeleting = true;
            this.StateHasChanged();

            try
            {
                await this.FieldTripsRepository.DeleteLocatedUnitAsync(this.Unit.FieldTripId, this.Unit.Id);
                await this.OnDeleted.InvokeAsync();
            }
            catch (BusinessException ex)
            {
                this.Notifier.Notify(Color.Danger, ex.Message);
            }

            this.IsDeleting = false;
            this.StateHasChanged();
        }

        public async Task EditAsync()
        {
            await this.OnEdit.InvokeAsync();
            this.IsEditing = true;
            this.StateHasChanged();
        }

        public async Task CancelEditionAsync()
        {
            await this.OnEditCancelled.InvokeAsync();
            this.IsEditing = false;
            this.StateHasChanged();
        }
        
        public async Task UpdateAsync()
        {
            await this.OnUpdate.InvokeAsync();
            this.IsEditing = false;
            this.StateHasChanged();
        }
    }
}