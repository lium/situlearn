﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Microsoft.Extensions.Configuration;
using SituLearn.Player.Repositories;
using Ozytis.Common.Core.Web.Razor.Interfaces;

namespace SituLearn.Player.Pages.PlayedFieldTrips
{
    public partial class PlayedReadingActivityPage : ComponentBase
    {
        [Parameter]
        public PlayedActivity PlayedReadingActivityToShow { get; set; }
        [Parameter]
        public Entities.LocatedActivity Activity { get; set; }

        [Parameter]
        public Action OnClosing { get; set; }

        [Parameter]
        public Guid? PlayerId { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [Inject]
        public IConfiguration Configuration { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        public string BasePictureUrl { get; set; }

        public LocatedActivity LocatedActivity { get; set; }

        public int IndexPhotoToDisplay { get; set; }

        public int IndexAudioToDisplay { get; set; }

        public IEnumerable<PlayedActivityFile> Photos { get; set; }

        public IEnumerable<PlayedActivityFile> AudioRecordings { get; set; }

        public bool DisplayFullScreenPhoto { get; set; }

        public bool IsSharing { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.BasePictureUrl = "";
            this.LocatedActivity = this.Activity ?? this.PlayedReadingActivityToShow.Activity;
            this.Photos = this.PlayedReadingActivityToShow.Files?.Where(f => f.MimeType == "image/jpeg" || f.MimeType == "image/png");
            this.AudioRecordings = this.PlayedReadingActivityToShow.Files?.Where(f => f.MimeType.Contains("audio/"));

            await base.OnInitializedAsync();
        }

        //protected async Task ShareReadingAsync()
        //{
        //    this.IsSharing = true;
        //    this.StateHasChanged();

        //    try
        //    {
        //        await this.GamesRepository.ShareReadingAsync(this.PlayedReadingActivityToShow.Id);
        //        this.Notifier.Notify(Color.Success, "Votre demande de partage de relevé a bien été prise en compte : veuillez maintenant consulter votre messagerie afin de le retrouver.");
        //    }
        //    catch (Exception e)
        //    {
        //        this.Notifier.Notify(Color.Danger, $"Erreur lors de la demande de partage de relevé : { e.Message }");
        //        throw;
        //    }
        //    finally
        //    {
        //        this.IsSharing = false;
        //        this.StateHasChanged();
        //    }
            
        //}

    }
}
