﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Entities;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.JSInterop;
using SituLearn.Player.Pages.User;
using SituLearn.Player.Repositories;
using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace SituLearn.Player.Pages.FieldTrips
{
    [Route(Url)]
	[Route(Url2)]
	[Route(TestUrl)]
	public partial class FieldTripPage : ComponentBase
	{
		public const string Url = "jouer/{fieldTripTag}";
		public const string Url2 = "jouer/id/{fieldTripId:guid}";
		public const string TestUrl = "test/{fieldTripId:guid}";

		public static string GetUrl(Guid fieldTripId)
		{
			return Url2.Replace("{fieldTripId:guid}", fieldTripId.ToString());
		}

		[Parameter]
		public string FieldTripTag { get; set; }

		[Parameter]
		public Guid FieldTripId { get; set; }

		[Inject]
		public NavigationManager NavigationManager { get; set; }

		[Inject]
		public IConfiguration Configuration { get; set; }

        [Parameter]
        [SupplyParameterFromQuery(Name = "backUrl")]
		public string BackUrl { get; set; }

		[Inject]
		public FieldTripsRepository FieldTripsRepository { get; set; }

		[Inject]
		public UsersRepository UsersRepository { get; set; }

		[Inject]
		public GamesRepository GamesRepository { get; set; }

		[Inject]
		public IConnectivityService ConnectivityService { get; set; }

		[Inject]
		private IJSRuntime JSRuntime { get; set; }

		public FieldTrip FieldTrip { get; set; }

		public bool NotFound { get; set; }

		public string BasePictureUrl { get; set; }

		[CascadingParameter]
		public AppLayout Layout { get; set; }

		public bool TestEnv => this.Layout.TestEnv;

        public bool DisplayPictureInFullScreen { get; set; }

        public string PictureUrlToDisplayInFullScreen { get; set; }

        [JSInvokable]
		public async Task AutoLoginForTest(string jsonString)
		{
			try
			{
				LoginResult jsObject = JsonSerializer.Deserialize<LoginResult>(jsonString, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
				this.Layout.TestEnv = true;
				await this.UsersRepository.LogInAuto(jsObject);

				this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId);

				if (this.FieldTrip.Journeys.Count == 1)
				{
					this.NavigationManager.NavigateTo(JourneyPage.GetUrl(this.FieldTripId, this.FieldTrip.Journeys.First().Id));
				}
				else
				{
					this.StateHasChanged();
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				throw;
			}

		}

		private DotNetObjectReference<FieldTripPage> objRef;

		protected override async Task OnInitializedAsync()
		{
			this.BasePictureUrl = "";

			if (this.NavigationManager.ToBaseRelativePath(this.NavigationManager.Uri).StartsWith("test"))
			{
				await CommonScriptLoader.EnsureOzytisCommonIsPresentAsync(this.JSRuntime);
				await this.JSRuntime.InvokeVoidAsync("ozytis.loadScript", "js/simulation.js");

				objRef = DotNetObjectReference.Create(this);
				await this.JSRuntime.InvokeVoidAsync("registerRemote", this.Configuration["ServerUrl"].TrimEnd('/'), objRef);

				return;
			}

			if (this.FieldTripId != default)
			{
				this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId);
			}
			else if (this.FieldTripTag != default)
			{
				var stringIdBytes = Base64UrlEncoder.DecodeBytes(this.FieldTripTag);
				var fid = new Guid(stringIdBytes);

				if(!this.GamesRepository.IsOnDiscoveryMode && this.UsersRepository.CurrentUser == null)
				{
					//User is not logged in and came here via a direct link (e.g copied from Editor)
					string url = QueryHelpers.AddQueryString(LoginPage.Url, "fieldTripId", fid.ToString());
					this.NavigationManager.NavigateTo(url);
				}

				this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(fid);
			}


			if (this.FieldTrip == null)
			{
				this.NotFound = true;
			}

			this.StateHasChanged();
		}

        protected void GoBack()
		{
			var backUrl = this.BackUrl ?? (this.GamesRepository.IsOnDiscoveryMode ? string.Concat(FieldTripsPage.Url, "?discovery") : FieldTripsPage.Url);

            this.NavigationManager.NavigateTo(backUrl);
        }

        public void DisplayFullScreenPicture(string pictureUrl)
        {
            this.DisplayPictureInFullScreen = true;
            this.PictureUrlToDisplayInFullScreen = pictureUrl;
            this.StateHasChanged();
        }

        public void CloseDisplayPictureInFullScreen()
        {
            this.DisplayPictureInFullScreen = false;
            this.PictureUrlToDisplayInFullScreen = null;
            this.StateHasChanged();
        }

    }
}
