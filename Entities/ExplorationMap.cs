﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;
using Entities.Abstract;
using NetTopologySuite.Geometries;

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    public class ExplorationMap : SoftDelete
    {
        public Guid Id { get; set; }

        public bool IsGeolocatable { get; set; }

        [MaxLength(500)]
        public string CustomBackgroundMap { get; set; }

        public bool ParticipantOutOfZoneAlert { get; set; }

        public bool OrganizerOutOfZoneAlert { get; set; }

        public Geometry ExplorationArea { get; set; }
        public ActivationAreaType ExplorationShape { get; set; }
        [Column(TypeName = "decimal(8,5)")]
        public decimal? ExplorationAreaCenterLatitude { get; set; }
        [Column(TypeName = "decimal(8,5)")]
        public decimal? ExplorationAreaCenterLongitude { get; set; }
        [Column(TypeName = "decimal(7,1)")]
        public decimal ExplorationAreaRadius { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? CustomBackgroundUpperLeftLatitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? CustomBackgroundUpperLeftLongitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? CustomBackgroundLowerRightLatitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? CustomBackgroundLowerRightLongitude { get; set; }

        [Column(TypeName = "decimal(5,2)")]
        public decimal? CustomBackgroundRotation { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? CenterLatitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? CenterLongitude { get; set; }

        public int Zoom { get; set; } = 13;

        [Column(TypeName = "decimal(3,2)")]
        public decimal? CustomBackgroundOpacity { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? SouthWestLatitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? SouthWestLongitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? NorthEastLatitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? NorthEastLongitude { get; set; }
    }
}