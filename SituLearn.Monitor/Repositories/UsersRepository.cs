﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using ClientApi;
using Ozytis.Common.Core.ClientApi;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.Razor.Utilities;
using SituLearn.Monitor.Security;
using System;
using System.Threading.Tasks;

namespace SituLearn.Monitor.Repositories
{
    public class UsersRepository
    {
        private const string TokenKey = "token";
        private const string CurrentUserKey = "user";
        private readonly AccountsService accountsService = new AccountsService();

        public UsersRepository(OzytisAuthStateProvider stateProvider, LocalStorageService localStorageService)
        {
            this.StateProvider = stateProvider;
            this.LocalStorageService = localStorageService;
        }

        public event EventHandler OnCurrentUserUpdated;

        public UserModel CurrentUser { get; set; }

        public LocalStorageService LocalStorageService { get; }

        public OzytisAuthStateProvider StateProvider { get; }

        public async Task InitializeAsync()
        {
            this.CurrentUser = await this.LocalStorageService.GetItemAsync<UserModel>(CurrentUserKey);
        }

        public async Task LoginAsync(string login, string password, bool rememberMe)
        {
            this.CurrentUser = null;

            OperationResult<LoginResult> result = await this.accountsService.LoginAsync(new LoginModel
            {
                Password = password,
                StayConnected = rememberMe,
                UserName = login
            }, true);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }

            this.CurrentUser = result.Data.User;

            await this.LocalStorageService.SetItemAsync(TokenKey, result.Data.AccessToken);
            await this.LocalStorageService.SetItemAsync(CurrentUserKey, result.Data.User);

            BaseService.SetBearerToken(result.Data.AccessToken);

            this.StateProvider.NotifyStateChanged(this.CurrentUser);
        }

        public async Task LogoutAsync()
        {
            BaseService.SetBearerToken(null);
            await this.LocalStorageService.RemoveItemAsync(TokenKey);
            await this.LocalStorageService.RemoveItemAsync(CurrentUserKey);
            this.CurrentUser = null;
            await this.accountsService.LogOutAsync();
            this.StateProvider.NotifyStateChanged(null);
        }

        public async Task RefreshCurrentUserAsync(bool emitEvent = false)
        {
            this.CurrentUser = await this.accountsService.GetCurrentUserAsync();

            if (emitEvent)
            {
                this.OnCurrentUserUpdated?.Invoke(this, new EventArgs());
            }
        }
    }
}