// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using BlazorPro.BlazorSize;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.JSInterop;

using Newtonsoft.Json;

using Ozytis.Common.Core.ClientApi;
using Ozytis.Common.Core.ClientApi.Jobs;
using Ozytis.Common.Core.ClientApi.Storage;
using Ozytis.Common.Core.GeoJson;
using Ozytis.Common.Core.Logs.NetCore.Client;
using Ozytis.Common.Core.Web.Razor.Utilities;

using SituLearn.Player;
using SituLearn.Player.Repositories;
using SituLearn.Player.Repositories.IndexedDbManager;
using SituLearn.Player.Security;
using SituLearn.Player.Utilities;

using Syncfusion.Blazor;
using Syncfusion.Licensing;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace SituLearn.Player
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            SyncfusionLicenseProvider.RegisterLicense(SyncfusionKey.Key);
            WebAssemblyHostBuilder builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            BaseService.BaseUrl = builder.Configuration["ServerUrl"];
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.Configuration["ServerUrl"]) });

            builder.Services.AddSingleton<AuthenticationStateProvider, Security.OzytisAuthStateProvider>();
            builder.Services.AddSingleton<IAuthorizationPolicyProvider, Security.OzytisAuthPolicyProvider>();
            builder.Services.AddSingleton<IAuthorizationService, Security.OzytisAuthService>(sp => new OzytisAuthService(sp));
            builder.Services.AddSingleton<OzytisAuthStateProvider>(sp => sp.GetService<AuthenticationStateProvider>() as OzytisAuthStateProvider);

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                MaxDepth = 30,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Converters = GeoJsonUtils.GetSerializer().Converters
            };

            builder.Services.AddSingleton<LocalStorageService>();
            builder.Services.AddSingleton<BatteryService>();
            builder.Services.AddSingleton<NavigationWithHistoryManager>();

            builder.Services.AddSingleton<IClientStorageService, IndexDbStorageService>(sp =>
            {
                IndexDbStorageService indexDbStorageService = new IndexDbStorageService(sp.GetService<IJSRuntime>());
                indexDbStorageService.JsonSerializerSettings = JsonConvert.DefaultSettings();

                return indexDbStorageService;
            });

            builder.Services.AddSingleton<AdditionalAssemblyProvider>();
            builder.Services.AddOzLogsDashboard();


            foreach (Type manager in typeof(FieldTripsManager).Assembly.GetTypes().Where(t => t.Name.EndsWith("Manager")))
            {
                builder.Services.AddSingleton(manager);
            }

            foreach (Type manager in typeof(UsersRepository).Assembly.GetTypes().Where(t => t.Name.EndsWith("Repository")))
            {
                builder.Services.AddSingleton(manager);
            }

            builder.Services.AddSingleton<ResizeListener>();
            builder.Services.AddSingleton<GeoLocationService>();
            builder.Services.AddSingleton<AppViewModel>();
            builder.Services.AddSingleton<IMediaQueryService, MediaQueryService>();

            builder.Services.AddSingleton(services =>
            {
                var maintenanceRepository = services.GetService<MaintenanceRepository>();

                return new ConnectivityServiceOptions
                {
                    PingAction = () => maintenanceRepository.PingAsync(),
                    //PingInterval = 5000
                    PingInterval = 10000
                };
            });

            builder.Services.AddSingleton<IConnectivityService, ConnectivityService>();

            builder.Services.AddSingleton<JobsManager>();

            builder.Services.Configure<RequestLocalizationOptions>(options =>
            {
                // Define the list of cultures your app will support
                var supportedCultures = new List<System.Globalization.CultureInfo>()
                {
                    new System.Globalization.CultureInfo("fr"),
                };

                // Set the default culture
                options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("fr");

                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                options.RequestCultureProviders = new List<Microsoft.AspNetCore.Localization.IRequestCultureProvider>() {
                    new Microsoft.AspNetCore.Localization.QueryStringRequestCultureProvider()
                };
            });

            builder.Services.AddSingleton(typeof(ISyncfusionStringLocalizer), typeof(SfFrLocalizer));

            builder.Services.AddSyncfusionBlazor(options =>
            {
                options.IgnoreScriptIsolation = true;
            });

            var buildInformation = new BuildInformation
            {
                BuildDate = Assembly.GetAssembly(typeof(Program)).GetCustomAttribute<BuildInformationAttribute>()?.BuildDate,
                Branch = ThisAssembly.Git.Branch,
                Commit = ThisAssembly.Git.Commit
            };
            builder.Services.AddSingleton<BuildInformation>(buildInformation);

            WebAssemblyHost host = builder.Build();

            await host.Services.GetService<IConnectivityService>().StartListeningAsync();

            BaseService.SetBearerToken(await host.Services.GetService<LocalStorageService>().GetItemAsync<string>("token"));
            BaseService.JobsManager = host.Services.GetService<JobsManager>();
            BaseService.OnAuthorizeRequired += (sender, args) =>
            {
                var navMan = host.Services.GetService<NavigationManager>();
                var relativePath = navMan.ToBaseRelativePath(navMan.Uri);

                if (!(relativePath.StartsWith("activate-account") || relativePath.StartsWith("password-reset") || relativePath.StartsWith("login") || relativePath.StartsWith("test")))
                {
                    navMan.NavigateTo(Pages.User.LoginPage.Url);
                }
            };

            host.Services.UseOzLogsDashboard();

            IClientStorageService storageService = host.Services.GetService<IClientStorageService>();

            await storageService.InitializeAsync(new StorageOptions
            {
                Description = "SituLearnold",
                Database = "SituLearnold",
                Store = "offline",
                WriteWithWorker = false,
            });

            await host.Services.GetService<UsersRepository>().InitializeAsync();

            if (host.Services.GetService<UsersRepository>().CurrentUser != null)
            {
                await host.Services.GetService<FieldTripsRepository>().InitializeAsync();
                await host.Services.GetService<GamesRepository>().InitializeAsync();
            }

            await BaseService.JobsManager.InitAsync();

            await host.RunAsync();
        }
    }
}