﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.FieldTrips.Simulation
{
    [Route(Url)]
    public partial class Home
    {
        public const string Url = "/jouer/{fieldTripTag}";

		[Parameter]
        public string FieldTripTag { get; set; }

		[Inject]
		public FieldTripsRepository FieldTripsRepository { get; set; }

		[Inject]
		public IConfiguration Configuration { get; set; }

		[Inject]
		private IJSRuntime JSRuntime { get; set; }

		public string IframeSrc { get; set; }
		public LoginResult Token { get; set; }
		public string EditorUrl { get; set; }
		public string PlayerUrl { get; set; }

		private bool FrameQueryDone { get; set; } = false;
		
		protected override async Task OnInitializedAsync()
		{
			var stringIdBytes = Base64UrlEncoder.DecodeBytes(this.FieldTripTag);
			var fid = new Guid(stringIdBytes);

			FieldTripTestingModel result = await this.FieldTripsRepository.TestFieldTrip(fid);
			this.IframeSrc = result.PlayerUrl + $"test/{fid}?v={DateTime.UtcNow.Ticks}";
			this.Token = result.LoginResult;
			this.EditorUrl = result.EditorUrl;
			this.PlayerUrl = result.PlayerUrl.TrimEnd('/');

			await base.OnInitializedAsync();
		}

		protected override async Task OnAfterRenderAsync(bool firstRender)
		{
			if (!firstRender && !this.FrameQueryDone)
			{
				await this.JSRuntime.InvokeVoidAsync("registerRemote", this.Token, this.PlayerUrl);

				this.FrameQueryDone = true;
			}

			await base.OnAfterRenderAsync(firstRender);
		}
	}
}
