﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Activity
{
    public class QuestionCreateModel : ICreateModel
    {
        #region interface implementation
        public Guid LocatedGameUnitId { get; set; }

        [MaxLength(100, ErrorMessage = "Nom limité à {1} caractères")]
        [Required(ErrorMessage = "Nom obligatoire")]
        public string Name { get; set; }

        [MaxLength(250, ErrorMessage = "Titre limité à {1} caractères")]
        public string Title { get; set; }

        [MaxLength(500, ErrorMessage = "L'énoncé comporte trop de caractères")]
        public string Statement { get; set; }

        public string Picture { get; set; }
        #endregion

        [Required(ErrorMessage = "Gain de l'activité obligatoire")]
        public int Gain { get; set; }

        public bool ClueEnabled { get; set; }

        public int ActivityQuestionType { get; set; }

        [MaxLength(400, ErrorMessage = "Message retour bonne réponse trop long : limité à {1} caractères (hors formattage HTML)")]
        public string TextToDisplayForCorrectAnswer { get; set; }

        [MaxLength(400, ErrorMessage = "Message retour mauvaise réponse trop long : limité à {1} caractères (hors formattage HTML)")]
        public string TextToDisplayForWrongAnswer { get; set; }

        public List<AnswerUpdateModel> Answers { get; set; } = new();

        public ClueCreateModel Clue { get; set; } = new();
    }

    public class AnswerCreateModel
    {
        public string Content { get; set; }
        public bool IsCorrect { get; set; }
    }

    public class ClueCreateModel
    {
        [MaxLength(300, ErrorMessage = "Indice limité à {1} caractères")]
        public string Content { get; set; }

        public int Cost { get; set; } = 1;

        public string Picture { get; set; }
    }
}
