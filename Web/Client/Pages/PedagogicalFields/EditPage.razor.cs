﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api.PedagogicalFields;
using Common.Security;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.PedagogicalFields
{
    [Route(Url)]
    [Authorize(Policy = Policy.UserIsAdministrator)]
    public partial class EditPage : FormComponent
    {
        public const string Url = "/pedagogical-field/{id}";

        public static string GetUrl(string id, string backUrl = null)
        {
            var url = Url.Replace("{id}", id.ToString());

            if (!string.IsNullOrEmpty(backUrl))
            {
                url += $"?backUrl={Uri.EscapeDataString(backUrl)}";
            }

            return url;
        }

        [Parameter]
        public string Id { get; set; }

        [Inject]
        public PedagogicalFieldsRepository PedagogicalFieldsRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        public PedagogicalFieldUpdateModel Model { get; set; }

        protected override async Task OnInitializedAsync()
        {
            if (!int.TryParse(this.Id, out int fieldId))
            {
                this.Notifier.Notify(Color.Danger, "Identifiant de domaine pédagogique invalide.");
                await base.OnInitializedAsync();
                return;
            }

            var existing = await this.PedagogicalFieldsRepository.GetField(fieldId);

            this.Model = new();

            if (existing != null)
            {
                this.Model.Id = existing.Id;
                this.Model.Name = existing.Name;
            }

            await base.OnInitializedAsync();
        }

        public override async Task OnProcessAsync()
        {
            try
            {
                await this.PedagogicalFieldsRepository.UpdatePedagocicalField(this.Model);
                this.Notifier.Notify(Color.Primary, "Modification effectuée.", 1500, async () => { this.NavigationManager.NavigateTo(ListPage.Url); await Task.CompletedTask; });
            }
            catch (Exception e)
            {
                this.Notifier.Notify(Color.Danger, "Une erreur est survenue. \n" + e.Message);
                throw;
            }
        }
    }
}
