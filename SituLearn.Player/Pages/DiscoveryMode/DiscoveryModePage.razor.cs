﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using SituLearn.Player.Pages.FieldTrips;
using SituLearn.Player.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Player.Pages.DiscoveryMode
{
    [Route(Url)]
    public partial class DiscoveryModePage : ComponentBase
    {
        public const string Url = "/discovery-mode";

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [CascadingParameter]
        public AppLayout Layout { get; set; }

        public void NavigateTo(string to)
        {
            this.NavigationManager.NavigateTo(to);
        }

        protected async Task StartDiscoveryMode()
        {
            if (!this.GamesRepository.ConnectivityService.IsOnline)
            {
                this.Layout.Notify(Color.Danger, "Le mode Découverte n'est pas disponible hors ligne");
            }
            else
            {
                await this.GamesRepository.InitializeIndexedDbForDiscoveryModeAsync();

                var gamesInCache = await this.GamesRepository.GetAllGamesFromCacheAsync();

                if (gamesInCache != null && gamesInCache.Count() > 0)
                {
                    var answer = await Layout.AskAsync("Vous vous êtes précédemment connecté avec un compte : si vous avez effectué une sortie hors ligne sans avoir récupéré de connexion internet depuis, vos données de participation à cette sortie vont être perdues. Voulez-vous continuer ?", "Oui", "Non");

                    if (!answer.HasValue || !answer.Value)
                    {
                        return;
                    }
                }

                await this.GamesRepository.InitializeDiscoveryModeAsync();
                var uri = this.NavigationManager.ToAbsoluteUri(NavigationManager.Uri);
                var queryParameters = QueryHelpers.ParseQuery(uri.Query);

                //User came from a direct link to a fieldtrip but was not logged in nor in discovery mode
                //was redirected to login page and chose discovery mode, so we redirect him to the fieldTrip page
                if (queryParameters.TryGetValue("fieldTripId", out var fieldTripId) && Guid.TryParse(fieldTripId, out Guid fId))
                {
                    this.NavigateTo(FieldTripPage.GetUrl(fId));
                }
                else
                {
                    this.NavigationManager.NavigateTo(string.Concat(Pages.FieldTrips.FieldTripsPage.Url, "?discovery"));
                }
            }
            
            this.StateHasChanged();
        }
    }
}