﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using DataAccess;
using Entities;
using Microsoft.EntityFrameworkCore;
using Ozytis.Common.Core.Storage;
using Ozytis.Common.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace Business
{
	public class LocatedGameUnitsManager : BaseEntityManager<LocatedGameUnit>
	{
		public LocatedGameUnitsManager(DataContext context, IFileSystem fileSystem, ActivitiesManager activitiesManager) : base(context)
		{
			this.ActivitiesManager = activitiesManager;
			this.FileSystem = fileSystem;
		}

		public IFileSystem FileSystem { get; }
		public ActivitiesManager ActivitiesManager { get; }

		public async Task<LocatedGameUnit> CreateAsync(LocatedGameUnit duplicatedGameUnit)
		{
			var newEntity = this.DataContext.LocatedGameUnits.Add(duplicatedGameUnit).Entity;
			await this.SaveChangesAsync();
			return newEntity;
		}

		public async Task SoftDeleteAsync(Guid LocatedGameUnitId)
		{
			var gameUnit = await this.SelectAsync(LocatedGameUnitId);
			gameUnit.IsDeleted = true;
			await this.SaveChangesAsync();
		}

		public async Task<LocatedGameUnit> UpdateAsync(LocatedGameUnit locatedGameUnit, LocatedGameUnitUpdateModel model)
		{
			locatedGameUnit.Name = model.Name;
			locatedGameUnit.InformationSheetEnabled = model.InformationSheetEnabled;
			locatedGameUnit.ActivitiesEnabled = model.ActivitiesEnabled;
			locatedGameUnit.ConclusionEnabled = model.ConclusionEnabled;

			model.LocationSupportPicture = await this.FileSystem.SetFile(model.LocationSupportPicture, $"game-units/{locatedGameUnit.Id}/location-support");
			model.InformationSheetPicture = await this.FileSystem.SetFile(model.InformationSheetPicture, $"game-units/{locatedGameUnit.Id}/information-sheet");
			model.ConclusionPicture = await this.FileSystem.SetFile(model.ConclusionPicture, $"game-units/{locatedGameUnit.Id}/conclusion");

			locatedGameUnit.LocationAidType = model.LocationAidType;
			locatedGameUnit.LocationValidationType = model.LocationValidationType;
			locatedGameUnit.LocationGain = model.LocationGain;
			locatedGameUnit.LocationSupportTitle = model.LocationSupportTitle;
			locatedGameUnit.LocationSupportDescription = model.LocationSupportDescription;
			locatedGameUnit.LocationSupportPicture = model.LocationSupportPicture;
			locatedGameUnit.InformationSheetTitle = model.InformationSheetTitle;
			locatedGameUnit.InformationSheetDescription = model.InformationSheetDescription;
			locatedGameUnit.InformationSheetPicture = model.InformationSheetPicture;
			locatedGameUnit.ConclusionTitle = model.ConclusionTitle;
			locatedGameUnit.ConclusionText = model.ConclusionText;
			locatedGameUnit.ConclusionPicture = model.ConclusionPicture;

			locatedGameUnit.FieldTrip.LastUpdateDate = DateTime.UtcNow;

			var updatedLocatedGameUnit = this.DataContext.Update(locatedGameUnit).Entity;
			await this.SaveChangesAsync();
			return updatedLocatedGameUnit;
		}

		public async Task<LocatedGameUnit> DuplicateLocatedGameUnitAsync(Guid fieldTripId, LocatedGameUnit originalGameUnit, bool directSave = true, FieldTrip fieldTrip = null)
		{
			FieldTrip targetFieldtrip;

            if (fieldTrip != null)
			{
				targetFieldtrip = fieldTrip;
			}
			else
			{
                targetFieldtrip = this.DataContext.FieldTrips
					.Include(f => f.Journeys)
						.ThenInclude(j => j.JourneyLocatedGameUnits)
					.Include(f => f.ExplorationMap)
					.FirstOrDefault(x => x.Id == fieldTripId);
            }			

			if (targetFieldtrip == null)
			{
				throw new BusinessException("Sortie cible inconnue.");
            }

            using TransactionScope transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            LocatedGameUnit duplicatedGameUnit = new()
			{
				Id = Guid.NewGuid(),
				FieldTripId = fieldTripId,

				Name = originalGameUnit.Name,
				Longitude = originalGameUnit.Longitude,
				Latitude = originalGameUnit.Latitude,
				Order = originalGameUnit.Order,
				LocationValidationType = !targetFieldtrip.ExplorationMap.IsGeolocatable ? Common.LocationValidationType.QrCode : originalGameUnit.LocationValidationType,
				LocationAidType = !targetFieldtrip.ExplorationMap.IsGeolocatable ? Common.LocationAidType.MapWithoutGps : originalGameUnit.LocationAidType,
				LocationGain = originalGameUnit.LocationGain,

				ActivationArea = originalGameUnit.ActivationArea,
				ActivationRadius = originalGameUnit.ActivationRadius,
				ActivationAreaCenterLatitude = originalGameUnit.ActivationAreaCenterLatitude,
				ActivationAreaCenterLongitude = originalGameUnit.ActivationAreaCenterLongitude,
				ActivationAreaType = originalGameUnit.ActivationAreaType,

				LocationSupportTitle = originalGameUnit.LocationSupportTitle,
				LocationSupportDescription = originalGameUnit.LocationSupportDescription,
				//LocationSupportPicture = originalGameUnit.LocationSupportPicture,

				ConclusionTitle = originalGameUnit.ConclusionTitle,
				ConclusionText = originalGameUnit.ConclusionText,
				//ConclusionPicture = originalGameUnit.ConclusionPicture,
				ConclusionEnabled = originalGameUnit.ConclusionEnabled,

				InformationSheetTitle = originalGameUnit.InformationSheetTitle,
				InformationSheetDescription = originalGameUnit.InformationSheetDescription,
				//InformationSheetPicture = originalGameUnit.InformationSheetPicture,
				InformationSheetEnabled = originalGameUnit.InformationSheetEnabled,

				ActivitiesEnabled = originalGameUnit.ActivitiesEnabled,
				Activities = new List<LocatedActivity>()
			};

			if (!string.IsNullOrEmpty(originalGameUnit.LocationSupportPicture) && await this.FileSystem.ExistsAsync(originalGameUnit.LocationSupportPicture))
			{
				duplicatedGameUnit.LocationSupportPicture = await this.FileSystem.Duplicate(originalGameUnit.LocationSupportPicture, originalGameUnit.LocationSupportPicture.Replace($"{originalGameUnit.Id}", $"{duplicatedGameUnit.Id}"));
			}

			if (!string.IsNullOrEmpty(originalGameUnit.ConclusionPicture) && await this.FileSystem.ExistsAsync(originalGameUnit.ConclusionPicture))
			{
				duplicatedGameUnit.ConclusionPicture = await this.FileSystem.Duplicate(originalGameUnit.ConclusionPicture, originalGameUnit.ConclusionPicture.Replace($"{originalGameUnit.Id}", $"{duplicatedGameUnit.Id}"));
			}

			if (!string.IsNullOrEmpty(originalGameUnit.InformationSheetPicture) && await this.FileSystem.ExistsAsync(originalGameUnit.InformationSheetPicture))
			{
				duplicatedGameUnit.InformationSheetPicture = await this.FileSystem.Duplicate(originalGameUnit.InformationSheetPicture, originalGameUnit.InformationSheetPicture.Replace($"{originalGameUnit.Id}", $"{duplicatedGameUnit.Id}"));
			}

			targetFieldtrip.LastUpdateDate = DateTime.UtcNow;
			this.DataContext.LocatedGameUnits.Add(duplicatedGameUnit);

			foreach (var journey in targetFieldtrip.Journeys)
			{
				journey.JourneyLocatedGameUnits ??= new();

				journey.JourneyLocatedGameUnits.Add(new JourneyLocatedGameUnit
				{
					JourneyId = journey.Id,
					LocatedGameUnitId = duplicatedGameUnit.Id,
					Order = journey.JourneyLocatedGameUnits.Any() ? journey.JourneyLocatedGameUnits.Max(l => l.Order) + 5 : 0
				});
			}

			if (directSave)
			{
				await this.DataContext.SaveChangesAsync();
			}

			foreach (var activity in originalGameUnit.Activities.Where(a => !a.IsDeleted))
			{
				duplicatedGameUnit.Activities.Add(
					await this.ActivitiesManager.DuplicateLocatedActivityAsync(activity.Id, duplicatedGameUnit.Id, directSave, duplicatedGameUnit)
				);
			}

			if (directSave)
			{
				await this.SaveChangesAsync();
            }

            transactionScope.Complete();

            return duplicatedGameUnit;
		}
	}
}
