﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

window.situlearn = window.situlearn || {};
window.situlearn.indexedDB = {
    initalize: function (version) {


        return new Promise((resolve, reject) => {
            const request = window.indexedDB.open("SituLearn", version);

            console.log("init v" + version);
            request.onsuccess = function (event) {
                resolve();
            }

            request.onerror = function (event) {
                reject(request.error);
            }

            request.onupgradeneeded = function (event) {
                const db = request.result;
                console.log(event.oldVersion + "->" + event.newVersion);
                let storeName = "FieldTripHeaders";
                if (!db.objectStoreNames.contains(storeName)) {
                    const store = db.createObjectStore(storeName, { keyPath: 'key' });
                    console.log("create store!", storeName);
                }

                storeName = "SavedFieldTrips";
                if (!db.objectStoreNames.contains(storeName)) {
                    const store = db.createObjectStore(storeName, { keyPath: 'key' });
                    console.log("create store!", storeName);
                }

                storeName = "Games";
                if (!db.objectStoreNames.contains(storeName)) {
                    const store = db.createObjectStore(storeName, { keyPath: 'key' });
                    console.log("create store!", storeName);
                    let indexName = "FielTripId";
                    if (!store.indexNames.contains(indexName)) {
                        store.createIndex(indexName, "value.indexes." + indexName, { unique: false });
                    }
                }
                localStorage.setItem("SituLearnDBVersion", JSON.stringify("" + event.newVersion));
                resolve();
            };
        });
    },
    getItemByIndex: function (storeName, index, value) {
        return new Promise((resolve, reject) => {
            const request = window.indexedDB.open("SituLearn");
            //console.log("start getByIndex " + storeName);
            request.onsuccess = function () {
                const db = request.result;
                const transaction = db.transaction(storeName, 'readonly')

                const store = transaction.objectStore(storeName);
                const indexStore = store.index(index);
                const read = indexStore.openCursor(IDBKeyRange.only(value));

                const result = [];
                read.onsuccess = (event) => {
                    const cursor = event.target.result;
                    if (!cursor) {
                        //console.log("end getByIndex " + storeName);
                        resolve(result);
                    }
                    else {
                        result.push(cursor.value.value.serializedData);
                        cursor.continue();
                    }

                    //resolve(cursor.value.serializedData);
                }
                read.onerror = () => reject(read.error);
            }
            request.onerror = function () {
                reject(request.error);
            }
        });
    },
    setItem: function (storeName, key, value) {
        return new Promise((resolve, reject) => {

                const request = window.indexedDB.open("SituLearn");
            //console.log("start set " + storeName);

                request.onsuccess = function () {
                    const db = request.result;
                    const transaction = db.transaction(storeName, 'readwrite')

                    const store = transaction.objectStore(storeName);
                    const insert = store.put({ value: value, key: key });

                    insert.onsuccess = () => {
                        //console.log("end set " + storeName);
                        resolve();
                    }
                    insert.onerror = () => reject(insert.error);
                }

                request.onerror = function () {
                    reject(request.error);
                }
            
        });

    },
    setBulkItems: function (storeName, data) {
        return new Promise((resolve, reject) => {
            //console.log("start bulk " + storeName);
                const request = window.indexedDB.open("SituLearn");
                request.onsuccess = function () {
                    const db = request.result;
                    const transaction = db.transaction(storeName, 'readwrite')

                    const store = transaction.objectStore(storeName);
                    for (let obj of data) {
                        const insert = store.put({ value: obj.value, key: obj.key });
                        insert.onerror = () => reject(insert.error);
                    }
                    //console.log("end bulk " + storeName);

                    resolve();
                    //insert.onsuccess = () => resolve();
                }

                request.onerror = function () {
                    reject(request.error);
                }
        });

    },
    getItem: function (storeName, key) {

        return new Promise((resolve, reject) => {

            const request = window.indexedDB.open("SituLearn");
            //console.log("start get " + storeName);

            request.onsuccess = function () {
                const db = request.result;
                const transaction = db.transaction(storeName, 'readonly')

                const store = transaction.objectStore(storeName);
                const read = store.get(key);

                read.onsuccess = () => {

                    if (!read.result) {
                        resolve(null);
                        return;
                    }
                    //console.log("end get " + storeName);
                    if (typeof read.result.value === 'string') {
                        resolve(read.result.value);
                    }
                    else {
                        resolve(read.result.value.serializedData);
                    }
                }
                read.onerror = () => reject(read.error);
            }
            request.onerror = function () {
                reject(request.error);
            }
        });
    },
    searchItemsByPartialKey: function (storeName, partialKey) {
        return new Promise((resolve, reject) => {
            const request = window.indexedDB.open("SituLearn");
            //console.log("start selectAll " + storeName);

            request.onsuccess = function () {
                const db = request.result;

                const transaction = db.transaction(storeName, 'readonly');
                const store = transaction.objectStore(storeName);
                var data = [];
                var query = store.openCursor();
                query.onsuccess = function (event) {
                    var cursor = event.target.result;
                    if (cursor) {
                        if (cursor.key.indexOf(partialKey) > -1) {
                            if (typeof cursor.value.value === 'string') {
                                data.push(cursor.value.value);
                            }
                            else {
                                data.push(cursor.value.value.serializedData);
                            }
                        }
                        cursor.continue();
                    }
                    else {
                        //console.log("end selectAll " + storeName);
                        //console.log(data);
                        resolve(data);
                    }
                };
                query.onerror = () => reject(read.error);
            }

            request.onerror = function () {
                reject(request.error);
            }
        });
    },
    searchKeysByPartialKey: function (storeName, partialKey) {
        return new Promise((resolve, reject) => {
            //console.log("start selectAllKeys " + storeName);

            const request = window.indexedDB.open("SituLearn");

            request.onsuccess = function () {
                const db = request.result;
                const transaction = db.transaction(storeName, 'readonly');
                const store = transaction.objectStore(storeName);
                var data = [];
                var query = store.openCursor();
                query.onsuccess = function (event) {
                    var cursor = event.target.result;
                    if (cursor) {
                        if (cursor.key.indexOf(partialKey) > -1) {
                            data.push(cursor.key);
                        }
                        cursor.continue();
                    }
                    else {
                        //console.log("end selectAllKeys " + storeName);
                        //console.log(data);
                        resolve(data);
                    }
                };
                query.onerror = () => reject(read.error);
            }

            request.onerror = function () {
                reject(request.error);
            }
        });
    },
    removeItem: function (storeName, key) {

        return new Promise((resolve, reject) => {
            //console.log("start remove " + storeName);

            const request = window.indexedDB.open("SituLearn");

            request.onsuccess = function () {
                const db = request.result;
                const transaction = db.transaction(storeName, 'readwrite')

                const store = transaction.objectStore(storeName);
                const read = store.delete(key);

                read.onsuccess = () => {
                    //console.log("end remove " + storeName);

                    resolve();
                }
                read.onerror = () => reject(read.error);
            }
            request.onerror = function () {
                reject(request.error);
            }
        })
    },
};