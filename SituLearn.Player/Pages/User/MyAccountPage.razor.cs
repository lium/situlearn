﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using SituLearn.Player.Repositories;
using System;
using System.Threading.Tasks;

namespace SituLearn.Player.Pages.User
{
    [Route(MyAccountPage.Url)]
    [Microsoft.AspNetCore.Authorization.Authorize]
    public partial class MyAccountPage : ComponentBase
    {
        public const string Url = "/mon-compte";

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }
        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [Inject]
        private IJSRuntime JSRuntime { get; set; }


        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [CascadingParameter]
        public AppLayout Confirm { get; set; }

        [CascadingParameter]
        public AppLayout Layout { get; set; }

        public bool TestEnv => this.Layout.TestEnv;

        [SupplyParameterFromQuery]
        public string BackUrl { get; set; }

        public UserModel UserModel { get; set; }

        public UserUpdateModel UpdateModel { get; set; }

        public string[] Errors { get; set; }

        public bool IsEditing { get; set; }

        public bool IsProcessing { get; set; }

        public bool IsDeleting { get; set; }

        public bool IsDeletingMyGames { get; set; }

        protected void ChangeEditingMode()
        {
            this.Errors = null;
            this.ReinitUpdateModel();
            this.IsEditing = !this.IsEditing;
        }

        protected override async Task OnInitializedAsync()
        {
            if (this.GamesRepository.CurrentPlayer != null)
            {
                this.GamesRepository.CurrentPlayer.PlayerState = Common.PlayerState.Cancelled;
            }

            this.ReinitUpdateModel();

            await base.OnInitializedAsync();
        }

        protected void ReinitUpdateModel()
        {
            this.UserModel = UsersRepository.CurrentUser;

            this.UpdateModel = new UserUpdateModel
            {
                FirstName = this.UserModel.FirstName,
                LastName = this.UserModel.LastName,
                Email = this.UserModel.Email,
                PhoneNumber = this.UserModel.PhoneNumber,
                Establishment = this.UserModel.Establishment,
                Role = this.UserModel.Role,
                Language = this.UserModel.Language
            };
        }

        protected async Task SaveAsync()
        {
            if (this.IsProcessing)
            {
                return;
            }

            this.IsProcessing = true;
            this.Errors = null;

            try
            {
                await this.UsersRepository.UpdateCurrentUserAsync(this.UpdateModel);

                if (this.UpdateModel.PhoneNumber != this.UserModel.PhoneNumber)
                {
                    await this.UsersRepository.RequestPhoneNumberChangeAsync(new PhoneNumberUpdateModel { PhoneNumber = this.UpdateModel.PhoneNumber });
                }

                if (this.UpdateModel.Email != this.UserModel.Email)
                {
                    await this.UsersRepository.RequestEmailChangeAsync(new EmailUpdateModel { Email = this.UpdateModel.Email });

                    this.Notifier.Notify(Color.Success, $"Votre demande a bien été prise en compte. Un e-mail contenant les instructions vous permettant de valider votre demande de changement d'adresse e-mail vous a été envoyé à l'adresse {this.UpdateModel.Email}");
                }
                else
                {
                    this.Notifier.Notify(Color.Success, "Vos changements ont bien été pris en compte");
                }

                this.IsEditing = false;
                this.ReinitUpdateModel();
            }
            catch (BusinessException ex)
            {
                this.Errors = ex.Messages;
                this.Notifier.Notify(Color.Danger, "Erreur lors de la mise à jour des données");
            }

            this.IsProcessing = false;
        }

        public async Task DeleteMyAccount()
        {
            if (this.IsDeleting)
            {
                return;
            }

            this.IsDeleting = true;

            var answer = await Confirm.AskAsync("Êtes-vous sûr de vouloir supprimer votre compte ? Cette action est irréversible.");

            if (!answer.HasValue || !answer.Value)
            {
                this.IsDeleting = false;
                return;
            }

            try
            {
                await this.UsersRepository.DeleteUserAsync(this.UsersRepository.CurrentUser.Id);

                this.Notifier.Notify(Color.Success, "Votre compte a bien été supprimé. Vous allez être déconnecté.");
                await Task.Delay(2000);

                await this.UsersRepository.LogoutAsync();
            }
            catch (Exception e)
            {
                this.Notifier.Notify(Color.Danger, e.Message);
            }
            finally
            {
                this.IsDeleting = false;
                this.StateHasChanged();
            }
        }

        public async Task OnKeyDown(KeyboardEventArgs args)
        {
            if (args.Key == "Enter")
            {
                await this.JSRuntime.InvokeVoidAsync("window.blurActiveElement");
            }
        }

        public async Task DeleteMyGames()
        {
            if (this.IsDeletingMyGames)
            {
                return;
            }

            if (!this.GamesRepository.ConnectivityService.IsOnline)
            {
                this.Layout.Notify(Color.Danger, "La suppression des données de participation n'est pas disponible hors ligne");
            }
            else
            {
                this.IsDeletingMyGames = true;

                var answer = await Confirm.AskAsync("Êtes-vous sûr de vouloir supprimer vos données de participation ? Cette action est irréversible.");

                if (!answer.HasValue || !answer.Value)
                {
                    this.IsDeletingMyGames = false;
                    return;
                }

                try
                {
                    await this.GamesRepository.DeleteMyGamesAsync(this.UsersRepository.CurrentUser.Id);

                    this.Notifier.Notify(Color.Success, "Vos données de participation ont bien été supprimées.");
                }
                catch (Exception e)
                {
                    this.Notifier.Notify(Color.Danger, e.Message);
                }
                finally
                {
                    this.IsDeletingMyGames = false;
                    this.StateHasChanged();
                }
            }            
        }
    }
}
