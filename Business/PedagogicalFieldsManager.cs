﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api.PedagogicalFields;
using DataAccess;

using Entities;
using Ozytis.Common.Core.Utilities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
	public class PedagogicalFieldsManager : BaseEntityManager<PedagogicalField>
    {
        public PedagogicalFieldsManager(DataContext context) : base(context)
        {
        }

        public async Task<PedagogicalField> Create(PedagogicalFieldCreateModel model)
        {
            var anyUsingName = this.SelectAll().Where(d => d.Name == model.Name).Any();
            if (anyUsingName)
            {
                throw new BusinessException("Création impossible. Un domaine pédagogique existe déjà avec ce nom");
            }

            var result = this.DataContext.PedagogicalFields.Add(new PedagogicalField()
            {
                Name = model.Name,
            }).Entity;

            await this.SaveChangesAsync();
            return result;
        }

        public async Task<PedagogicalField> Update(PedagogicalFieldUpdateModel model)
        {
            var exists = await this.SelectAsync(model.Id);
            if (exists == null)
            {
                throw new BusinessException("Modification impossible. Aucun domaine pédagogique n'existe avec cet Id");
            }

            var anyUsingName = this.SelectAll().Where(d => d.Id != model.Id && d.Name == model.Name).Any();
            if (anyUsingName)
            {
                throw new BusinessException("Modification impossible. Un domaine pédagogique existe déjà avec ce nom");
            }

            exists.Name = model.Name;

            var result = this.DataContext.PedagogicalFields.Update(exists).Entity;

            await this.SaveChangesAsync();
            return result;
        }


        public async Task Delete(int fieldId)
        {
            var exists = await this.SelectAsync(fieldId);

            if (exists == null)
            {
                throw new BusinessException("Suppression impossible. Aucun domaine pédagogique n'existe avec cet Id");
            }

            this.DataContext.PedagogicalFields.Remove(exists);

            await this.DataContext.SaveChangesAsync();
        }
    }
}
