﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Common
{
    public static class ContextualHelpKeyInventory
    {
        public const string FieldTripEditionPage_Description = "FieldTripEditionPage_Description";
        public const string FieldTripEditionPage_PedagogicalField = "FieldTripEditionPage_PedagogicalField";
        public const string FieldTripEditionPage_Type = "FieldTripEditionPage_Type";
        public const string FieldTripEditionPage_HomePage = "FieldTripEditionPage_HomePage";
        public const string FieldTripEditionPage_LocatedGameUnits = "FieldTripEditionPage_LocatedGameUnits";
        public const string FieldTripEditionPage_EndPage = "FieldTripEditionPage_EndPage";
        public const string FieldTripEditionPage_OptionsDuration = "FieldTripEditionPage_OptionsDuration";
        public const string FieldTripEditionPage_OptionsDisplayStopWatch = "FieldTripEditionPage_OptionsDisplayStopWatch";
        public const string FieldTripEditionPage_OptionsTeamName = "FieldTripEditionPage_OptionsTeamName";
        public const string FieldTripMapEditionPage_GeolocatableMap = "FieldTripMapEditionPage_GeolocatableMap";
        public const string FieldTripEditionPage_Visibility = "FieldTripEditionPage_Visibility";
        public const string FieldTripEditionPage_PublishState = "FieldTripEditionPage_PublishState";
        public const string LocatedGameUnitEditionPage_LocationSupport = "LocatedGameUnitEditionPage_LocationSupport";
        public const string LocatedGameUnitEditionPage_InformationSheet = "LocatedGameUnitEditionPage_InformationSheet";
        public const string LocatedGameUnitEditionPage_Activities = "LocatedGameUnitEditionPage_Activities";
        public const string LocatedGameUnitEditionPage_Conclusion = "LocatedGameUnitEditionPage_Conclusion";
        public const string QuestionActivityCreationPage_Statement = "QuestionActivityCreationPage_Statement";
        public const string QuestionActivityCreationPage_Answer = "QuestionActivityCreationPage_Answer";
        public const string QuestionActivityCreationPage_TextToDisplayAfterAnswer = "QuestionActivityCreationPage_TextToDisplayAfterAnswer";
        public const string QuestionActivityCreationPage_Clue = "QuestionActivityCreationPage_Clue";
        public const string ReadingActivityCreationPage_Statement = "ReadingActivityCreationPage_Statement";
        public const string ReadingActivityCreationPage_Settings = "ReadingActivityCreationPage_Settings";
        public const string LocatedGameUnitEditionPage_Validation = "LocatedGameUnitEditionPage_Validation";
        public const string LocatedGameUnitEditionPage_Orientation = "LocatedGameUnitEditionPage_Orientation";
        public const string LocatedGameUnitEditionPage_Points = "LocatedGameUnitEditionPage_Points";
        public const string FieldTripMapEditionPage_Poi = "FieldTripMapEditionPage_Poi";
        public const string FieldTripMapEditionPage_Area = "FieldTripMapEditionPage_Area";
        public const string FieldTripMapEditionPage_Background = "FieldTripMapEditionPage_Background";

        public static List<string> GetKeys()
        {
            return typeof(ContextualHelpKeyInventory).GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly)
                .Select(x => x.GetRawConstantValue().ToString())
                .Distinct()
                .ToList();
        }
    }
}