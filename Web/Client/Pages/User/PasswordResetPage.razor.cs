﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.WebUtilities;
using Ozytis.Common.Core.Utilities;

using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.User
{
    [Route(PasswordResetPage.Url)]
    [AllowAnonymous]
    public partial class PasswordResetPage : ComponentBase
    {
        public const string Url = "/password-reset";

        public PasswordResetPage()
        {
           
        }

        protected override void OnInitialized()
        {
            var uri = this.NavigationManager.ToAbsoluteUri(NavigationManager.Uri);
            var queryParameters = QueryHelpers.ParseQuery(uri.Query);

            if (queryParameters.TryGetValue("email", out var email))
            {
                this.Email = email;
            }

            if (queryParameters.TryGetValue("token", out var token))
            {
                this.Token = token;
            }

            base.OnInitialized();
        }

        [Parameter]
        public string Token { get; set; }

        [Parameter]
        public string Email { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        public string[] Errors { get; set; }

        public bool IsProcessing { get; set; }

        public string NewPassword { get; set; }

        public string NewPasswordConfirmation { get; set; }

        public async Task ProcessAsync()
        {
            if (this.IsProcessing)
            {
                return;
            }

            this.IsProcessing = true;
            this.Errors = null;

            if (this.NewPassword != this.NewPasswordConfirmation)
            {
                this.IsProcessing = false;
                this.Errors = new[] { "Le mot de passe et sa confirmation ne correspondent pas" };
                return;
            }

            try
            {
                await this.UsersRepository.ResetPasswordAsync(this.Email, this.Token, this.NewPassword);
                this.Notifier.Notify(Color.Success, "Votre mot de passe a été modifié");
                await Task.Delay(2000);
                this.NavigationManager.NavigateTo(LoginPage.Url);
            }
            catch (BusinessException ex)
            {
                this.Errors = ex.Messages;
            }

            this.IsProcessing = false;
        }
    }
}
