﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

async function queryAndSaveAsFile(url, forceDownload, dotNetReference) {

    try {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        const bearer = "Bearer " + localStorage.getItem("token");
        xhr.setRequestHeader("Authorization", bearer);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.responseType = 'json';

        xhr.onload = () => {
            const result = xhr.response;
            console.log(xhr);
            console.log(!xhr.response);
            console.log(!xhr.response.data);
            console.log(!xhr.response.fileName);
            console.log(!xhr.response.mimeType);
            if (!xhr.response || !xhr.response.data || !xhr.response.fileName || !xhr.response.mimeType) {
                //dotNetReference.invokeMethodAsync("HandleSendingFail", xhr.response);
                dotNetReference.invokeMethodAsync("HandleSendingFail", JSON.parse(JSON.stringify(xhr.response)).$values);
                return;
            }

            var link = document.createElement('a');

            link.download = result.fileName;

            const blob = b64toBlob(result.fileName, result.data, result.mimeType);
            const href =/* URL.createObjectURL(blob);*/ window.URL.createObjectURL(new File([blob], result.fileName, { type: result.mimeType }));
            link.rel = "noopener noreferrer";
            link.target = '_blank';
            link.href = href;

            document.body.appendChild(link); // Needed for Firefox
            link.click();
            document.body.removeChild(link);
            dotNetReference.invokeMethodAsync("HandleSendingSuccess");
        };

        xhr.onerror = (ex) => {
            console.error(err);
            dotNetReference.invokeMethodAsync("HandleSendingFail", null);
        };

        xhr.send();
    }
    catch (ex) {
        console.error(ex);
        let arrError = [];
        if (typeof (ex) == typeof ("")) {
            arrError = ex;
        }
        else {
            arrError.push(...ex);
        }

        dotNetReference.invokeMethodAsync("HandleSendingFail", arrError);
    }
}

const b64toBlob = (fileName, b64Data, contentType = '', sliceSize = 512) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);
        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, { type: contentType, fileName: fileName });
}