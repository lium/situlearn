﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Common;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Client.Repositories;
using BlazorCaptcha;
using Ozytis.Common.Core.Utilities;
using Microsoft.AspNetCore.Components.Forms;
using System.ComponentModel.DataAnnotations;

namespace Web.Client.Pages.User
{
    [AllowAnonymous]
    [Route(RegisterPage.Url)]
    public partial class RegisterPage : FormComponent
    {
        public const string Url = "/register";

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        public UserCreationModel Model { get; set; }

        public bool ShowSuccessMessage { get; set; }
        public int TripsCount { get; private set; }
        public Captcha CaptchaComponent { get; set; }
        public string Captcha { get; set; }
        public string CaptchaInput { get; set; }
        public bool DisplayCaptcha { get; set; }

        public EditContext EditContext { get; set; }

        const int CaptchaLength = 5;
        const string CaptchaKey = "CaptchaKey";
        protected override async Task OnInitializedAsync()
        {
            this.Captcha = BlazorCaptcha.Tools.GetCaptchaWord(CaptchaLength);

            this.Model = new UserCreationModel()
            {
                Role = UserRoleNames.Designer
            };

            this.TripsCount = await this.FieldTripsRepository.GetPublishedFieldTripsCountAsync();
            this.EditContext = new EditContext(this.Model);

            this.SuccessMessage = "Votre compte a bien été créé ! Pour finaliser votre inscription, veuillez maintenant consulter votre messagerie afin d'initialiser votre mot de passe.";

            await base.OnInitializedAsync();
        }

        public override async Task OnProcessAsync()
        {
            if(!this.CaptchaInput.Equals(this.Captcha, StringComparison.CurrentCultureIgnoreCase))
            {
                this.CaptchaInput = string.Empty;
                this.StateHasChanged();
                this.ErrorMessage = "Veuillez saisir le texte contenu dans l'image";
                throw new BusinessException("Captcha invalide");
            }

            try
            {
                await this.UsersRepository.RegisterAsync(this.Model);
            }
            catch (Exception e)
            {
                this.ErrorMessage = e.Message;
                throw;
            }
        }

        public override async Task OnSuccessAsync()
        {
            await Task.Run(() =>
            {
                this.Errors = null;
                this.ShowSuccessMessage = true;
            });
        }

        public void ShowCaptcha()
        {
            ICollection<ValidationResult> results = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(this.Model, new ValidationContext(this.Model), results, true);
            if (isValid)
            {
                this.DisplayCaptcha = true;
                this.StateHasChanged();
            }
            else
            {
                this.Errors = results.Select(r => r.ErrorMessage).ToArray();
                this.StateHasChanged();
            }
        }
    }
}
