﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class EntitiesInitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clues",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    Cost = table.Column<int>(type: "int", nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clues", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExplorationMaps",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IsGeolocatable = table.Column<bool>(type: "bit", nullable: false),
                    CustomBackgroundMap = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    ParticipantOutOfZoneAlert = table.Column<bool>(type: "bit", nullable: false),
                    OrganizerOutOfZoneAlert = table.Column<bool>(type: "bit", nullable: false),
                    ExplorationArea = table.Column<Geometry>(type: "geography", nullable: true),
                    CustomBackgroundUpperLeftLatitude = table.Column<decimal>(type: "decimal(8,5)", nullable: false),
                    CustomBackgroundUpperLeftLongitude = table.Column<decimal>(type: "decimal(8,5)", nullable: false),
                    CustomBackgroundLowerRightLatitude = table.Column<decimal>(type: "decimal(8,5)", nullable: false),
                    CustomBackgroundLowerRightLongitude = table.Column<decimal>(type: "decimal(8,5)", nullable: false),
                    CustomBackgroundRotation = table.Column<decimal>(type: "decimal(5,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExplorationMaps", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PedagogicalFields",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PedagogicalFields", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FieldTrips",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: true),
                    PedagogicalFieldName = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    FieldTripType = table.Column<int>(type: "int", nullable: false),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    IsVisible = table.Column<bool>(type: "bit", nullable: false),
                    IsPublished = table.Column<bool>(type: "bit", nullable: false),
                    TimeIsLimited = table.Column<bool>(type: "bit", nullable: false),
                    AllottedTimeDuration = table.Column<int>(type: "int", nullable: false),
                    ReadingsAreVisible = table.Column<bool>(type: "bit", nullable: false),
                    ReadingsVisibilityStartDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ReadingsVisibilityEndDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ExplorationMapId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    HomePagePicture = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    EndPagePicture = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    DesignerId = table.Column<string>(type: "nvarchar(450)", maxLength: 450, nullable: true),
                    HomePageText = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    HomePageTitle = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    EndPageText = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    EndPageTitle = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FieldTrips", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FieldTrips_AspNetUsers_DesignerId",
                        column: x => x.DesignerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FieldTrips_ExplorationMaps_ExplorationMapId",
                        column: x => x.ExplorationMapId,
                        principalTable: "ExplorationMaps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Journeys",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    Order = table.Column<int>(type: "int", nullable: false),
                    Color = table.Column<int>(type: "int", nullable: false),
                    FieldTripId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Journeys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Journeys_FieldTrips_FieldTripId",
                        column: x => x.FieldTripId,
                        principalTable: "FieldTrips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LocatedGameUnits",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Latitude = table.Column<decimal>(type: "decimal(8,5)", nullable: false),
                    Longitude = table.Column<decimal>(type: "decimal(8,5)", nullable: false),
                    ActivationArea = table.Column<Geometry>(type: "geography", nullable: true),
                    Order = table.Column<int>(type: "int", nullable: false),
                    LocationAidType = table.Column<int>(type: "int", nullable: false),
                    LocationValidationType = table.Column<int>(type: "int", nullable: false),
                    LocationGain = table.Column<int>(type: "int", nullable: false),
                    InformationSheetTitle = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    InformationSheetDescription = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    InformationSheetPicture = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    ConclusionTitle = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    ConclusionText = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    ConclusionPicture = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    FieldTripId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    JourneyId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocatedGameUnits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LocatedGameUnits_FieldTrips_FieldTripId",
                        column: x => x.FieldTripId,
                        principalTable: "FieldTrips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LocatedGameUnits_Journeys_JourneyId",
                        column: x => x.JourneyId,
                        principalTable: "Journeys",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "LocatedActivities",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Title = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Order = table.Column<int>(type: "int", nullable: false),
                    Statement = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    LocatedGameUnitId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocatedActivities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LocatedActivities_LocatedGameUnits_LocatedGameUnitId",
                        column: x => x.LocatedGameUnitId,
                        principalTable: "LocatedGameUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuestionActivities",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    QuestionActivityType = table.Column<int>(type: "int", nullable: false),
                    TextToDisplayForCorrectAnswer = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    TextToDisplayForWrongAnswer = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Gain = table.Column<int>(type: "int", nullable: false),
                    ClueId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionActivities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuestionActivities_Clues_ClueId",
                        column: x => x.ClueId,
                        principalTable: "Clues",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_QuestionActivities_LocatedActivities_Id",
                        column: x => x.Id,
                        principalTable: "LocatedActivities",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Answers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    IsCorrectAnswer = table.Column<bool>(type: "bit", nullable: false),
                    QuestionActivityId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Answers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Answers_QuestionActivities_QuestionActivityId",
                        column: x => x.QuestionActivityId,
                        principalTable: "QuestionActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Answers_QuestionActivityId",
                table: "Answers",
                column: "QuestionActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_FieldTrips_DesignerId",
                table: "FieldTrips",
                column: "DesignerId");

            migrationBuilder.CreateIndex(
                name: "IX_FieldTrips_ExplorationMapId",
                table: "FieldTrips",
                column: "ExplorationMapId");

            migrationBuilder.CreateIndex(
                name: "IX_Journeys_FieldTripId",
                table: "Journeys",
                column: "FieldTripId");

            migrationBuilder.CreateIndex(
                name: "IX_LocatedActivities_LocatedGameUnitId",
                table: "LocatedActivities",
                column: "LocatedGameUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_LocatedGameUnits_FieldTripId",
                table: "LocatedGameUnits",
                column: "FieldTripId");

            migrationBuilder.CreateIndex(
                name: "IX_LocatedGameUnits_JourneyId",
                table: "LocatedGameUnits",
                column: "JourneyId");

            migrationBuilder.CreateIndex(
                name: "IX_PedagogicalFields_Name",
                table: "PedagogicalFields",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionActivities_ClueId",
                table: "QuestionActivities",
                column: "ClueId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Answers");

            migrationBuilder.DropTable(
                name: "PedagogicalFields");

            migrationBuilder.DropTable(
                name: "QuestionActivities");

            migrationBuilder.DropTable(
                name: "Clues");

            migrationBuilder.DropTable(
                name: "LocatedActivities");

            migrationBuilder.DropTable(
                name: "LocatedGameUnits");

            migrationBuilder.DropTable(
                name: "Journeys");

            migrationBuilder.DropTable(
                name: "FieldTrips");

            migrationBuilder.DropTable(
                name: "ExplorationMaps");
        }
    }
}
