﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    public class Player
    {
        public Guid Id { get; set; }

        [ForeignKey(nameof(Game))]
        public Guid GameId { get; set; }

        public Game Game { get; set; }

        [ForeignKey(nameof(Journey))]
        public Guid JourneyId { get; set; }

        public Journey Journey { get; set; }

        [MaxLength(ApplicationUser.IdMaxLength)]
        [ForeignKey(nameof(User))]
        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

        public ICollection<PlayedUnit> PlayedUnits { get; set; }

        [ForeignKey(nameof(CurrentUnit))]
        public Guid? CurrentUnitId { get; set; }

        /// <summary>
        /// Poi actuel
        /// </summary>
        public LocatedGameUnit CurrentUnit { get; set; }

        public DateTime StartPlayedDate { get; set; }

        public DateTime? EndGameDate { get; set; }

        public PlayerState PlayerState { get; set; }

        public DateTime? StartCurrentUnitDate { get; set; }

        public DateTime? CurrentUnitReachedDate { get; set; }
        public bool CurrentUnitActivationHasBeenUnlocked { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? CurrentLatitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? CurrentLongitude { get; set; }

        [Column(TypeName = "decimal(4,2)")]
        public decimal? BatteryLevel { get; set; }

        public DateTime? LastUpdatedPositionDate { get; set; }

		[MaxLength(100)]
		public string TeamName { get; set; }

		[MaxLength(2000)]
		public string TeamPicture { get; set; }
    }
}
