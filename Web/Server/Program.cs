﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Ozytis.Common.Core.Logs.Core;
using Ozytis.Common.Core.Utilities;

using System;
using System.IO;

namespace Web.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                 .ConfigureAppConfiguration((context, builder) =>
                 {
                     builder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                     builder.AddJsonFile($"appSettings.{context.HostingEnvironment.EnvironmentName}.json", optional: true, reloadOnChange: true);
                 })
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStaticWebAssets();
                webBuilder.UseStartup<Startup>();

                webBuilder.ConfigureLogging(loggingBuilder =>
                {
                    loggingBuilder.ClearProviders();
                    loggingBuilder.AddConsole();
                    loggingBuilder.AddAzureWebAppDiagnostics();

                    loggingBuilder.AddOzytisLogging(options =>
                    {
                        options.GetConnectionString = (services) =>
                        {
                            var config = services.GetService<IConfiguration>();
                            return config.GetConnectionString(Startup.ConnectionStringName);
                        };

                        options.TempLogsDirectory = Path.Combine(Environment.CurrentDirectory, "TempLogs");
                        options.SiteName = "Situlearn";
                        options.AutomaticallyCollectLogs = true;
                        options.GetCurrentUrl = (services) =>
                        {
                            IHttpContextAccessor httpContextAccessor = services?.GetService<IHttpContextAccessor>();
                            var req = httpContextAccessor?.HttpContext?.Request;
                            return req != null ? UriHelper.GetDisplayUrl(req) : "Url inconnue";
                        };

                        options.GetCurrentUser ??= (services) =>
                        {
                            IHttpContextAccessor httpContextAccessor = services?.GetService<IHttpContextAccessor>();
                            return (httpContextAccessor?.HttpContext?.User?.Identity?.Name) ?? "Non identifié";
                        };
                    });
                });
            });
        }
    }
}
