﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System.ComponentModel.DataAnnotations;

namespace Api
{
    public class UserCreationModel
    {
        [EmailAddress(ErrorMessage = "Adresse email invalide")]
        [Required(ErrorMessage = "Adresse email requise")]
        [MaxLength(256, ErrorMessage = "L'adresse email ne peut faire plus de {0} caractères")]
        public string Email { get; set; }

        public string Password { get; set; }

        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Rôle requis")]
        public string Role { get; set; }

        [MaxLength(5)]
        public string Language { get; set; }

        [Required(ErrorMessage = "Prénom requis")]
        [MaxLength(50, ErrorMessage = "Le prénom ne peut faire plus de 50 caractères")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Nom requis")]
        [MaxLength(100, ErrorMessage = "Le nom ne peut faire plus de 100 caractères")]
        public string LastName { get; set; }

        [MaxLength(100, ErrorMessage = "L'établissement ne peut faire plus de 100 caractères")]
        public string Establishment { get; set; }
    }
}
