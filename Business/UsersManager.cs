﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;
using Common.Security;

using DataAccess;

using Entities;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using MimeKit;

using Ozytis.Common.Core.Emails.Core;
using Ozytis.Common.Core.Managers;
using Ozytis.Common.Core.Utilities;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;
using static Business.EmailContentsManager;

namespace Business
{
    public class UsersManager : UserManager<ApplicationUser>
    {
        public UsersManager(IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators,
            IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors,
            IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger, DataContext dataContext,
            IConfiguration configuration, EmailContentsManager emailContentsManager, FieldTripsManager fieldTripsManager, GamesManager gamesManager)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            this.DataContext = dataContext;
            this.Configuration = configuration;
            this.EmailContentsManager = emailContentsManager;
            this.FieldTripsManager = fieldTripsManager;
            this.GamesManager = gamesManager;
        }

        public DataContext DataContext { get; }

   
        public IConfiguration Configuration { get; }

        public EmailContentsManager EmailContentsManager { get; }

        public FieldTripsManager FieldTripsManager { get; set; }

        public GamesManager GamesManager { get; set; }

        public IQueryable<ApplicationUser> SelectAllUsers(params Expression<Func<ApplicationUser, object>>[] includes)
        {
            return this.DataContext.Users.LoadIncludes(includes);
        }

        public async Task<ApplicationUser> CreateUserAsync(ApplicationUser user, string role, bool requestUserIsAdmin)
        {
            if (user == null)
            {
                return null;
            }

            IdentityResult result = await this.CreateAsync(user);

            if (!result.Succeeded)
            {
                throw new BusinessException(result.Errors.Select(e => e.Description).ToArray());
            }

            user = await this.FindByEmailAsync(user.Email);

            if (user == null)
            {
                throw new BusinessException("Erreur lors de la création de l'utilisateur");
            }

            if (role == UserRoleNames.Administrator && !requestUserIsAdmin)
            {
                throw new BusinessException("Vous n'êtes pas autorisé à créer un compte administrateur");
            }

            await this.AddToRoleAsync(user, role);

            if (role == UserRoleNames.Administrator)
            {
                await this.AddClaimAsync(user, new Claim(Policy.UserIsAdministrator, "true"));
            }

            return user;
        }

        public async Task<ApplicationUser> SelectUserAsync(string userId, params Expression<Func<ApplicationUser, object>>[] includes)
        {
            return await this.SelectAllUsers(includes)
                .FirstOrDefaultAsync(user => user.Id == userId);
        }

        public async Task<string[]> SendPasswordIsLostAsync(string emailAddress, ApplicationUser sender, string linkTemplate = "")
        {
            ApplicationUser user = this.SelectAllUsersWithRoles().FirstOrDefault(u => u.Email == emailAddress);

            if (user == null)
            {
                throw new BusinessException("Adresse email inconnue");
            }

            //adapter url en fonction rôle utilisateur (editor ou player)
            bool isParticipant = user.UserRoles.FirstOrDefault().Role.Name == UserRoleNames.Participant;
            var baseUrl = isParticipant ? this.Configuration["Data:SituLearn.Player:applicationUrl"] : this.Configuration["Data:Network:BaseUrl"];

            linkTemplate = string.IsNullOrEmpty(linkTemplate) ?
                $"{baseUrl}password-reset?token={{0}}&email={{1}}"
                : linkTemplate;

            string token = await this.GeneratePasswordResetTokenAsync(user);

            string link = string.Format(
                CultureInfo.InvariantCulture,
                linkTemplate
                    .Replace("%7B", "{", StringComparison.InvariantCultureIgnoreCase)
                    .Replace("%7D", "}", StringComparison.InvariantCultureIgnoreCase),
                WebUtility.UrlEncode(token), WebUtility.UrlEncode(emailAddress));

            List<EmailTemplateVar> variables = new List<EmailTemplateVar>()
            {
                new()  {
                    Id = "Link",
                    Replacement = link,
                    ReplacementType = EmailTemplateVarReplacementType.Button,
                    NewText = "Redéfinition de mon mot de passe"
                }
            };

            var emailTemplate = await this.EmailContentsManager
               .GetPopulatedEmailTemplateAsync(EmailContentsManager.PasswordLostEmail, user.Language, variables);

            MimeMessage email = new MimeMessage();

            if (sender == null)
            {
                email.From.Add(new MailboxAddress(this.Configuration["Data:Emails:DefaultSenderName"], this.Configuration["Data:Emails:DefaultSenderEmail"]));
            }
            else
            {
                email.From.Add(new MailboxAddress($"{sender.UserName}", sender.Email));
                email.Sender = new MailboxAddress(this.Configuration["Data:Emails:DefaultSenderName"], this.Configuration["Data:Emails:DefaultSenderEmail"]);
            }

            email.To.Add(new MailboxAddress($"{user.FirstName} {user.LastName}", user.Email));

            email.Subject = emailTemplate.Subject;
            email.AddBody(null, emailTemplate.Content);

            await email.SendWithSmtpAsync();

            return new[] { user.Email };
        }

        public async Task<ApplicationUser> UpdateUserSelfAccountAsync(ApplicationUser user, string currentUserLogin)
        {
            var dbUser = this.SelectAllUsersWithRoles().FirstOrDefault(u => u.UserName == currentUserLogin);

            if (dbUser == null)
            {
                return null;
            }

            dbUser.FirstName = user.FirstName;
            dbUser.LastName = user.LastName;
            dbUser.Language = user.Language;
            dbUser.Establishment = user.Establishment;
           
            await this.SaveChangesAsync();

            return dbUser;
        }

        public async Task SaveChangesAsync()
        {
            await this.DataContext.SaveChangesAsync();
        }

        public async Task RequestEmailChangeAsync(ApplicationUser user, string emailAddress, bool isParticipant)
        {
            if (user == null)
            {
                throw new BusinessException("Adresse email inconnue");
            }

            string token = await this.GenerateChangeEmailTokenAsync(user, emailAddress);

            Console.WriteLine($"{user.Email} - {emailAddress} {token}");

            //adapter url en fonction rôle utilisateur (editor ou player)
            var baseUrl = isParticipant ? this.Configuration["Data:SituLearn.Player:applicationUrl"] : this.Configuration["Data:Network:BaseUrl"];

            string link = $"{baseUrl}confirm-email-change?token={WebUtility.UrlEncode(token)}&email={WebUtility.UrlEncode(emailAddress)}";

            List<EmailTemplateVar> variables = new List<EmailTemplateVar>()
            {
                new()  {
                    Id = "Link",
                    Replacement = link,
                    ReplacementType = EmailTemplateVarReplacementType.Link
                },
                 new()  {
                    Id = "FirstName",
                    Replacement = user.FirstName,
                },
                  new()  {
                    Id = "LastName",
                    Replacement = user.LastName,
                }
            };

            var emailTemplate = await this.EmailContentsManager
               .GetPopulatedEmailTemplateAsync(EmailContentsManager.ConfirmEmailAddressEmail, user.Language, variables);

            MimeMessage email = new MimeMessage();
            email.From.Add(new MailboxAddress(this.Configuration["Data:Emails:DefaultSenderName"], this.Configuration["Data:Emails:DefaultSenderEmail"]));


            email.To.Add(new MailboxAddress($"{user.FirstName} {user.LastName}", emailAddress));

            email.Subject = emailTemplate.Subject;
            email.AddBody(null, emailTemplate.Content);

            await email.SendWithSmtpAsync();
        }

        public IQueryable<ApplicationUser> SelectAllUsersWithRoles(params Expression<Func<ApplicationUser, object>>[] includes)
        {
            return this.DataContext.Users
                .Include(u => u.UserRoles)
                    .ThenInclude(x => x.Role)
                .LoadIncludes(includes);
        }

        public async Task DeleteUserAsync(string userId, string currentUser, bool requestUserIsAdmin)
        {
            try
            {
                var current = this.SelectAllUsersWithRoles().FirstOrDefault(u => u.UserName == currentUser);

                var user = await this.SelectUserAsync(userId, u => u.FieldTrips, u => u.UserFieldTrips, u => u.Players);

                if (requestUserIsAdmin && user.Id == current.Id)
                {
                    throw new BusinessException("Vous ne pouvez pas supprimer votre compte");
                }

                if (!requestUserIsAdmin && user.Id != current.Id)
                {
                    throw new BusinessException("Vous n'êtes pas autorisé à supprimer le compte d'un autre utilisateur");
                }

                using TransactionScope transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
                
                if (user.FieldTrips.Any())
                {
                    foreach (var fieldTrip in user.FieldTrips)
                    {
                        await this.FieldTripsManager.DeleteFieldTripAsync(fieldTrip.Id, current);
                    }
                }

                if (user.UserFieldTrips.Any())
                {
                    foreach (var userFieldTrips in user.UserFieldTrips)
                    {
                        await this.FieldTripsManager.AddOrRemoveFromFavoritesAsync(userFieldTrips.FieldTripId, current);
                    }
                }

                if (user.Players.Any())
                {
                    foreach (var player in user.Players)
                    {
                        await this.GamesManager.DeletePlayerAsync(player.Id);
                    }
                }

                await this.SaveChangesAsync();

                await this.DeleteAsync(user);

                transactionScope.Complete();
            }
            catch (Exception e)
            {
                throw new BusinessException(e.Message);
            }
        }
    }
}
