﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class JourneyColorsHelper
    {
        public const int MaxNumber = 8;

        static Dictionary<ColorJourney, string> HexColors = new()
        {
            { ColorJourney.Brown, "#382510" },
            { ColorJourney.Orange, "#FF6E03" },
            { ColorJourney.Grey, "#575757" },
            { ColorJourney.Green, "#A5E09C" },
            { ColorJourney.Purple, "#490779" },
            { ColorJourney.White, "#FBFBFB" },
            { ColorJourney.Yellow, "#F6E165" },
            { ColorJourney.Blue, "#069EFB" },
        };

        public static ColorJourney[] GetAvailableColors() => HexColors.Keys.ToArray();



        public static string GetColorHex(ColorJourney color)
        {
            return HexColors[color];
        }

        public static string[] GetColorsHex()
        {
            return HexColors.Select(k => k.Value).ToArray();
        }

        public static ColorJourney GetColorFromHex(string hex)
        {
            return HexColors.First(k => string.Equals(k.Value, hex, StringComparison.OrdinalIgnoreCase)).Key;
        }
    }
}
