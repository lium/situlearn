﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using SituLearn.Player.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Player.Pages.PlayedFieldTrips
{
    [Route(Url)]
    [Authorize]
    public partial class PlayedFieldTripsPage : ComponentBase
    {
        public const string Url = "/played-fieldtrips";

        [SupplyParameterFromQuery]
        public string BackUrl { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [CascadingParameter]
        public AppLayout Layout { get; set; }

        public bool TestEnv => this.Layout.TestEnv;

        public FieldTripMadeHeaderModel[] FieldTripsMade { get; set; }

        public int TotalPoints { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.FieldTripsMade = (await this.GamesRepository.GetAllFieldTripsMadeHeadersAsync()).ToArray() ?? new FieldTripMadeHeaderModel[0];

            this.TotalPoints = this.FieldTripsMade.Sum(ftm => ftm.TotalPoints);

            var uri = this.NavigationManager.ToAbsoluteUri(this.NavigationManager.Uri);

            if (this.GamesRepository.CurrentPlayer != null)
            {
                this.GamesRepository.CurrentPlayer.PlayerState = Common.PlayerState.Cancelled;
            }

            await base.OnInitializedAsync();
        }
    }
}
