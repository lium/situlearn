﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.User
{
    public partial class PasswordRecovery : ComponentBase
    {
        public string Email { get; set; }

        public string[] Errors { get; set; }

        public bool IsProcessing { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }
              
        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        public void NavigateTo(string to)
        {
            this.NavigationManager.NavigateTo(to);
        }

        public async Task SendPasswordRecoveryAsync()
        {
            if (this.IsProcessing)
            {
                return;
            }

            this.Errors = null;
            this.IsProcessing = true;

            try
            {
                await this.UsersRepository.SendPasswordRecoveryAsync(this.Email);
                this.Notifier.Notify(Color.Success, "Un email contenant les instructions pour réinitialiser votre mot de passe vous a été envoyé");
                await Task.Delay(2000);

                this.NavigateTo(LoginPage.Url);
            }
            catch (Ozytis.Common.Core.Utilities.BusinessException ex)
            {
                this.Errors = ex.Messages ?? new[] { ex.Message };
            }

            this.IsProcessing = false;
        }

        protected override Task OnInitializedAsync()
        {
            return base.OnInitializedAsync();
        }
    }
}