﻿// Caution! Be sure you understand the caveats before publishing an application with
// offline support. See https://aka.ms/blazor-offline-considerations

self.importScripts('./service-worker-assets.js');
self.addEventListener('install', event => event.waitUntil(onInstall(event)));
self.addEventListener('activate', event => event.waitUntil(onActivate(event)));
self.addEventListener('fetch', event => event.respondWith(onFetch(event)));
self.addEventListener('message', event => handleMessage(event));

const cacheNamePrefix = 'offline-cache-';
const cacheName = `${cacheNamePrefix}${self.assetsManifest.version}`;
const offlineAssetsInclude = [/\.dll$/, /\.pdb$/, /\.wasm/, /\.html/, /\.js$/, /\.json$/, /\.css$/, /\.woff$/, /\.png$/, /\.jpe?g$/, /\.gif$/, /\.ico$/, /\.blat$/, /\.dat$/, /\.svg$/];
const offlineAssetsExclude = [/^service-worker\.js$/, /\.es5.js/];

async function onInstall(event) {
    console.info('Service worker: Install');

    self.skipWaiting();

    // Fetch and cache all matching items from the assets manifest
    const assetsRequests = self.assetsManifest.assets
        .filter(asset => offlineAssetsInclude.some(pattern => pattern.test(asset.url)))
        .filter(asset => !offlineAssetsExclude.some(pattern => pattern.test(asset.url)))
        .map(asset => new Request(asset.url, { /*integrity: asset.hash,*/ cache: 'no-cache' }));
    await caches.open(cacheName).then(cache => cache.addAll(assetsRequests));
}

async function onActivate(event) {
    console.info('Service worker: Activate');
    //event.waitUntil(clients.claim());
    self.clients.claim();
    // Delete unused caches
    const cacheKeys = await caches.keys();
    await Promise.all(cacheKeys
        .filter(key => key.startsWith(cacheNamePrefix) && key !== cacheName)
        .map(key => caches.delete(key)));
}

async function onFetch(event) {
    let cachedResponse = null;
    if (event.request.method === 'GET') {
        // For all navigation requests, try to serve index.html from cache
        // If you need some URLs to be server-rendered, edit the following check to exclude those URLs
        const shouldServeIndexHtml = event.request.mode === 'navigate';
        const request = shouldServeIndexHtml ? 'index.html' : event.request;
        const cache = await caches.open(cacheName);
        cachedResponse = await cache.match(request);
    }

    return cachedResponse || fetch(event.request);
}

async function handleMessage(event) {
    if (event.data.message == "addToCache") {
        console.log("adding to cache: " + event.data.data);
        let openedCache = null;

        try {
            openedCache = await caches.open(cacheName);
        }
        catch(e) {
            console.error(`Unable to open cache ${cacheName} while adding ${event.data.data}`);
            throw e;
        }

        try {
            await openedCache.add(new Request(event.data.data, { cache: 'no-cache' }));
            console.log(`${event.data.data} successfully added to cache`);
        }
        catch (e) {
            console.error(`Unable to add to cache ${event.data.data}`);
            throw e;
        }
        //await caches.open(cacheName).then(cache => cache.addAll([new Request(event.data.data, { cache: 'no-cache' })]));
    }
    else if (event.data.message == "removeFromCache") {
        await caches.open(cacheName).then(cache => cache.delete([new Request(event.data.data, { cache: 'no-cache' })]));
    }
    else if (event.data.message == "removeUselessPicturesFromCache") {
        await caches.open(cacheName).then(function (cache) {
            cache.keys().then(function (keys) {
                keys.forEach(function (request, index, array) {
                    if (request.url.includes('/files?') && !event.data.data.includes(request.url)) {
                        cache.delete(request, { cache: 'no-cache' });
                    }                    
                });
            });
        });

    }
}