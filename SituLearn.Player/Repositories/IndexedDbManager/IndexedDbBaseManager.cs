﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Ozytis.Common.Core.ClientApi.Storage;
using System.Threading.Tasks;
using System;
using Microsoft.JSInterop;
using Newtonsoft.Json;

namespace SituLearn.Player.Repositories.IndexedDbManager
{
    public abstract class IndexedDbBaseManager<EntityType>
    {
        public const string DatabaseName = "Situlearn";
        public const string IndexedDbVersionKey = "SituLearnDBVersion";
        public abstract string StoreName { get; }
        public IndexedDbBaseManager(LocalStorageService localStorageService, IJSRuntime jsRunTime, bool deserializeInheritance = false): base()
        {
            this.JSRuntime = jsRunTime;
            this.LocalStorageService = localStorageService;
            this.deserializeInheritance = deserializeInheritance;
        }
        private bool deserializeInheritance = false;
        public LocalStorageService LocalStorageService { get; }
        public IJSRuntime JSRuntime { get; set; }
        public Func<string, string, string> GetKey = (x, y) => $"{x}_{y}";



        protected string EntitiyType
        {
            get
            {
                return typeof(EntityType).Name;
            }
        }


        public virtual async Task InitializeAsync()
        {

			int version = 2;
            string versionString = await this.LocalStorageService.GetItemAsync<string>(IndexedDbVersionKey);
			if(!string.IsNullOrEmpty(versionString))
			{
				int.TryParse(versionString, out version);
			}

			await this.JSRuntime.InvokeVoidAsync("situlearn.indexedDB.initalize", version);
        }

        public async Task<EntityType[]> SelectAll()
        {
            var r = await this.GetItemsByPartialKeyAsync<EntityType>(this.EntitiyType, deserializeInheritance);
            return r ?? Array.Empty<EntityType>();
        }

        public async Task<EntityType> SelectById(string id)
        {
            return await this.GetItemAsync<EntityType>(GetKey(id, this.EntitiyType), deserializeInheritance);
        }

        public async Task DeleteById(string id)
        {
            await this.RemoveItemAsync(GetKey(id, this.EntitiyType));
        }

        public async Task DeleteByKey(string key)
        {
            await this.RemoveItemAsync(key);
        }

        public virtual async Task CreateOrUpdateById(string id, EntityType entity)
        {
            await this.SetItemAsync(GetKey(id, this.EntitiyType), entity);
        }

        public virtual async Task BulkCreateOrUpdateById(Dictionary<string, EntityType> data)
        {
            var transformed = data.ToDictionary(kv => GetKey(kv.Key, this.EntitiyType), kv => kv.Value);

            await this.SetBulkItemsAsync(transformed);
        }

        public virtual async Task BulkCreateOrUpdateById(Dictionary<Guid, EntityType> data)
        {
            var transformed = data.ToDictionary(kv => GetKey(kv.Key.ToString(), this.EntitiyType), kv => (object)kv.Value);

            await this.SetBulkItemsAsync(transformed);
        }

        public async Task DeleteNotExist(List<string> ids)
        {
            var existing = await this.GetKeysByPartialKeyAsync(this.EntitiyType) ?? Array.Empty<string>();
            foreach (var toDelete in existing.Where(id => !ids.Contains(id)))
            {
                await this.DeleteByKey(toDelete);
            }
        }

        public async Task<EntityType[]> SelectByIndex(string index, string value)
        {
            return await this.GetItemsByIndexAsync<EntityType>(index, value, deserializeInheritance);

        }


		private async Task<T[]> GetItemsByPartialKeyAsync<T>(string key, bool deserializeInheritance = false)
		{
			var serializerSettings = this.JsonSerializerSettings;


			if (deserializeInheritance)
			{
				serializerSettings.TypeNameHandling = TypeNameHandling.All;
			}

			var results = await this.JSRuntime.InvokeAsync<string[]>("situlearn.indexedDB.searchItemsByPartialKey", this.StoreName, key);


			if (results == null || results.Length == 0)
			{
				return default;
			}
			var dataString = new List<string>();
			foreach (var result in results)
			{
				if (result.StartsWith("\"") && result.EndsWith("\""))
				{
					dataString.Add(JsonConvert.DeserializeObject<string>(result, serializerSettings));
				}
				else
				{
					dataString.Add(result);
				}
			}
			var data = new List<T>();
			foreach (var d in dataString)
			{
				data.Add(JsonConvert.DeserializeObject<T>(d, this.JsonSerializerSettings));
			}

			return data.ToArray();
		}

		private async Task<T> GetItemAsync<T>(string key, bool deserializeInheritance = false)
		{
			// Console.WriteLine($"Getting item from IndexDB : {key}");
			var serializerSettings = this.JsonSerializerSettings;

			// Console.WriteLine($"Deserializing inheritance : {deserializeInheritance}");

			if (deserializeInheritance)
			{
				serializerSettings.TypeNameHandling = TypeNameHandling.All;
			}
			var result = await this.JSRuntime.InvokeAsync<string>("situlearn.indexedDB.getItem", this.StoreName, key);


			if (string.IsNullOrEmpty(result))
			{
				return default;
			}

			if (result.StartsWith("\"") && result.EndsWith("\""))
			{
				result = JsonConvert.DeserializeObject<string>(result, serializerSettings);
			}

			//result = result.Replace("\\n", "");

			// Console.WriteLine($"Getting item from IndexDB : {key} - cleaned result : {result}");
			if (result == null || result.Length == 0)
			{
				return default;
			}
			return JsonConvert.DeserializeObject<T>(result, serializerSettings);
		}

		protected async Task SetItemAsync(string key, object value)
		{
			var json = JsonConvert.SerializeObject(value, this.JsonSerializerSettings);

			await this.JSRuntime.InvokeVoidAsync("situlearn.indexedDB.setItem", this.StoreName, key, json);
		}

		protected async Task SetItemAsync(string key, object value, Dictionary<string, object> indexes)
		{
			var json = JsonConvert.SerializeObject(value, this.JsonSerializerSettings);

			await this.JSRuntime.InvokeVoidAsync("situlearn.indexedDB.setItem", this.StoreName, key, new ItemWithIndex { SerializedData = json, Indexes = indexes });

		}

		private async Task SetBulkItemsAsync<T>(Dictionary<string, T> items)
		{
			var data = items.Select(it => new { Value = JsonConvert.SerializeObject(it.Value, this.JsonSerializerSettings), Key = it.Key }).ToArray();
			await this.JSRuntime.InvokeVoidAsync("situlearn.indexedDB.setBulkItems", this.StoreName, data);
		}


		protected async Task SetBulkItemsAsync<T>(Dictionary<string, T> items, Dictionary<string, Dictionary<string, object>> indexes)
		{
			var data = items.Select(it => new { Value = new ItemWithIndex { SerializedData = JsonConvert.SerializeObject(it.Value, this.JsonSerializerSettings), Indexes = indexes[it.Key] }, Key = it.Key }).ToArray();
			await this.JSRuntime.InvokeVoidAsync("situlearn.indexedDB.setBulkItems", this.StoreName, data);
		}

		private async Task RemoveItemAsync(string key)
		{
			await this.JSRuntime.InvokeVoidAsync("situlearn.indexedDB.removeItem", this.StoreName, key);
		}

		private async Task<string[]> GetKeysByPartialKeyAsync(string key)
		{
			var results = await this.JSRuntime.InvokeAsync<string[]>("situlearn.indexedDB.searchKeysByPartialKey", this.StoreName, key);


			if (results == null || results.Length == 0)
			{
				return default;
			}

			return results;
		}

		private async Task<T[]> GetItemsByIndexAsync<T>(string index, string value, bool deserializeInheritance = false)
		{
			var serializerSettings = this.JsonSerializerSettings;

			// Console.WriteLine($"Deserializing inheritance : {deserializeInheritance}");

			if (deserializeInheritance)
			{
				serializerSettings.TypeNameHandling = TypeNameHandling.All;
			}

			var result = await this.JSRuntime.InvokeAsync<string[]>("situlearn.indexedDB.getItemByIndex", this.StoreName, index, value);
			if(result == null)
			{
				return null;
			}
			return result.Select(r => JsonConvert.DeserializeObject<T>(r, this.JsonSerializerSettings)).ToArray();
			// Console.WriteLine($"Getting item from IndexDB : {key} - result : {result}");
		}

		public JsonSerializerSettings JsonSerializerSettings { get; set; } = new()
		{
			NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
			ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Serialize,
			MaxDepth = 15,
			PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.All,
			TypeNameHandling = TypeNameHandling.All,
		};

		class ItemWithIndex
		{
			public string SerializedData { get; set; }
			public Dictionary<string, object> Indexes { get; set; }
		}
	}
}
