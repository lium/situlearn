﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Business;
using Common;
using Common.Security;
using Hangfire;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Server
{
    public class Setup
    {
        public Setup(UsersManager usersManager, RoleManager<IdentityRole> roleManager, EmailContentsManager emailContentsManager)
        {
            this.UsersManager = usersManager;
            this.RoleManager = roleManager;
            this.EmailContentsManager = emailContentsManager;
        }

        public UsersManager UsersManager { get; }
        public RoleManager<IdentityRole> RoleManager { get; }
        public EmailContentsManager EmailContentsManager { get; }

        public void Init()
        {
            string job = BackgroundJob.Enqueue(() => this.CheckRoles());
            job = BackgroundJob.ContinueJobWith(job, () => this.CreateAdmin());
            job = BackgroundJob.ContinueJobWith(job, () => this.CheckCustomerEmailTemplates());
        }

        public async Task CheckCustomerEmailTemplates()
        {
            await this.EmailContentsManager.InstallTemplatesAsync();
        }

        public async Task CheckRoles()
        {
            try
            {
                string[] roles = new[] {
                    UserRoleNames.Administrator,
                    UserRoleNames.Designer,
                    UserRoleNames.Participant
                };

                foreach (var role in roles)
                {
                    if (!await this.RoleManager.RoleExistsAsync(role))
                    {
                        await this.RoleManager.CreateAsync(new IdentityRole(role));
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task CreateAdmin()
        {
            try
            {
                var count = this.UsersManager.Users.Count();

                if (count == 0)
                {
                    await this.UsersManager.CreateAsync(new Entities.ApplicationUser
                    {
                        Email = "admin@situlearn.fr",
                        EmailConfirmed = true,
                        UserName = "admin@situlearn.fr",
                        LastName = "principal",
                        FirstName = "administrateur"
                    }, "**/§¤SL4RN");

                    var user = await this.UsersManager.SelectAllUsers().FirstOrDefaultAsync();

                    await this.UsersManager.AddClaimAsync(user, new System.Security.Claims.Claim(Policy.UserIsAdministrator, "true"));
                    await this.UsersManager.AddToRoleAsync(user, UserRoleNames.Administrator);

                    await this.UsersManager.UpdateAsync(user);
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
