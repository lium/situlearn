﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api.Activity;
using Common;
using DataAccess;
using Entities;
using Microsoft.EntityFrameworkCore;
using Ozytis.Common.Core.Storage;
using Ozytis.Common.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
	public class ActivitiesManager : BaseEntityManager<LocatedActivity>
	{
		public ActivitiesManager(DataContext context, IFileSystem fileSystem) : base(context)
		{
			this.FileSystem = fileSystem;
		}

		public IFileSystem FileSystem { get; }

		public async Task<LocatedActivity> GetActivity(Guid activityId)
		{
			LocatedActivity activity = this.DataContext.QuestionActivities
				.Include(q => q.Clue)
				.Include(q => q.Answers)
				.FirstOrDefault(a => a.Id == activityId);

			if (activity == null)
			{
				activity = await this.DataContext.ReadingActivities.FindAsync(activityId);
				if (activity != null)
				{
					activity.ActivityType = ActivityType.Statement;
				}
			}
			else
			{
				activity.ActivityType = ActivityType.Question;
			}

			return activity;
		}

		public ReadingActivity GetStatement(Guid activityId)
		{
			return this.DataContext.ReadingActivities
				.FirstOrDefault(ra => ra.Id == activityId);
		}

		public QuestionActivity GetQuestion(Guid activityId)
		{
			var question = this.DataContext.QuestionActivities
				.Include(qa => qa.Clue)
				.Include(qa => qa.Answers)
				.FirstOrDefault(qa => qa.Id == activityId);

			question.InvalidAnswers = false;

			if (!question.Answers.Any(q => q.IsCorrectAnswer))
			{
				question.InvalidAnswers = true;
			}

			return question;
		}

		public QuestionActivity GetQuestionWithFieldTrip(Guid activityId)
		{
			var question = this.DataContext.QuestionActivities
				.Include(q => q.LocatedGameUnit)
					.ThenInclude(l => l.FieldTrip)
				.Include(qa => qa.Clue)
				.Include(qa => qa.Answers)
				.FirstOrDefault(qa => qa.Id == activityId);

			return question;
		}

		public async Task AddAnswer(QuestionActivity question, Answer newAnswer)
		{
			newAnswer.Content = await this.FileSystem.SetFile(newAnswer.Content, $"activities/{question.Id}/answers-{newAnswer.Id}");

			question.LocatedGameUnit.FieldTrip.LastUpdateDate = DateTime.UtcNow;

			this.DataContext.Answers.Add(newAnswer);

			await this.SaveChangesAsync();
		}

		public async Task<LocatedActivity> UpdateStatementAsync(StatementUpdateModel model)
		{
			var statement = this.GetStatement(model.Id);
			if (statement == null)
			{
				throw new BusinessException("Relevé inconnu");
			}

			statement.Name = model.Name;
			statement.Title = model.Title ?? model.Name;
			statement.Statement = model.Statement;
			statement.Picture = await this.FileSystem.SetFile(model.Picture, $"activities/{statement.Id}/picture");
			statement.ActivityType = ActivityType.Statement;

			statement.AllowsAudioRecordings = model.AllowsAudioRecordings;
			statement.AllowsNoteTaking = model.AllowsNoteTaking;
			statement.AllowsPhotos = model.AllowsPhotos;
			statement.ReadingsAreUnlimited = model.ReadingsAreUnlimited;

			this.DataContext.ReadingActivities.Update(statement);
			await this.SaveChangesAsync();

			return statement;
		}

		public async Task<LocatedActivity> DuplicateLocatedActivityAsync(Guid locatedActivityId, Guid locatedGameUnitId, bool directSave = true, LocatedGameUnit unit = null)
		{
			var original = await this.SelectAsync(locatedActivityId);

			if (original == null)
			{
				throw new BusinessException("Activité inconnue");
			}

			LocatedGameUnit targetGameUnit;

			if (unit != null)
			{
				targetGameUnit = unit;
			}
			else
			{
                targetGameUnit = this.DataContext.LocatedGameUnits
					.Include(gu => gu.FieldTrip)
					.FirstOrDefault(x => x.Id == locatedGameUnitId);
            }            

			if (targetGameUnit == null)
			{
				throw new BusinessException("Unité de jeu cible inconnue");
			}

			LocatedActivity duplicate;
			switch (original.ActivityType)
			{
				case ActivityType.Question:
					duplicate = new QuestionActivity
					{
						Id = Guid.NewGuid()
					};

					var originalQuestion = this.GetQuestion(locatedActivityId);
					(duplicate as QuestionActivity).QuestionActivityType = originalQuestion.QuestionActivityType;
					(duplicate as QuestionActivity).Gain = originalQuestion.Gain;
					(duplicate as QuestionActivity).TextToDisplayForCorrectAnswer = originalQuestion.TextToDisplayForCorrectAnswer;
					(duplicate as QuestionActivity).TextToDisplayForWrongAnswer = originalQuestion.TextToDisplayForWrongAnswer;
					(duplicate as QuestionActivity).ClueEnabled = originalQuestion.ClueEnabled;

					if (originalQuestion.ClueId.HasValue)
					{
						var newClueId = Guid.NewGuid();
						(duplicate as QuestionActivity).ClueId = originalQuestion.ClueId;
						(duplicate as QuestionActivity).Clue = new Clue
						{
							Id = newClueId,
							Content = originalQuestion.Clue.Content,
							Picture = originalQuestion.Clue.Picture,
							Cost = originalQuestion.Clue.Cost
						};

						if (!string.IsNullOrEmpty(originalQuestion.Clue.Picture) && !this.FileSystem.Exists(originalQuestion.Clue.Picture))
						{
							(duplicate as QuestionActivity).Clue.Picture =
								await this.FileSystem.Duplicate(originalQuestion.Clue.Picture, $"activities/{duplicate.Id}/clue");
						}
					}

					List<Answer> newAnswers = new();
					foreach (var originalAnswer in originalQuestion.Answers)
					{
						var newAnswer = new Answer
						{
							Id = Guid.NewGuid(),
							IsCorrectAnswer = originalAnswer.IsCorrectAnswer,
						};

						if (!string.IsNullOrEmpty(originalAnswer.Content))
						{
							if (await this.FileSystem.ExistsAsync(originalAnswer.Content))
							{
								newAnswer.Content =
									await this.FileSystem.Duplicate(originalAnswer.Content, $"activities/{duplicate.Id}/answers-{newAnswer.Id}.{originalAnswer.Content.Split('.').Last()}");
							}
							else
							{
								newAnswer.Content = originalAnswer.Content;
							}
						}

						newAnswers.Add(newAnswer);
					}
					(duplicate as QuestionActivity).Answers = newAnswers;

					break;
				case ActivityType.Statement:
					duplicate = new ReadingActivity();

					var originalStatement = this.GetStatement(locatedActivityId);
					(duplicate as ReadingActivity).AllowsAudioRecordings = originalStatement.AllowsAudioRecordings;
					(duplicate as ReadingActivity).AllowsNoteTaking = originalStatement.AllowsNoteTaking;
					(duplicate as ReadingActivity).AllowsPhotos = originalStatement.AllowsPhotos;
					(duplicate as ReadingActivity).ReadingsAreUnlimited = originalStatement.ReadingsAreUnlimited;

					break;
				default:
					throw new BusinessException("Type d'activité inconnue");
			}

			duplicate.Title = original.Title;
			duplicate.Name = original.Name;
			duplicate.Statement = original.Statement;
			duplicate.Picture = original.Picture;
			duplicate.Order = original.Order;
			duplicate.LocatedGameUnitId = locatedGameUnitId;
			duplicate.ActivityType = original.ActivityType;

			targetGameUnit.FieldTrip.LastUpdateDate = DateTime.UtcNow;

			this.DataContext.LocatedActivities.Add(duplicate);

			if (directSave)
			{
				await this.SaveChangesAsync();
			}

			return duplicate;
		}

		public async Task<LocatedActivity> CreateStatementAsync(StatementCreateModel model, Guid fieldTripId)
		{
			FieldTrip fieldTrip = await this.DataContext.FieldTrips
				.Include(f => f.LocatedGameUnits)
				.ThenInclude(lgu => lgu.Activities)
				.FirstOrDefaultAsync(f => f.Id == fieldTripId);

			LocatedGameUnit unit = fieldTrip.LocatedGameUnits.FirstOrDefault(lgu => lgu.Id == model.LocatedGameUnitId);

			int order = unit.Activities == null ? 0 : unit.Activities.Count();

			Guid newRef = Guid.NewGuid();
			var original = new ReadingActivity()
			{
				Id = newRef,
				LocatedGameUnitId = model.LocatedGameUnitId,
				Name = model.Name,
				Title = model.Title ?? model.Name,
				Statement = model.Statement,
				Picture = model.Picture,
				ActivityType = ActivityType.Statement,

				AllowsAudioRecordings = model.AllowsAudioRecordings,
				AllowsNoteTaking = model.AllowsNoteTaking,
				AllowsPhotos = model.AllowsPhotos,
				ReadingsAreUnlimited = model.ReadingsAreUnlimited,
				Order = order
			};

			original.Picture = await this.FileSystem.SetFile(original.Picture, $"activities/{original.Id}/picture");

			this.DataContext.ReadingActivities.Add(original);

			fieldTrip.LastUpdateDate = DateTime.UtcNow;

			await this.SaveChangesAsync();

			return original;
		}

		public async Task SoftDeleteAsync(Guid activityId)
		{
			var activity = await this.SelectAsync(activityId, a => a.LocatedGameUnit);
			activity.IsDeleted = true;

			FieldTrip fieldTrip = await this.DataContext.FieldTrips
				.Include(f => f.LocatedGameUnits)
				.ThenInclude(lgu => lgu.Activities)
				.FirstOrDefaultAsync(f => f.Id == activity.LocatedGameUnit.FieldTripId);

			if (fieldTrip == null)
			{
				Console.WriteLine("Fieldtrip not found");
				return;
			}

			LocatedGameUnit unit = fieldTrip.LocatedGameUnits.FirstOrDefault(lgu => lgu.Id == activity.LocatedGameUnitId);

			List<LocatedActivity> activitiesToUpdate = unit.Activities.Where(a => a.Id != activityId && !a.IsDeleted && a.Order > activity.Order).ToList();

			foreach (var activityToUpdate in activitiesToUpdate)
			{
				activityToUpdate.Order--;
			}

			fieldTrip.LastUpdateDate = DateTime.UtcNow;

			await this.SaveChangesAsync();
		}

		public async Task<LocatedActivity> UpdateQuestionAsync(QuestionUpdateModel model)
		{
			var original = this.GetQuestionWithFieldTrip(model.Id);

			original.Name = model.Name;
			original.Title = model.Title ?? model.Name;
			original.Statement = model.Statement;
			original.Picture = await this.FileSystem.SetFile(model.Picture, $"activities/{original.Id}/picture");

			original.Gain = model.Gain;
			original.TextToDisplayForCorrectAnswer = model.TextToDisplayForCorrectAnswer;
			original.TextToDisplayForWrongAnswer = model.TextToDisplayForWrongAnswer;
			original.QuestionActivityType = (QuestionActivityType)model.ActivityQuestionType;
			//original.Order = 0; /* order set though game unit activity panel */

			//var incomingAnswersIds = model.Answers.Where(a => a.Id.HasValue).Select(a => a.Id.Value);
			//original.Answers.RemoveAll(x => !incomingAnswersIds.Contains(x.Id));

			var removing = original.Answers.Where(a => !model.Answers.Where(a => a.Id.HasValue).Select(x => x.Id).Contains(a.Id)).ToArray();
			this.DataContext.Answers.RemoveRange(removing);

			//foreach (var item in model.Answers)
			//{
			//	Guid newAnswerId = Guid.NewGuid();
			//	Answer answer = original.Answers.FirstOrDefault(a => a.Id == item.Id) ?? new Answer
			//	{
			//		Id = newAnswerId,
			//		QuestionActivityId = original.Id,
			//	};

			//	answer.Content = await this.FileSystem.SetFile(item.Content, $"activities/{original.Id}/answer-{answer.Id}");
			//	answer.IsCorrectAnswer = item.IsCorrect;

			//	if (answer.Id == newAnswerId)
			//	{
			//		this.dataContext.Answers.Add(answer);
			//	}

			//	original.Answers.Add(answer);
			//}

			//

			//foreach (var item in removing)
			//{
			//    original.Answers.Remove(item);
			//    this.dataContext.Answers.Remove(item);
			//}

			original.ClueEnabled = model.ClueEnabled;
			if (model.ClueEnabled && model.Clue.Cost > 0)
			{
				original.Clue ??= new Clue();
				original.Clue.Content = model.Clue.Content;
				original.Clue.Cost = model.Clue.Cost;
				original.Clue.Picture = model.Clue.Picture;

				original.Clue.Picture = await this.FileSystem.SetFile(original.Clue.Picture, $"activities/{original.Id}/clue");
			}
			else if (model.ClueEnabled)
			{
				throw new BusinessException("Un indice doit présenter un coût supérieur à 0");
			}
			else
			{
				if (original.ClueId.HasValue)
				{
					original.ClueId = null;
				}

				if (original.Clue != null)
				{
					this.DataContext.Clues.Remove(original.Clue);
					original.Clue = null;
				}
			}

			original.LocatedGameUnit.FieldTrip.LastUpdateDate = DateTime.UtcNow;

			await this.SaveChangesAsync();

			return original;
		}

		public async Task<QuestionActivity> CreateQuestionAsync(QuestionCreateModel model, Guid fieldTripId)
		{
			FieldTrip fieldTrip = await this.DataContext.FieldTrips
				.Include(f => f.LocatedGameUnits)
				.ThenInclude(lgu => lgu.Activities)
				.FirstOrDefaultAsync(f => f.Id == fieldTripId);

			LocatedGameUnit unit = fieldTrip.LocatedGameUnits.FirstOrDefault(lgu => lgu.Id == model.LocatedGameUnitId);

			int order = unit.Activities == null ? 0 : unit.Activities.Count();

			Guid newRef = Guid.NewGuid();
			var original = new QuestionActivity()
			{
				Id = newRef,
				LocatedGameUnitId = model.LocatedGameUnitId,
				QuestionActivityType = (QuestionActivityType)model.ActivityQuestionType,
				Name = model.Name,
				Title = model.Title ?? model.Name,
				Statement = model.Statement,
				Picture = model.Picture,
				TextToDisplayForCorrectAnswer = model.TextToDisplayForCorrectAnswer,
				TextToDisplayForWrongAnswer = model.TextToDisplayForWrongAnswer,
				Gain = model.Gain,
				Answers = new List<Answer>(),
				Clue = null,
				ClueEnabled = model.ClueEnabled,
				ActivityType = ActivityType.Question,
				Order = order
			};

			original.Picture = await this.FileSystem.SetFile(original.Picture, $"activities/{original.Id}/picture");

			if (model.ClueEnabled && model.Clue.Cost > 0)
			{
				original.Clue = new Clue
				{
					Id = Guid.NewGuid(),
					Cost = model.Clue.Cost,
					Picture = model.Clue.Picture
				};

				original.Clue.Picture = await this.FileSystem.SetFile(original.Clue.Picture, $"activities/{original.Id}/clue");
			}

			switch ((QuestionActivityType)model.ActivityQuestionType)
			{
				case QuestionActivityType.Question:
				case QuestionActivityType.Mcq:
					original.Answers = model.Answers.Select(a => new Answer()
					{
						Content = a.Content,
						IsCorrectAnswer = a.IsCorrect
					}).ToList();
					break;
				case QuestionActivityType.PictureMcq:
					foreach (var answerModel in model.Answers)
					{
						var answer = new Answer()
						{
							Id = Guid.NewGuid(),
							IsCorrectAnswer = answerModel.IsCorrect,
							Content = answerModel.Content
						};

						answer.Content = await this.FileSystem.SetFile(answer.Content, $"activities/{original.Id}/answers-{answer.Id}");

						if (answer.Content == null)
						{
							continue;
						}

						original.Answers.Add(answer);
					}
					break;
				default:
					throw new BusinessException("Type de question inconnue");
			}

			this.DataContext.QuestionActivities.Add(original);

			fieldTrip.LastUpdateDate = DateTime.UtcNow;

			await this.SaveChangesAsync();

			return original;
		}

		public async Task UpdateLocatedActivityOrderAsync(Guid fieldTripId, Guid activityLinkId, int newOrder, ApplicationUser updater)
		{
			FieldTrip fieldTrip = await this.DataContext.FieldTrips
				.Include(f => f.LocatedGameUnits)
				.ThenInclude(lgu => lgu.Activities)
				.FirstOrDefaultAsync(f => f.Id == fieldTripId);

			if (fieldTrip == null)
			{
				Console.WriteLine("Fieldtrip not found");
				return;
			}

			Console.WriteLine("Fieldtrip found");

			if (updater.Id != fieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
			{
				Console.WriteLine("Not authorized");
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			LocatedActivity dbActivity = await this.SelectAll().FirstOrDefaultAsync(a => a.Id == activityLinkId);

			int oldOrder = dbActivity.Order;

			LocatedGameUnit unit = fieldTrip.LocatedGameUnits.FirstOrDefault(lgu => lgu.Id == dbActivity.LocatedGameUnitId);

			List<LocatedActivity> activitiesToUpdate = new();

			if (newOrder > oldOrder)
			{
				activitiesToUpdate = unit.Activities.Where(a => !a.IsDeleted && a.Order > oldOrder && a.Order <= newOrder).ToList();
			}
			else
			{
				activitiesToUpdate = unit.Activities.Where(a => !a.IsDeleted && a.Order >= newOrder && a.Order < oldOrder).ToList();
			}

			foreach (var activity in activitiesToUpdate)
			{
				if (newOrder > oldOrder)
				{
					activity.Order--;
				}
				else
				{
					activity.Order++;
				}
			}

			dbActivity.Order = newOrder;

			fieldTrip.LastUpdateDate = DateTime.UtcNow;
			await this.SaveChangesAsync();
		}

		public async Task<Answer> GetAnswer(Guid answerId)
		{
			return await this.DataContext.Answers
				.Include(a => a.QuestionActivity)
					.ThenInclude(q => q.LocatedGameUnit)
						.ThenInclude(l => l.FieldTrip)
				.Include(a => a.QuestionActivity)
					.ThenInclude(q => q.Answers)
				.FirstOrDefaultAsync(a => a.Id == answerId);
		}

		public async Task UpdateAnswer(Answer answer, bool isCorrect, string content)
		{
			answer.Content = await this.FileSystem.SetFile(content, $"activities/{answer.Id}/answers-{answer.Id}");
			answer.IsCorrectAnswer = isCorrect;

			if (answer.QuestionActivity.Answers.All(a => !a.IsCorrectAnswer) && !isCorrect)
			{
				throw new BusinessException("Il faut au moins une bonne réponse");
			}

			if (answer.QuestionActivity.QuestionActivityType != QuestionActivityType.Question)
			{
                foreach (var item in answer.QuestionActivity.Answers)
                {
                    if (item.Id != answer.Id && isCorrect)
                    {
                        item.IsCorrectAnswer = false;
                    }
                }
            }			

			answer.QuestionActivity.LocatedGameUnit.FieldTrip.LastUpdateDate = DateTime.UtcNow;
			await this.SaveChangesAsync();
		}

		public async Task DeleteAnswer(Answer answer)
		{
			this.DataContext.Answers.Remove(answer);

			answer.QuestionActivity.LocatedGameUnit.FieldTrip.LastUpdateDate = DateTime.UtcNow;
			await this.SaveChangesAsync();
		}

		public async Task RemoveStatements(Guid fieldTripId, DateTime fromDate, DateTime toDate)
		{
			var playedActivities = this.DataContext.PlayedActivities
				.Include(pa => pa.Files)
				.Include(pa => pa.Activity)
				.Where(pa => pa.Activity.LocatedGameUnit.FieldTripId == fieldTripId).ToArray();

			List<PlayedActivityFile> files = playedActivities.SelectMany(pa => pa.Files).ToList();

			foreach (var played in playedActivities)
			{
				played.Answer = String.Empty;
				played.Latitude = null;
				played.Longitude = null;
				played.Files.Clear();
			}

			this.DataContext.PlayedActivityFiles.RemoveRange(files);

			await this.DataContext.SaveChangesAsync();

			foreach (var item in files.Select(paf => paf.Url))
			{
				this.FileSystem.Delete(item);
			}
		}

        public async Task RemoveReadingFilesOlderThanThreeMonthsAsync()
        {
            var playedActivityFilesOlderThanThreeMonthsToDelete = this.DataContext.PlayedActivityFiles
                .Include(paf => paf.PlayedActivity)
                .Where(paf => !string.IsNullOrEmpty(paf.Url) && paf.PlayedActivity.AnswerDate.Date < DateTime.UtcNow.AddMonths(-3).Date)
                .ToArray();

			foreach (var file in playedActivityFilesOlderThanThreeMonthsToDelete)
			{
				await this.FileSystem.DeleteAsync(file.Url);

				file.Url = null;
			}

			await this.DataContext.SaveChangesAsync();
        }
    }
}
