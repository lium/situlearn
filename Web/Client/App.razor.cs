﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

using Ozytis.Common.Core.ClientApi;
using Ozytis.Common.Core.Web.Razor.Utilities;

using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using Web.Client.Pages.User;
using Web.Client.Utilities;

namespace Web.Client
{
    public partial class App : ComponentBase
    {
        public App() : base()
        {
            BaseService.OnAuthorizeRequired += (sender, args) => this.NavigationManager.NavigateTo(LoginPage.Url);
        }

        [Inject]
        public IJSRuntime JSRuntime { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public AdditionalAssemblyProvider AdditionalAssemblyProvider { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await Ozytis.Common.Core.Web.Razor.CommonScriptLoader.EnsureOzytisCommonIsPresentAsync(this.JSRuntime);
            this.AdditionalAssemblies = this.AdditionalAssemblyProvider.GetAssemblies();

            Console.WriteLine("Additional assemblies loaded : " + string.Join(", ", this.AdditionalAssemblies.Select(a => a.FullName)));

            BlazorHelper.CurrentTheme.EnableClosedMobileSidebar = true;
            BlazorHelper.CurrentTheme.EnableClosedSidebar = true;
            BlazorHelper.CurrentTheme.Update();

            await base.OnInitializedAsync();
        }

        public Assembly[] AdditionalAssemblies { get; set; }
    }
}