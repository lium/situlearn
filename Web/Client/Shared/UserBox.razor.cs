﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.Components.Authorization;
using System.Linq;
using System.Threading.Tasks;
using Web.Client.Pages.User;
using Web.Client.Repositories;

namespace Web.Client.Shared
{
    public partial class UserBox : ComponentBase
    {
        [Inject]
        public UsersRepository UsersRepository { get; set; }      

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [CascadingParameter]
        public Task<AuthenticationState> AuthenticationStateTask { get; set; }

        protected override async Task OnInitializedAsync()
        {
            AuthenticationState state = await this.AuthenticationStateTask;
            System.Security.Claims.ClaimsPrincipal user = state.User;

            this.Email = this.UsersRepository.CurrentUser?.Email ?? user.Claims.FirstOrDefault(c => c.Type == System.Security.Claims.ClaimTypes.Email)?.Value;
            this.User = this.UsersRepository.CurrentUser;
            this.IsConnectedAs = await this.UsersRepository.IsUserConnectedAsAsync();
            
            this.UsersRepository.OnCurrentUserUpdated += this.UsersRepository_OnCurrentUserUpdated;            
           

            await base.OnInitializedAsync();
        }     

        private async void UsersRepository_OnCurrentUserUpdated(object sender, System.EventArgs e)
        {
            this.User = this.UsersRepository.CurrentUser;
            this.IsConnectedAs = await this.UsersRepository.IsUserConnectedAsAsync();
            
            this.StateHasChanged();
        }

        public string Email { get; set; }

        public UserModel User { get; set; }

        public bool IsConnectedAs { get; set; }

        public async Task LogOutAsync()
        {
            await this.UsersRepository.LogoutAsync();
            this.NavigationManager.NavigateTo(LoginPage.Url);
        }

        protected async Task CancelConnectAsAsync()
        {
            await this.UsersRepository.CancelConnectAs();
            this.NavigationManager.NavigateTo(Pages.FieldTrips.FieldTripsPage.IndexUrl, true);
        }       
    }
}
