// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using BlazorPro.BlazorSize;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

using Newtonsoft.Json;

using Ozytis.Common.Core.ClientApi;
using Ozytis.Common.Core.GeoJson;
using Ozytis.Common.Core.Logs.NetCore.Client;
using Ozytis.Common.Core.Web.Razor.Utilities;
using SituLearn.Monitor.Repositories;
using SituLearn.Monitor.Security;
using SituLearn.Monitor.Utilities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace SituLearn.Monitor
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            WebAssemblyHostBuilder builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            BaseService.BaseUrl = builder.Configuration["ServerUrl"];
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.Configuration["ServerUrl"]) });

            builder.Services.AddSingleton<AuthenticationStateProvider, Security.OzytisAuthStateProvider>();
            builder.Services.AddSingleton<IAuthorizationPolicyProvider, Security.OzytisAuthPolicyProvider>();
            builder.Services.AddSingleton<IAuthorizationService, Security.OzytisAuthService>(sp => new OzytisAuthService(sp));
            builder.Services.AddSingleton<OzytisAuthStateProvider>(sp => sp.GetService<AuthenticationStateProvider>() as OzytisAuthStateProvider);

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                MaxDepth = 30,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Converters = GeoJsonUtils.GetSerializer().Converters
            };

            builder.Services.AddSingleton<LocalStorageService>();
            builder.Services.AddSingleton<NavigationWithHistoryManager>();

            builder.Services.AddSingleton<AdditionalAssemblyProvider>();
            builder.Services.AddOzLogsDashboard();

            foreach (Type manager in typeof(UsersRepository).Assembly.GetTypes().Where(t => t.Name.EndsWith("Repository")))
            {
                builder.Services.AddSingleton(manager);
            }

            builder.Services.AddSingleton<ResizeListener>();
            builder.Services.AddSingleton<GeoLocationService>();
            builder.Services.AddSingleton<AppViewModel>();
            builder.Services.AddSingleton<IMediaQueryService, MediaQueryService>();

            builder.Services.Configure<RequestLocalizationOptions>(options =>
            {
                // Define the list of cultures your app will support
                var supportedCultures = new List<System.Globalization.CultureInfo>()
                {
                    new System.Globalization.CultureInfo("fr"),
                };

                // Set the default culture
                options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("fr");

                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                options.RequestCultureProviders = new List<Microsoft.AspNetCore.Localization.IRequestCultureProvider>() {
                    new Microsoft.AspNetCore.Localization.QueryStringRequestCultureProvider()
                };
            });

            builder.Services.AddSingleton(services =>
            {
                return new ConnectivityServiceOptions
                {
                    PingAction = () => services.GetService<MaintenanceRepository>().PingAsync(),
                    PingInterval = 5000
                };
            });

            builder.Services.AddSingleton<IConnectivityService, ConnectivityService>();

            var buildInformation = new BuildInformation
            {
                BuildDate = Assembly.GetAssembly(typeof(Program)).GetCustomAttribute<BuildInformationAttribute>()?.BuildDate,
                Branch = ThisAssembly.Git.Branch,
                Commit = ThisAssembly.Git.Commit
            };
            builder.Services.AddSingleton<BuildInformation>(buildInformation);

            WebAssemblyHost host = builder.Build();

            await host.Services.GetService<IConnectivityService>().StartListeningAsync();

            BaseService.SetBearerToken(await host.Services.GetService<LocalStorageService>().GetItemAsync<string>("token"));
            BaseService.OnAuthorizeRequired += (sender, args) =>
            {
                var navMan = host.Services.GetService<NavigationManager>();
                var relativePath = navMan.ToBaseRelativePath(navMan.Uri);

                navMan.NavigateTo(Pages.User.LoginPage.Url);
            };

            host.Services.UseOzLogsDashboard();

            await host.Services.GetService<UsersRepository>().InitializeAsync();

            await host.RunAsync();
        }
    }
}