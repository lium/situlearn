﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Components;
using SituLearn.Player.Repositories;
using System;
using System.Threading.Tasks;
using Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using System.Linq;

namespace SituLearn.Player.Pages.FieldTrips
{
    [Route(Url)]
    public partial class EndGamePage : ComponentBase
    {
        public const string Url = "terminee/{playerId:guid}";

        public static string GetUrl(Guid playerId)
        {
            return Url.Replace("{playerId:guid}", playerId.ToString());
        }

        [Parameter]
        public Guid PlayerId { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IConfiguration Configuration { get; set; }

        [Inject]
        public IConnectivityService ConnectivityService { get; set; }

        public Entities.Player Player { get; set; }

        public FieldTrip FieldTrip { get; set; }

        public Journey Journey { get; set; }

        public string BasePictureUrl { get; set; }

        public int TotalPoints { get; set; }

        public bool IsSubmitting { get; set; }

        public int ElapsedTime { get; set; }

        [CascadingParameter]
        public AppLayout Layout { get; set; }

        public bool TestEnv => this.Layout.TestEnv;

        [Inject]
        private IJSRuntime JSRuntime { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.Player = this.GamesRepository.CurrentPlayer;

            this.FieldTrip = this.GamesRepository.CurrentFieldTrip;

            this.Journey = this.FieldTrip.Journeys.FirstOrDefault(j => j.Id == this.Player.JourneyId);

            this.ElapsedTime = (int)(this.GamesRepository.CurrentPlayer.EndGameDate.Value - this.GamesRepository.CurrentPlayer.StartPlayedDate).TotalMinutes;

            this.BasePictureUrl = "";

            this.TotalPoints = this.GamesRepository.CalculatePlayerTotalPoints();

            await base.OnInitializedAsync();
        }

        protected async Task EndGameAsync()
        {
            if (this.IsSubmitting)
            {
                return;
            }

            this.IsSubmitting = true;

			if (this.TestEnv)
			{
                this.IsSubmitting = false;
                this.StateHasChanged();

                await this.JSRuntime.InvokeVoidAsync("closeSelf", this.Configuration["ServerUrl"].TrimEnd('/'));
                return;
            }

            this.StateHasChanged();

            await this.GamesRepository.EndGameAsync();

            this.IsSubmitting = false;

            this.StateHasChanged();

            if (this.GamesRepository.IsOnDiscoveryMode)
            {
                await this.GamesRepository.RemoveCacheAsync();
                this.NavigationManager.NavigateTo(string.Concat(FieldTripsPage.Url, "?discovery"));
            }
            else
            {
                this.NavigationManager.NavigateTo(PlayedFieldTrips.PlayedFieldTripsPage.Url);
            }            
        }

    }
}
