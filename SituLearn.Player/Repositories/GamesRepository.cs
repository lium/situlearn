﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using ClientApi;
using Common;
using Entities;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Ozytis.Common.Core.ClientApi.Storage;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.Razor.Utilities.GeoLocation;
using SituLearn.Player.Repositories.IndexedDbManager;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Player.Repositories
{
    public class GamesRepository : BaseRepository
    {
        private const string GamesKeyCacheName = "games";
        private const decimal LocationChangedThreshold = 0.00005m;
        private const string LastUpdateDateKey = "lastUpdateDate";
        private readonly GamesService gamesService = new();

        public GamesRepository(IClientStorageService storageService, IConnectivityService connectivityService,
            UsersRepository usersRepository, GeoLocationService geoLocationService, BatteryService batteryService, LocalStorageService localStorageService, GamesManager gamesManager,
            FieldTripsRepository fieldTripsRepository, IConfiguration configuration) : base(storageService)
        {
            this.UsersRepository = usersRepository;
            this.GeoLocationService = geoLocationService;
            this.ConnectivityService = connectivityService;
            this.BatteryService = batteryService;
            this.LocalStorageService = localStorageService;
            this.GamesManager = gamesManager;
            this.FieldTripsRepository = fieldTripsRepository;
            this.Configuration = configuration;
        }

        public UsersRepository UsersRepository { get; }
        public IConnectivityService ConnectivityService { get; }
        public GeoLocationService GeoLocationService { get; }
        public BatteryService BatteryService { get; }
        public LocalStorageService LocalStorageService { get; }
        public GamesManager GamesManager { get; }

        public FieldTripsRepository FieldTripsRepository { get; set; }

        public event EventHandler<LocatedGameUnit> OnUnitReached;

        public event EventHandler<Game[]> OnGamesRefreshed;

        public event EventHandler<GeoLocationResult> OnRadarDistanceFromUnitHaveChanged;

        public event EventHandler<GeoLocationCoordinates> OnExplorationAreaLeft;

        public event EventHandler<List<LocatedGameUnit>> OnNearbyUnitDetected;

        public Entities.Player CurrentPlayer { get; set; }
        public Game CurrentGame { get; set; }
        public FieldTrip CurrentFieldTrip { get; set; }
        public DateTime LastUpdatedLocationDate { get; set; } = DateTime.UtcNow;
        public GeoLocationCoordinates LastUpdatedLocation { get; set; }

        public DateTime LastUpdatedRadarDistanceDate { get; set; } = DateTime.UtcNow;

        public DateTime LastUpdatedCheckOutOfExplorationAreaDate { get; set; }

        public DateTime LastOutOfExplorationAreaAlertDate { get; set; }

        public DateTime LastUpdatedNearbyUnitsDetectedDate { get; set; } = DateTime.UtcNow;

        public string TeamName { get; set; }
        public string TeamPicture { get; set; }

        public bool IsOnDiscoveryMode { get; set; }

        public IConfiguration Configuration { get; set; }

        private HubConnection HubConnection { get; set; }

        public bool HubConnectionIsConnected => this.HubConnection?.State == HubConnectionState.Connected;

        //public async Task CheckStatementRemoval()
        //{
        //    if (this.ConnectivityService.IsOnline)
        //    {
        //        var allGames = await this.GamesManager.SelectAll();
        //        var currentPlayedActivities = allGames.SelectMany(v => v.Players)
        //                .SelectMany(p => p.PlayedUnits ?? Array.Empty<PlayedUnit>())
        //                .SelectMany(pu => pu?.PlayedActivities ?? Array.Empty<PlayedActivity>());

        //        List<Guid> affectedGames = new();
        //        if (currentPlayedActivities.Any() && !string.IsNullOrEmpty(this.UsersRepository.CurrentUser?.Id))
        //        {
        //            // contrôle du token utilisateur
        //            await this.UsersRepository.GetUserAsync(this.UsersRepository.CurrentUser.Id);

        //            var activityStatementsArePurgedById = (await this.gamesService.CheckPlayedActivitiesForStatementsRemovalAsync(currentPlayedActivities.Select(x => x.Id).ToList())).CheckIfSuccess();

        //            foreach (var playedActivity in currentPlayedActivities)
        //            {
        //                if (activityStatementsArePurgedById.ContainsKey(playedActivity.Id) && activityStatementsArePurgedById[playedActivity.Id])
        //                {
        //                    playedActivity.Latitude = null;
        //                    playedActivity.Longitude = null;
        //                    playedActivity.Answer = String.Empty;

        //                    if (playedActivity.Files != null)
        //                    {
        //                        playedActivity.Files.Clear();
        //                    }
        //                    if (!affectedGames.Contains(playedActivity.PlayedUnit.Player.GameId))
        //                    {
        //                        affectedGames.Add(playedActivity.PlayedUnit.Player.GameId);
        //                    }

        //                }
        //            }
        //            var gamesToUpdate = affectedGames.Select(id => new { Id = id, Game = allGames.FirstOrDefault(g => g.Id == id) }).ToDictionary(g => g.Id, g => g.Game);
        //            await this.GamesManager.BulkCreateOrUpdateById(gamesToUpdate);
        //            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, c => c.Id);
        //        }
        //    }
        //}


        public async Task InitializeAsync()
        {
            var games = await this.GamesManager.SelectAll();
            //this.CachedGames = games.ToDictionary(g => g.Id, g => g);
            if (this.ConnectivityService.IsOnline)
            {
                await this.SaveGamesOnRemoteServerAsync();
                await this.LoadGamesFromRemoteAsync();
            }

            //this.CachedGames ??= new();

            this.CurrentGame = games.FirstOrDefault(g => g.Players.Any(p => p.UserId == this.UsersRepository.CurrentUser?.Id && p.PlayerState == PlayerState.Playing));
            this.CurrentPlayer = this.CurrentGame?.Players
                .FirstOrDefault(p => p.UserId == this.UsersRepository.CurrentUser?.Id
                    && p.PlayerState == Common.PlayerState.Playing);

            this.GeoLocationService.OnCurrentPositionChanged += this.GeoLocationService_OnCurrentPositionChanged;
            await this.GeoLocationService.WatchCurrentPositionAsync(new GeoLocationOptions() { EnableHighAccuracy = true });

            this.ConnectivityService.OnConnectionStateChanged += this.Connectivity_ConnectivityChanged;
        }

        private async void Connectivity_ConnectivityChanged(object sender, bool isConnected)
        {
            if (!this.IsOnDiscoveryMode && isConnected && this.CurrentFieldTrip != null && (this.HubConnection is null || (this.HubConnection is not null && this.HubConnection.State == HubConnectionState.Disconnected)))
            {
                await this.InitializeHubConnection();

                await this.HubConnection.SendAsync("RefreshPlayer", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(this.CurrentPlayer.StartPlayedDate.ToLocalTime()), this.ConvertPlayerToMonitorApiResult(this.CurrentPlayer));
            }
        }

        public async Task<Game> GetGameFromCacheAsync(Guid gameId)
        {
            return await this.GamesManager.SelectById(gameId.ToString());
        }

        public async Task<Game[]> GetGamesFromCacheByFieldTripIdAsync(Guid fieldTripId)
        {
            return await this.GamesManager.SelectByIndex("FielTripId", fieldTripId.ToString());
        }

        public async Task<(Game Game, Entities.Player Player)> GetPlayerForUserByFieldTripIdAsync(Guid fieldTripId, string userId)
        {
            var games = await this.GetGamesFromCacheByFieldTripIdAsync(fieldTripId);
            return games.Select(g => (g, g.Players.FirstOrDefault(p => p.UserId == userId))).FirstOrDefault(g => g.Item2 != null);
        }

        public async Task SaveGamesOnRemoteServerAsync()
        {
            var games = await this.GamesManager.SelectAll();
            if (games != null && games.Any())
            {
                var result = await this.gamesService.UpdateMyGamesAsync(games);
            }
        }

        public async Task LoadGamesFromRemoteAsync()
        {
            DateTime lasUpdate = await this.GetLastUpdateDateAsync();
            var remote = await this.gamesService.GetMyGamesAsync(lasUpdate);
            await this.UpdateLastUpdateDateAsync(DateTime.UtcNow);
            if (remote == null)
            {
                return;
            }

            var cachedGames = remote.ToDictionary(g => g.Id, g => g);
            //Console.WriteLine($"Updating {remote.Count()} games");
            //IDB: UPDATE en masse
            //foreach (var game in remote)
            //{
            //    await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);
            //}
            await this.GamesManager.BulkCreateOrUpdateById(cachedGames);
            this.OnGamesRefreshed?.Invoke(this, remote.ToArray());
        }

        public async Task<Game> StartGameAsync(FieldTrip fieldTrip, Journey journey)
        {
            //Console.WriteLine("Lancement de la géolocalisation");
            try
            {
                await this.GeoLocationService.WatchCurrentPositionAsync(new GeoLocationOptions() { EnableHighAccuracy = true });
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Erreur au lancement de la géolocalisation: {ex.GetType().ToString()} - {ex.Message}");
            }

            //Console.WriteLine("Lancement partie : début mise à jour du cache avec données de la nouvelle partie");
            this.CurrentFieldTrip = fieldTrip;
            if (this.CurrentPlayer != null && this.CurrentPlayer.PlayerState == Common.PlayerState.Playing)
            {
                // on annule la partie en cours

                this.CurrentPlayer.PlayerState = Common.PlayerState.Cancelled;
                await this.GamesManager.CreateOrUpdateById(this.CurrentGame.Id.ToString(), this.CurrentGame);
                //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, c => c.Id);
            }

            //Game game = (this.CachedGames?.Any()??false) ? this.CachedGames.FirstOrDefault(g => g.Value.FielTripId == fieldTrip.Id && g.Value.Players.Any(p => p.UserId == this.UsersRepository.CurrentUser.Id)).Value : null;
            Game game = (await this.GetGamesFromCacheByFieldTripIdAsync(fieldTrip.Id))?.FirstOrDefault(g => g.Players.Any(p => p.UserId == this.UsersRepository.CurrentUser.Id));
            //Si l'utilisateur a déjà fait une partie, on la réinitialise
            if (game != null)
            {
                //game.FieldTrip = fieldTrip;
                game.FieldTripName = fieldTrip.Name;
                game.StartDate = DateTime.UtcNow;
                game.EndDate = null;
                game.Players = new List<Entities.Player>();
            }
            else//Sinon, on la crée 
            {
                game = new()
                {
                    //FieldTrip = fieldTrip,
                    FielTripId = fieldTrip.Id,
                    FieldTripName = fieldTrip.Name,
                    Id = Guid.NewGuid(),
                    StartDate = DateTime.UtcNow,
                    Players = new List<Entities.Player>()
                };
            }

            Entities.Player player = new Entities.Player
            {
                Id = Guid.NewGuid(),
                GameId = game.Id,
                //Journey = journey,
                JourneyId = journey.Id,
                Game = game,
                StartPlayedDate = DateTime.UtcNow,
                UserId = this.IsOnDiscoveryMode ? null : this.UsersRepository.CurrentUser.Id,
                PlayerState = Common.PlayerState.Playing,
                TeamName = this.TeamName,
                TeamPicture = this.TeamPicture
            };

            game.Players.Add(player);

            //if (!this.CachedGames.ContainsKey(game.Id))
            //{
            //    this.CachedGames.Add(game.Id, game);
            //}
            //else
            //{
            //    this.CachedGames[game.Id] = game;
            //}

            await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);
            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, c => c.Id);
            //Console.WriteLine("Lancement partie : fin mise à jour du cache avec données de la nouvelle partie");

            if (!this.IsOnDiscoveryMode)
            {
                //Console.WriteLine("Lancement partie : début synchronisation des données de la nouvelle partie vers le serveur");
                await this.gamesService.UpdateMyGameAsync(game);
                //Console.WriteLine("Lancement partie : fin synchronisation des données de la nouvelle partie vers le serveur");

                if (this.FieldTripsRepository.ConnectivityService.IsOnline)
                {
                    await this.InitializeHubConnection();

                    if (this.HubConnection is not null && this.HubConnectionIsConnected)
                    {
                        await this.HubConnection.SendAsync("SendNewPlayerInGame", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(player.StartPlayedDate.ToLocalTime()), this.ConvertPlayerToMonitorApiResult(player));
                    }
                }                
            }
            
            this.CurrentPlayer = player;
            this.CurrentGame = game;
            switch (fieldTrip.FieldTripType)
            {
                case Common.FieldTripType.TreasureHunt:

                    // on cherche le premier POI
                    //Console.WriteLine("Lancement partie : recherche de la 1ère unité de jeu");
                    player.CurrentUnitId = await this.PlayNextUnitAsync(game.Id, player.Id);

                    break;
                case Common.FieldTripType.InteractiveWalk:
                case Common.FieldTripType.ActivityHub:

                    break;
                default:
                    throw new NotImplementedException();
            }

            return game;
        }

        public PlayerForMonitorModel ConvertPlayerToMonitorApiResult(Entities.Player source)
        {
            int pointsForLocatingUnits = source.PlayedUnits != null ? source.PlayedUnits.Sum(u => u.LocationPoints) : 0;

            var pointsForQuestionActivities = source.PlayedUnits == null ? 0 : source.PlayedUnits
                .Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
                .SelectMany(pu => pu.PlayedActivities)
                .Sum(pa => pa.Points ?? 0);

            var score = pointsForLocatingUnits + pointsForQuestionActivities;

            int pointsToWinForLocatingUnits = this.CurrentFieldTrip.Journeys.FirstOrDefault(j => j.Id == source.JourneyId).JourneyLocatedGameUnits
                .Select(jlgu => jlgu.LocatedGameUnit)
                .Where(u => !u.IsDeleted)
                .Sum(u => u.LocationGain);

            int pointsToWinForQuestionActivities = this.CurrentFieldTrip.Journeys.FirstOrDefault(j => j.Id == source.JourneyId).JourneyLocatedGameUnits
                .Select(jlgu => jlgu.LocatedGameUnit)
                .Where(u => u.Activities != null && u.Activities.Any())
                .SelectMany(u => u.Activities)
                .Where(a => !a.IsDeleted)
                .Sum(a => a is QuestionActivity question ? question.Gain : 0);

            int totalPointsToWin = pointsToWinForLocatingUnits + pointsToWinForQuestionActivities;

            var questionActivities = this.CurrentFieldTrip.Journeys.FirstOrDefault(j => j.Id == source.JourneyId).JourneyLocatedGameUnits.Select(jlgu => jlgu.LocatedGameUnit).SelectMany(u => u.Activities).Where(a => a is QuestionActivity).Select(a => a as QuestionActivity);

            int answeredQuestionsNumber = source.PlayedUnits == null ? 0 : source.PlayedUnits
                .Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
                .SelectMany(pu => pu.PlayedActivities)
                .Where(pa => questionActivities.Select(qa => qa.Id).Contains(pa.ActivityId))
                .Count();

            int correctAnswersNumber = source.PlayedUnits == null ? 0 : source.PlayedUnits
                .Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
                .SelectMany(pu => pu.PlayedActivities)
                .Where(pa => pa.Points.HasValue && pa.Points.Value >= ((questionActivities.FirstOrDefault(qa => qa.Id == pa.ActivityId).Gain - questionActivities.FirstOrDefault(qa => qa.Id == pa.ActivityId).Clue.Cost)))
                .Count();

            var readingActivities = this.CurrentFieldTrip.Journeys.FirstOrDefault(j => j.Id == source.JourneyId).JourneyLocatedGameUnits.Select(jlgu => jlgu.LocatedGameUnit).SelectMany(u => u.Activities).Where(a => a is ReadingActivity).Select(a => a.Id);

            int readingsNumber = source.PlayedUnits == null ? 0 : source.PlayedUnits
                .Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
                .SelectMany(pu => pu.PlayedActivities)
                .Where(pa => readingActivities.Contains(pa.ActivityId))
                .Count();

            bool outOfExplorationAreaAlert = false;

            if (source.CurrentLongitude.HasValue && source.CurrentLatitude.HasValue && this.CurrentFieldTrip.ExplorationMap.ExplorationArea != null && this.CurrentFieldTrip.ExplorationMap.OrganizerOutOfZoneAlert)
            {
                var point = new NetTopologySuite.Geometries.Point(
                    new NetTopologySuite.Geometries.Coordinate((double)source.CurrentLongitude.Value,
                    (double)source.CurrentLatitude.Value)
                );

                if (!this.CurrentFieldTrip.ExplorationMap.ExplorationArea.ContainsPoint(point))
                {
                    outOfExplorationAreaAlert = true;
                }
            }

            string currentUnitName = source.PlayedUnits != null ?
                this.CurrentFieldTrip.Journeys.FirstOrDefault(j => j.Id == source.JourneyId).JourneyLocatedGameUnits.FirstOrDefault(jlgu => jlgu.LocatedGameUnitId == source.CurrentUnitId).LocatedGameUnit.Name : 
                this.CurrentFieldTrip.Journeys.FirstOrDefault(j => j.Id == source.JourneyId).JourneyLocatedGameUnits.FirstOrDefault().LocatedGameUnit.Name;

            var result = new PlayerForMonitorModel
            {
                Id = source.Id,
                UserId = source.UserId,
                Name = !string.IsNullOrEmpty(source.TeamName) ? source.TeamName : $"{this.UsersRepository.CurrentUser.FirstName} {this.UsersRepository.CurrentUser.LastName.ToUpperInvariant()}",
                PlayedUnitsNumber = source.EndGameDate.HasValue ? source.PlayedUnits.Count : (source.PlayedUnits != null && source.PlayedUnits.Any() ? source.PlayedUnits.Count - 1 : 0),
                UnitsToPlayNumber = this.CurrentFieldTrip.Journeys.FirstOrDefault(j => j.Id == source.JourneyId).JourneyLocatedGameUnits.Count(),
                PhoneNumber = this.UsersRepository.CurrentUser.PhoneNumber,
                CurrentUnitName = currentUnitName,
                Score = score,
                TotalPointsToWin = totalPointsToWin,
                CorrectAnswersNumber = correctAnswersNumber,
                AnsweredQuestionsNumber = answeredQuestionsNumber,
                ReadingsNumber = readingsNumber,
                CurrentLatitude = source.CurrentLatitude.HasValue ? source.CurrentLatitude.Value : default,
                CurrentLongitude = source.CurrentLongitude.HasValue ? source.CurrentLongitude.Value : default,
                OutOfExplorationAreaAlert = outOfExplorationAreaAlert,
                LowBatteryAlert = source.BatteryLevel.HasValue && source.BatteryLevel.Value < 0.20M
            };

            return result;
        }

        private async Task InitializeHubConnection()
        {
            this.HubConnection = new HubConnectionBuilder()
                .WithUrl($"{this.Configuration["ServerUrl"]}monitorhub")
                .WithAutomaticReconnect()
                .Build();

            this.HubConnection.Reconnected += async (connectionId) =>
            {
                await this.HubConnection.SendAsync("RefreshPlayer", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(this.CurrentPlayer.StartPlayedDate.ToLocalTime()), this.ConvertPlayerToMonitorApiResult(this.CurrentPlayer));
            };

            await this.HubConnection.StartAsync();
        }

        public async Task UnlockPositionAsync()
        {
            this.CurrentPlayer.CurrentUnitActivationHasBeenUnlocked = true;
            this.CurrentPlayer.CurrentUnitReachedDate = DateTime.UtcNow;

            var currentPlayedUnit = this.CurrentPlayer.PlayedUnits.FirstOrDefault(pu => pu.UnitId == this.CurrentPlayer.CurrentUnitId);
            currentPlayedUnit.LocalisationReachDate = this.CurrentPlayer.CurrentUnitReachedDate;
            var currentUnit = this.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == currentPlayedUnit.UnitId);
            if (currentUnit.LocationValidationType == LocationValidationType.Manual)
            {
                currentPlayedUnit.HaveLocatedUnit = true;
                currentPlayedUnit.LocationPoints = currentUnit.LocationGain;
            }

            await this.GamesManager.CreateOrUpdateById(this.CurrentGame.Id.ToString(), this.CurrentGame);

            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, c => c.Id);

            if (!this.IsOnDiscoveryMode)
            {
                await this.gamesService.UpdatePlayedUnitAsync(new PlayedUnitUpdateModel
                {
                    Id = currentPlayedUnit.Id,
                    HaveLocatedUnit = currentPlayedUnit.HaveLocatedUnit,
                    LocalisationReachDate = currentPlayedUnit.LocalisationReachDate,
                    PlayerId = currentPlayedUnit.PlayerId,
                    UnitId = currentPlayedUnit.UnitId,
                    GameId = this.CurrentPlayer.GameId,
                    FielTripId = this.CurrentGame.FielTripId,
                    JourneyId = this.CurrentPlayer.JourneyId,
                    StartCurrentUnitDate = this.CurrentPlayer.StartCurrentUnitDate.Value,
                    CurrentUnitActivationHasBeenUnlocked = this.CurrentPlayer.CurrentUnitActivationHasBeenUnlocked,
                    LocationPoints = currentPlayedUnit.LocationPoints
                });

                if (this.HubConnection is not null && this.HubConnectionIsConnected)
                {
                    var playedUnitsNumber = this.CurrentPlayer.PlayedUnits.Count - 1;
                    await this.HubConnection.SendAsync("SendCurrentUnitIsLocated", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(this.CurrentPlayer.StartPlayedDate.ToLocalTime()), currentPlayedUnit.PlayerId, playedUnitsNumber, currentUnit.Name, currentPlayedUnit.LocationPoints);
                }
            }
        }

        public async Task<bool> CheckCodeValidationAsync(string value)
        {
            if (value != (this.CurrentPlayer.CurrentUnitId.ToString() + this.CurrentFieldTrip.Id.ToString()))
            {
                return false;
            }

            await this.ValidatePoiLocalisationIsReachedAsync();
            var currentUnit = this.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == this.CurrentPlayer.CurrentUnitId);
            this.OnUnitReached?.Invoke(this, currentUnit);

            return true;
        }

        private async void GeoLocationService_OnCurrentPositionChanged(object sender, Ozytis.Common.Core.Web.Razor.Utilities.GeoLocation.GeoLocationResult locationResult)
        {
            if (locationResult.Error != null)
            {
                Console.WriteLine(locationResult.Error.Message);
                return;
            }

            if (this.CurrentFieldTrip == null)
            {
                return;
            }

            if (this.CurrentPlayer != null && this.CurrentPlayer.CurrentUnitId != null && this.CurrentPlayer.PlayerState == PlayerState.Playing)
            {
                bool tooClose = false;
                if (this.LastUpdatedLocation != null)
                {
                    tooClose = Math.Abs((locationResult.Location.Coords.Latitude + locationResult.Location.Coords.Longitude) - ((this.LastUpdatedLocation.Latitude) + (this.LastUpdatedLocation.Longitude))) < LocationChangedThreshold;
                }

                this.CurrentPlayer.CurrentLatitude = locationResult.Location?.Coords?.Latitude;
                this.CurrentPlayer.CurrentLongitude = locationResult.Location?.Coords?.Longitude;

                bool tooSoon = (DateTime.UtcNow - this.LastUpdatedLocationDate) < TimeSpan.FromSeconds(10);

                if (!tooClose && !tooSoon)
                {
                    if (this.ConnectivityService.IsOnline)
                    {
                        await this.GamesManager.CreateOrUpdateById(this.CurrentGame.Id.ToString(), this.CurrentGame);
                        //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);

                        decimal? batteryLevel = await this.BatteryService.GetBatteryLevelAsync();

                        if (!this.IsOnDiscoveryMode)
                        {
                            await this.gamesService.UpdatePositionAsync(new PlayerPositionUpdateModel
                            {
                                PlayerId = this.CurrentPlayer.Id,
                                Latitude = locationResult.Location.Coords.Latitude,
                                Longitude = locationResult.Location.Coords.Longitude,
                                BatteryLevel = batteryLevel,
                                LastUpdatedPositionDate = DateTime.UtcNow
                            });

                            if (this.HubConnection is not null && this.HubConnectionIsConnected)
                            {
                                await this.HubConnection.SendAsync("SendNewPositionAndBatteryLevel", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(this.CurrentPlayer.StartPlayedDate.ToLocalTime()), this.CurrentPlayer.Id, locationResult.Location.Coords.Latitude, locationResult.Location.Coords.Longitude, batteryLevel);
                            }
                        }
                    }

                    this.LastUpdatedLocationDate = DateTime.UtcNow;
                    this.LastUpdatedLocation = locationResult.Location.Coords;
                }
                var currentUnit = this.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == this.CurrentPlayer.CurrentUnitId);
                if (currentUnit.LocationAidType == LocationAidType.GpsRadar)
                {
                    tooSoon = (DateTime.UtcNow - this.LastUpdatedRadarDistanceDate) < TimeSpan.FromSeconds(2);

                    if (!tooSoon)
                    {
                        this.LastUpdatedRadarDistanceDate = DateTime.UtcNow;
                        this.OnRadarDistanceFromUnitHaveChanged?.Invoke(this, locationResult);
                    }
                }

                switch (this.CurrentFieldTrip.FieldTripType)
                {
                    case Common.FieldTripType.TreasureHunt:
                    case Common.FieldTripType.InteractiveWalk:
                    case Common.FieldTripType.ActivityHub:

                        await this.CheckCurrentUnitLocationAsync(locationResult.Location.Coords);

                        break;
                    default:
                        throw new NotImplementedException();
                }
            }

            if (this.CurrentPlayer != null && this.CurrentPlayer.PlayerState == PlayerState.Playing)
            {
                Game game = this.CurrentGame;
                var explorationMap = this.CurrentFieldTrip.ExplorationMap;

                if (explorationMap != null && explorationMap.IsGeolocatable && explorationMap.ParticipantOutOfZoneAlert && explorationMap.ExplorationArea != null)
                {
                    bool tooSoon = (DateTime.UtcNow - this.LastUpdatedCheckOutOfExplorationAreaDate) < TimeSpan.FromSeconds(10);

                    if (!tooSoon)
                    {
                        await this.CheckIfCurrentUserIsOutOfExplorationArea(locationResult.Location.Coords, explorationMap);

                        this.LastUpdatedCheckOutOfExplorationAreaDate = DateTime.UtcNow;
                    }
                }
            }

            if (this.CurrentPlayer != null && this.CurrentPlayer.CurrentUnitId == null && this.CurrentPlayer.PlayerState == PlayerState.Playing
                && this.CurrentFieldTrip.FieldTripType == FieldTripType.InteractiveWalk)
            {
                var tooSoon = (DateTime.UtcNow - this.LastUpdatedNearbyUnitsDetectedDate) < TimeSpan.FromSeconds(2);

                if (!tooSoon)
                {
                    this.LastUpdatedNearbyUnitsDetectedDate = DateTime.UtcNow;
                    this.CheckProximityToANewGameUnit(locationResult.Location.Coords);
                }
            }
        }

        private async Task CheckCurrentUnitLocationAsync(GeoLocationCoordinates coords)
        {
            var currentPoi = this.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == this.CurrentPlayer.CurrentUnitId);

            if (currentPoi == null)
            {
                return;
            }

            //on vérifie la position du jour par rapport au POI seulement si la validation de la localisation de l'unité de jeu est de type GPS et s'il ne l'a pas déjà atteinte
            if (currentPoi.LocationValidationType == LocationValidationType.GpsLocation && !this.CurrentPlayer.CurrentUnitReachedDate.HasValue)
            {
                var point = new NetTopologySuite.Geometries.Point(
                new NetTopologySuite.Geometries.Coordinate((double)coords.Longitude, (double)coords.Latitude));

                if (currentPoi.ActivationAreaType != Common.ActivationAreaType.None
                    && currentPoi.ActivationArea != null
                    && currentPoi.ActivationArea.ContainsPoint(point))
                {
                    await this.ValidatePoiLocalisationIsReachedAsync();
                    this.OnUnitReached?.Invoke(this, currentPoi);
                }
                else if (currentPoi.ActivationAreaType == Common.ActivationAreaType.None && !this.CurrentPlayer.CurrentUnitReachedDate.HasValue)
                {
                    await this.ValidatePoiLocalisationIsReachedAsync();
                    this.OnUnitReached?.Invoke(this, currentPoi);
                }
            }
        }

        public async Task ValidatePoiLocalisationIsReachedAsync()
        {
            this.CurrentPlayer.CurrentUnitReachedDate = DateTime.UtcNow;

            var currentPlayedUnit = this.CurrentPlayer.PlayedUnits.FirstOrDefault(pu => pu.UnitId == this.CurrentPlayer.CurrentUnitId);
            var currentUnit = this.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == currentPlayedUnit.UnitId);

            currentPlayedUnit.LocalisationReachDate = this.CurrentPlayer.CurrentUnitReachedDate;
            currentPlayedUnit.HaveLocatedUnit = true;
            currentPlayedUnit.LocationPoints = currentUnit.LocationGain;
            await this.GamesManager.CreateOrUpdateById(this.CurrentGame.Id.ToString(), this.CurrentGame);

            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, c => c.Id);

            if (!this.IsOnDiscoveryMode)
            {
                await this.gamesService.UpdatePlayedUnitAsync(new PlayedUnitUpdateModel
                {
                    Id = currentPlayedUnit.Id,
                    HaveLocatedUnit = currentPlayedUnit.HaveLocatedUnit,
                    LocalisationReachDate = currentPlayedUnit.LocalisationReachDate,
                    PlayerId = currentPlayedUnit.PlayerId,
                    UnitId = currentPlayedUnit.UnitId,
                    GameId = this.CurrentPlayer.GameId,
                    FielTripId = this.CurrentGame.FielTripId,
                    JourneyId = this.CurrentPlayer.JourneyId,
                    StartCurrentUnitDate = this.CurrentPlayer.StartCurrentUnitDate.Value,
                    CurrentUnitActivationHasBeenUnlocked = this.CurrentPlayer.CurrentUnitActivationHasBeenUnlocked,
                    LocationPoints = currentPlayedUnit.LocationPoints
                });

                if (this.HubConnection is not null && this.HubConnectionIsConnected)
                {
                    var playedUnitsNumber = this.CurrentPlayer.PlayedUnits.Count - 1;

                    await this.HubConnection.SendAsync("SendCurrentUnitIsLocated", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(this.CurrentPlayer.StartPlayedDate.ToLocalTime()), currentPlayedUnit.PlayerId, playedUnitsNumber, currentUnit.Name, currentPlayedUnit.LocationPoints);
                }
            }
        }

        private async Task CheckIfCurrentUserIsOutOfExplorationArea(GeoLocationCoordinates coords, ExplorationMap explorationMap)
        {
            var point = new NetTopologySuite.Geometries.Point(
            new NetTopologySuite.Geometries.Coordinate((double)coords.Longitude, (double)coords.Latitude));

            bool isOutOfExplorationArea = !explorationMap.ExplorationArea.ContainsPoint(point);

            if (isOutOfExplorationArea)
            {
                //Console.WriteLine($"Out of exploration area, current position: ({coords.Longitude}, {coords.Latitude})");

                bool tooSoon = (DateTime.UtcNow - this.LastOutOfExplorationAreaAlertDate) < TimeSpan.FromMinutes(2);

                if (!tooSoon)
                {
                    this.LastOutOfExplorationAreaAlertDate = DateTime.UtcNow;
                    this.OnExplorationAreaLeft?.Invoke(this, coords);
                }
            }
            
            if (!this.IsOnDiscoveryMode)
            {
                if (this.HubConnection is not null && this.HubConnectionIsConnected && (this.CurrentFieldTrip.ExplorationMap?.OrganizerOutOfZoneAlert ?? false))
                {
                    await this.HubConnection.SendAsync("SendOutOfExplorationAreaAlert", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(this.CurrentPlayer.StartPlayedDate.ToLocalTime()), this.CurrentPlayer.Id, isOutOfExplorationArea);
                }
            }
        }

        public async Task<Guid?> PlayNextUnitAsync(Guid gameId, Guid playerId)
        {
            //if (!this.CachedGames.ContainsKey(gameId))
            //{
            //    // voir que faire dans ce cas là. Redémarrer une partie ?
            //    throw new BusinessException("Partie inconnue");
            //}

            Game game = this.CurrentGame;

            Entities.Player player = game.Players.FirstOrDefault(p => p.Id == playerId);

            if (player == null)
            {
                // voir que faire dans ce cas là. Redémarrer une partie ?
                Console.WriteLine("Player null");
                return null;
            }

            Journey journey = this.CurrentFieldTrip.Journeys.FirstOrDefault(j => j.Id == player.JourneyId);

            List<PlayedUnit> alreadyPlayed = player.PlayedUnits?.ToList() ?? new();
            List<Guid> alreadyPlayedIds = alreadyPlayed.Select(p => p.UnitId).ToList();

            LocatedGameUnit locatedGameUnit = journey.JourneyLocatedGameUnits
                        .OrderBy(j => j.Order)
                        .FirstOrDefault(l => !alreadyPlayedIds.Contains(l.LocatedGameUnitId))?.LocatedGameUnit;
            //Console.WriteLine("PNU GR lgu");
            //Console.WriteLine(locatedGameUnit);
            //player.CurrentUnit = locatedGameUnit;
            player.CurrentUnitId = locatedGameUnit?.Id;
            player.CurrentUnitReachedDate = null;
            player.CurrentUnitActivationHasBeenUnlocked = false;

            if (locatedGameUnit == null)
            {
                //Console.WriteLine("PNu GR enddate");
                // si plus d'unité c'est que la partie du joueur est finie
                game.EndDate = DateTime.UtcNow;

                player.EndGameDate = game.EndDate;
                player.PlayerState = Common.PlayerState.Complete;
                player.StartCurrentUnitDate = null;
            }
            else
            {
                player.StartCurrentUnitDate = DateTime.UtcNow;

                alreadyPlayed.Add(
                    new PlayedUnit
                    {
                        Id = Guid.NewGuid(),
                        PlayerId = player.Id,
                        Player = player,
                        UnitId = locatedGameUnit.Id,
                        //Unit = locatedGameUnit
                    }
                );

                player.PlayedUnits = alreadyPlayed;

                if (!this.IsOnDiscoveryMode && this.HubConnection is not null && this.HubConnectionIsConnected)
                {
                    var playedUnitsNumber = player.PlayedUnits.Count - 1;

                    await this.HubConnection.SendAsync("SendStartNewUnit", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(this.CurrentPlayer.StartPlayedDate.ToLocalTime()), player.Id, playedUnitsNumber, locatedGameUnit.Name);
                }
            }

            this.CurrentPlayer = player;
            this.CurrentGame = game;
            await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);
            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);
            //Console.WriteLine("PNU GR lgu after");

            //Console.WriteLine(locatedGameUnit);
            //Console.WriteLine("ID" +locatedGameUnit?.Id);

            return locatedGameUnit?.Id;
        }

        public async Task<LocatedActivity> PlayNextActivityAsync()
        {

            Game game = this.CurrentGame;

            Entities.Player player = game.Players.FirstOrDefault(p => p.Id == this.CurrentPlayer.Id);
            LocatedGameUnit locatedGameUnit = this.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == this.CurrentPlayer.CurrentUnitId);
            //Console.WriteLine("NExtActivity locatedGameUnit");
            //Console.WriteLine(locatedGameUnit);
            var playedUnit = player.PlayedUnits?.FirstOrDefault(u => u.UnitId == locatedGameUnit.Id);

            List<PlayedActivity> alreadyPlayed = playedUnit.PlayedActivities?.ToList() ?? new();
            List<Guid> alreadyPlayedIds = alreadyPlayed.Select(p => p.ActivityId).ToList();

            LocatedActivity activity = null;

            if (locatedGameUnit.ActivitiesEnabled)
            {
                activity = locatedGameUnit.Activities
                .OrderBy(u => u.Order)
                .FirstOrDefault(a => !alreadyPlayedIds.Contains(a.Id));
                //Console.WriteLine("alreadyPlayed");
                //Console.WriteLine(string.Join(",", alreadyPlayedIds));
                //Console.WriteLine("activities");
                //Console.WriteLine(string.Join(",", locatedGameUnit.Activities.Select(a => a.Id)));
                //Console.WriteLine("act");
                //Console.WriteLine(activity);
            }
            else
            {
                Console.WriteLine("Activities disabled");
            }

            if (activity != null)
            {
                alreadyPlayed.Add(
                    new PlayedActivity()
                    {
                        Id = Guid.NewGuid(),
                        PlayedUnitId = playedUnit.Id,
                        PlayedUnit = playedUnit,
                        ActivityId = activity.Id,
                        //Activity = activity
                        ActivityName = activity.Name
                    }
                );

                player.PlayedUnits.FirstOrDefault(u => u.UnitId == locatedGameUnit.Id).PlayedActivities = alreadyPlayed;

                this.CurrentPlayer = player;
                this.CurrentGame = game;
                await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);
                //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);
            }

            return activity;
        }

        public async Task<LocatedActivity> ResumeActivity()
        {
            Game game = this.CurrentGame;

            Entities.Player player = game.Players.FirstOrDefault(p => p.Id == this.CurrentPlayer.Id);

            LocatedGameUnit locatedGameUnit = this.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == this.CurrentPlayer.CurrentUnitId);

            var playedUnit = player.PlayedUnits?.FirstOrDefault(u => u.UnitId == locatedGameUnit.Id);

            List<PlayedActivity> alreadyPlayed = playedUnit.PlayedActivities?.ToList() ?? new();
            List<Guid> alreadyPlayedIds = alreadyPlayed.Select(p => p.ActivityId).ToList();
            PlayedActivity lastPlayedActivity = playedUnit.PlayedActivities?.LastOrDefault();


            LocatedActivity activity = null;

            if (locatedGameUnit.ActivitiesEnabled && locatedGameUnit.Activities != null)
            {
                if (lastPlayedActivity != null)
                {
                    if (lastPlayedActivity.AnswerDate == default)
                    {
                        activity = locatedGameUnit.Activities.FirstOrDefault(a => a.Id == lastPlayedActivity.ActivityId);
                    }

                }
                else
                {
                    activity = locatedGameUnit.Activities.OrderBy(a => a.Order).FirstOrDefault();
                }


            }

            if (activity != null)
            {
                player.PlayedUnits.FirstOrDefault(u => u.UnitId == locatedGameUnit.Id).PlayedActivities = alreadyPlayed.Where(a => a.Id != activity.Id).ToList();

                this.CurrentPlayer = player;
                this.CurrentGame = game;
                await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);
                //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);
            }

            return activity;
        }

        public async Task<bool> ValidateAnswerAsync(QuestionActivity question, string answerText, Guid answerIdSelected)
        {
            bool answerIsCorrect = false;

            Game game = this.CurrentGame;

            Entities.Player player = game.Players.FirstOrDefault(p => p.Id == this.CurrentPlayer.Id);

            var currentUnit = player.PlayedUnits.FirstOrDefault(pu => pu.UnitId == this.CurrentPlayer.CurrentUnitId);

            var currentActivity = currentUnit.PlayedActivities?.FirstOrDefault(pa => pa.ActivityId == question.Id);

            currentActivity.AnswerDate = DateTime.UtcNow;

            if (question.QuestionActivityType == QuestionActivityType.PictureMcq || question.QuestionActivityType == QuestionActivityType.Mcq)
            {
                var correctAnwerId = question.Answers.FirstOrDefault(a => a.IsCorrectAnswer).Id;

                answerIsCorrect = answerIdSelected == correctAnwerId;

                currentActivity.Answer = answerIdSelected.ToString();
            }

            if (question.QuestionActivityType == QuestionActivityType.Question)
            {
                if (question.Answers.Any())
                {
                    var acceptedAnwersText = question.Answers?.Where(a => a.IsCorrectAnswer).Select(a => a.Content ?? "");

                    if (acceptedAnwersText == null || !acceptedAnwersText.Any())
                    {
                        answerIsCorrect = false;
                    }
                    else
                    {
                        answerIsCorrect = acceptedAnwersText.Any(c =>
                            string.Compare(c.ToLowerInvariant(), answerText.ToLowerInvariant().Trim(), CultureInfo.CurrentCulture, CompareOptions.IgnoreNonSpace) == 0
                        );
                    }
                }
                else
                {
                    answerIsCorrect = false;
                }

                currentActivity.Answer = answerText;
            }

            if (answerIsCorrect)
            {
                currentActivity.Points = (currentActivity.Points ?? 0) + question.Gain;
            }

            this.CurrentPlayer = player;
            this.CurrentGame = game;
            await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);

            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);

            if (!this.IsOnDiscoveryMode)
            {
                await this.gamesService.UpdatePlayedActivityAsync(new PlayedActivityUpdateModel
                {
                    ActivityId = currentActivity.ActivityId,
                    ActivityName = currentActivity.ActivityName,
                    Answer = currentActivity.Answer,
                    AnswerDate = currentActivity.AnswerDate,
                    Id = currentActivity.Id,
                    Latitude = currentActivity.Latitude,
                    Longitude = currentActivity.Longitude,
                    PlayedUnitId = currentActivity.PlayedUnitId,
                    Points = currentActivity.Points,
                    PlayerId = player.Id,
                    UnitId = question.LocatedGameUnitId,
                    GameId = this.CurrentPlayer.GameId,
                    FielTripId = this.CurrentGame.FielTripId,
                    JourneyId = this.CurrentPlayer.JourneyId
                });

                if (this.HubConnection is not null && this.HubConnectionIsConnected)
                {
                    await this.HubConnection.SendAsync("SendNewQuestionAnswered", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(this.CurrentPlayer.StartPlayedDate.ToLocalTime()), player.Id, answerIsCorrect, currentActivity.Points ?? 0);
                }
            }

            return answerIsCorrect;
        }

        public async Task<int> RemoveClueCostToCurrentActivityPoints(Guid activityId, int points)
        {
            var currentUnit = this.CurrentPlayer.PlayedUnits.FirstOrDefault(pu => pu.UnitId == this.CurrentPlayer.CurrentUnitId);

            var currentActivity = currentUnit.PlayedActivities?.FirstOrDefault(pa => pa.ActivityId == activityId);

            if (currentActivity == null)
            {
                return 0;
            }

            currentActivity.Points = -points;

            var totalPoints = this.CalculatePlayerTotalPoints();
            await this.GamesManager.CreateOrUpdateById(this.CurrentGame.Id.ToString(), this.CurrentGame);

            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);

            return totalPoints;
        }

        public int CalculatePlayerTotalPoints()
        {
            int pointsForLocatingUnits = this.CurrentPlayer.PlayedUnits.Sum(u => u.LocationPoints);

            var pointsForQuestionActivities = this.CurrentPlayer.PlayedUnits
                .Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
                .SelectMany(pu => pu.PlayedActivities)
                .Sum(pa => pa.Points ?? 0);

            var totalPoints = pointsForLocatingUnits + pointsForQuestionActivities;

            return totalPoints;
        }

        public async Task RegisterReadingAsync(ReadingActivity reading, string answerText, List<PlayedActivityFile> playedActivityFiles)
        {
            Game game = this.CurrentGame;

            Entities.Player player = game.Players.FirstOrDefault(p => p.Id == this.CurrentPlayer.Id);

            var currentUnit = player.PlayedUnits.FirstOrDefault(pu => pu.UnitId == this.CurrentPlayer.CurrentUnitId);

            var currentActivity = currentUnit.PlayedActivities?.FirstOrDefault(pa => pa.ActivityId == reading.Id && pa.AnswerDate == default);

            currentActivity.AnswerDate = DateTime.UtcNow;

            currentActivity.Answer = answerText;

            foreach (var file in playedActivityFiles)
            {
                file.PlayedActivity = currentActivity;
                file.PlayedActivityId = currentActivity.Id;
            }

            currentActivity.Files = playedActivityFiles;

            currentActivity.Latitude = this.CurrentPlayer.CurrentLatitude;
            currentActivity.Longitude = this.CurrentPlayer.CurrentLongitude;

            this.CurrentPlayer = player;
            this.CurrentGame = game;
            await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);

            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);

            if (!this.IsOnDiscoveryMode)
            {
                await this.gamesService.UpdatePlayedActivityAsync(new PlayedActivityUpdateModel
                {
                    ActivityId = currentActivity.ActivityId,
                    ActivityName = currentActivity.ActivityName,
                    Answer = currentActivity.Answer,
                    AnswerDate = currentActivity.AnswerDate,
                    Id = currentActivity.Id,
                    Latitude = currentActivity.Latitude,
                    Longitude = currentActivity.Longitude,
                    PlayedUnitId = currentActivity.PlayedUnitId,
                    Points = currentActivity.Points,
                    Files = currentActivity.Files,
                    PlayerId = player.Id,
                    UnitId = reading.LocatedGameUnitId,
                    GameId = this.CurrentPlayer.GameId,
                    FielTripId = this.CurrentGame.FielTripId,
                    JourneyId = this.CurrentPlayer.JourneyId
                });

                if (this.HubConnection is not null && this.HubConnectionIsConnected)
                {
                    await this.HubConnection.SendAsync("SendIncrementReadingsNumber", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(this.CurrentPlayer.StartPlayedDate.ToLocalTime()), player.Id);
                }
            }
        }

        public async Task AddNewReadingAsync(ReadingActivity reading)
        {
            Game game = this.CurrentGame;

            Entities.Player player = game.Players.FirstOrDefault(p => p.Id == this.CurrentPlayer.Id);

            LocatedGameUnit locatedGameUnit = this.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == this.CurrentPlayer.CurrentUnitId);

            var playedUnit = player.PlayedUnits?.FirstOrDefault(u => u.UnitId == locatedGameUnit.Id);

            List<PlayedActivity> alreadyPlayed = playedUnit.PlayedActivities?.ToList();

            LocatedActivity activity = locatedGameUnit.Activities.FirstOrDefault(a => a.Id == reading.Id);

            if (activity != null)
            {
                alreadyPlayed.Add(
                    new PlayedActivity()
                    {
                        Id = Guid.NewGuid(),
                        PlayedUnitId = playedUnit.Id,
                        PlayedUnit = playedUnit,
                        ActivityId = activity.Id,
                        //Activity = activity
                        ActivityName = activity.Name
                    }
                );

                player.PlayedUnits.FirstOrDefault(u => u.UnitId == locatedGameUnit.Id).PlayedActivities = alreadyPlayed;

                this.CurrentPlayer = player;
                this.CurrentGame = game;
                await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);

                //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);
            }
        }

        public async Task EndGameAsync()
        {
            Game game = this.CurrentGame;

            Entities.Player player = game.Players.FirstOrDefault(p => p.Id == this.CurrentPlayer.Id);

            player.CurrentUnit = null;
            player.CurrentUnitActivationHasBeenUnlocked = false;
            player.StartCurrentUnitDate = null;
            player.CurrentUnitReachedDate = null;

            this.CurrentPlayer = null;
            this.CurrentGame = null;
            this.CurrentFieldTrip = null;
            await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);
            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, c => c.Id);

            if (!this.IsOnDiscoveryMode)
            {
                await this.gamesService.EndGameAsync(new EndGameUpdateModel
                {
                    GameId = game.Id,
                    PlayerId = player.Id,
                    EndGameDate = game.EndDate.Value
                });

                if (this.HubConnection is not null && this.HubConnectionIsConnected)
                {
                    await this.HubConnection.SendAsync("SendEndGame", game.FielTripId, TimeOnly.FromDateTime(player.StartPlayedDate.ToLocalTime()), player.Id);

                    await this.HubConnection.DisposeAsync();
                }
            }

            await this.DeleteElapsedTimeAsync(game.FielTripId);
        }

        public async Task ResumePlayer(Entities.Game game, Entities.Player playerToResume)
        {
            //Game game = this.CachedGames[playerToResume.GameId];

            //Entities.Player player = game.Players.FirstOrDefault(p => p.Id == playerToResume.Id);

            playerToResume.PlayerState = PlayerState.Playing;

            this.CurrentPlayer = playerToResume;
            this.CurrentGame = game;
            this.CurrentFieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(game.FielTripId);
            await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);

            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);

            if (!this.IsOnDiscoveryMode)
            {
                if (this.FieldTripsRepository.ConnectivityService.IsOnline)
                {
                    await this.InitializeHubConnection();

                    if (this.HubConnection is not null && this.HubConnectionIsConnected)
                    {
                        await this.HubConnection.SendAsync("SendNewPlayerInGame", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(playerToResume.StartPlayedDate.ToLocalTime()), this.ConvertPlayerToMonitorApiResult(playerToResume));
                    }
                }
            }
        }

        public async Task<IEnumerable<FieldTripMadeHeaderModel>> GetAllFieldTripsMadeHeadersAsync()
        {
            IEnumerable<FieldTripMadeHeaderModel> fieldTripsMade;

            await this.ConnectivityService.RefreshStateAsync();

            if (this.ConnectivityService.IsOnline)
            {
                fieldTripsMade = await this.gamesService.GetAllFieldTripsMadeHeadersAsync();
            }
            else
            {
                var currentUser = this.UsersRepository.CurrentUser;

                var games = (await this.GamesManager.SelectAll())
                    .Where(g => g.Players.Any(p => g.EndDate.HasValue && p.UserId == currentUser.Id))
                    .OrderByDescending(g => g.EndDate.Value);

                var savedFieldTrips = this.FieldTripsRepository.SavedFieldTrips.Select(sft => sft.Value);

                List<FieldTripMadeHeaderModel> fieldTripsMadeHeaderModels = new List<FieldTripMadeHeaderModel>();

                int fieldTripNumber = 0;

                foreach (var game in games)
                {
                    var savedFieldTrip = savedFieldTrips.FirstOrDefault(f => f.Id == game.FielTripId);

                    var player = game.Players.FirstOrDefault(p => p.UserId == currentUser.Id);

                    fieldTripNumber++;

                    if (savedFieldTrip != null)
                    {
                        foreach (var playedUnit in player.PlayedUnits)
                        {
                            playedUnit.Unit = savedFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == playedUnit.UnitId);

                            if (playedUnit.PlayedActivities != null && playedUnit.PlayedActivities.Any())
                            {
                                foreach (var playedActivity in playedUnit.PlayedActivities)
                                {
                                    playedActivity.Activity = savedFieldTrip.LocatedGameUnits.SelectMany(u => u.Activities).FirstOrDefault(a => a.Id == playedActivity.ActivityId);
                                }
                            }
                        }

                        FieldTripMadeHeaderModel fieldTripMadeHeaderModel = new FieldTripMadeHeaderModel 
                        {
                            GameId = game.Id,
                            FieldTrip = savedFieldTrip,
                            PlayedUnits = player.PlayedUnits
                                .Select(pu => pu.Unit)
                                .ToList(),
                            PlayedReadingActivities = player.PlayedUnits
                                .Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
                                .SelectMany(pu => pu.PlayedActivities)
                                .Where(pa => pa.Activity.ActivityType == ActivityType.Statement && pa.Latitude.HasValue && pa.Longitude.HasValue)
                                .OrderBy(pa => pa.AnswerDate)
                                .ToList(),
                            TotalPoints = player.PlayedUnits
                                .Sum(pu => pu.LocationPoints)
                                +
                                player.PlayedUnits
                                    .Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
                                    .SelectMany(pu => pu.PlayedActivities)
                                    .Sum(pa => pa.Points ?? 0),
                            RealizedTime = (int)(player.EndGameDate.Value - player.StartPlayedDate).TotalMinutes
                        };

                        fieldTripsMadeHeaderModels.Add(fieldTripMadeHeaderModel);
                    }
                    else
                    {
                        FieldTripMadeHeaderModel fieldTripMadeHeaderModel = new FieldTripMadeHeaderModel
                        {
                            GameId = game.Id,
                            FieldTrip = new FieldTrip { Name = game.FieldTripName ?? $"Sortie n°{fieldTripNumber}" },
                            PlayedUnits = new(),
                            PlayedReadingActivities = player.PlayedUnits
                                .Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
                                .SelectMany(pu => pu.PlayedActivities)
                                .Where(pa => pa.Latitude.HasValue && pa.Longitude.HasValue)
                                .OrderBy(pa => pa.AnswerDate).ToList(),
                            TotalPoints = player.PlayedUnits
                                .Sum(pu => pu.LocationPoints)
                                +
                                player.PlayedUnits
                                    .Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
                                    .SelectMany(pu => pu.PlayedActivities)
                                    .Sum(pa => pa.Points ?? 0),
                            RealizedTime = (int)(player.EndGameDate.Value - player.StartPlayedDate).TotalMinutes
                        };

                        fieldTripsMadeHeaderModels.Add(fieldTripMadeHeaderModel);
                    }
                }

                fieldTripsMade = fieldTripsMadeHeaderModels;
            }

            return fieldTripsMade;
        }

        public async Task StopGameAsync()
        {
            Game game = this.CurrentGame;

            Entities.Player player = game.Players.FirstOrDefault(p => p.Id == this.CurrentPlayer.Id);

            game.EndDate = DateTime.UtcNow;

            player.CurrentUnit = null;
            player.CurrentUnitId = null;
            player.CurrentUnitReachedDate = null;
            player.EndGameDate = game.EndDate;
            player.PlayerState = Common.PlayerState.Complete;
            await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);

            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, c => c.Id);

            this.CurrentPlayer = player;
            this.CurrentGame = game;
        }

        //public async Task ShareReadingAsync(Guid readingId)
        //{
        //    await this.gamesService.ShareReadingAsync(readingId);
        //}

        public async Task ShareReportAsync(Guid gameId, string email = null)
        {
            await this.gamesService.ShareReportAsync(gameId, email);
        }

        public async Task CancelAllGamesAsync()
        {
            //foreach (var game in this.CachedGames.Where(g => g.Value.Players.Any(p => p.PlayerState == PlayerState.Playing)))
            //{
            //bool changed = false;
            //foreach (var player in game.Value.Players.Where(p => p.PlayerState == PlayerState.Playing))
            //{
            //    player.PlayerState = Common.PlayerState.Cancelled;
            //    changed = true;
            //}

            //if (changed)
            //{
            //    await this.GamesManager.CreateOrUpdateById(game.Value.Id.ToString(), game.Value);
            //}
            //}

            var games = await this.GamesManager.SelectAll();
            List<Game> changedGames = new();
            foreach (var game in games.Where(g => g.Players.Any(p => p.PlayerState == PlayerState.Playing)))
            {
                bool changed = false;
                foreach (var player in game.Players.Where(p => p.PlayerState == PlayerState.Playing))
                {
                    player.PlayerState = Common.PlayerState.Cancelled;
                    changed = true;
                }

                if (changed)
                {
                    //TODO: faire ça en bulk
                    changedGames.Add(game);

                    //await this.GamesManager.CreateOrUpdateById(game.Id.ToString(), game);
                }
            }
            if(changedGames.Any())
            {
                await this.GamesManager.BulkCreateOrUpdateById(changedGames.ToDictionary(g => g.Id, g => g));
            }
        }

        public async Task<bool> CheckGeolocationIsAvailableAsync()
        {
            var result = await this.GeoLocationService.GetCurrentGeoLocationAsync();
            return result.Error == null;
        }

        private void CheckProximityToANewGameUnit(GeoLocationCoordinates coords)
        {
            List<PlayedUnit> unitsAlreadyPlayed = this.CurrentPlayer.PlayedUnits?.ToList() ?? new();
            List<Guid> unitAlreadyPlayedIds = unitsAlreadyPlayed.Select(p => p.UnitId).ToList();
            Journey currentJourney = this.CurrentFieldTrip.Journeys.FirstOrDefault(j => j.Id == this.CurrentPlayer.JourneyId);
            List<LocatedGameUnit> remainingLocatedGameUnits = currentJourney.JourneyLocatedGameUnits
                .Where(jlgu => !unitAlreadyPlayedIds.Contains(jlgu.LocatedGameUnitId))
                .Select(jlgu => jlgu.LocatedGameUnit)
                .ToList();

            List<LocatedGameUnit> nearbyDetectedUnits = new();

            foreach (var gameUnit in remainingLocatedGameUnits)
            {
                var distance = this.CalculateDistanceBetweenTwoPoints(coords.Latitude, coords.Longitude, gameUnit.Latitude, gameUnit.Longitude);

                if (distance <= 100)
                {
                    nearbyDetectedUnits.Add(gameUnit);
                }
            }

            if (nearbyDetectedUnits.Any())
            {
                this.OnNearbyUnitDetected?.Invoke(this, nearbyDetectedUnits);
            }
        }

        public int CalculateDistanceBetweenTwoPoints(decimal latP1, decimal lonP1, decimal latP2, decimal lonP2)
        {
            var lat1 = (double)latP1;
            var lon1 = (double)lonP1;
            var lat2 = (double)latP2;
            var lon2 = (double)lonP2;

            var earthRadiusKm = 6371;

            var dLat = (lat2 - lat1) * Math.PI / 180;
            var dLon = (lon2 - lon1) * Math.PI / 180;

            lat1 = (lat1) * Math.PI / 180;
            lat2 = (lat2) * Math.PI / 180;

            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            var distance = (int)(earthRadiusKm * c * 1000);

            return distance;
        }

        public async Task StartUnitAsync(LocatedGameUnit unit)
        {
            //this.CurrentPlayer.CurrentUnit = unit;
            this.CurrentPlayer.CurrentUnitId = unit?.Id;
            this.CurrentPlayer.CurrentUnitReachedDate = null;
            this.CurrentPlayer.StartCurrentUnitDate = DateTime.UtcNow;

            if (this.CurrentPlayer.PlayedUnits == null)
            {
                this.CurrentPlayer.PlayedUnits = new List<PlayedUnit>();
            }

            this.CurrentPlayer.PlayedUnits.Add(
                new PlayedUnit
                {
                    Id = Guid.NewGuid(),
                    PlayerId = this.CurrentPlayer.Id,
                    Player = this.CurrentPlayer,
                    UnitId = unit.Id,
                    //Unit = unit
                }
            );
            await this.GamesManager.CreateOrUpdateById(this.CurrentGame.Id.ToString(), this.CurrentGame);

            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);

            if (!this.IsOnDiscoveryMode && this.HubConnection is not null && this.HubConnectionIsConnected)
            {
                var playedUnitsNumber = this.CurrentPlayer.PlayedUnits.Count - 1;

                await this.HubConnection.SendAsync("SendStartNewUnit", this.CurrentFieldTrip.Id, TimeOnly.FromDateTime(this.CurrentPlayer.StartPlayedDate.ToLocalTime()), this.CurrentPlayer.Id, playedUnitsNumber, unit.Name);
            }
        }

        public async Task<bool> EndUnitAsync()
        {
            this.CurrentPlayer.CurrentUnit = null;
            this.CurrentPlayer.CurrentUnitId = null;
            this.CurrentPlayer.CurrentUnitReachedDate = null;
            this.CurrentPlayer.StartCurrentUnitDate = null;
            this.CurrentPlayer.CurrentUnitActivationHasBeenUnlocked = false;

            //on vérifie s'il reste d'autres unités de jeu à faire
            List<PlayedUnit> unitsAlreadyPlayed = this.CurrentPlayer.PlayedUnits?.ToList() ?? new();
            List<Guid> unitAlreadyPlayedIds = unitsAlreadyPlayed.Select(p => p.UnitId).ToList();
            Journey currentJourney = this.CurrentFieldTrip.Journeys.FirstOrDefault(j => j.Id == this.CurrentPlayer.JourneyId);

            List<LocatedGameUnit> remainingLocatedGameUnits = currentJourney.JourneyLocatedGameUnits
                .Where(jlgu => !unitAlreadyPlayedIds.Contains(jlgu.LocatedGameUnitId))
                .Select(jlgu => jlgu.LocatedGameUnit)
                .ToList();

            bool gameOver = !remainingLocatedGameUnits.Any();

            if (gameOver)
            {
                // si plus d'unité c'est que la partie du joueur est finie
                Game game = this.CurrentGame;
                game.EndDate = DateTime.UtcNow;

                this.CurrentPlayer.EndGameDate = game.EndDate;
                this.CurrentPlayer.PlayerState = Common.PlayerState.Complete;
            }
            await this.GamesManager.CreateOrUpdateById(this.CurrentGame.Id.ToString(), this.CurrentGame);

            //await this.StoreCacheAsync(GamesKeyCacheName, this.CachedGames, g => g.Id);

            return gameOver;
        }

        public async Task DeleteElapsedTimeAsync(Guid fieldTripId)
        {
            await this.LocalStorageService.RemoveItemAsync($"elapsed-time-{fieldTripId}");
        }

        public async Task StoreElapsedTimeAsync(Guid fieldTripId, TimeSpan time)
        {
            await this.LocalStorageService.SetItemAsync($"elapsed-time-{fieldTripId}", time);
        }

        public async Task<TimeSpan> GetElapsedTimeAsync(Guid fieldTripId)
        {
            string key = $"elapsed-time-{fieldTripId}";
            return await this.LocalStorageService.GetItemAsync<TimeSpan>(key);
        }

        private async Task<DateTime> GetLastUpdateDateAsync()
        {
            var dateAsString = await this.LocalStorageService.GetItemAsync<string>(LastUpdateDateKey);
            if (dateAsString != null)
            {
                return DateTime.Parse(dateAsString, null, DateTimeStyles.RoundtripKind);
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        private async Task UpdateLastUpdateDateAsync(DateTime dateTime)
        {
            await this.LocalStorageService.SetItemAsync<string>(LastUpdateDateKey, dateTime.ToString("O"));
        }

        public async Task InitializeIndexedDbForDiscoveryModeAsync()
        {
            await this.GamesManager.InitializeAsync();
        }

        public async Task InitializeDiscoveryModeAsync()
        {
            await this.RemoveCacheAsync();

            await this.UsersRepository.LogoutAsync();

            this.IsOnDiscoveryMode = true;

            this.CurrentGame = null;
            this.CurrentPlayer = null;

            this.GeoLocationService.OnCurrentPositionChanged += this.GeoLocationService_OnCurrentPositionChanged;
            await this.GeoLocationService.WatchCurrentPositionAsync(new GeoLocationOptions() { EnableHighAccuracy = true });
        }

        public async Task RemoveCacheAsync()
        {
            var games = await this.GamesManager.SelectAll();

            foreach (var game in games)
            {
                await this.GamesManager.DeleteById(game.Id.ToString());
            }
        }

        public async Task ExitDiscoveryModeAsync()
        {
            await this.RemoveCacheAsync();
            this.GeoLocationService.OnCurrentPositionChanged -= this.GeoLocationService_OnCurrentPositionChanged;
            this.IsOnDiscoveryMode = false;

            this.CurrentGame = null;
            this.CurrentPlayer = null;
        }

        public async Task<Game[]> GetAllGamesFromCacheAsync()
        {
            return await this.GamesManager.SelectAll();
        }

        public async Task DeleteMyGamesAsync(string userId)
        {
            try
            {
                var result = await this.gamesService.DeleteMyGamesAsync(userId);

                if (!result.Success)
                {
                    throw new BusinessException(result.Errors);
                }

                await this.RemoveCacheAsync();
            }
            catch (Exception e)
            {
                throw new BusinessException(e.Message);
            }
        }
    }
}
