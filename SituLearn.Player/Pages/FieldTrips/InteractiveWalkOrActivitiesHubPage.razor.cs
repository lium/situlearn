// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Entities;
using Microsoft.AspNetCore.Components;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using Ozytis.Common.Core.Web.Razor.Utilities.GeoLocation;
using SituLearn.Player.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Player.Pages.FieldTrips
{
    [Route(Url)]
    [Route(ResumeUrl)]
    public partial class InteractiveWalkOrActivitiesHubPage : ComponentBase, IDisposable
    {
        public const string Url = "/balade-interactive";
        public const string ResumeUrl = "/balade-interactive/reprendre";

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [CascadingParameter]
        public AppLayout Confirm { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [CascadingParameter]
        public AppLayout Layout { get; set; }

        public bool TestEnv => this.Layout.TestEnv;

        public Journey Journey { get; set; }

        public FieldTrip FieldTrip { get; set; }

        public bool DisplayNearbyUnitDetectedAlert { get; set; }

        public List<LocatedGameUnit> DetectedUnits { get; set; }

        public int TotalPoints { get; set; }

        public TimeSpan ElapsedTime { get; set; }

        public System.Timers.Timer ElapsedTimeTimer { get; set; }

        public bool DisplayTimeAlert { get; set; }

        public bool HaveRefusedToStartUnit { get; set; }

        public bool IsStartingUnit { get; set; }

        public bool DisplayOutOfExplorationAreaAlert { get; set; }

        protected override async Task OnInitializedAsync()
        {
            if (this.GamesRepository.CurrentPlayer == null)
            {
                this.Notifier.Notify(Color.Info, "Pas de partie en cours");
                this.NavigationManager.NavigateTo(FieldTripsPage.Url);
                return;
            }

            this.GamesRepository.CurrentPlayer.PlayerState = Common.PlayerState.Playing;

            this.FieldTrip = this.GamesRepository.CurrentFieldTrip;
            this.Journey = this.FieldTrip.Journeys.FirstOrDefault(j => j.Id == this.GamesRepository.CurrentPlayer.JourneyId);


			try
			{
                var elapsedTime = await this.GamesRepository.GetElapsedTimeAsync(this.GamesRepository.CurrentPlayer.Game.FielTripId);

                if (elapsedTime == default)
                {
                    this.ElapsedTime = TimeSpan.FromMinutes(0);
                }
                else
                {
                    this.ElapsedTime = elapsedTime;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.GetType().Name);
                Console.WriteLine(e.Message);
                this.ElapsedTime = TimeSpan.FromMinutes(0);
            }

            if (this.FieldTrip.FieldTripType == Common.FieldTripType.InteractiveWalk)
            {
                this.GamesRepository.OnNearbyUnitDetected += this.GamesRepository_OnNearbyUnitDetected;
            }            

            if (this.GamesRepository.CurrentPlayer.PlayedUnits != null)
            {
                this.TotalPoints = this.GamesRepository.CalculatePlayerTotalPoints();
                this.StateHasChanged();
            }

            this.ElapsedTimeTimer = new System.Timers.Timer(60000);
            this.ElapsedTimeTimer.Elapsed += async (sender, e) => await this.ElapsedTimeTimer_OnTimeElapsed(sender, e);

            this.ElapsedTimeTimer.AutoReset = true;
            this.ElapsedTimeTimer.Enabled = true;

            this.GamesRepository.OnExplorationAreaLeft += this.OnExplorationAreaLeft;

            await base.OnInitializedAsync();
        }

        public void Dispose()
        {
            if (this.FieldTrip.FieldTripType == Common.FieldTripType.InteractiveWalk)
            {
                this.GamesRepository.OnNearbyUnitDetected -= this.GamesRepository_OnNearbyUnitDetected;
            }
                
            this.ElapsedTimeTimer.Elapsed -= async (sender, e) => await this.ElapsedTimeTimer_OnTimeElapsed(sender, e);
            this.ElapsedTimeTimer.Enabled = false;

            this.GamesRepository.OnExplorationAreaLeft -= this.OnExplorationAreaLeft;
        }

        private void GamesRepository_OnNearbyUnitDetected(object sender, List<LocatedGameUnit> e)
        {
            if (!this.HaveRefusedToStartUnit)
            {
                this.DisplayNearbyUnitDetectedAlert = true;
            }

            this.DetectedUnits = e;

            this.StateHasChanged();
        }

        private async Task PlayUnitAsync(LocatedGameUnit unit)
        {
            if (this.IsStartingUnit)
            {
                return;
            }

            this.IsStartingUnit = true;
            this.StateHasChanged();

            await this.GamesRepository.StartUnitAsync(unit);
            this.NavigationManager.NavigateTo(LocatedGameUnitPage.GetUrl(unit.Id));

            this.IsStartingUnit = false;
            this.StateHasChanged();
        }

        private async Task ElapsedTimeTimer_OnTimeElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.ElapsedTime = this.ElapsedTime + TimeSpan.FromMinutes(1);
            await this.GamesRepository.StoreElapsedTimeAsync(this.GamesRepository.CurrentPlayer.Game.FielTripId, this.ElapsedTime);

            if ((int)this.ElapsedTime.TotalMinutes == this.FieldTrip.AllottedTimeDuration - 1)
            {
                this.ElapsedTimeTimer.AutoReset = false;
            }


            if (this.FieldTrip.TimeIsLimited)
            {
                var remainingTime = this.FieldTrip.AllottedTimeDuration - this.ElapsedTime.TotalMinutes;

                switch (remainingTime)
                {
                    case < 1 and >= 0:
                        this.DisplayTimeAlert = true;
                        this.ElapsedTimeTimer.Enabled = false;
                        await this.GamesRepository.StopGameAsync();
                        break;
                    case < 6 and >= 5:
                    case < 11 and >= 10:
                        this.DisplayTimeAlert = true;
                        break;

                    default:
                        this.DisplayTimeAlert = false;
                        break;
                }
            }

            this.StateHasChanged();

            await Task.CompletedTask;
        }

        protected void CloseNearbyUnitDetectedAlert()
        {
            this.DisplayNearbyUnitDetectedAlert = false;
            this.HaveRefusedToStartUnit = true;
            this.StateHasChanged();
        }

        protected void CloseDisplayTimeAlert(int remainingTime)
        {
            this.DisplayTimeAlert = false;
            this.StateHasChanged();

            if (remainingTime == 0)
            {
                this.NavigationManager.NavigateTo(EndGamePage.GetUrl(this.GamesRepository.CurrentPlayer.Id));
            }
        }

        private void OnExplorationAreaLeft(object sender, GeoLocationCoordinates e)
        {
            this.DisplayOutOfExplorationAreaAlert = true;
            this.StateHasChanged();
        }
    }
}