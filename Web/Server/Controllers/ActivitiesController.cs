﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Api.Activity;
using Business;
using Common;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Server.Controllers
{
	[Authorize]
	[Route("api/activities")]
	public class ActivitiesController : ControllerBase
	{
		public ActivitiesController(ActivitiesManager activitiesManager, UsersManager usersManager)
		{
			this.ActivitiesManager = activitiesManager;
			this.UsersManager = usersManager;
		}

		public ActivitiesManager ActivitiesManager { get; }
		public UsersManager UsersManager { get; }

		[HttpGet()]
		[WebApiQueryable2(nameof(LocatedActivity))]
		public async Task<IEnumerable<LocatedActivity>> GetAllLocatedActivitiesAsync()
		{
			return await this.ActivitiesManager.SelectAll()
				.Where(a => !a.IsDeleted)
				.Include(a => a.LocatedGameUnit)
					.ThenInclude(lgu => lgu.FieldTrip.Designer)
				.ApplyQueryV2Options(this.Request)
				.ToArrayAsync();
		}


		[HttpGet("{activityId:guid}")]
		public async Task<LocatedActivity> GetActivity(Guid activityId)
		{
			return await this.ActivitiesManager.GetActivity(activityId);
		}

		[HttpGet("question/{activityId:guid}")]
		public QuestionActivity GetQuestion(Guid activityId)
		{
			return this.ActivitiesManager.GetQuestion(activityId);
		}

		[HttpGet("statement/{activityId:guid}")]
		public ReadingActivity GetStatement(Guid activityId)
		{
			return this.ActivitiesManager.GetStatement(activityId);
		}

		[HttpPost("question")]
		[ValidateModel]
		[HandleBusinessException]
		public async Task<LocatedActivity> CreateQuestionAsync([FromBody] QuestionCreateModel model, Guid fieldTripId)
		{
			return await this.ActivitiesManager.CreateQuestionAsync(model, fieldTripId);
		}

		[HttpPut("question")]
		[ValidateModel]
		[HandleBusinessException]
		public async Task<LocatedActivity> UpdateQuestionAsync([FromBody] QuestionUpdateModel model)
		{
			return await this.ActivitiesManager.UpdateQuestionAsync(model);
		}

		[HttpPost("statement")]
		[ValidateModel]
		[HandleBusinessException]
		public async Task<LocatedActivity> CreateStatementAsync([FromBody] StatementCreateModel model, Guid fieldTripId)
		{
			return await this.ActivitiesManager.CreateStatementAsync(model, fieldTripId);
		}

		[HttpPut("statement")]
		[ValidateModel]
		[HandleBusinessException]
		public async Task<LocatedActivity> UpdateStatementAsync([FromBody] StatementUpdateModel model)
		{
			return await this.ActivitiesManager.UpdateStatementAsync(model);
		}

		[HttpDelete("{activityId:guid}")]
		[HandleBusinessException]
		public async Task RemoveActivity(Guid activityId)
		{
			await this.ActivitiesManager.SoftDeleteAsync(activityId);
		}

		[HttpPut("duplicate/{locatedActivityId:guid}/{locatedGameUnitId:guid}")]
		[ValidateModel, HandleBusinessException]
		public async Task<LocatedActivity> DuplicateActivityAsync(Guid locatedActivityId, Guid locatedGameUnitId)
		{
			var originalActivity = await this.ActivitiesManager.SelectAsync(locatedActivityId, a => a.LocatedGameUnit.FieldTrip);

			if (originalActivity == null)
			{
				throw new BusinessException("Activité inconnue");
			}

			return await this.ActivitiesManager.DuplicateLocatedActivityAsync(locatedActivityId, locatedGameUnitId);
		}

		[HttpPut("{fieldTripId:guid}/order")]
		[ValidateModel, HandleBusinessException]
		public async Task UpdateLocatedActivityOrderAsync(Guid fieldTripId, [FromBody] DraggableObjectOrderUpdateModel model)
		{
			ApplicationUser updater = await this.UsersManager
						   .SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
						   .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.ActivitiesManager.UpdateLocatedActivityOrderAsync(fieldTripId, model.LinkId, model.NewOrder, updater);
		}


		[HttpPost("addAnswer/{questionId:guid}/{isCorrect:bool}")]
		[HandleBusinessException]
		public async Task<Guid> AddAnswerToQuestionActivity(Guid questionId, bool isCorrect, [FromBody] string value)
		{
			ApplicationUser updater = await this.UsersManager
				.SelectAllUsers()
					.Include(u => u.UserRoles)
						.ThenInclude(r => r.Role)
				.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			var question = this.ActivitiesManager.GetQuestionWithFieldTrip(questionId);

			if (updater == null
				|| (updater.UserRoles.Count == 1 && updater.UserRoles.First().Role.Name == UserRoleNames.Participant)
				|| (updater.UserRoles.Count == 1 && updater.UserRoles.First().Role.Name == UserRoleNames.Designer && question.LocatedGameUnit.FieldTrip.DesignerId != updater.Id)
				)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			var newAnswer = new Answer
			{
				IsCorrectAnswer = isCorrect,
				Content = value,
				QuestionActivityId = questionId,
				Id = Guid.NewGuid()
			};

			await this.ActivitiesManager.AddAnswer(question, newAnswer);

			return newAnswer.Id;
		}

		[HttpPost("updateAnswer/{answerId:guid}/{isCorrect:bool}")]
		[HandleBusinessException]
		public async Task UpdateAnswer(Guid answerId, bool isCorrect, [FromBody] string value)
		{
			ApplicationUser updater = await this.UsersManager
				.SelectAllUsers()
					.Include(u => u.UserRoles)
						.ThenInclude(r => r.Role)
				.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			var answer = await this.ActivitiesManager.GetAnswer(answerId);

			if (updater == null
				|| (updater.UserRoles.Count == 1 && updater.UserRoles.First().Role.Name == UserRoleNames.Participant)
				|| (updater.UserRoles.Count == 1 && updater.UserRoles.First().Role.Name == UserRoleNames.Designer && answer.QuestionActivity.LocatedGameUnit.FieldTrip.DesignerId != updater.Id)
				)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.ActivitiesManager.UpdateAnswer(answer, isCorrect, value);
		}

		[HttpDelete("updateAnswer/{answerId:guid}")]
		[HandleBusinessException]
		public async Task RemoveAnswer(Guid answerId)
		{
			ApplicationUser updater = await this.UsersManager
				.SelectAllUsers()
					.Include(u => u.UserRoles)
						.ThenInclude(r => r.Role)
				.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			var answer = await this.ActivitiesManager.GetAnswer(answerId);

			if (updater == null
				|| (updater.UserRoles.Count == 1 && updater.UserRoles.First().Role.Name == UserRoleNames.Participant)
				|| (updater.UserRoles.Count == 1 && updater.UserRoles.First().Role.Name == UserRoleNames.Designer && answer.QuestionActivity.LocatedGameUnit.FieldTrip.DesignerId != updater.Id)
				)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.ActivitiesManager.DeleteAnswer(answer);
		}
	}
}
