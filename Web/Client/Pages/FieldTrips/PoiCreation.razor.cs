// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Common;
using Entities;
using Ozytis.Common.Core.Utilities;
using System;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.FieldTrips
{
    public partial class PoiCreation : ComponentBase
    {
        [Parameter]
        public FieldTrip FieldTrip { get; set; }

        [Parameter]
        public decimal Latitude { get; set; }

        [Parameter]
        public decimal Longitude { get; set; }

        [Parameter]
        public EventCallback<LocatedGameUnit> OnCreated { get; set; }

        [Parameter]
        public EventCallback OnCancel { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [Parameter]
        public bool MapIsGeolocalized { get; set; }

        public string[] Errors { get; set; }

        public bool IsProcessing { get; set; }

        public LocatedUnitCreationModel Model { get; set; } = new();

        protected override void OnInitialized()
        {
            this.Model.FieldTripId = this.FieldTrip.Id;
            this.Model.Latitude = this.Latitude;
            this.Model.Longitude = this.Longitude;
            this.Model.IsGeolocatable = this.FieldTrip.ExplorationMap.IsGeolocatable;

			this.StateHasChanged();
        }

        [Parameter]
        public Func<ActivationAreaType, LocatedGameUnit, Task> SetDefaultGeometryMethod { get; set; }

        protected async Task ProcessAsync()
        {
            if (this.IsProcessing)
            {
                return;
            }

            this.IsProcessing = true;
            this.StateHasChanged();

            try
            {                
                var unit = await this.FieldTripsRepository.AddLocatedUnitAsync(this.Model);
                await this.SetDefaultGeometryMethod(ActivationAreaType.Circle, unit);
                await this.FieldTripsRepository.UpdateLocatedUnitInfoAsync(new LocatedUnitUpdateInfoModel
                {
                    ActivationArea = unit.ActivationArea,
                    ActivationAreaCenterLatitude = unit.ActivationAreaCenterLatitude,
                    ActivationAreaCenterLongitude = unit.ActivationAreaCenterLongitude,
                    ActivationAreaType = unit.ActivationAreaType,
                    ActivationRadius = unit.ActivationRadius,
                    Id = unit.Id,
                    FieldTripId = unit.FieldTripId,
                    Latitude = unit.Latitude,
                    Longitude = unit.Longitude,
                    Name = unit.Name
                });
                await this.OnCreated.InvokeAsync(unit);
            }
            catch (BusinessException ex)
            {
                this.Notifier.Notify(Color.Danger, ex.Message ?? String.Join(", ", ex.Messages));
                this.Errors = ex.Messages;
            }

            this.IsProcessing = false;
            this.StateHasChanged();
        }
    }
}