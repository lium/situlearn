﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.WebUtilities;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.User
{
    [Route(ConfirmEmailChangedPage.Url)]
    public partial class ConfirmEmailChangedPage : ComponentBase
    {
        public const string Url = "/confirm-email-change";

        
        public string Token { get; set; }

        
        public string Email { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        public bool? IsSuccess { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var uri = this.NavigationManager.ToAbsoluteUri(NavigationManager.Uri);
            var queryParameters = QueryHelpers.ParseQuery(uri.Query);

            if (queryParameters.TryGetValue("email", out var email))
            {
                this.Email = email;
            }

            if (queryParameters.TryGetValue("token", out var token))
            {
                this.Token = token;
            }

            try
            {
                await this.UsersRepository.ConfirmEmailChangeAsync(new EmailUpdateConfirmationModel { Email = this.Email, Token = this.Token });
                await this.UsersRepository.LogoutAsync();
                this.IsSuccess = true;                
            }
            catch
            {
                this.IsSuccess = false;
            }

            this.StateHasChanged();
        }
    }
}
