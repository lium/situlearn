﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using ClientApi;
using Entities;
using Newtonsoft.Json;
using Ozytis.Common.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Web.Client.Repositories
{
    public class LocatedGameUnitsRepository
    {
        private readonly LocatedGameUnitsService locatedGameUnitsService = new();

        public async Task<IEnumerable<LocatedGameUnit>> GetAllLocatedGameUnitsAsync(Expression<Func<IQueryable<LocatedGameUnit>, IQueryable<LocatedGameUnit>>> query)
        {
            return await this.locatedGameUnitsService.GetAllLocatedGameUnitsAsync(query);
        }

        public async Task<LocatedGameUnit> GetLocatedGameUnitAsync(Guid locatedGameUnitId)
        {
            return await this.locatedGameUnitsService.GetLocatedGameUnitAsync(locatedGameUnitId);
        }

        public async Task<LocatedGameUnit> DuplicateLocatedGameUnitAsync(Guid locatedGameUnit, Guid FieldTripId)
        {
            var result = await this.locatedGameUnitsService.DuplicateLocatedGameUnitAsync(locatedGameUnit, FieldTripId);

            if (!result.Success)
            {
                throw new BusinessException("Une erreur est survenue. " + string.Join(" / ", result.Errors));
            }

            return result.Data;
        }

        public async Task DeleteAsync(Guid id)
        {
            await this.locatedGameUnitsService.DeleteGameUnitAsync(id);
        }

        public async Task<LocatedGameUnit> UpdateLocationGameUnitParams(Guid locationGameUnitId, LocatedGameUnitUpdateModel model)
        {
            var result = await this.locatedGameUnitsService.UpdateLocatedGameUnitAsync(locationGameUnitId, model);

            if (result.Success)
            {
                return result.Data;
            }
            else if (result.Errors.Any())
            {
                throw new BusinessException(result.Errors);
            }
            else
            {
                Console.WriteLine("une erreur est survenue...");
                Console.WriteLine(JsonConvert.SerializeObject(result));
                throw new BusinessException("une erreur est survenur");
            }
        }

        public async Task ToggleActivityUsage(Guid locatedGameUnitId, bool activitiesEnabled)
        {
            await this.locatedGameUnitsService.EnableActivitiesAsync(locatedGameUnitId, activitiesEnabled);
        }
    }
}
