// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;

using Entities;

using Microsoft.AspNetCore.Components;

using Ozytis.Common.Core.Web.Razor;
using Ozytis.Common.Core.Web.Razor.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Web.Client.Repositories;
using Web.Client.Utilities;

using FontAwesomeIcons = Ozytis.Common.Core.Web.Razor.FontAwesomeIcons;

namespace Web.Client.Pages.FieldTrips
{
    [Route(Url)]
    [Route(IndexUrl)]
    public partial class FieldTripsPage : ComponentBase
    {
        public const string IndexUrl = "/";
        public const string Url = "/fieldtrips";

        public static readonly string Icon = FontAwesomeIcons.MapSigns;

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        public IEnumerable<FilterOption> EstablishmentsFilterOptions { get; set; } = new List<FilterOption>();

        public IEnumerable<FilterOption> DesignersFilterOptions { get; set; } = new List<FilterOption>();

        public IEnumerable<FilterOption> PedagogicFieldsFilterOptions { get; set; } = new List<FilterOption>();

        public IEnumerable<FilterOption> FieldtripsFilterOptions { get; set; } = new List<FilterOption>();

        public IEnumerable<FilterOption> TypesFilterOptions { get; set; } = new List<FilterOption>();

        public bool IsAdmin { get; private set; }

        public OzSimpleRemoteTable<FieldTrip, FieldTrip> Table { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await this.InitFiltersAsync();

            this.IsAdmin = this.UsersRepository.CurrentUser?.Role == UserRoleNames.Administrator;
        }

        protected async Task RemoveTripAync(FieldTrip fieldTrip)
        {
            await this.Table.RemoveEntityAsync(fieldTrip);
            await this.InitFiltersAsync();
        }
        protected async Task UpdateTripAsync(FieldTrip fieldTrip)
        {
            await this.Table.UpdateEntityAsync(fieldTrip);
            await this.InitFiltersAsync();
        }

        public bool InitiatedFilters { get; set; } = false;
        private async Task InitFiltersAsync()
        {
            this.FieldtripsFilterOptions = (await this.FieldTripsRepository.GetFieldTripIdsAndNamesAsync())
                .OrderBy(x => x.Value)
                .Select(x => new FilterOption { Label = x.Value, Value = x.Value })
                .Append(new FilterOption { Label = "----- TOUS -----", Value = null });

            this.EstablishmentsFilterOptions = (await this.FieldTripsRepository.SelectAllEstablishmentsAsync())
                .OrderBy(e => e)
                .Select(e => new FilterOption { Label = e, Value = e })
                .Append(new FilterOption { Label = "----- TOUS -----", Value = null })
                .ToArray();

            this.DesignersFilterOptions = (await this.FieldTripsRepository.SelectAllDesignersAsync())
                .OrderBy(d => $"{d.FirstName} {d.LastName}")
                .Select(d => new FilterOption { Label = d.FirstName + " " + d.LastName, Value = d.Id })
                .Append(new FilterOption { Label = "----- TOUS -----", Value = null })
                .ToArray();

            this.PedagogicFieldsFilterOptions = (await this.FieldTripsRepository.SelectAllPedagogicFieldsAsync())
                .OrderBy(e => e)
                .Select(d => new FilterOption { Label = d, Value = d })
                .Append(new FilterOption { Label = "----- TOUS -----", Value = null })
                .ToArray();

            this.TypesFilterOptions = (await this.FieldTripsRepository.SelectAllTypesAsync())
                .OrderBy(d => d.GetFriendlyName())
                .Select(d => new FilterOption { Label = d.GetFriendlyName(), Value = (int)d })
                .Append(new FilterOption { Label = "----- TOUS -----", Value = null })
                .ToArray();

            this.InitiatedFilters = true;
        }

        protected async Task CreateTripAsync()
        {
            this.NavigationManager.NavigateTo(FieldTripEditionPage.CreationUrl);
            await Task.CompletedTask;
        }
    }
}