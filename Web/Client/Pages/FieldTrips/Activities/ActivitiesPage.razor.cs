﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.FieldTrips.Activities
{
    [Route(ActivitiesPage.Url)]
    public partial class ActivitiesPage : ComponentBase
    {
        public const string Url = "/activities/{fieldTripId:guid}/{locatedGameUnitId:guid}";

        public static string GetUrl(Guid fieldTripId, Guid locatedGameUnitId) => Url
            .Replace("{fieldTripId:guid}", $"{fieldTripId}")
            .Replace("{locatedGameUnitId:guid}", $"{locatedGameUnitId}");

        [Parameter]
        public Guid FieldTripId { get; set; }
        public FieldTrip FieldTrip { get; set; }

        [Parameter]
        public Guid LocatedGameUnitId { get; set; }
        public LocatedGameUnit LocatedGameUnit { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public ActivitiesRepository ActivitiesRepository { get; set; }

        [Inject]
        public LocatedGameUnitsRepository LocatedGameUnitsRepository { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }
        public bool CanEdit => this.UsersRepository.CurrentUser.Role == UserRoleNames.Administrator || (this.FieldTrip?.DesignerId == this.UsersRepository.CurrentUser.Id);
        public bool IsAdminOrOwner => this.UsersRepository.CurrentUser.Role != UserRoleNames.Participant;

        public List<LocatedActivity> Activites { get; set; }

        public OzSimpleRemoteTable<LocatedActivity, LocatedActivity> Table { get; set; }

        public IEnumerable<FilterOption> EstablishmentsFilterOptions { get; set; } = new List<FilterOption>();

        public IEnumerable<FilterOption> DesignersFilterOptions { get; set; } = new List<FilterOption>();

        public IEnumerable<FilterOption> PedagogicFieldsFilterOptions { get; set; } = new List<FilterOption>();

        public IEnumerable<FilterOption> FieldtripsFilterOptions { get; set; } = new List<FilterOption>();

        protected override async Task OnInitializedAsync()
        {
            this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId);
            this.LocatedGameUnit = await this.LocatedGameUnitsRepository.GetLocatedGameUnitAsync(this.LocatedGameUnitId);

            this.FieldtripsFilterOptions = (await this.FieldTripsRepository.GetFieldTripIdsAndNamesAsync())
                .OrderBy(x => x.Value)
                .Select(x => new FilterOption { Label = x.Value, Value = x.Value });

            this.EstablishmentsFilterOptions = (await this.FieldTripsRepository.SelectAllEstablishmentsAsync())
                .OrderBy(e => e)
                .Select(e => new FilterOption { Label = e, Value = e }).ToArray();

            this.DesignersFilterOptions = (await this.FieldTripsRepository.SelectAllDesignersAsync())
                .OrderBy(d => $"{d.FirstName} {d.LastName}")
                .Select(d => new FilterOption { Label = d.FirstName + " " + d.LastName, Value = d.Id })
                .ToArray();

            this.PedagogicFieldsFilterOptions = (await this.FieldTripsRepository.SelectAllPedagogicFieldsAsync())
                .OrderBy(e => e)
                .Select(d => new FilterOption { Label = d, Value = d })
                .ToArray();

            await base.OnInitializedAsync();
        }

        public async Task CloneActivity(Guid activityId)
        {
            LocatedActivity newActivity = await this.ActivitiesRepository.DuplicateActivityAsync(activityId, this.LocatedGameUnitId);
            this.NavigationManager.NavigateTo(ActivityEdition.GetEditionUrl(this.FieldTripId, this.LocatedGameUnitId, newActivity.Id));
        }
    }
}
