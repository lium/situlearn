﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api
{
    public class FieldTripUpdateModel : FieldTripCreationModel
    {
        public Guid Id { get; set; }

        public bool TimeIsLimited { get; set; }

        public int AllottedTimeDuration { get; set; }

        //public bool ReadingsAreVisible { get; set; }

        //public DateTime? ReadingsVisibilityStartDate { get; set; }

        //public DateTime? ReadingsVisibilityEndDate { get; set; }

        public string HomePagePicture { get; set; }

        public string EndPagePicture { get; set; }

        //[MaxLength(1000, ErrorMessage = "Le texte de la page d'accueil ne peut faire plus de 1000 caractères")]
        public string HomePageText { get; set; }

        [MaxLength(250, ErrorMessage = "Le titre de la page d'accueil ne peut faire plus de 250 caractères")]
        public string HomePageTitle { get; set; }

        //[MaxLength(1000, ErrorMessage = "Le texte de la page de fin ne peut faire plus de 1000 caractères")]
        public string EndPageText { get; set; }

        [MaxLength(250, ErrorMessage = "Le titre de la page de fin ne peut faire plus de 250 caractères")]
        public string EndPageTitle { get; set; }

        public bool ShowChrono { get; set; }

        public bool AllowTeamNames { get; set; }
    }
}
