﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Common;
using Entities;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.Razor.Leaflet;
using Syncfusion.Blazor.BarcodeGenerator;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.FieldTrips.LocatedGameUnits
{
    [Route(Url)]
    public partial class LocatedGameUnitEdition : FormComponent
    {
        public const string Url = "/located-game-units/{fieldTripId:guid}/{locatedGameUnitId:guid}";

        public static string GetUrl(Guid fieldTripId, Guid locatedGameUnitId)
        {
            return Url.Replace("{fieldTripId:guid}/{locatedGameUnitId:guid}", $"{fieldTripId}/{locatedGameUnitId}");
        }

        [Parameter]
        public Guid FieldTripId { get; set; }

        [Parameter]
        public Guid LocatedGameUnitId { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public LocatedGameUnitsRepository LocatedGameUnitsRepository { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public ActivitiesRepository ActivitiesRepository { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

		[Inject]
        public IJSRuntime JSRuntime { get; set; }

        public bool CanEdit => this.UsersRepository.CurrentUser.Role == UserRoleNames.Administrator || (this.FieldTrip.DesignerId == this.UsersRepository.CurrentUser.Id);

        public LocatedGameUnit LocatedGameUnit { get; set; }
        public FieldTrip FieldTrip { get; set; }
        public LocatedGameUnitUpdateModel Model { get; set; } = new();

        public bool GeneralIsExpanded { get; set; } = true;
        public bool MandatoryExpanded { get; set; } = true;
        public bool LocalizationIsExpanded { get; set; } = true;

        [Description("Fiche d'information")]
        public bool InformationSheetIsExpended { get; set; } = false;
        [Description("Indice pour trouver le POI")]
        public bool LocationSupportIsExpended { get; set; } = true;
        [Description("Conclusion de l'unité")]
        public bool ConclusionIsExpended { get; set; } = false;
        [Description("Activités sur place")]
        public bool ActivitiesIsExpanded { get; set; } = false;

        public bool SiteMapIsExpanded { get; set; } = true;
        public LeafletMap LeafletMap { get; set; }

        public bool ShowContextualHelpModal { get; set; }
        public string ContextualHelpIdToShow { get; set; }

        public bool ActivitiesAreDragOver { get; set; }

        public bool DisplayActivitiesOptions { get; set; }

        public void DisplayContextualHelpModal(string contextualHelpId)
        {
            Console.WriteLine(contextualHelpId);
            this.ContextualHelpIdToShow = contextualHelpId;
            this.ShowContextualHelpModal = true;
            this.StateHasChanged();
        }

        public string GetExpendableDescription(string property)
        {
            PropertyInfo propertyInfo = this.GetType().GetProperty(property);
            DescriptionAttribute attribute = (DescriptionAttribute)propertyInfo.GetCustomAttribute(typeof(DescriptionAttribute));
            return attribute?.Description;
        }

        public void CloseContextualHelpModal()
        {
            this.ShowContextualHelpModal = false;
            this.ContextualHelpIdToShow = null;
            this.StateHasChanged();
        }

        protected override async Task OnInitializedAsync()
        {
            this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId, true);
            await this.MapFromId(this.LocatedGameUnitId);

			var uri = this.NavigationManager.ToAbsoluteUri(this.NavigationManager.Uri);

			this.DisplayActivitiesOptions = QueryHelpers.ParseQuery(uri.Query).ContainsKey("activities-options");

			await base.OnInitializedAsync();
        }

        public override async Task OnProcessAsync()
        {
            await this.LocatedGameUnitsRepository.UpdateLocationGameUnitParams(this.LocatedGameUnit.Id, this.Model);

            this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId, false);
        }

        public async Task AutomaticSaveAsync()
        {
            this.IsProcessing = true;

            try
            {
                await this.LocatedGameUnitsRepository.UpdateLocationGameUnitParams(this.LocatedGameUnit.Id, this.Model);
                this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId, false);
            }
            catch (BusinessException ex)
            {
                this.Errors = ex.Messages;

                this.Notifier.Notify(Color.Danger, this.ErrorMessage ?? "Erreur lors de la mise à jour des données");
            }
            finally 
            { 
                this.IsProcessing = false;
            }
        }

        public SfQRCodeGenerator QrCode { get; set; }

        public void ExportQrCode()
        {
            this.QrCode.Export($"unite-{this.LocatedGameUnit.Name}", BarcodeExportType.PNG);
        }

        protected async Task OnTabActivated(BigTab tab)
        {
            if (string.Equals(tab.Title, "localisation", StringComparison.OrdinalIgnoreCase))
            {
                await this.LeafletMap.RefreshAsync();
            }
        }

        public async Task OnRemoveActivity(LocatedActivity activity)
        {
            this.LocatedGameUnit.Activities.Remove(activity);

            this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId, false);
		}

		public async Task OnUnitDeleted(Guid unitId)
		{
            if (unitId == this.LocatedGameUnitId)
            {
				this.NavigationManager.NavigateTo(FieldTripEditionPage.GetEditionUrl(this.FieldTripId));
			}			

			await Task.CompletedTask;
		}

		public async Task OnActivityDeleted(Guid activityId)
		{
            LocatedActivity activityToDelete = this.LocatedGameUnit.Activities.FirstOrDefault(a => a.Id == activityId);

            if (activityToDelete != null)
			{
				await this.OnRemoveActivity(activityToDelete);
			}

            this.StateHasChanged();
		}

		public async Task ToggleActivityUsage(bool activitiesEnabled)
        {
            this.ActivitiesIsExpanded = activitiesEnabled;
            this.Model.ActivitiesEnabled = activitiesEnabled;
            await this.LocatedGameUnitsRepository.ToggleActivityUsage(this.LocatedGameUnitId, activitiesEnabled);
        }

        public async Task MapFromId(Guid gameUnitId)
        {
            this.LocatedGameUnit = await this.LocatedGameUnitsRepository.GetLocatedGameUnitAsync(gameUnitId);

            if (this.LocatedGameUnit != null)
            {
                this.Model = new();
                this.Model.Name = this.LocatedGameUnit.Name;
                this.Model.InformationSheetEnabled = this.LocatedGameUnit.InformationSheetEnabled;
                this.Model.ActivitiesEnabled = this.LocatedGameUnit.ActivitiesEnabled;
                this.Model.ConclusionEnabled = this.LocatedGameUnit.ConclusionEnabled;

                this.Model.LocationAidType = this.LocatedGameUnit.LocationAidType;
                this.Model.LocationValidationType = this.LocatedGameUnit.LocationValidationType;
                this.Model.LocationGain = this.LocatedGameUnit.LocationGain;
                this.Model.InformationSheetTitle = this.LocatedGameUnit.InformationSheetTitle;
                this.Model.InformationSheetDescription = this.LocatedGameUnit.InformationSheetDescription;
                this.Model.InformationSheetPicture = this.LocatedGameUnit.InformationSheetPicture;
                this.Model.LocationSupportTitle = this.LocatedGameUnit.LocationSupportTitle;
                this.Model.LocationSupportDescription = this.LocatedGameUnit.LocationSupportDescription;
                this.Model.LocationSupportPicture = this.LocatedGameUnit.LocationSupportPicture;
                this.Model.ConclusionTitle = this.LocatedGameUnit.ConclusionTitle;
                this.Model.ConclusionText = this.LocatedGameUnit.ConclusionText;
                this.Model.ConclusionPicture = this.LocatedGameUnit.ConclusionPicture;

                this.InformationSheetIsExpended = this.Model.InformationSheetEnabled;
                this.ActivitiesIsExpanded = this.Model.ActivitiesEnabled;
                this.ConclusionIsExpended = this.Model.ConclusionEnabled;
            }
            else
            {
                Console.WriteLine("modèle d'unité de jeu NULL");
            }
        }

        public InlineImageChooser LocationSupportPicture { get; set; }

        public async Task Refresh(Guid gameUnitId)
        {
            this.LocatedGameUnit = null;
            this.StateHasChanged();

            await this.MapFromId(gameUnitId);
            this.StateHasChanged();
        }

        public async Task UpdateField(string field, object value)
        {
            switch (field)
            {
                case "location_support_title":
                    this.Model.LocationSupportTitle = (string)value;
                    break;
                case "location_support_description":
                    this.Model.LocationSupportDescription = (string)value;
                    break;
                case "location_support_picture":
                    this.Model.LocationSupportPicture = (string)value;
                    break;
                case "location_aid_type":
                    this.Model.LocationAidType = (LocationAidType)value;
                    break;
                case "location_validation_type":
                    this.Model.LocationValidationType = (LocationValidationType)value;
                    break;

                case "information_title":
                    this.Model.InformationSheetTitle = (string)value;
                    break;
                case "information_description":
                    this.Model.InformationSheetDescription = (string)value;
                    break;
                case "information_picture":
                    this.Model.InformationSheetPicture = (string)value;
                    break;

                case "conclusion_title":
                    this.Model.ConclusionTitle = (string)value;
                    break;
                case "conclusion_text":
                    this.Model.ConclusionText = (string)value;
                    break;
                case "conclusion_picture":
                    this.Model.ConclusionPicture = (string)value;
                    break;
                default:
                    break;
            }

            this.Errors = null;

            //await this.ProcessAsync();
            await this.AutomaticSaveAsync();
            this.StateHasChanged();

            await this.JSRuntime.InvokeVoidAsync("gotoLastScroll");
        }

        public async Task RefreshActivitiesOrder()
        {
            this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId, false);
			this.LocatedGameUnit = await this.LocatedGameUnitsRepository.GetLocatedGameUnitAsync(this.LocatedGameUnitId);
            this.ActivitiesAreDragOver = false;
            this.StateHasChanged();
        }

        private void OnMaxUploadedFileSizeReached(int size)
        {
            string sizeToDisplay = "";

            if (size >= 1_000 && size < 1_000_000)
            {
                sizeToDisplay = $"{decimal.Round((decimal)size / 1_000, 1, MidpointRounding.AwayFromZero)} Ko";
            }
            else if (size >= 1_000_000 && size < 1_000_000_000)
            {
                sizeToDisplay = $"{decimal.Round((decimal)size / 1_000_000, 1, MidpointRounding.AwayFromZero)} Mo";
            }
            else
            {
                sizeToDisplay = $"{size} o)";
            }

            this.Notifier.Notify(Color.Warning, $"Attention, la taille de votre fichier est de {sizeToDisplay}");
        }
    }
}
