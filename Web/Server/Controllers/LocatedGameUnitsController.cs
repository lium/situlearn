﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Business;
using Common;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ozytis.Common.Core.Storage;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Server.Controllers
{
    [Authorize]
    [Route("api/game-units")]
    public class LocatedGameUnitsController : ControllerBase
    {
        public LocatedGameUnitsController(IFileSystem fileSystem, LocatedGameUnitsManager locatedGameUnitManager, FieldTripsManager fieldTripsManager, UsersManager usersManager)
        {
            this.LocatedGameUnitManager = locatedGameUnitManager;
            this.FieldTripsManager = fieldTripsManager;
            this.UsersManager = usersManager;
            this.IFileSystem = fileSystem;
        }

        public LocatedGameUnitsManager LocatedGameUnitManager { get; }
        public FieldTripsManager FieldTripsManager { get; }
        public UsersManager UsersManager { get; }
        public IFileSystem IFileSystem { get; }

        [HttpGet]
        [WebApiQueryable2(nameof(LocatedGameUnit))]
        public async Task<IEnumerable<LocatedGameUnit>> GetAllLocatedGameUnitsAsync()
        {
            return await this.LocatedGameUnitManager.SelectAll()
                .Include(lgu => lgu.Activities)
                .Include(lgu => lgu.FieldTrip)
                    .ThenInclude(ft => ft.Designer)
                .Where(lgu => lgu.IsDeleted == false)
                .ApplyQueryV2Options(this.Request)
                .ToArrayAsync();
        }

        [HttpGet("{locatedGameUnitId:guid}")]
        public async Task<LocatedGameUnit> GetLocatedGameUnitAsync(Guid locatedGameUnitId)
        {
            return await this.LocatedGameUnitManager.SelectAsync(locatedGameUnitId, lgu => lgu.Activities.OrderBy(a => a.Order));
        }

        [HttpPut("duplicate/{locatedGameUnitId:guid}/{fieldTripId:guid}")]
        [ValidateModel, HandleBusinessException]
        public async Task<LocatedGameUnit> DuplicateLocatedGameUnitAsync(Guid locatedGameUnitId, Guid fieldTripId)
        {
            var originalGameUnit = await this.LocatedGameUnitManager.SelectAsync(locatedGameUnitId, lgu => lgu.Activities);

            if (originalGameUnit == null)
            {
                throw new BusinessException("Unité de jeu inconnue");
            }

            return await this.LocatedGameUnitManager.DuplicateLocatedGameUnitAsync(fieldTripId, originalGameUnit);
        }

        [HttpDelete("{locatedGameUnitId:guid}")]
        public async Task DeleteGameUnit(Guid locatedGameUnitId)
        {
            await this.LocatedGameUnitManager.SoftDeleteAsync(locatedGameUnitId);
        }

        [HttpPost("{locatedGameUnitId:guid}")]
        [HandleBusinessException]
        public async Task EnableActivities(Guid locatedGameUnitId, [FromBody] bool activitiesEnabled)
        {
            var gameUnit = await this.LocatedGameUnitManager.SelectAsync(locatedGameUnitId);
            gameUnit.ActivitiesEnabled = activitiesEnabled;
            await this.LocatedGameUnitManager.SaveChangesAsync();
        }

        [HttpPut("{locatedGameUnitId:guid}")]
        [ValidateModel, HandleBusinessException]
        public async Task<LocatedGameUnit> UpdateLocatedGameUnitAsync(Guid locatedGameUnitId, [FromBody] LocatedGameUnitUpdateModel model)
        {
            ApplicationUser updater = await this.UsersManager
                .SelectAllUsers()
                .Include(u => u.UserRoles)
                    .ThenInclude(r => r.Role)
                .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

            if (updater == null)
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            var locatedGameUnit = await this.LocatedGameUnitManager.SelectAsync(locatedGameUnitId, lgu => lgu.FieldTrip);

            if (locatedGameUnit == null)
            {
                return null;
            }

            if (updater.Id != locatedGameUnit.FieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            return await this.LocatedGameUnitManager.UpdateAsync(locatedGameUnit, model);
        }
    }
}
