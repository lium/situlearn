﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using SituLearn.Player.Pages.User;
using SituLearn.Player.Repositories;
using SituLearn.Player.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace SituLearn.Player.Pages.FieldTrips
{
    [Route(Url)]
    public partial class FieldTripsPage : ComponentBase
    {
        public const string Url = "/";

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        private IJSRuntime JSRuntime { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [CascadingParameter]
        public AppLayout Layout { get; set; }

        public FieldTripHeaderModel[] FieldTrips { get; set; }

        public string FilterType { get; set; } = "name";

        public string Filter { get; set; }

        public string OrderWay { get; set; } = "asc";

        public bool ShowEnrichedMap { get; set; }

        public Guid? ShowingFieltripMapId { get; set; }

        public async Task LoadMap(Guid fieldTripId)
        {
            this.ShowingFieltripMapId = fieldTripId;
            this.ShowEnrichedMap = true;
            this.StateHasChanged();

            await Task.Delay(3000).ContinueWith(x =>
            {
                this.ShowingFieltripMapId = null;
                this.ShowEnrichedMap = false;
                this.StateHasChanged();
            });
        }

        private List<string> LeafletIconsPath = new()
        {
            "layers-2x.png",
            "layers.png",
            "marker-icon-2x-black.png",
            "marker-icon-2x-blue.png",
            "marker-icon-2x-gold.png",
            "marker-icon-2x-green.png",
            "marker-icon-2x-grey.png",
            "marker-icon-2x-orange.png",
            "marker-icon-2x-red.png",
            "marker-icon-2x-violet.png",
            "marker-icon-2x-yellow.png",
            "marker-icon-2x.png",
            "marker-icon-black.png",
            "marker-icon-blue.png",
            "marker-icon-gold.png",
            "marker-icon-green.png",
            "marker-icon-grey.png",
            "marker-icon-orange.png",
            "marker-icon-red.png",
            "marker-icon-violet.png",
            "marker-icon-yellow.png",
            "marker-icon.png",
            "marker-shadow.png"
        };

        public List<OptionItem<string>> FilterOptions { get; set; } = new()
        {
            new OptionItem<string>("Nom de la sortie", "name"),
            new OptionItem<string>("Type de la sortie", "type"),
            new OptionItem<string>("Domaine pédagogique", "field"),
            new OptionItem<string>("Concepteur", "designer"),
            new OptionItem<string>("Etablissement", "establishment"),
        };

        public bool FilterFavorites { get; set; }

        public bool FilterDownloaded { get; set; }

        public Timer GeoLocationTimer { get; set; }

        public List<string> Errors { get; set; } = new();

        public bool ControlsAfterRenderAreDone { get; set; }

        public bool AllPicturesAreUpdatedInCache { get; set; }

        public bool IsOnDiscoveryMode { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var uri = this.NavigationManager.ToAbsoluteUri(this.NavigationManager.Uri);

            this.FilterFavorites = QueryHelpers.ParseQuery(uri.Query).ContainsKey("favorites");
            this.FilterDownloaded = QueryHelpers.ParseQuery(uri.Query).ContainsKey("downloaded");

            this.IsOnDiscoveryMode = this.GamesRepository.IsOnDiscoveryMode;

            if (!this.IsOnDiscoveryMode && this.UsersRepository.CurrentUser == null)
            {
                await this.Layout.LogOut();
            }
            else
            {
                this.FieldTripsRepository.OnCacheUnavailable += this.OnCacheUnavailable;

                this.FieldTrips = (await this.FieldTripsRepository.GetAllFieldTripsHeadersAsync()).ToArray();

                this.NavigationManager.LocationChanged += RefreshFavorisOrDownloadedState;
                //await this.FieldTripsRepository.RemoveUselessPicturesFromCacheAsync();
            }

            await base.OnInitializedAsync();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender && this.FieldTrips != null && !this.ControlsAfterRenderAreDone)
            {
                if (!this.IsOnDiscoveryMode && this.UsersRepository.CurrentUser != null)
                {
                    await this.GamesRepository.CancelAllGamesAsync();
                    //await this.GamesRepository.CheckStatementRemoval();
                }

                await this.CheckGeoLocationAsync();

                this.ControlsAfterRenderAreDone = true;
            }

            if (!this.IsOnDiscoveryMode && this.UsersRepository.CurrentUser != null)
            {
                if (!firstRender && this.FieldTrips != null && !this.AllPicturesAreUpdatedInCache && !this.FieldTripsRepository.HasAddedPicturestoCache)
                {
                    bool serviceWorkerIsActivated = false;

                    do
                    {
                        serviceWorkerIsActivated = await this.JSRuntime.InvokeAsync<bool>("window.checkIfServiceWorkerIsActivated");

                        if (!serviceWorkerIsActivated)
                        {
                            await Task.Delay(1000);
                        }

                    } while (!serviceWorkerIsActivated);

                    await this.FieldTripsRepository.AddFieldTripsHeadersPicturesInCacheAsync();
                    await this.FieldTripsRepository.LoadSavedFieldTripsAsync();
                    //await this.FieldTripsRepository.RemoveUselessPicturesFromCacheAsync();

                    if (this.NavigationManager != null)
                    {
                        List<Task> tasks = new();
                        foreach (var item in this.FieldTripsRepository.localPictures)
                        {
                            tasks.Add(this.FieldTripsRepository.AddPictureToCacheAsync(NavigationManager.BaseUri + item));
                        }

                        foreach (var item in this.LeafletIconsPath)
                        {
                            tasks.Add(this.FieldTripsRepository.AddPictureToCacheAsync(NavigationManager.BaseUri + "_content/Ozytis.Common.Core.Web.Razor.Leaflet/images/" + item));
                        }

                        await Task.WhenAll(tasks);
                    }

                    this.AllPicturesAreUpdatedInCache = true;
                    this.FieldTripsRepository.HasAddedPicturestoCache = true;
                }
            }            

            await base.OnAfterRenderAsync(firstRender);
        }

        private async void GeoLocationTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            bool geolocationAvailable = await this.GamesRepository.CheckGeolocationIsAvailableAsync();
            if(!geolocationAvailable)
            {
                if(!this.Errors.Any())
                {
                    this.Errors = new() { "La géolocalisation n'est pas disponible, veuillez l'activer." };
                }
            }
            else
            {
                this.Errors.Clear();
                this.GeoLocationTimer.AutoReset = false;
                this.GeoLocationTimer.Stop();
            }

            this.StateHasChanged();
        }

        public async Task CheckGeoLocationAsync()
        {
            bool geolocationAvailable = await this.GamesRepository.CheckGeolocationIsAvailableAsync();

            if (!geolocationAvailable)
            {
                this.Errors = new() { "La géolocalisation n'est pas disponible, veuillez l'activer." };
                this.GeoLocationTimer = new Timer(10000);
                this.GeoLocationTimer.AutoReset = true;
                this.GeoLocationTimer.Elapsed += GeoLocationTimer_Elapsed;
                this.GeoLocationTimer.Start();
                this.StateHasChanged();
            }
        }

        public void OnCacheUnavailable(object sender, EventArgs e)
        {
            this.Notifier.Notify(color: Ozytis.Common.Core.Web.Razor.Color.Danger, "Accès au cache impossible");
            this.FieldTripsRepository.OnCacheUnavailable -= this.OnCacheUnavailable;
        }

        protected void SwitchFavoritesDisplay()
        {
            this.FilterFavorites = !this.FilterFavorites;

            if (this.FilterFavorites)
            {
                this.FilterDownloaded = false;
            }

            foreach (var fieldTrip in FieldTrips)
            {
                if (fieldTrip.ShowDescription)
                {
                    fieldTrip.ShowDescription = false;
                }
            }

            this.StateHasChanged();
        }

        protected void RefreshFavorisOrDownloadedState(object sender, LocationChangedEventArgs e)
        {
            var uri = this.NavigationManager.ToAbsoluteUri(this.NavigationManager.Uri);

            this.FilterFavorites = QueryHelpers.ParseQuery(uri.Query).ContainsKey("favorites");
            this.FilterDownloaded = QueryHelpers.ParseQuery(uri.Query).ContainsKey("downloaded");

            foreach (var fieldTrip in FieldTrips)
            {
                if (fieldTrip.ShowDescription)
                {
                    fieldTrip.ShowDescription = false;
                }
            }

            this.StateHasChanged();
        }

        protected void SwitchDownloadedDisplay()
        {
            this.FilterDownloaded = !this.FilterDownloaded;

            if (this.FilterDownloaded)
            {
                this.FilterFavorites = false;
            }

            foreach (var fieldTrip in FieldTrips)
            {
                if (fieldTrip.ShowDescription)
                {
                    fieldTrip.ShowDescription = false;
                }
            }

            this.StateHasChanged();
        }

        public async Task OnKeyDown(KeyboardEventArgs args)
        {
            if(args.Key == "Enter")
            {
                await this.JSRuntime.InvokeVoidAsync("window.blurActiveElement");
            }
        }

        public async void PlayedFieldTripsOnLineOrOffLine()
        {
            this.NavigationManager.NavigateTo(PlayedFieldTrips.PlayedFieldTripsPage.Url);
        }
    }
}
