﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System.Threading.Tasks;
using Api;
using Web.Client.Repositories;
using Ozytis.Common.Core.Utilities;

namespace Web.Client.Pages.User
{
    public partial class MyPassword : ComponentBase
    {
        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        public string[] Errors { get; set; }

        public PasswordUpdateModel Model { get; set; } = new PasswordUpdateModel();

        public string PasswordConfirmation { get; set; }

        public bool IsUpdating { get; set; }

        protected async Task SaveAsync()
        {
            if (this.IsUpdating)
            {
                return;
            }

            this.Errors = null;

            if (this.PasswordConfirmation != this.Model.NewPassword)
            {
                this.Errors = new[] { "Le nouveau mot de passe et sa confirmation ne correspondent pas" };
            }


            this.IsUpdating = true;

            try
            {
                await this.UsersRepository.UpdatePasswordAsync(this.Model);
                this.Notifier.Notify(Color.Success, "Vos informations ont bien été mises à jour");
            }
            catch (BusinessException ex)
            {
                this.Errors = ex.Messages;
            }

            this.IsUpdating = false;
        }
    }
}
