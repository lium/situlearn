﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using SituLearn.Monitor.Repositories;
using SituLearn.Monitor.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Monitor.Pages.FieldTrips
{
    [Route(Url)]
    [Authorize]

    public partial class FieldTripsPage : ComponentBase
    {
        public const string Url = "/";

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        public FieldTripHeaderForMonitorModel[] FieldTrips { get; set; }

        public string FilterType { get; set; } = "name";

        public string Filter { get; set; }

        public string OrderWay { get; set; } = "asc";

        public List<OptionItem<string>> FilterOptions { get; set; } = new()
        {
            new OptionItem<string>("Nom de la sortie", "name"),
            new OptionItem<string>("Type de la sortie", "type"),
            new OptionItem<string>("Domaine pédagogique", "field"),
        };

        protected override async Task OnInitializedAsync()
        {
            this.FieldTrips = (await this.FieldTripsRepository.GetAllFieldTripsHeadersForMonitorAsync()).ToArray();

            await base.OnInitializedAsync();
        }
    }
}
