﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Business;
using Common;
using Common.Security;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Ozytis.Common.Core.Api;
using Ozytis.Common.Core.Storage;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.Mvc;
using Ozytis.Common.Core.Web.WebApi;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Web.ApiTransform;
using Web.Controllers;

namespace Web.Server.Controllers
{
	[Authorize]
	[Route("api/fieldtrips")]
	public class FieldTripsController : ControllerBase
	{
		public FieldTripsController(FieldTripsManager fieldTripsManager, UsersManager usersManager,
			IConfiguration configuration, ActivitiesManager activitiesManager, IFileSystem fileSystem, GamesManager gamesManager)
		{
			this.FieldTripsManager = fieldTripsManager;
			this.UsersManager = usersManager;
			this.ActivitiesManager = activitiesManager;
			this.FileSystem = fileSystem;
			this.Configuration = configuration;
			this.PictureFileUrlPrefix = this.Configuration["Data:Network:BaseUrl"] + "files?path=";
			this.GamesManager = gamesManager;
        }

		public IConfiguration Configuration { get; }
		public FieldTripsManager FieldTripsManager { get; }
		public UsersManager UsersManager { get; }
		public ActivitiesManager ActivitiesManager { get; }
        public GamesManager GamesManager { get; }
        public IFileSystem FileSystem { get; }

		public string PictureFileUrlPrefix { get; }


		[HttpGet]
		[WebApiQueryable2(nameof(FieldTrip))]
		public async Task<IEnumerable<FieldTrip>> GetAllFieldTripsAsync()
		{
			IEnumerable<FieldTrip> fieldtrips = await this.FieldTripsManager.SelectAll()
				.Include(f => f.LocatedGameUnits)
				.Include(f => f.Journeys)
				.Include(f => f.Designer)
				.ApplyQueryV2Options(this.Request).ToArrayAsync();

			return fieldtrips;
		}

        [AllowAnonymous]
        [HttpGet("headers")]
		public async Task<IEnumerable<FieldTripHeaderModel>> GetAllFieldTripsHeadersAsync()
		{
			var fieldTrips = await this.FieldTripsManager.SelectAll()
				.Include(t => t.Designer)
				.Include(t => t.Journeys)
				.Include(t => t.UserFieldTrips)
					.ThenInclude(uft => uft.User)
				.Where(ft => ft.IsPublished && !ft.IsDeleted)
				.Select(fieldTrip => new FieldTripHeaderModel
				{
					FieldName = fieldTrip.PedagogicalFieldName,
					Type = fieldTrip.FieldTripType,
					Id = fieldTrip.Id,
					Name = fieldTrip.Name,
					Picture = fieldTrip.HomePagePicture,
					DesignerName = fieldTrip.Designer.FirstName + " " + fieldTrip.Designer.LastName,
					EstablishmentName = fieldTrip.Establishment,
					Description = fieldTrip.Description,
					IsFavorite = fieldTrip.UserFieldTrips.Any(uft => uft.User.UserName == this.User.Identity.Name && uft.IsFavorite),
					JourneysCounter = fieldTrip.Journeys.Count,
					JourneyIdFirst = fieldTrip.Journeys.Any() ? fieldTrip.Journeys.First().Id : null,
					ShowDescription = false,
					IsDownloaded = fieldTrip.UserFieldTrips.Any(uft => uft.User.UserName == this.User.Identity.Name && uft.IsDownloaded),
					AllowTeamNames = fieldTrip.AllowTeamNames
				}
				).ToArrayAsync();

			foreach (var fieldtrip in fieldTrips)
			{
				fieldtrip.Picture = this.PrefixPictureUrl(fieldtrip.Picture);
			}

			return fieldTrips;
        }

        [Authorize(Policy = Policy.UserCanCreateFieldTrips)]
        [HttpGet("monitor-headers")]
        public async Task<IEnumerable<FieldTripHeaderForMonitorModel>> GetAllFieldTripsHeadersForMonitorAsync()
        {
            ApplicationUser designer = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

            if (designer == null)
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            var fieldTrips = await this.FieldTripsManager.SelectAll()
                .Include(t => t.Designer)
                .Where(ft => ft.IsPublished && !ft.IsDeleted && ft.DesignerId == designer.Id)
                .Select(fieldTrip => new FieldTripHeaderForMonitorModel
                {
                    FieldName = fieldTrip.PedagogicalFieldName,
                    Type = fieldTrip.FieldTripType,
                    Id = fieldTrip.Id,
                    Name = fieldTrip.Name,
                    Picture = fieldTrip.HomePagePicture,
                    EstablishmentName = fieldTrip.Establishment,
                    Description = fieldTrip.Description,
                    ShowDescription = false
                }
                ).ToArrayAsync();

            foreach (var fieldtrip in fieldTrips)
            {
                fieldtrip.Picture = this.PrefixPictureUrl(fieldtrip.Picture);
            }

            return fieldTrips;
        }

        [Authorize(Policy = Policy.UserCanCreateFieldTrips)]
		[HttpPost]
		[HandleBusinessException, ValidateModel]
		public async Task<FieldTrip> CreateFieldTripAsync([FromBody] FieldTripCreationModel model)
		{
			ApplicationUser designer = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

			if (designer == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			return await this.FieldTripsManager.CreateFieldTripAsync(new FieldTrip
			{
				Name = model.Name,
				PedagogicalFieldName = model.PedagogicalFieldName,
				Description = model.Description,
				FieldTripType = model.FieldTripType,
				IsPublished = model.IsPublished,
				IsVisible = model.IsVisible,
			}, designer);
		}

		[AllowAnonymous]
		[HttpGet("pusblished-count")]
		public async Task<int> GetPublishedFieldTripsCountAsync()
		{
			return await this.FieldTripsManager.SelectAll().Where(f => f.IsPublished).CountAsync();
		}

		[Authorize]
		[HttpGet("establishments")]
		public async Task<IEnumerable<string>> SelectAllEstablishmentsAsync()
		{
			return await this.FieldTripsManager.SelectAll().Select(t => t.Establishment)
				.Where(x => x != null)
				.Distinct().OrderBy(s => s).ToArrayAsync();
		}

		[Authorize]
		[HttpGet("designers")]
		public async Task<IEnumerable<ApplicationUser>> SelectAllDesignersAsync()
		{
			return await this.FieldTripsManager.SelectAll().Select(t => t.Designer)
				.Distinct().OrderBy(s => s.LastName + " " + s.FirstName).ToArrayAsync();
		}

		[Authorize]
		[HttpGet("types")]
		public async Task<IEnumerable<FieldTripType>> SelectAllTypesAsync()
		{
			return await this.FieldTripsManager.SelectAll().Select(t => t.FieldTripType)
				.Distinct().OrderBy(s => s).ToArrayAsync();
		}

		[Authorize]
		[HttpGet("pedagogicfields")]
		public async Task<IEnumerable<string>> SelectAllPedagogicFieldsAsync()
		{
			return await this.FieldTripsManager.SelectAll().Select(t => t.PedagogicalFieldName)
				.Distinct().OrderBy(s => s).ToArrayAsync();
		}

        [AllowAnonymous]
        [HttpGet("{fieldTripId:guid}/{expandUrl:bool?}/{minimizeForPlayer:bool?}")]
		[DeserializeInheritance]
		public async Task<FieldTrip> GetFieldTripAsync(Guid fieldTripId, bool expandUrl = true, bool minimizeForPlayer = false)
		{
			var fieldTrip = await this.FieldTripsManager.SelectAll()
				.Include(f => f.LocatedGameUnits.Where(lgu => !lgu.IsDeleted))
					.ThenInclude(g => g.Activities.Where(a => !a.IsDeleted))
				.Include(f => f.Journeys)
					.ThenInclude(j => j.JourneyLocatedGameUnits)
					.ThenInclude(jlg => jlg.LocatedGameUnit)
					.ThenInclude(lgu => lgu.Activities)
					.ThenInclude(a => (a as QuestionActivity).Answers.OrderBy(a => Guid.NewGuid()))//ordre aléatoire des réponses
				.Include(f => f.Journeys)
					.ThenInclude(j => j.JourneyLocatedGameUnits)
					.ThenInclude(jlg => jlg.LocatedGameUnit)
					.ThenInclude(lgu => lgu.Activities)
					.ThenInclude(a => (a as QuestionActivity).Clue)
				.Include(f => f.Designer)
				.Include(f => f.ExplorationMap)
				.Where(f => f.Id == fieldTripId)
				.FirstOrDefaultAsync();

			fieldTrip.HomePagePicture = this.PrefixPictureUrl(fieldTrip.HomePagePicture, expandUrl);
			fieldTrip.EndPagePicture = this.PrefixPictureUrl(fieldTrip.EndPagePicture, expandUrl);
			fieldTrip.ExplorationMap.CustomBackgroundMap = this.PrefixPictureUrl(fieldTrip.ExplorationMap.CustomBackgroundMap, expandUrl);

			if (fieldTrip.LocatedGameUnits != null)
			{
				fieldTrip.LocatedGameUnits = fieldTrip.LocatedGameUnits.Where(lgu => !lgu.IsDeleted).ToList();

				foreach (var locatedGameUnit in fieldTrip.LocatedGameUnits)
				{
					locatedGameUnit.InformationSheetPicture = this.PrefixPictureUrl(locatedGameUnit.InformationSheetPicture, expandUrl);
					locatedGameUnit.ConclusionPicture = this.PrefixPictureUrl(locatedGameUnit.ConclusionPicture, expandUrl);
					locatedGameUnit.LocationSupportPicture = this.PrefixPictureUrl(locatedGameUnit.LocationSupportPicture, expandUrl);

					var activities = locatedGameUnit.Activities.Where(a => !a.IsDeleted).ToList();
					locatedGameUnit.Activities = new();

					foreach (var item in activities)
					{
						if (item.IsDeleted)
							continue;

						if (item.ActivityType == ActivityType.Question)
						{
							var question = this.ActivitiesManager.GetQuestion(item.Id);
							question.Picture = this.PrefixPictureUrl(question.Picture, expandUrl);


							if (question.Clue != null)
							{
								question.Clue.Picture = this.PrefixPictureUrl(question.Clue.Picture, expandUrl);
							}

							if (question.QuestionActivityType == QuestionActivityType.PictureMcq)
							{
								foreach (var answer in question.Answers)
								{
									answer.Content = this.PrefixPictureUrl(answer.Content, expandUrl);
								}
							}

							locatedGameUnit.Activities.Add(question);
						}
						else
						{
							var statement = this.ActivitiesManager.GetStatement(item.Id);
							statement.Picture = this.PrefixPictureUrl(statement.Picture, expandUrl);
							locatedGameUnit.Activities.Add(statement);
						}
					}
				}
			}

			if (fieldTrip.Journeys != null)
			{
				foreach (var journey in fieldTrip.Journeys)
				{
					journey.JourneyLocatedGameUnits = journey.JourneyLocatedGameUnits.Where(jlgu => !jlgu.LocatedGameUnit.IsDeleted).ToList();

					foreach (var journeyLocatedGameUnit in journey.JourneyLocatedGameUnits)
					{
						journeyLocatedGameUnit.LocatedGameUnit.Activities = journeyLocatedGameUnit.LocatedGameUnit.Activities.Where(a => !a.IsDeleted).ToList();
					}
                }

				if (minimizeForPlayer)
                {
                    var locatedGameUnitsIncludedInJourneys = fieldTrip.Journeys.SelectMany(j => j.JourneyLocatedGameUnits).Select(jlgu => jlgu.LocatedGameUnitId).Distinct().ToList();

                    fieldTrip.LocatedGameUnits = fieldTrip.LocatedGameUnits.Where(u => locatedGameUnitsIncludedInJourneys.Contains(u.Id)).ToList();
                }
            }

			// clear "journey" to avoid serialization loop length exception
			foreach (var jlgu in fieldTrip.Journeys.SelectMany(j => j.JourneyLocatedGameUnits))
			{
				jlgu.Journey = null;
			}

			return fieldTrip;
		}

		[HttpGet("filterOptions")]
		[DeserializeInheritance]
		public async Task<Dictionary<Guid, string>> GetFieldTripIdsAndNames()
		{
			var dictioFields = (await this.FieldTripsManager.SelectAll().ToArrayAsync())
				.ToDictionary(x => x.Id, x => x.Name);

			return dictioFields;
		}

		[HttpDelete("{fieldTripId:guid}")]
		[HandleBusinessException]
		public async Task DeleteFieldTripAsync([FromRoute]Guid fieldTripId)
		{
			ApplicationUser deleter = await this.UsersManager
				.SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
				.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (deleter == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.FieldTripsManager.DeleteFieldTripAsync(fieldTripId, deleter);
		}

		[HttpPut("{fieldTripId:guid}/visibility")]
		[ValidateModel, HandleBusinessException]
		public async Task<FieldTrip> UpdateVisibilityAsync(Guid fieldTripId)
		{
			ApplicationUser updater = await this.UsersManager
				.SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
				.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			return await this.FieldTripsManager.UpdateVisibilityAsync(fieldTripId, updater);
		}

		[HttpPut("{fieldTripId:guid}/publish-status")]
		[ValidateModel, HandleBusinessException]
		public async Task<FieldTrip> UpdatePublishStatusAsync(Guid fieldTripId)
		{
			ApplicationUser updater = await this.UsersManager
				.SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
				.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			return await this.FieldTripsManager.UpdatePublishStatusAsync(fieldTripId, updater);
		}

		[HttpPost("{fieldTripId:guid}/locatedunit")]
		[ValidateModel, HandleBusinessException]
		public async Task<LocatedGameUnit> AddLocatedUnitAsync(Guid fieldTripId, [FromBody] LocatedUnitCreationModel model)
		{
			ApplicationUser updater = await this.UsersManager
							.SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
							.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			return await this.FieldTripsManager.AddLocatedUnitAsync(new LocatedGameUnit
			{
				FieldTripId = model.FieldTripId,
				Longitude = model.Longitude,
				Latitude = model.Latitude,
				Name = model.Name,
				LocationAidType = model.IsGeolocatable ? LocationAidType.MapWithGps : LocationAidType.MapWithoutGps,
				LocationValidationType = model.IsGeolocatable ? LocationValidationType.GpsLocation : LocationValidationType.QrCode
			}, updater);
		}

		[HttpPut("{fieldTripId:guid}/locatedunit/{locatedUnitId:guid}/infos")]
		[ValidateModel, HandleBusinessException]
		public async Task<LocatedGameUnit> UpdateLocatedUnitInfoAsync(Guid fieldTripId, [FromBody] LocatedUnitUpdateInfoModel model)
		{
			ApplicationUser updater = await this.UsersManager
							.SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
							.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			return await this.FieldTripsManager.UpdateLocatedUnitInfoAsync(new LocatedGameUnit
			{
				Id = model.Id,
				FieldTripId = model.FieldTripId,
				Longitude = model.Longitude,
				Latitude = model.Latitude,
				Name = model.Name,
				ActivationArea = model.ActivationArea,
				ActivationAreaType = model.ActivationAreaType,
				ActivationAreaCenterLatitude = model.ActivationAreaCenterLatitude,
				ActivationAreaCenterLongitude = model.ActivationAreaCenterLongitude,
				ActivationRadius = model.ActivationRadius
			}, updater);
		}

		[HttpDelete("{fieldTripId:guid}/locatedunit/{locatedUnitId:guid}")]
		[HandleBusinessException]
		public async Task DeleteLocatedUnitAsync(Guid fieldTripId, Guid locatedUnitId)
		{
			ApplicationUser deleter = await this.UsersManager
			  .SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
			  .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (deleter == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.FieldTripsManager.DeleteLocatedUnitAsync(fieldTripId, locatedUnitId, deleter);
		}

		[HttpPut]
		[HandleBusinessException, ValidateModel]
		public async Task UpdateFieldTripAsync([FromBody] FieldTripUpdateModel model)
		{
			ApplicationUser updater = await this.UsersManager
						   .SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
						   .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.FieldTripsManager.UpdateFieldTripAsync(new FieldTrip
			{
				Id = model.Id,
				Name = model.Name,
				AllottedTimeDuration = model.TimeIsLimited ? model.AllottedTimeDuration : 0,
				Description = model.Description,
				EndPagePicture = model.EndPagePicture,
				EndPageText = model.EndPageText,
				EndPageTitle = model.EndPageTitle,
				HomePagePicture = model.HomePagePicture,
				HomePageText = model.HomePageText,
				HomePageTitle = model.HomePageTitle,
				PedagogicalFieldName = model.PedagogicalFieldName,
				//ReadingsAreVisible = model.ReadingsAreVisible,
				//ReadingsVisibilityEndDate = model.ReadingsVisibilityEndDate,
				//ReadingsVisibilityStartDate = model.ReadingsVisibilityStartDate,
				TimeIsLimited = model.TimeIsLimited,
				ShowElapsedTime = model.ShowChrono,
				AllowTeamNames = model.AllowTeamNames
			}, updater);
		}

		[HttpPost("{fieldTripId:guid}/journeys")]
		[HandleBusinessException, ValidateModel]
		public async Task<Journey> AddNewJourneyAsync(Guid fieldTripId)
		{
			ApplicationUser updater = await this.UsersManager
						 .SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
						 .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			return await this.FieldTripsManager.AddNewJourneyAsync(fieldTripId, updater);
		}

		[HttpPut("{fieldTripId:guid}/journeys")]
		[HandleBusinessException, ValidateModel]
		public async Task<Journey> UpdateJourneyInfoAsync(Guid fieldTripId, [FromBody] JourneyInfoUpdateModel model)
		{
			ApplicationUser updater = await this.UsersManager
						.SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
						.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			return await this.FieldTripsManager.UpdateJourneyInfoAsync(new Journey
			{
				Id = model.Id,
				FieldTripId = fieldTripId,
				Color = model.Color,
			}, updater);
		}

		[HttpDelete("{fieldTripId:guid}/journeys/{journeyId:guid}")]
		[HandleBusinessException, ValidateModel]
		public async Task DeleteJourneyAsync(Guid fieldTripId, Guid journeyId)
		{
			ApplicationUser deleter = await this.UsersManager
					   .SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
					   .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (deleter == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.FieldTripsManager.DeleteJourneyAsync(fieldTripId, journeyId, deleter);
		}

		[HttpPost("{fieldTripId:guid}/journeys/{journeyId:guid}/units")]
		[ValidateModel, HandleBusinessException]
		public async Task AddLocatedUnitToJourneyAsync(Guid fieldTripId, Guid journeyId, [FromBody] LocatedGameUnitJourneyCreationModel model)
		{
			ApplicationUser updater = await this.UsersManager
						   .SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
						   .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.FieldTripsManager.AddLocatedUnitToJourneyAsync(
				fieldTripId,
				new JourneyLocatedGameUnit
				{
					JourneyId = model.JourneyId,
					LocatedGameUnitId = model.LocatedGameUnitId,
					Order = model.Order ?? int.MaxValue
				},
				updater);
		}

		[HttpDelete("{fieldTripId:guid}/journeys/{journeyId:guid}/units/{journeyUnitLinkId:guid}")]
		[ValidateModel, HandleBusinessException]
		public async Task RemoveUnitFromJourneyAsync(Guid fieldTripId, Guid journeyId, Guid journeyUnitLinkId)
		{
			ApplicationUser deleter = await this.UsersManager
					 .SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
					 .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (deleter == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.FieldTripsManager.RemoveUnitFromJourneyAsync(fieldTripId, journeyId, journeyUnitLinkId, deleter);
		}

		[HttpPut("{fieldTripId:guid}/journeys/{journeyId:guid}/units/order")]
		[ValidateModel, HandleBusinessException]
		public async Task UpdateGameUnitOrderAsync(Guid fieldTripId, Guid journeyId, [FromBody] DraggableObjectOrderUpdateModel model)
		{
			ApplicationUser updater = await this.UsersManager
						   .SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
						   .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.FieldTripsManager.UpdateGameUnitOrderAsync(fieldTripId, journeyId, model.LinkId, model.NewOrder, updater);
		}

		[HttpPut("{fieldTripId:guid}/map")]
		[HandleBusinessException, ValidateModel]
		public async Task UpdateMapPositionAsync(Guid fieldTripId, [FromBody] UpdateMapPositionModel model)
		{
			ApplicationUser updater = await this.UsersManager
						   .SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
						   .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			if (model.ExplorationArea == null && model.ExplorationZoneIsActivated)
			{
				throw new BusinessException("La zone d'exploration est activée mais non définie.");
			}

			await this.FieldTripsManager.UpdateMapPositionAsync(fieldTripId, new ExplorationMap
			{
				CenterLatitude = model.CenterLatitude,
				CenterLongitude = model.CenterLongitude,
				Zoom = model.Zoom,
				IsGeolocatable = model.MapIsGeolocalized,
				CustomBackgroundLowerRightLatitude = model.MapBackgroundLowerRightLatitude,
				CustomBackgroundLowerRightLongitude = model.MapBackgroundLowerRightLongitude,
				CustomBackgroundMap = model.MapBackground,
				CustomBackgroundOpacity = model.MapBackgroundOpacity,
				CustomBackgroundRotation = model.MapRotation,
				CustomBackgroundUpperLeftLatitude = model.MapBackgroundUpperLeftLatitude,
				CustomBackgroundUpperLeftLongitude = model.MapBackgroundUpperLeftLongitude,
				SouthWestLatitude = model.SouthWestLatitude,
				SouthWestLongitude = model.SouthWestLongitude,
				NorthEastLatitude = model.NorthEastLatitude,
				NorthEastLongitude = model.NorthEastLongitude,
				OrganizerOutOfZoneAlert = model.AlertUserOutOfExplorationZoneInMonitor,
				ParticipantOutOfZoneAlert = model.AlertUserOutOfExplorationZoneInPlayer,
				ExplorationArea = model.ExplorationArea,
				ExplorationAreaCenterLatitude = model.ExplorationAreaCenterLatitude,
				ExplorationAreaCenterLongitude = model.ExplorationAreaCenterLongitude,
				ExplorationAreaRadius = model.ExplorationAreaRadius,
				ExplorationShape = model.ExplorationAreaShape
			}, updater);
		}

		[HttpPut("{fieldTripId:guid}/favorite")]
		[ValidateModel, HandleBusinessException]
		public async Task AddOrRemoveFromFavoritesAsync(Guid fieldTripId)
		{
			ApplicationUser updater = await this.UsersManager
				.SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
				.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			await this.FieldTripsManager.AddOrRemoveFromFavoritesAsync(fieldTripId, updater);
		}

		[HttpDelete("statements/{fieldTripId:guid}/{from:long}/{to:long}")]
		[HandleBusinessException]
		public async Task RemoveStatements(Guid fieldTripId, long from, long to)
		{
			var fromDate = new DateTime(from);
			var toDate = new DateTime(to);

			await this.ActivitiesManager.RemoveStatements(fieldTripId, fromDate, toDate);
		}

		[HttpGet("ducplicate/{fieldTripId:guid}")]
		[HandleBusinessException]
		public async Task<Guid> DuplicateFieldTrip(Guid fieldTripId)
		{
			ApplicationUser updater = await this.UsersManager
			   .SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
			   .FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			return await this.FieldTripsManager.DuplicateAsync(fieldTripId, updater);
		}

		[HttpPut("{fieldTripId:guid}/download")]
		[ValidateModel, HandleBusinessException]
		public async Task<bool> AddOrRemoveFromDownloadsAsync(Guid fieldTripId)
		{
			ApplicationUser updater = await this.UsersManager
				.SelectAllUsers().Include(u => u.UserRoles).ThenInclude(r => r.Role)
				.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			if (updater == null)
			{
				throw new BusinessException("Vous n'avez pas le droit de faire cela");
			}

			return await this.FieldTripsManager.AddOrRemoveFromDownloadsAsync(fieldTripId, updater);
		}

		[HttpGet("saved")]
		public async Task<IEnumerable<FieldTrip>> GetAllSavedFieldTripsAsync()
		{
			ApplicationUser user = await this.UsersManager
				.SelectAllUsers()
				.FirstOrDefaultAsync(u => u.UserName == this.User.Identity.Name);

			var fieldTrips = await this.FieldTripsManager.GetAllSavedFieldTrips(user.Id).ToArrayAsync();
			foreach (var fieldTrip in fieldTrips)
			{
				fieldTrip.HomePagePicture = this.PrefixPictureUrl(fieldTrip.HomePagePicture);
				fieldTrip.EndPagePicture = this.PrefixPictureUrl(fieldTrip.EndPagePicture);


				fieldTrip.ExplorationMap.CustomBackgroundMap = this.PrefixPictureUrl(fieldTrip.ExplorationMap.CustomBackgroundMap);

				if (fieldTrip.LocatedGameUnits != null)
				{
					fieldTrip.LocatedGameUnits = fieldTrip.LocatedGameUnits.Where(lgu => !lgu.IsDeleted).ToList();

					foreach (var locatedGameUnit in fieldTrip.LocatedGameUnits)
					{
						locatedGameUnit.InformationSheetPicture = this.PrefixPictureUrl(locatedGameUnit.InformationSheetPicture);
						locatedGameUnit.ConclusionPicture = this.PrefixPictureUrl(locatedGameUnit.ConclusionPicture);
						locatedGameUnit.LocationSupportPicture = this.PrefixPictureUrl(locatedGameUnit.LocationSupportPicture);

						var activities = locatedGameUnit.Activities.Where(a => !a.IsDeleted).ToList();
						locatedGameUnit.Activities = new();

						foreach (var item in activities)
						{
							if (item.IsDeleted)
								continue;

							if (item.ActivityType == ActivityType.Question)
							{
								var question = this.ActivitiesManager.GetQuestion(item.Id);

								question.Picture = this.PrefixPictureUrl(question.Picture);

								if (question.Clue != null)
								{
									question.Clue.Picture = this.PrefixPictureUrl(question.Clue.Picture);
								}

								if (question.QuestionActivityType == QuestionActivityType.PictureMcq)
								{
									foreach (var answer in question.Answers)
									{
										answer.Content = this.PrefixPictureUrl(answer.Content);
									}
								}

								locatedGameUnit.Activities.Add(question);
							}
							else
							{
								var statement = this.ActivitiesManager.GetStatement(item.Id);

								statement.Picture = this.PrefixPictureUrl(statement.Picture);

								locatedGameUnit.Activities.Add(statement);
							}
						}
					}
				}

				if (fieldTrip.Journeys != null)
				{
					foreach (var journey in fieldTrip.Journeys)
					{
						journey.JourneyLocatedGameUnits = journey.JourneyLocatedGameUnits.Where(jlgu => !jlgu.LocatedGameUnit.IsDeleted).ToList();

						foreach (var journeyLocatedGameUnit in journey.JourneyLocatedGameUnits)
						{
							journeyLocatedGameUnit.LocatedGameUnit.Activities = journeyLocatedGameUnit.LocatedGameUnit.Activities.Where(a => !a.IsDeleted).ToList();
						}
					}

                    var locatedGameUnitsIncludedInJourneys = fieldTrip.Journeys.SelectMany(j => j.JourneyLocatedGameUnits).Select(jlgu => jlgu.LocatedGameUnitId).Distinct().ToList();

                    fieldTrip.LocatedGameUnits = fieldTrip.LocatedGameUnits?.Where(u => locatedGameUnitsIncludedInJourneys.Contains(u.Id)).ToList();
                }

				// clear "journey" to avoid serialization loop length exception
				foreach (var jlgu in fieldTrip.Journeys.SelectMany(j => j.JourneyLocatedGameUnits))
				{
					jlgu.Journey = null;
				}
			}

			return fieldTrips;
		}

		private async Task<string> GetFileContent(string url)
		{
			if (string.IsNullOrEmpty(url))
			{
				return url;
			}

			string content;

			try
			{
				content = $"data:image/jpeg;base64,{Convert.ToBase64String(await this.FileSystem.ReadAllBytesAsync(url))}";
			}
			catch (Exception)
			{
				return url;
			}

			if (string.IsNullOrEmpty(content))
			{
				content = url;
			}

			return content;
		}

		private string PrefixPictureUrl(string url, bool expandUrl = true)
		{
			if (string.IsNullOrEmpty(url))
			{
				return null;
			}

			return (expandUrl ? this.PictureFileUrlPrefix : string.Empty) + url;
		}

		[HttpGet("test/{fieldTripId:guid}")]
		public async Task<FieldTripTestingModel> TestFieldTrip(Guid fieldTripId)
		{
			var user = await this.UsersManager.SelectAllUsers().Select(u => new
			{
				User = u,
				Roles = u.UserRoles.Select(ur => ur.Role),
			}).FirstOrDefaultAsync(u => u.User.UserName == this.Request.HttpContext.User.Identity.Name);

			List<Claim> claims = (await this.UsersManager.GetClaimsAsync(user.User)).ToList();
			claims = AccountsController.GenerateClaims(user.User, claims).ToList();

			foreach (var role in user.Roles)
			{
				claims.Add(new Claim(ClaimTypes.Role, role.Name));
			}

			var tokenValidityDuration = TimeSpan.FromHours(2);

			byte[] key = Encoding.ASCII.GetBytes(this.Configuration["Authentication:AuthenticationTokenSecretSigningKey"]);
			var tokenSigningKey = new SymmetricSecurityKey(key);
			DateTime expirationDate = DateTime.UtcNow.Add(tokenValidityDuration);

			JwtSecurityToken jwt = new(
				 claims: claims,
				 expires: DateTime.UtcNow.Add(tokenValidityDuration),
				 signingCredentials: new SigningCredentials(tokenSigningKey, SecurityAlgorithms.HmacSha256)
			 );

			JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
			string wroteToken = tokenHandler.WriteToken(jwt);

			LoginResult response = new()
			{
				AccessToken = wroteToken,
				AccessTokenValidUntil = expirationDate,
				User = user.User.ToApiResult(user.Roles)
			};

			return new FieldTripTestingModel
			{
				PlayerUrl = $"{this.Configuration["Data:SituLearn.Player:applicationUrl"]}",
				LoginResult = response,
				EditorUrl = $"{this.Configuration["Data:Network:BaseUrl"]}"
			};
        }

        [Authorize(Policy = Policy.UserCanCreateFieldTrips)]
        [HttpGet("games/{fieldTripId:guid}")]
        public async Task<IEnumerable<Player>> SelectAllPlayersByFieldTripIdAsync(Guid fieldTripId)
        {
            var result = await this.GamesManager.SelectAll(g => g.Players)
				.Where(g => g.FielTripId == fieldTripId)
				.SelectMany(g => g.Players)
				.OrderBy(p => p.StartPlayedDate)
				.ToListAsync();

            return result;
        }

        [Authorize(Policy = Policy.UserCanCreateFieldTrips)]
        [HttpGet("report/{fieldTripId:guid}")]
		[NoGeneration]
        public async Task<FileResultModel> ExportReportByFieldTripIdForAPeriodAsync(Guid fieldTripId, DateTime startDateSelected, DateTime endDateSelected)
        {
			var fieldTrip = await this.FieldTripsManager.SelectAll()
				.Include(f => f.LocatedGameUnits)
					.ThenInclude(u => u.Activities)
				.FirstOrDefaultAsync(f => f.Id == fieldTripId);

            var result = await this.GamesManager.ExportReportByFieldTripIdForAPeriodAsync(fieldTripId, startDateSelected, endDateSelected, fieldTrip);

            return new FileResultModel(Convert.ToBase64String(result), "application/zip", $"{fieldTrip.Name}_{startDateSelected.ToString("dd MM yyyy")}.zip");
        }
    }
}
