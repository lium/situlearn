// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.Components;
using SituLearn.Player.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Player.Pages.Scanner
{
    [Route(Url)]
    public partial class ScannerPage : ComponentBase
    {
        public const string Url = "/scanner";

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [CascadingParameter]
        public AppLayout Confirm { get; set; }

        [SupplyParameterFromQuery]
        public string BackUrl { get; set; }

        protected async Task OnDataFoundAsync(string data)
        {
            string[] parts = data.Split('/', StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length < 3)
            {
                Console.WriteLine("not a valid url");
                return;
            }

            bool wantsToPlay = false;
            string partyId = parts[parts.Length - 1];

            if (parts.Length >= 4 && parts[parts.Length - 3] == "jouer")
            {
                wantsToPlay = true;
                partyId = string.Join("/", parts.Skip(parts.Length - 2));
            }

            if (parts[parts.Length - 2] == "jouer")
            {
                wantsToPlay = true;
            }

            if (wantsToPlay)
            {
                if (this.FieldTripsRepository.FieldTrips == null || this.FieldTripsRepository.FieldTrips.Count == 0)
                {
                    await this.FieldTripsRepository.LoadFieldTripsHeadersAsync();
                }

                this.FieldTripToPlay = null;

                foreach (var storedFieldTrip in this.FieldTripsRepository.FieldTrips.Values)
                {
                    var tag = Convert.ToBase64String(storedFieldTrip.Id.ToByteArray()).TrimEnd('=');

                    if (tag == partyId)
                    {
                        this.FieldTripToPlay = storedFieldTrip;
                        this.StateHasChanged();
                        break;
                    }
                }

                this.StateHasChanged();

                var answer = await Confirm.AskAsync($"Voulez vous jouer � {this.FieldTripToPlay.Name} ?");

                if (this.FieldTripToPlay != null && (answer.HasValue && answer.Value))
                {                    
                    this.NavigationManager.NavigateTo(FieldTrips.FieldTripPage.GetUrl(this.FieldTripToPlay.Id));
                }

                this.FieldTripToPlay = null;
                this.StateHasChanged();
            }
        }
        public FieldTripHeaderModel FieldTripToPlay { get; set; }
    }
}