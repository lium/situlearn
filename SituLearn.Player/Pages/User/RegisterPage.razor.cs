// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using SituLearn.Player.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Player.Pages.User
{
    [AllowAnonymous]
    [Route(Url)]
    public partial class RegisterPage : ComponentBase
    {
        public const string Url = "/inscription";

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }
        [Inject]
        private IJSRuntime JSRuntime { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        public UserCreationModel Model { get; set; }

        public string[] Errors { get; set; }

        public bool IsSubmitting { get; set; }
        public string Captcha { get; set; }
        public string CaptchaInput { get; set; }
        public bool DisplayCaptcha { get; set; }
        public EditContext EditContext { get; set; }
        const int CaptchaLength = 5;

        protected override async Task OnInitializedAsync()
        {
            this.Captcha = BlazorCaptcha.Tools.GetCaptchaWord(CaptchaLength);

            this.Model = new UserCreationModel()
            {
                Role = UserRoleNames.Participant
            };

            this.EditContext = new EditContext(this.Model);
            await base.OnInitializedAsync();
        }

        public async Task SubmitRegisterAsync()
        {
            if (this.IsSubmitting)
            {
                return;
            }

            if (!this.CaptchaInput.Equals(this.Captcha, StringComparison.CurrentCultureIgnoreCase))
            {
                this.CaptchaInput = string.Empty;
                this.Notifier.Notify(Color.Danger, "Veuillez saisir le texte de l'image");
                return;
            }

            this.Errors = null;
            this.IsSubmitting = true;

            this.StateHasChanged();

            try
            {
                await this.UsersRepository.RegisterAsync(this.Model);

                this.Notifier.Notify(Color.Success, "Votre compte a bien �t� cr��. Pour finaliser votre inscription, veuillez maintenant consulter votre messagerie afin d'initialiser votre mot de passe.");

                this.NavigationManager.NavigateTo(LoginPage.Url);
            }
            catch (Ozytis.Common.Core.Utilities.BusinessException ex)
            {
                this.Notifier.Notify(Color.Danger, "Erreur lors de la mise � jour des donn�es");
                this.Errors = ex.Messages ?? new[] { ex.Message };
            }

            this.IsSubmitting = false;

            this.StateHasChanged();

            
        }

        public void ShowCaptcha()
        {
            ICollection<ValidationResult> results = new List<ValidationResult>();
            bool isValid = Validator.TryValidateObject(this.Model, new ValidationContext(this.Model), results, true);
            if (isValid)
            {
                this.DisplayCaptcha = true;
                this.StateHasChanged();
            }
            else
            {
                this.Errors = results.Select(r => r.ErrorMessage).ToArray();
                this.StateHasChanged();
            }
        }

        public async Task OnKeyDown(KeyboardEventArgs args)
        {
            if (args.Key == "Enter")
            {
                await this.JSRuntime.InvokeVoidAsync("window.blurActiveElement");
            }
        }

    }
}