﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Ozytis.Common.Core.Utilities;

using System;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.User
{
    public partial class MyEmailEdition : ComponentBase
    {
        [Parameter]
        public Func<Task> OnCloseAsync { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        public EmailUpdateModel Model { get; set; }

        public string[] Errors { get; set; }

        public bool IsProcessing { get; set; }

        public bool IsSuccess { get; set; }

        protected async Task ProcessAsync()
        {
            if (this.IsProcessing)
            {
                return;
            }

            this.IsProcessing = true;
            this.IsSuccess = false;
            this.Errors = null;

            try
            {
                await this.UsersRepository.RequestEmailChangeAsync(this.Model);
                
                this.Notifier.Notify(Color.Success, $"Votre demande a bien été prise en compte. Un email contenant les instructions vous permettant de valider votre demande vous a été envoyé à l'adresse {this.Model.Email}");

                await this.OnCloseAsync();
                this.IsSuccess = true;               
            }
            catch (BusinessException ex)
            {
                this.Errors = ex.Messages;
            }

            this.IsProcessing = false;
        }

        protected override void OnInitialized()
        {
            this.Model = new EmailUpdateModel
            {
                Email = this.UsersRepository.CurrentUser.Email
            };

            base.OnInitialized();
        }
    }
}
