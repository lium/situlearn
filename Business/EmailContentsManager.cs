﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using DataAccess;
using Entities;
using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using Ozytis.Common.Core.Storage;
using Ozytis.Common.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business
{
	public class EmailContentsManager : BaseEntityManager<EmailContent>
    {
        public const string NewAccountEmail = "NewAccount";
        public const string PasswordLostEmail = "PasswordLost";
        public const string MessageEmail = "Message";
        public const string AccountConfirmedEmail = "AccountConfirmed";
        public const string ValidateAccountEmail = "ValidateAccount";
        public const string CardIsExpiringEmail = "CardIsExpiring";
        public const string SubscriptionIsExpiringEmail = "SubscriptionIsExpiring";
        public const string FreePeriodIsExpiringEmail = "FreePeriodIsExpiring";
        public const string FirmAdminAccountCreatedEmail = "FirmAdminAccountCreated";
        public const string FirmAdminAccessGrantedEmail = "FirmAdminAccessGranted";
        public const string FirmCustomerAccountCreatedEmail = "FirmCustomerAccountCreated";
        public const string FirmCustomerAccountGrantedEmail = "FirmCustomerAccountGranted";        
        public const string ConfirmEmailAddressEmail = "ConfirmEmailAddress";

        public EmailContentsManager(DataContext context, IFileSystem fileSystem) : base(context)
        {
            this.FileSystem = fileSystem;
        }

        public IFileSystem FileSystem { get; }

        public async Task InstallTemplatesAsync()
        {
            List<EmailContent> basicEmails = new()
            {
                new EmailContent()
                {
                    Name = ConfirmEmailAddressEmail,
                    Language = "fr-FR",
                    Subject = "SITULEARN - Veuillez confirmer votre nouvelle adresse",
                    Content = @"><p>Bonjour <span class=""merge-var"" data-display=""Prénom"" data-var=""FirstName"" contenteditable=""false"">﻿<span contenteditable=""false"">Prénom</span>﻿</span> <span class=""merge-var"" data-display=""Nom"" data-var=""LastName"" contenteditable=""false"">﻿<span contenteditable=""false"">Nom</span>﻿</span></p><p>afin de finaliser votre demande de changement d'adresse email, nous vous invitons à cliquer sur le lien suivant :</p><p><span class=""merge-var"" data-display=""Lien"" data-var=""Link"" contenteditable=""false"">﻿<span contenteditable=""false"">Lien</span>﻿</span></p><p><br></p><p>Cordialement,</p><p>L'équipe SITULEARN</p>",
                    Variables = @"[{""Display"":""Prénom"",""Id"":""FirstName""},{""Display"":""Nom"",""Id"":""LastName""},{""Display"":""Lien"",""Id"":""Link""}]"
                },
                new EmailContent()
                {
                    Name = PasswordLostEmail,
                    Language = "fr-FR",
                    Subject = "SITULEARN - Récupération de votre mot de passe",
                    Content = @"<p>Madame, Monsieur</p><p><span style=""color: rgb(37, 37, 37);"">Vous avez demandé à réinitialiser votre&nbsp;mot de passe. Pour terminer cette procédure, veuillez cliquer sur </span>le bouton suivant : <span class=""merge-var"" data-display=""Lien de récupération"" data-var=""Link"" contenteditable=""false"">﻿<span contenteditable=""false"">Lien de récupération</span>﻿</span></p><p>Vous aurez alors accès à une page vous permettant de définir votre nouveau mot de passe</p><p>Cordialement,</p><p>L'équipe SITULEARN</p>",
                    Variables = @"[{""Id"":""Link"",""Display"":""Lien de récupération"",""Html"":""<div class=\\\""grapevar\\\""><a href=\\\""@Link\\\"">Lien de récupération</a></div>""}]"
                },
                new EmailContent()
                {
                    Name = NewAccountEmail,
                    Language = "fr-FR",
                    Subject = "SITULEARN - Initialisation du mot de passe",
                    Content = @"<div><p>Bonjour <span class=""merge-var"" data-display=""Civilité Prénom Nom"" data-var=""UserNameAndCivility"" contenteditable=""false"">﻿<span contenteditable=""false"">Civilité Prénom Nom</span></span></p><p>Afin de définir votre mot de passe, veuillez cliquer sur le bouton suivant : <span class=""merge-var"" data-display=""Lien d'initialisation du mot de passe"" data-var=""Link"" contenteditable=""false"">﻿<span contenteditable=""false"">Lien d'initialisation du mot de passe</span>﻿</span></p></div>",
                    Variables = @"[{""id"": ""UserNameAndCivility"",""display"": ""Nom et civilité"",""html"": ""<div class=\""grapevar\"">@UserNameAndCivility</div>""},{""id"": ""Link"",""display"": ""Lien d'initialisation du mot de passe"",""html"": ""<div class=\""grapevar\"">@Link</div>""}]"
                }
            };

            using (this.DataContext)
            {
                foreach (var item in basicEmails)
                {
                    if (!this.DataContext.EmailContents.Any(x => x.Name == item.Name))
                    {
                        this.DataContext.EmailContents.Add(item);
                    }
                }

                await this.DataContext.SaveChangesAsync();
            };
        }

        public async Task<EmailContent> CreateEmailContentAsync(EmailContent emailContent)
        {
            var result = this.DataContext.EmailContents.Add(emailContent).Entity;
            await this.SaveChangesAsync();
            return result;
        }

        public async Task DeleteEmailAsync(int emailId)
        {
            var email = await this.SelectAsync(emailId);

            if (email == null)
            {
                return;
            }

            this.DataContext.EmailContents.Remove(email);
            await this.SaveChangesAsync();
        }

        public async Task<EmailContent> GetEmailTemplateAsync(string name, string language)
        {
            var results = await this.SelectAll()
                .Where(t => t.Name == name && (t.Language == language || t.Language == "fr-FR"))
                .ToArrayAsync();

            return results.FirstOrDefault(t => t.Language == language) ?? results.FirstOrDefault();
        }

        public static string PopulateEmail(string content, IEnumerable<EmailTemplateVar> variables)
        {
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml("<div style=\"font-size:13px;font-family:'Helvetica, Arial, sans-serif'\"><table style='width:100%'><tr><td></td><td style='width:800px' id='mainCell'>" + content + "</td><td></td></tr></table></div>");

            foreach (var variable in variables)
            {
                var elements = htmlDocument.DocumentNode.SelectNodes($"//*[@data-var='{variable.Id}']");

                if (elements != null)
                {
                    foreach (var element in elements)
                    {
                        switch (variable.ReplacementType)
                        {
                            case EmailTemplateVarReplacementType.Text:
                                element.InnerHtml = variable.Replacement;
                                break;
                            case EmailTemplateVarReplacementType.Link:

                                var link = htmlDocument.CreateElement("a");
                                link.SetAttributeValue("href", variable.Replacement);
                                link.InnerHtml = variable.Replacement;

                                element.ParentNode.ReplaceChild(link, element);

                                break;
                            case EmailTemplateVarReplacementType.Button:

                                var buttonContainer = htmlDocument.CreateElement("div");
                                buttonContainer.SetAttributeValue("style", "text-align:center; padding:6px;");

                                var button = htmlDocument.CreateElement("a");
                                button.SetAttributeValue("href", variable.Replacement);
                                button.SetAttributeValue("style", "display:block; padding:6px;text-align:center;text-decoration:none;border-radius:3px;background-color:#009bd9;color:#fff");
                                button.InnerHtml = variable.NewText ?? variable.Replacement;

                                buttonContainer.AppendChild(button);

                                element.ParentNode.ReplaceChild(buttonContainer, element);
                                break;
                            default:
                                break;
                        }

                        element.RemoveClass();
                    }
                }
            }

            return htmlDocument.DocumentNode.OuterHtml;
        }

        public async Task<EmailContent> GetPopulatedEmailTemplateAsync(string name, string language, IEnumerable<EmailTemplateVar> variables)
        {
            var content = await this.GetEmailTemplateAsync(name, language);

            if (content == null)
            {
                return null;
            }

            if (variables != null && variables.Any())
            {
                content.Content = PopulateEmail(content.Content, variables);
            }

            return content;
        }

        public async Task<EmailContent> UpdateEmailAsync(EmailContent emailContent)
        {
            var email = await this.SelectAsync(emailContent.Id);

            if (email == null)
            {
                return email;
            }

            string content = emailContent.Content;

            if (!string.IsNullOrEmpty(content))
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(content);

                var dataImg = htmlDocument.DocumentNode.SelectNodes("//img[starts-with(@src,'data:')]");

                if (dataImg != null)
                {
                    foreach (var image in dataImg)
                    {
                        string path = $"images/{image.Attributes["src"].Value.ToMd5()}";
                        await this.FileSystem.SaveBase64FileAsync(path, image.Attributes["src"].Value);
                        image.SetAttributeValue("src", this.FileSystem.GetUrl(path));
                    }
                }

                content = htmlDocument.DocumentNode.OuterHtml;
            }

            email.Subject = emailContent.Subject;
            email.Content = content;
            //email.Language = emailContent.Language;
            email.Variables = emailContent.Variables;
            email.Name = emailContent.Name;

            await this.SaveChangesAsync();

            return email;
        }

        public async Task<EmailContent> GetEmailContentAsync(int emailContentId)
        {
            EmailContent email = await this.SelectAll().FirstOrDefaultAsync(ec => ec.Id == emailContentId);

            if (email == null)
            {
                return email;
            }

            string content = email.Content;

            if (!string.IsNullOrEmpty(content))
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(content);

                var dataImg = htmlDocument.DocumentNode.SelectNodes("//img[starts-with(@src,'images/')]");

                if (dataImg != null)
                {
                    foreach (var image in dataImg)
                    {
                        string src = $"data:image/jpeg;base64,{Convert.ToBase64String(await this.FileSystem.ReadAllBytesAsync(image.Attributes["src"].Value))}";
                        image.SetAttributeValue("src", src);
                    }
                }

                content = htmlDocument.DocumentNode.OuterHtml;

                email.Content = content;
            }

            return email;
        }

        public class EmailTemplateVar
        {
            public string Id { get; set; }

            public string Replacement { get; set; }

            public string NewText { get; set; }

            public EmailTemplateVarReplacementType ReplacementType { get; set; }
        }

        public enum EmailTemplateVarReplacementType
        {
            Text,
            Link,
            Button
        }
    }
}
