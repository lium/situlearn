﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Ozytis.Common.Core.Utilities;
using System;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.User
{
    public partial class MyPhoneNumberEdition : ComponentBase
    {
        public int Step { get; set; }

        [Parameter]
        public Func<Task> OnCloseAsync { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        public PhoneNumberUpdateModel Model { get; set; }

        public PhoneNumberConfirmationModel ConfirmModel { get; set; } = new PhoneNumberConfirmationModel();

        public string[] Errors { get; set; }

        public bool IsProcessingStep1 { get; set; }

        public bool IsProcessingStep2 { get; set; }

        protected async Task ProcessStep1Async()
        {
            if (this.IsProcessingStep1)
            {
                return;
            }

            this.IsProcessingStep1 = true;
            this.Errors = null;

            try
            {
                if (await this.UsersRepository.RequestPhoneNumberChangeAsync(this.Model))
                {
                    this.Step = 1;
                    this.ConfirmModel.PhoneNumber = this.Model.PhoneNumber;
                }
                else
                {
                    await this.OnCloseAsync();
                }
            }
            catch (BusinessException ex)
            {
                this.Errors = ex.Messages;
            }

            this.IsProcessingStep1 = false;
        }

        protected async Task ProcessStep2Async()
        {
            if (this.IsProcessingStep2)
            {
                return;
            }

            this.IsProcessingStep2 = true;
            this.Errors = null;

            try
            {
                await this.UsersRepository.ConfirmPhoneNumberChangeAsync(this.ConfirmModel);
                this.Notifier.Notify(Color.Success, "Votre numéro de téléphone a bien été validé");
                await this.OnCloseAsync();

                this.IsProcessingStep2 = false;
            }
            catch (BusinessException ex)
            {
                this.Errors = ex.Messages;
            }

            this.IsProcessingStep2 = false;
        }

        protected override void OnInitialized()
        {
            this.Model = new PhoneNumberUpdateModel
            {
                PhoneNumber = this.UsersRepository.CurrentUser.PhoneNumber
            };

            base.OnInitialized();
        }
    }
}
