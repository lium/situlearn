﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api
{
    public class PlayerForMonitorModel
    {
        public Guid Id { get; set; }

        public string UserId { get; set; }

        //TODO à remettre ?
        //public Guid JourneyId { get; set; }

        public string Name { get; set; }

        public string Color { get; set; }

        public int PlayedUnitsNumber { get; set; }

        //TODO à enlever ?
        public int UnitsToPlayNumber { get; set; }

        public string PhoneNumber { get; set; }

        //TODO à remettre ?
        //public Guid CurrentUnitId { get; set; }

        //TODO à enlever ?
        public string CurrentUnitName { get; set; }

        public int Score { get; set; }

        public int TotalPointsToWin { get; set; }

        public int CorrectAnswersNumber { get; set; }

        public int AnsweredQuestionsNumber { get; set; }

        public int ReadingsNumber { get; set; }

        public decimal CurrentLatitude { get; set; }

        public decimal CurrentLongitude { get; set; }

        public bool OutOfExplorationAreaAlert { get; set; }

        public bool LateAlert { get; set; }

        public bool LowBatteryAlert { get; set; }

        public bool IsHidden { get; set; }

        public bool ShowDescription { get; set; }
    }
}
