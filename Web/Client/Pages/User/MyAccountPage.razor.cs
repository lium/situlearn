﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.User
{
    [Route(MyAccountPage.Url)]
    [Microsoft.AspNetCore.Authorization.Authorize]
    public partial class MyAccountPage : ComponentBase
    {
        public const string Url = "/mon-compte";

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [CascadingParameter]
        public IConfirmProvider Confirm { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public bool IsDeleting { get; set; }

        protected override void OnInitialized()
        {
            this.Email = this.UsersRepository.CurrentUser.Email;
            this.PhoneNumber = this.UsersRepository.CurrentUser.PhoneNumber;
            base.OnInitialized();
        }

        public bool ShowEmailEditionModal { get; set; }

        public async Task CloseEmailEditionAsync()
        {
            await this.UsersRepository.RefreshCurrentUserAsync();
            this.Email = this.UsersRepository.CurrentUser.Email;
            this.ShowEmailEditionModal = false;
            this.StateHasChanged();
        }

        public bool ShowPhoneNumberEditionModal { get; set; }

        public void EditPhoneNumber()
        {
            this.ShowPhoneNumberEditionModal = true;
        }

        public async Task ClosePhoneNumberEdition()
        {
            await this.UsersRepository.RefreshCurrentUserAsync();
            this.PhoneNumber = this.UsersRepository.CurrentUser.PhoneNumber;
            this.ShowPhoneNumberEditionModal = false;
            this.StateHasChanged();
        }

        public async Task DeleteMyAccount()
        {
            if (this.IsDeleting)
            {
                return;
            }

            this.IsDeleting = true;

            if (!await Confirm.AskAsync("Etes vous sûr de vouloir supprimer votre compte ? Cette action est irréversible."))
            {
                this.IsDeleting = false;
                return;
            }

            try
            {
                await this.UsersRepository.DeleteUserAsync(this.UsersRepository.CurrentUser.Id);

                this.Notifier.Notify(Color.Success, "Votre compte a bien été supprimé. Vous allez être déconnecté.");
                await Task.Delay(2000);

                await this.UsersRepository.LogoutAsync();
            }
            catch (Exception e)
            {
                this.Notifier.Notify(Color.Danger, e.Message);
            }
            finally
            {
                this.IsDeleting = false;
                this.StateHasChanged();
            }
        }
    }
}
