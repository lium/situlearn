﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SituLearn.Player.Utilities
{
    public static class EnumExtensions
    {
        public static IEnumerable<OptionItem<T>> ToOptionItems<T>(this Type enumerationType)
        {
            var result = new List<OptionItem<T>>();

            foreach (var value in Enum.GetValues(enumerationType))
            {
                FieldInfo fieldInfo = enumerationType.GetField(value.ToString());

                DescriptionAttribute attribute = (DescriptionAttribute)fieldInfo.GetCustomAttribute(typeof(DescriptionAttribute));

                if (attribute != null)
                {
                    result.Add(new OptionItem<T>(attribute.Description, (T)value));
                }
            }

            return result;
        }

        public static string GetFriendlyName(this object value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            var type = value.GetType();
            var enumValue = Enum.GetName(type, value);
            var fieldInfo = type.GetField(enumValue);

            DescriptionAttribute attribute = (DescriptionAttribute)fieldInfo.GetCustomAttribute(typeof(DescriptionAttribute));

            if (attribute != null)
            {
                return attribute.Description;
            }

            return string.Empty;
        }

        public static T ConvertToBitFlags<T>(this int[] flags)
        {
            string strFlags = string.Empty;

            if (flags != null)
            {                
                foreach (var flag in flags)
                {
                    strFlags += strFlags == string.Empty ?  Enum.GetName(typeof(T), flag) : "," + Enum.GetName(typeof(T), flag);
                }
            }

            return (T)Enum.Parse(typeof(T), strFlags);
        }
    }
}
