﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using ClientApi;
using Entities;
using Ozytis.Common.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Web.Client.Repositories
{
    public class ContextualHelpsRepository
    {
        private readonly ContextualHelpsService contextualHelpsService = new();

        public async Task<IEnumerable<ContextualHelp>> GetAllContextualHelpsAsync(Expression<Func<IQueryable<ContextualHelp>, IQueryable<ContextualHelp>>> query)
        {
            return await this.contextualHelpsService.GetAllContextualHelpsAsync(query);
        }

        public async Task<ContextualHelp> GetContextualHelpAsync(string contextualHelpId)
        {
            return await this.contextualHelpsService.GetContextualHelpAsync(contextualHelpId);
        }

        public async Task<ContextualHelp> UpdateContextualHelpAsync(ContextualHelpUpdateModel model)
        {
            var result = await this.contextualHelpsService.UpdateContextualHelpAsync(model);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }

            return result.Data;
        }
    }
}
