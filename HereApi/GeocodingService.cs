﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

using System.Web;

namespace HereApi
{
    public class GeocodingService
    {
        public GeocodingService(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public async Task<GeocodingItem[]> GeocodeAsync(string query)
        {
            string url = $"https://discover.search.hereapi.com/v1/geocode?q={HttpUtility.UrlEncode(query)}&limit=5&apiKey={this.Configuration["Data:Here:Api"]}";

            using HttpClient httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync(url);

            return JsonConvert.DeserializeObject<GeocodingResult>(json).Items;
        }
    }
}