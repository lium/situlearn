﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api.PedagogicalFields;
using Business;
using Common.Security;
using Entities;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.WebApi;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Server.ApiTransform;

namespace Web.Server.Controllers
{
    [Route("api/pedagogical-fields")]
    public class PedagogicalFieldsController : ControllerBase
    {
        public PedagogicalFieldsController(UsersManager usersManager, PedagogicalFieldsManager pedagogicalFieldsManager)
        {
            this.UsersManager = usersManager;
            this.PedagogicalFieldsManager = pedagogicalFieldsManager;
        }

        public UsersManager UsersManager { get; }
        public PedagogicalFieldsManager PedagogicalFieldsManager { get; }

        [HttpGet]
        [WebApiQueryable2(nameof(PedagogicalField))]
        public async Task<IEnumerable<PedagogicalField>> GetAllPedagogicalFieldsAsync()
        {
            var pedagogicalFields = await this.PedagogicalFieldsManager
                .SelectAll()
                .ApplyQueryV2Options(this.Request)
                .ToArrayAsync();

            return pedagogicalFields.ToArray();
        }

        [HttpGet("{id:int}")]
        public async Task<PedagogicalField> GetPedagocialField(int id)
        {
            var field = await this.PedagogicalFieldsManager.SelectAsync(id);
            return field;
        }

        [HttpPost]
        [Authorize(Policy = Policy.UserIsAdministrator)]
        public async Task<PedagogicalField> CreatePedagogicalFieldAsync([FromBody] PedagogicalFieldCreateModel model)
        {
            var newField = await this.PedagogicalFieldsManager.Create(model);
            return newField;
        }

        [HttpPut]
        [Authorize(Policy = Policy.UserIsAdministrator)]
        public async Task<PedagogicalField> UpdatePedagogicalFieldAsync([FromBody] PedagogicalFieldUpdateModel model)
        {
            var newField = await this.PedagogicalFieldsManager.Update(model);
            return newField;
        }

        [HttpDelete]
        [Authorize(Policy = Policy.UserIsAdministrator)]
        public async Task DeletePedagogicalField(int fieldId)
        {
            await this.PedagogicalFieldsManager.Delete(fieldId);
        }
    }
}
