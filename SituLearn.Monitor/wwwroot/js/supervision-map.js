﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

/* This function is called at Blazor component startup to bind listeners on component instance.
** eventName is the event name we want to trigger
 * cSharpListenerArgsAsJson is the serialisation of the object we want to send to the component method
 * eventDatasetKeys is an array of properties we can retrieve from the html component's dataset
 *
 * the jsonized argument is made of the basic [cSharpListenerArgsAsJson] string where we replace {X} by the value in the dataset where name is the eventDatasetKeys at key X
*/
function addCustomEventListener(dotNetObjectRef, eventName, cSharpListenerArgsAsJson, eventDatasetKeys) {
    document.addEventListener(eventName, (event) => {
        var finalArg = cSharpListenerArgsAsJson;
        for (var i = 0; i < eventDatasetKeys.length; i++) {
            var value = event.detail[eventDatasetKeys[i]];
            finalArg = finalArg.replace("{" + i + "}", value);
        }

        // Calls a method by name with the [JSInokable] attribute (above)
        dotNetObjectRef.invokeMethodAsync('OnCustomEvent', finalArg);
    });
}


function displayPlayerInformation(e) {
    var progressEvent = new CustomEvent("display-player-information", { detail: e.dataset });

    document.dispatchEvent(progressEvent);
}