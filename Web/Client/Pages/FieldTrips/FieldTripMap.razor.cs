// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Entities;
using Microsoft.AspNetCore.Components;
using Ozytis.Common.Core.Web.Razor.Leaflet;
using Ozytis.Common.Core.Web.Razor.Utilities;
using System.Threading.Tasks;

namespace Web.Client.Pages.FieldTrips
{
	public partial class FieldTripMap : ComponentBase
    {
        private FieldTrip fieldTrip;

        bool rerender = false;

        [Parameter]
        public FieldTrip FieldTrip
        {
            get
            {
                return fieldTrip;
            }
            set
            {
                if (fieldTrip != value)
                {
                    fieldTrip = value;
                    rerender = true;
                }
            }
        }

        [Parameter]
        public bool FullScreen { get; set; }

		[Parameter]
        public bool ShowTileLayersSwitch { get; set; }

        [Parameter]
        public bool? PrintOptionsExportOnly { get; set; }

        [Inject]
        public GeoLocationService LocationService { get; set; }

        public LeafletCoordinates MapCenter { get; set; }

        public LeafletMap Map { get; set; }
        public LeafletMapPrintOptions PrintOptions { get; set; }

        public bool ShowMarkers { get; set; } = true;

        protected override async Task OnInitializedAsync()
        {
            if (this.FieldTrip?.ExplorationMap?.CenterLatitude != null)
            {
                this.MapCenter = new LeafletCoordinates
                {
                    Latitude = this.FieldTrip.ExplorationMap.CenterLatitude.Value,
                    Longitude = this.FieldTrip.ExplorationMap.CenterLongitude.Value,
                };

                this.StateHasChanged();
            }
            else
            {
                var center = await this.LocationService.GetCurrentGeoLocationAsync();

                if (center != null)
                {
                    this.MapCenter = new LeafletCoordinates
                    {
                        Latitude = center.Location.Coords.Latitude,
                        Longitude = center.Location.Coords.Longitude,
                    };

                    this.StateHasChanged();
                }
            }
        }

        public bool MarkersAreFit { get; set; }

        private System.Timers.Timer Timer;

        protected override async Task OnParametersSetAsync()
        {
            if (rerender && this.FieldTrip != null)
            {
                if (this.FieldTrip.ExplorationMap?.CenterLatitude != null)
                {
                    this.MapCenter = new LeafletCoordinates
                    {
                        Latitude = this.FieldTrip.ExplorationMap.CenterLatitude.Value,
                        Longitude = this.FieldTrip.ExplorationMap.CenterLongitude.Value,
                    };
                }
                else
                {
                    var center = await this.LocationService.GetCurrentGeoLocationAsync();

                    if (center != null)
                    {
                        this.MapCenter = new LeafletCoordinates
                        {
                            Latitude = center.Location.Coords.Latitude,
                            Longitude = center.Location.Coords.Longitude,
                        };

                        this.StateHasChanged();
                    }
                }

                this.StateHasChanged();

                this.Timer = new System.Timers.Timer(1000);
                this.Timer.Elapsed += async (sender, args) =>
                {
                    if (this.FieldTrip.ExplorationMap.NorthEastLatitude.HasValue)
                    {
                        var bounds = new Bounds
                        {
                            NorthEast = new()
                            {
                                Latitude = this.FieldTrip.ExplorationMap.NorthEastLatitude.Value,
                                Longitude = this.FieldTrip.ExplorationMap.NorthEastLongitude.Value,
                            },
                            SouthWest = new()
                            {
                                Longitude = this.FieldTrip.ExplorationMap.SouthWestLongitude.Value,
                                Latitude = this.FieldTrip.ExplorationMap.SouthWestLatitude.Value,
                            }
                        };

                        if (this.FullScreen)
                        {
                            await this.Map.RefreshAsync();
                        }

                        await this.Map.FitBoundsAsync(bounds);
                        await this.Map.SetMaxBoundsAsync(bounds);
                        await this.Map.RefreshAsync();
                    }
                };

                this.Timer.AutoReset = false;
                this.Timer.Enabled = true;

                rerender = false;
            }

            await Task.CompletedTask;
        }
    }
}