// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Entities;
using SituLearn.Monitor.Repositories;
using System;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Web.Razor.Leaflet;
using System.Text.Json;
using System.Linq;
using Ozytis.Common.Core.Web.Razor.Utilities;
using Ozytis.Common.Core.Web.Razor;
using Api;
using System.Collections.Generic;

namespace SituLearn.Monitor.Pages.Supervision
{
    public partial class SupervisionMap : ComponentBase
    {
        [Parameter]
        public FieldTrip FieldTrip { get; set; }

        [Parameter]
        public List<PlayerForMonitorModel> Players { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        public IConfiguration Configuration { get; set; }

        //[Inject]
        //private IJSRuntime JSRuntime { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        public string BasePictureUrl { get; set; }

        private CustomEventInterop Interop { get; set; }

        public LeafletMap Map { get; set; }

        public bool ShowPlayerDetailsWindow { get; set; }

        public PlayerForMonitorModel PlayerInformationToShow { get; set; }

        public bool Rerender { get; set; } = true;

        public LeafletCoordinates MapCenter { get; set; }

        private System.Timers.Timer Timer;

        protected override async Task OnInitializedAsync()
        {
            this.BasePictureUrl = this.Configuration["ServerUrl"] + "files?path=";
            
            await base.OnInitializedAsync();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender)
            {
                //await this.Map.FitAllMarkers();
                //return;
                if (this.Players != null && this.Players.Any(p => string.IsNullOrEmpty(p.Color)))
                {
                    this.Players = (await this.GamesRepository.AssignColorsToPlayers(this.Players)).ToList();

                    this.StateHasChanged();
                }
            }

            //await CommonScriptLoader.EnsureOzytisCommonIsPresentAsync(this.JSRuntime);

            //this.Interop = new(JSRuntime);

            //await JSRuntime.InvokeVoidAsync("ozytis.loadScript", "js/supervision-map.js?v=" + DateTime.UtcNow.Ticks);

            //await this.Interop.SetupCustomEventCallback(args => DisplayPlayerInformationModal(args), "display-player-information", "{\"Coordinates\": { \"Latitude\": {0}, \"Longitude\": {1} }, \"PlayerId\": \"{2}\" }", new string[] { "lat", "lng", "playerId" });

            await base.OnAfterRenderAsync(firstRender);
        }

        protected override async Task OnParametersSetAsync()
        {
            if (this.Rerender && this.FieldTrip != null)
            {
                if (this.FieldTrip.ExplorationMap?.CenterLatitude != null)
                {
                    this.MapCenter = new LeafletCoordinates
                    {
                        Latitude = this.FieldTrip.ExplorationMap.CenterLatitude.Value,
                        Longitude = this.FieldTrip.ExplorationMap.CenterLongitude.Value,
                    };
                }

                this.StateHasChanged();

                this.Timer = new System.Timers.Timer(1000);
                this.Timer.Elapsed += async (sender, args) =>
                {
                    //if (this.FieldTrip.ExplorationMap.NorthEastLatitude.HasValue)
                    //{
                    //    var bounds = new Bounds
                    //    {
                    //        NorthEast = new()
                    //        {
                    //            Latitude = this.FieldTrip.ExplorationMap.NorthEastLatitude.Value,
                    //            Longitude = this.FieldTrip.ExplorationMap.NorthEastLongitude.Value,
                    //        },
                    //        SouthWest = new()
                    //        {
                    //            Longitude = this.FieldTrip.ExplorationMap.SouthWestLongitude.Value,
                    //            Latitude = this.FieldTrip.ExplorationMap.SouthWestLatitude.Value,
                    //        }
                    //    };

                    //    await this.Map.FitBoundsAsync(bounds);
                    //    await this.Map.SetMaxBoundsAsync(bounds);
                    //    await this.Map.RefreshAsync();
                    //}

                    await this.Map.FitAllMarkers();
                };

                this.Timer.AutoReset = false;
                this.Timer.Enabled = true;

                this.Rerender = false;
            }

            await Task.CompletedTask;
        }

        //public async Task DisplayPlayerInformationModal(object args)
        //{
        //    DisplayInformationSheetEventArgs mapArgs = JsonSerializer.Deserialize<DisplayInformationSheetEventArgs>(args.ToString());

        //    if (!mapArgs.PlayerId.HasValue)
        //    {
        //        return;
        //    }

        //    if (mapArgs != null)
        //    {
        //        this.PlayerInformationToShow = this.Players.FirstOrDefault(p => p.Id == mapArgs.PlayerId);

        //        if (this.PlayerInformationToShow != null)
        //        {
        //            this.ShowPlayerInformation = true;
        //            this.StateHasChanged();
        //        }
        //    }

        //    await Task.CompletedTask;
        //}

        public async Task DisplayPlayerInformationModal(PlayerForMonitorModel player)
        {
            this.PlayerInformationToShow = player;

            if (this.PlayerInformationToShow != null)
            {
                this.ShowPlayerDetailsWindow = true;
                await this.Map.RefreshAsync();
                this.StateHasChanged();
            }

            await Task.CompletedTask;
        }

        public void ClosePlayerInformationModal()
        {
            this.ShowPlayerDetailsWindow = false;
            this.PlayerInformationToShow = null;
            this.StateHasChanged();
        }
    }
}

//public class DisplayInformationSheetEventArgs : MapEventArgs
//{
//    public Guid? PlayerId { get; set; }
//}