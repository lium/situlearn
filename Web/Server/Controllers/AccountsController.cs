﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Business;
using Common;
using Common.Security;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Ozytis.Common.Core.Api;
using Ozytis.Common.Core.Storage;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.WebApi;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Web.ApiTransform;

namespace Web.Controllers
{
    [Route("api/accounts")]
    public class AccountsController : ControllerBase
    {
        public AccountsController(UsersManager usersManager, SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration, ForbiddenTokenManager forbiddenTokenManager, SmsManager smsManager,
            IFileSystem fileSystem, EmailsManager emailsManager)
        {
            this.UsersManager = usersManager;
            this.SignInManager = signInManager;
            this.Configuration = configuration;
            this.ForbiddenTokenManager = forbiddenTokenManager;
            this.SmsManager = smsManager;
            this.FileSystem = fileSystem;
            this.EmailsManager = emailsManager;
        }

        public UsersManager UsersManager { get; }

        public SignInManager<ApplicationUser> SignInManager { get; }

        public IConfiguration Configuration { get; }

        public ForbiddenTokenManager ForbiddenTokenManager { get; }

        public SmsManager SmsManager { get; }

        public IFileSystem FileSystem { get; }

        public EmailsManager EmailsManager { get; }

		[NoGeneration]
        public static IEnumerable<Claim> GenerateClaims(ApplicationUser user, List<Claim> claims)
        {
            claims.Add(new Claim(ClaimTypes.Name, user.UserName.ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.UserName));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToUnixStamp().ToString(CultureInfo.InvariantCulture), ClaimValueTypes.Integer64));

            return claims;
        }

        [HttpPost("login"), ValidateModel, HandleBusinessException, AllowAnonymous]
        public async Task<LoginResult> Login([FromBody] LoginModel model)
        {
            if (model == null)
            {
                Console.WriteLine("Model null");
                throw new BusinessException("Données invalides");
            }

            Microsoft.AspNetCore.Identity.SignInResult result = null;

            try
            {
                result = await this.SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.StayConnected, false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new BusinessException("Données invalides");
            }

            if (!result.Succeeded)
            {
                Console.WriteLine($"Invalid data {model.UserName} {model.Password}");
                throw new BusinessException("Données invalides");
            }

            var user = await this.UsersManager.SelectAllUsers().Select(u => new
            {
                User = u,
                Roles = u.UserRoles.Select(ur => ur.Role),
            }).FirstOrDefaultAsync(u => u.User.UserName == model.UserName);

            if (user.Roles.Count() == 1 && user.Roles.Any(r => r.Name == UserRoleNames.Participant) && Request.Headers.ContainsKey("Origin"))
            {
                var backOfficeUrl = this.Configuration["Data:Network:BaseUrl"];
                if (backOfficeUrl.StartsWith(Request.Headers["Origin"], StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new BusinessException("Vous n'avez pas le droit de vous connecter à l'interface d'administration");
                }

                var monitorUrl = this.Configuration["Data:SituLearn.Monitor:applicationUrl"];
                if (monitorUrl.StartsWith(Request.Headers["Origin"], StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new BusinessException("Vous n'avez pas le droit de vous connecter à l'application SituLearn Monitor");
                }
            }

            List<Claim> claims = (await this.UsersManager.GetClaimsAsync(user.User)).ToList();
            claims = AccountsController.GenerateClaims(user.User, claims).ToList();

            foreach (var role in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Name));
            }

            byte[] key = Encoding.ASCII.GetBytes(this.Configuration["Authentication:AuthenticationTokenSecretSigningKey"]);
            var tokenSigningKey = new SymmetricSecurityKey(key);
            DateTime expirationDate = DateTime.UtcNow.Add(new TimeSpan(model.StayConnected ? 30 : 1, 0, 0, 0));

            JwtSecurityToken jwt = new JwtSecurityToken
            (
                 claims: claims,
                 expires: DateTime.UtcNow.Add(model.StayConnected ? TimeSpan.FromDays(365) : TimeSpan.FromHours(10)),
                 signingCredentials: new SigningCredentials(tokenSigningKey, SecurityAlgorithms.HmacSha256)
             );

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            string wroteToken = tokenHandler.WriteToken(jwt);

            LoginResult response = new LoginResult
            {
                AccessToken = wroteToken,
                AccessTokenValidUntil = expirationDate,
                User = user.User.ToApiResult(user.Roles)
            };

            return response;
        }

        [Authorize]
        [HttpGet("temptoken")]
        public async Task<TempTokenModel> GetTempTokenAsync()
        {
            byte[] key = Encoding.ASCII.GetBytes(this.Configuration["Authentication:AuthenticationTokenSecretSigningKey"]);
            var tokenSigningKey = new SymmetricSecurityKey(key);

            var user = await this.UsersManager.SelectAllUsers().Select(u => new
            {
                User = u,
                Roles = u.UserRoles.Select(ur => ur.Role)
            }).FirstOrDefaultAsync(u => u.User.UserName == this.User.Identity.Name);

            List<Claim> claims = (await this.UsersManager.GetClaimsAsync(user.User)).ToList();

            JwtSecurityToken jwt = new JwtSecurityToken
           (
                claims: claims,
                expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(1)),
                signingCredentials: new SigningCredentials(tokenSigningKey, SecurityAlgorithms.HmacSha256)
            );

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            return new TempTokenModel { Token = tokenHandler.WriteToken(jwt) };
        }

        [HttpPost("sendPasswordIsLost"), ValidateModel, AllowAnonymous]
        public async Task<SendPasswordIsLostResult> SendPasswordIsLost([FromBody] SendPasswordIsLostModel model)
        {
            if (model == null)
            {
                throw new BusinessException("Données invalides");
            }

            string[] emails = await this.UsersManager.SendPasswordIsLostAsync(model.Email, null);

            return new SendPasswordIsLostResult
            {
                Emails = emails
            };
        }

        [AllowAnonymous, HttpPut("resetpassword"), ValidateModel, HandleBusinessException]
        public async Task PasswordReset([FromBody] PasswordResetModel model)
        {
            if (model == null)
            {
                Console.WriteLine($"Password reset error model null");
                throw new BusinessException("Données invalides");
            }

            ApplicationUser user = await this.UsersManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                Console.WriteLine($"Password reset error user not found");
                throw new BusinessException("Données invalides");
            }

            Console.WriteLine($"Setting password {model.Password}");
            IdentityResult result = await this.UsersManager.ResetPasswordAsync(user, model.Token, model.Password);

            if (!result.Succeeded)
            {
                Console.WriteLine($"Password reset error {result.Errors}");
                throw new BusinessException(result.Errors.Select(e => e.Description).ToArray());
            }
        }

        [Authorize]
        [HttpGet("current")]
        public async Task<UserModel> GetCurrentUserAsync()
        {
            var user = await this.UsersManager.SelectAllUsers().Select(u => new
            {
                User = u,
                Roles = u.UserRoles.Select(ur => ur.Role)
            }).FirstOrDefaultAsync(u => u.User.UserName == this.User.Identity.Name);

            return user?.User?.ToApiResult(user.Roles);
        }

        [Authorize]
        [HttpGet("{userId}")]
        public async Task<UserModel> GetUserAsync(string userId)
        {
            var user = await this.UsersManager.SelectAllUsers().Select(u => new
            {
                User = u,
                Roles = u.UserRoles.Select(ur => ur.Role)
            }).FirstOrDefaultAsync(u => u.User.Id == userId);

            return user?.User?.ToApiResult(user.Roles);
        }

        [Authorize]
        [HttpDelete("token/{token}")]
        public async Task BanTokenAsync(string token)
        {
            await this.ForbiddenTokenManager.CreateTokenAsync(token);
        }

        [HttpPost]
        [ValidateModel, HandleBusinessException]
        [Authorize(Policy = Policy.UserIsAdministrator)]
        public async Task CreateUserAsync([FromBody] UserCreationModel model)
        {
            using TransactionScope transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            var user = await this.UsersManager.CreateUserAsync(new ApplicationUser
            {
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                UserName = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Establishment = model.Establishment
            }, model.Role, true);

            string token = await this.UsersManager.GeneratePasswordResetTokenAsync(user);
            string userFullName = user.FirstName + " " + user.LastName;
            string userEmail = user.Email;

            await this.EmailsManager.SendInvitationEmailAsync(userFullName, userEmail, token, model.Role == UserRoleNames.Participant);

            transactionScope.Complete();
        }

        [HttpPost("register")]
        [ValidateModel, HandleBusinessException]
        [AllowAnonymous]
        public async Task RegisterAsync([FromBody] UserCreationModel model)
        {
            using TransactionScope transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            var user = await this.UsersManager.CreateUserAsync(new ApplicationUser
            {
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                UserName = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Establishment = model.Establishment
            }, model.Role, false);

            string token = await this.UsersManager.GeneratePasswordResetTokenAsync(user);
            string userFullName = user.FirstName + " " + user.LastName;
            string userEmail = user.Email;

            await this.EmailsManager.SendInvitationEmailAsync(userFullName, userEmail, token, model.Role == UserRoleNames.Participant);

            transactionScope.Complete();
        }

        [HttpPut("myaccount")]
        [ValidateModel, HandleBusinessException]
        [Authorize]
        public async Task<UserModel> UpdateMyAccountAsync([FromBody] UserUpdateModel model)
        {
            ApplicationUser user = new ApplicationUser
            {
                LastName = model.LastName,
                FirstName = model.FirstName,
                Language = model.Language,
                Establishment = model.Establishment,
            };

            user = await this.UsersManager.UpdateUserSelfAccountAsync(user, this.User.Identity.Name);

            return user.ToApiResult(user.UserRoles.Select(x => x.Role));
        }

        [HttpPut("mypassword")]
        [ValidateModel, HandleBusinessException]
        [Authorize]
        public async Task UpdateMyPasswordAsync([FromBody] PasswordUpdateModel model)
        {
            var user = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

            if (user == null)
            {
                return;
            }

            var result = await this.UsersManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                throw new BusinessException(result.Errors.Select(e => e.Description).ToArray());
            }
        }

        [HttpPut("myphonenumber")]
        [ValidateModel, HandleBusinessException]
        [Authorize]
        public async Task<bool> RequestPhoneNumberChangeAsync([FromBody] PhoneNumberUpdateModel model)
        {
            var user = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

            if (user == null)
            {
                return false;
            }

            await this.UsersManager.SetPhoneNumberAsync(user, model.PhoneNumber);
            return false;
        }

        [HttpPut("myphonenumber/confirm")]
        [ValidateModel, HandleBusinessException]
        [Authorize]
        public async Task ConfirmPhoneNumberChangeAsync([FromBody] PhoneNumberConfirmationModel model)
        {
            var user = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

            if (user == null)
            {
                return;
            }

            var result = await this.UsersManager.ChangePhoneNumberAsync(user, model.PhoneNumber, model.ValidationCode);

            if (!result.Succeeded)
            {
                throw new BusinessException(result.Errors.Select(e => e.Description).ToArray());
            }
        }

        [HttpPut("email")]
        [ValidateModel, HandleBusinessException]
        [Authorize]
        public async Task RequestEmailChangeAsync([FromBody] EmailUpdateModel model)
        {
            var user = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

            if (user == null)
            {
                return;
            }

            await this.UsersManager.RequestEmailChangeAsync(user, model.Email, this.User.IsInRole(UserRoleNames.Participant));
        }

        [HttpPut("email/check")]
        [Authorize]
        [ValidateModel, HandleBusinessException]
        public async Task ChangeEmailAsync([FromBody] EmailUpdateConfirmationModel model)
        {
            var user = await this.UsersManager.FindByNameAsync(this.User.Identity.Name);

            if (user == null)
            {
                return;
            }

            var result = await this.UsersManager.ChangeEmailAsync(user, model.Email, model.Token);

            if (!result.Succeeded)
            {
                throw new BusinessException(result.Errors.Select(e => e.Description).ToArray());
            }

            user.UserName = model.Email;
            await this.UsersManager.SaveChangesAsync();
            await this.UsersManager.UpdateNormalizedUserNameAsync(user);
            await this.UsersManager.SaveChangesAsync();
        }

        [HttpGet("email/{email}")]
        [Authorize(Policy = Policy.UserIsAdministrator)]
        public async Task<UserModel> GetUserByEmailAsync(string email)
        {
            var user = await this.UsersManager.FindByEmailAsync(email);

            return user?.ToApiResult();
        }

        [HttpGet]
        [WebApiQueryable2(nameof(ApplicationUser))]
        [Authorize(Policy = Policy.UserIsAdministrator)]
        public async Task<UserModel[]> GetAllUsersAsync()
        {
            var users = await this.UsersManager
                .SelectAllUsers()
                .ApplyQueryV2Options(this.Request)
                .Select(u => new
                {
                    User = u,
                    Roles = u.UserRoles.Select(ur => ur.Role)
                }).ToArrayAsync();

            return users.Select(u => u.User.ToApiResult(u.Roles)).ToArray();
        }

        [HttpDelete("logout")]
        public async Task LogOut()
        {
            await Task.CompletedTask;
            this.SignOut();
            this.Response.Headers.Add("Clear-Site-Data", "*");
        }

        [HttpPost("loginas")]
        [ValidateModel, HandleBusinessException,]
        [Authorize(Policy = Policy.UserIsAdministrator)]
        public async Task<LoginResult> LoginAs([FromBody] ConnectAsModel model)
        {

            var user = await this.UsersManager.SelectAllUsers().Select(u => new
            {
                User = u,
                Roles = u.UserRoles.Select(ur => ur.Role),
            }).FirstOrDefaultAsync(u => u.User.Id == model.UserId);

            if (user == null || user.User == null)
            {
                return null;
            }

            try
            {
                await this.SignInManager.SignInAsync(user.User, false);
            }
            catch (Exception)
            {
                throw new BusinessException("Données invalides");
            }

            List<Claim> claims = (await this.UsersManager.GetClaimsAsync(user.User)).ToList();
            claims = AccountsController.GenerateClaims(user.User, claims).ToList();

            foreach (var role in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Name));
            }



            byte[] key = Encoding.ASCII.GetBytes(this.Configuration["Authentication:AuthenticationTokenSecretSigningKey"]);
            var tokenSigningKey = new SymmetricSecurityKey(key);
            DateTime expirationDate = DateTime.UtcNow.Add(new TimeSpan(false ? 30 : 1, 0, 0, 0));

            JwtSecurityToken jwt = new JwtSecurityToken
            (
                 claims: claims,
                 expires: DateTime.UtcNow.Add(false ? TimeSpan.FromDays(365) : TimeSpan.FromHours(10)),
                 signingCredentials: new SigningCredentials(tokenSigningKey, SecurityAlgorithms.HmacSha256)
             );

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            string wroteToken = tokenHandler.WriteToken(jwt);

            LoginResult response = new LoginResult
            {
                AccessToken = wroteToken,
                AccessTokenValidUntil = expirationDate,
                User = user.User.ToApiResult(user.Roles.ToArray())
            };

            return response;
        }

        [HttpPut]
        [ValidateModel, HandleBusinessException]
        [Authorize(Policy = Policy.UserIsAdministrator)]
        public async Task<UserModel> UpdateUserAsync([FromBody] UserUpdateModel model)
        {
            using TransactionScope ts = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var user = this.UsersManager.SelectAllUsersWithRoles().FirstOrDefault(u => u.Id == model.Id);

            if (user == null)
            {
                throw new BusinessException($"Utilisateur inconnu : [{model.Email}]");
            }

            user.Email = model.Email;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.PhoneNumber = model.PhoneNumber;
            user.Establishment = model.Establishment;

            await this.UsersManager.RemoveFromRoleAsync(user, user.UserRoles.FirstOrDefault().Role.Name);
            await this.UsersManager.AddToRoleAsync(user, model.Role);
            await this.UsersManager.UpdateAsync(user);

            await this.UsersManager.SaveChangesAsync();

            ts.Complete();

            return user.ToApiResult(user.UserRoles.Select(x => x.Role));
        }

        [HttpDelete("{userId}")]
        [Authorize]
        [HandleBusinessException]
        public async Task DeleteUserAsync(string userId)
        {
            await this.UsersManager.DeleteUserAsync(userId, this.User.Identity.Name, this.User.IsInRole(UserRoleNames.Administrator));
        }

        [HttpGet("resend-invitational/{userEmail}")]
        [ValidateModel, HandleBusinessException]
        public async Task ResendInvitationalEmail(string userEmail)
        {
            await this.UsersManager.SendPasswordIsLostAsync(userEmail, null);
        }

    }
}
