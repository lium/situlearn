﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

using System;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class DataContext : IdentityDbContext<ApplicationUser, IdentityRole, string,
      IdentityUserClaim<string>, ApplicationUserRole, IdentityUserLogin<string>, IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<EmailContent> EmailContents { get; set; }

        public DbSet<ForbiddenToken> ForbiddenTokens { get; set; }

        public DbSet<Answer> Answers { get; set; }

        public DbSet<Clue> Clues { get; set; }

        public DbSet<ExplorationMap> ExplorationMaps { get; set; }

        public DbSet<FieldTrip> FieldTrips { get; set; }

        public DbSet<Game> Games { get; set; }

        public DbSet<Journey> Journeys { get; set; }

        public DbSet<JourneyLocatedGameUnit> JourneyLocatedGameUnits { get; set; }

        public DbSet<LocatedActivity> LocatedActivities { get; set; }

        public DbSet<QuestionActivity> QuestionActivities { get; set; }

        public DbSet<ReadingActivity> ReadingActivities { get; set; }

        public DbSet<LocatedGameUnit> LocatedGameUnits { get; set; }

        public DbSet<PedagogicalField> PedagogicalFields { get; set; }

        public DbSet<PlayedActivity> PlayedActivities { get; set; }


        public DbSet<PlayedActivityFile> PlayedActivityFiles { get; set; }

        public DbSet<PlayedUnit> PlayedUnits { get; set; }

        public DbSet<Player> Players { get; set; }

        public DbSet<ContextualHelp> ContextualHelps { get; set; }

        public DbSet<UserFieldTrip> UserFieldTrips { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUserRole>().HasKey(p => new { p.UserId, p.RoleId });

            builder.Entity<ApplicationUser>()
                .HasMany(user => user.UserRoles)
                .WithOne().HasForeignKey("UserId");

            builder.Entity<ApplicationUser>()
                .HasMany(a => a.UserRoles)
                .WithOne(ur => ur.User).HasForeignKey(ur => ur.UserId);

            builder.Entity<ApplicationUserRole>()
                .HasOne(a => a.Role)
                .WithMany().HasForeignKey(ur => ur.RoleId);

            builder.Entity<PedagogicalField>()
                .HasIndex(pf => pf.Name)
                .IsUnique();

            builder.Entity<JourneyLocatedGameUnit>()
                .HasOne(jlgu => jlgu.Journey)
                .WithMany(j => j.JourneyLocatedGameUnits)
                .HasForeignKey(jlgu => jlgu.JourneyId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<JourneyLocatedGameUnit>()
                .HasOne(jlgu => jlgu.LocatedGameUnit)
                .WithMany(lgu => lgu.JourneyLocatedGameUnits)
                .HasForeignKey(jlgu => jlgu.LocatedGameUnitId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<UserFieldTrip>()
                .HasOne(uft => uft.User)
                .WithMany(u => u.UserFieldTrips)
                .HasForeignKey(uft => uft.UserId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<UserFieldTrip>()
                .HasOne(uft => uft.FieldTrip)
                .WithMany(ft => ft.UserFieldTrips)
                .HasForeignKey(uft => uft.FieldTripId)
                .OnDelete(DeleteBehavior.NoAction);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlServer(options =>
            {
                options.UseNetTopologySuite();
            });
        }
    }
}
