﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Business;
using Common.Security;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ozytis.Common.Core.Web.WebApi;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Server.Controllers
{
    [Authorize]
    [Route("api/contextual-helps")]
    public class ContextualHelpsController : ControllerBase
    {
        public ContextualHelpsController(ContextualHelpsManager contextualHelpsManager)
        {
            this.ContextualHelpsManager = contextualHelpsManager;
        }

        public ContextualHelpsManager ContextualHelpsManager { get; }

        [HttpGet]
        [WebApiQueryable2(nameof(ContextualHelp))]
        [Authorize(Policy = Policy.UserIsAdministrator)]
        public async Task<IEnumerable<ContextualHelp>> GetAllContextualHelpsAsync()
        {
            return await this.ContextualHelpsManager.SelectAll()
                .ApplyQueryV2Options(this.Request).ToArrayAsync();
        }

        [HttpGet("{contextualHelpId}")]
        public async Task<ContextualHelp> GetContextualHelpAsync(string contextualHelpId)
        {
            return await this.ContextualHelpsManager.SelectAll()
               .Where(ch => ch.Id == contextualHelpId)
               .FirstOrDefaultAsync();
        }

        [HttpPut]
        [ValidateModel, HandleBusinessException]
        [Authorize(Policy = Policy.UserIsAdministrator)]
        public async Task<ContextualHelp> UpdateContextualHelpAsync([FromBody] ContextualHelpUpdateModel model)
        {
            ContextualHelp contextualHelp = new ContextualHelp
            {
                Id = model.Id,
                Title = model.Title,
                Description = model.Description,
                Content = model.Content
            };

            ContextualHelp updatedcontextualHelp = await this.ContextualHelpsManager.UpdateContextualHelpAsync(contextualHelp);

            return updatedcontextualHelp;
        }
    }
}
