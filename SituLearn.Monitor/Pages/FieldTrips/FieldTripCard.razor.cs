// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;

using Microsoft.AspNetCore.Components;
using SituLearn.Monitor.Pages.Supervision;
using SituLearn.Monitor.Repositories;

namespace SituLearn.Monitor.Pages.FieldTrips
{
    public partial class FieldTripCard : ComponentBase
	{
		[Parameter]
		public FieldTripHeaderForMonitorModel FieldTrip { get; set; }

		[Inject]
		public NavigationManager NavigationManager { get; set; }

		[Inject]
		public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

		public void StartGamesSupervisionAsync()
		{
			this.NavigationManager.NavigateTo(TimeSelectSupervisionPage.GetUrl(this.FieldTrip.Id));
		}

		public void SwitchDescriptionDisplay()
		{
			this.FieldTrip.ShowDescription = !this.FieldTrip.ShowDescription;
			this.StateHasChanged();
		}
	}
}