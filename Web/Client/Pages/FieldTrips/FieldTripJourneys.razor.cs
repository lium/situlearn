// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Http;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using Web.Client;
using Web.Client.Repositories;
using static Ozytis.Common.Core.Web.Razor.Utilities.BlazorHelper;
using BlazorPro.BlazorSize;
using Ozytis.Common.Core.Web.Razor;
using Ozytis.Common.Core.Web.Razor.Layout;
using Ozytis.Common.Core.Web.Razor.ArchitectUI;

using Ozytis.Common.Core.Web.Razor.Leaflet;
using Ozytis.Common.Core.Web.Razor.Utilities;
using Ozytis.Common.Core.Api;
using FontAwesomeIcons = Ozytis.Common.Core.Web.Razor.FontAwesomeIcons;
using Entities;
using Api;
using Web.Client.Utilities;
using Syncfusion.Blazor;
using Syncfusion.Blazor.Popups;
using Syncfusion.Blazor.Buttons;
using Syncfusion.Blazor.Calendars;
using Syncfusion.Blazor.DropDowns;
using Syncfusion.Blazor.Inputs;
using Common;
using Ozytis.Common.Core.Utilities;
using Syncfusion.Blazor.Charts;
using Syncfusion.Blazor.Data;

namespace Web.Client.Pages.FieldTrips
{
    public partial class FieldTripJourneys : ComponentBase
    {
        [Parameter]
        public FieldTrip FieldTrip { get; set; }

        [Parameter]
        public EventCallback OnChange { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

		[Inject]
		public NavigationWithHistoryManager NavigationManager { get; set; }

		[CascadingParameter]
        public IConfirmProvider Confirm { get; set; }

        public bool CanEdit => this.UsersRepository.CurrentUser.Role == UserRoleNames.Administrator || (this.FieldTrip?.DesignerId == this.UsersRepository.CurrentUser.Id);

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        public List<Journey> Journeys { get; private set; }

        public bool DisplayAddUnitForm { get; set; }

        public Guid? UnitIdToAddSelected { get; set; }

        protected override void OnInitialized()
        {
            this.Journeys = this.FieldTrip.Journeys.OrderBy(j => j.Order).ToList();
            this.CurrentJourney = this.Journeys.FirstOrDefault();
            this.StateHasChanged();
        }

        protected async Task AddNewJourneyAsync()
        {
            if (!this.CanEdit)
            {
                return;
            }

            var journey = await this.FieldTripsRepository.AddNewJourneyAsync(this.FieldTrip.Id);
            this.Journeys.Add(journey);

            this.StateHasChanged();
            await this.OnChange.InvokeAsync(journey);

            this.CurrentJourney = journey;
            this.StateHasChanged();
        }

        protected async Task UpdateCurrentJourneyColorAsync(string color)
        {
            if (!this.CanEdit)
            {
                return;
            }

            this.CurrentJourney.Color = JourneyColorsHelper.GetColorFromHex(color);
            var updatedJourney = await this.FieldTripsRepository.UpdateJourneyInfoAsync(this.FieldTrip.Id, new JourneyInfoUpdateModel
            {
                Color = this.CurrentJourney.Color,
                Id = this.CurrentJourney.Id
            });
            this.Journeys.ReplaceElement((j) => j.Id == this.CurrentJourney.Id, updatedJourney);

            this.StateHasChanged();
            await this.OnChange.InvokeAsync();
        }

        public bool IsDeleting { get; set; }

        protected async Task DeleteCurrentJourneyAsync()
        {
            if (!this.CanEdit)
            {
                return;
            }

            if (this.IsDeleting || this.CurrentJourney == null || !await Confirm.AskAsync($"Etes-vous s�r de vouloir supprimer le parcours {this.CurrentJourney.Color.GetFriendlyName().ToLowerInvariant()} ?"))
            {
                return;
            }

            this.IsDeleting = true;
            this.StateHasChanged();

            try
            {
                await this.FieldTripsRepository.DeleteJourneyAsync(this.CurrentJourney.FieldTripId, this.CurrentJourney.Id);
            }
            catch (BusinessException ex)
            {
                this.Notifier.Notify(Color.Danger, ex.Message);
            }

            this.Journeys = this.Journeys.Where(j => j.Id != this.CurrentJourney.Id).ToList();
            this.CurrentJourney = this.Journeys.FirstOrDefault();
            this.StateHasChanged();

            await this.OnChange.InvokeAsync();

            this.IsDeleting = false;

        }

        public Journey CurrentJourney { get; set; }

        public bool JourneyIsDragOver { get; set; }

        protected async Task OnGameUnitAddedAsync(object droppedItem)
        {
            if (!this.CanEdit)
            {
                return;
            }

            Console.WriteLine("drop 1");

            this.JourneyIsDragOver = false;
            this.StateHasChanged();

            if (this.CurrentJourney == null || !this.CanEdit)
            {
                return;
            }

            if (droppedItem is LocatedGameUnit gameUnit)
            {
                Console.WriteLine($"Located game unit added {gameUnit.Name}");

                if (this.CurrentJourney.JourneyLocatedGameUnits.Any(link => link.LocatedGameUnitId == gameUnit.Id))
                {
                    this.Notifier.Notify(Color.Danger, $"Cette unit� est d�j� pr�sente dans le parcours {this.CurrentJourney.Name.ToLowerInvariant()}");
                }

                await this.FieldTripsRepository.AddLocatedUnitToJourneyAsync(this.FieldTrip.Id, this.CurrentJourney.Id, gameUnit.Id);

                await this.OnChange.InvokeAsync(droppedItem);

                return;
            }

            Console.WriteLine("drop");

            if (droppedItem is JourneyLocatedGameUnit link)
            {
                await this.FieldTripsRepository.UpdateGameUnitOrderAsync(this.FieldTrip.Id, this.CurrentJourney.Id, link.Id,
                    this.CurrentJourney.JourneyLocatedGameUnits.Count());

                await this.OnChange.InvokeAsync(droppedItem);
            }
        }

        protected override void OnParametersSet()
        {
            if (this.CurrentJourney != null)
            {
                this.CurrentJourney = this.FieldTrip.Journeys.FirstOrDefault(j => j.Id == this.CurrentJourney.Id);
            }
		}

		protected async Task AddUnitToCurrentJourneyAsync()
        {
            LocatedGameUnit unitToAdd = this.FieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == this.UnitIdToAddSelected);
			await this.OnGameUnitAddedAsync(unitToAdd); 
            this.UnitIdToAddSelected = null; 
            this.DisplayAddUnitForm = false;
            this.StateHasChanged();
		}

	}
}