﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Ozytis.Common.Core.Web.Razor;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using Ozytis.Common.Core.Web.Razor.Utilities;
using SituLearn.Monitor.Repositories;
using SituLearn.Monitor.Utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;
using static System.Net.Mime.MediaTypeNames;

namespace SituLearn.Monitor
{
    public partial class AppLayout : LayoutComponentBase, INotifier
    {
        public const string HeaderToolbarZone = "HeaderToolbarZone";

        public bool ShowMainMenu { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public BuildInformation BuildInformation { get; set; }

        protected Dictionary<string, RenderFragment> Zones { get; set; } = new();

        public string PageCssClass { get; set; }

        public bool ShowNotification { get; private set; }

        public string NotificationText { get; set; }

        public Color NotificationColor { get; set; }

        public Timer NotificationTimer { get; set; }

        public void SetZone(string name, RenderFragment zone, string cssClass)
        {
            this.Zones[name] = zone;
            this.PageCssClass = cssClass;
            this.StateHasChanged();
        }

        public async Task LogOut()
        {
            await this.UsersRepository.LogoutAsync();
            this.ShowMainMenu = false;
            this.Zones = new();
            this.StateHasChanged();
            this.NavigationManager.NavigateTo(Pages.User.LoginPage.Url);
        }

        public void Notify(Color color, string text, int duration = 4000, Func<Task> actionEnding = null)
        {
            this.NotificationColor = color;
            this.NotificationText = text;
            this.ShowNotification = true;
            this.StateHasChanged();

            this.NotificationTimer = new Timer(duration);
            this.NotificationTimer.Elapsed += (sender, args) =>
            {
                this.ShowNotification = false;
                this.NotificationTimer.Stop();
                this.StateHasChanged();
            };

            this.NotificationTimer.AutoReset = false;
            this.NotificationTimer.Start();
        }

        //public Timer NotificationTimer { get; set; }

        //public void Notify(Color color, string text, int duration = 4000, Func<Task> actionEnding = null)
        //{
        //    // Console.WriteLine($"Notify {color.Name} : {text}");

        //    this.NotificationColor = color;
        //    this.NotificationText = text;
        //    this.ShowNotification = true;
        //    this.StateHasChanged();

        //    this.NotificationTimer = new Timer(duration);
        //    this.NotificationTimer.Elapsed += (sender, args) =>
        //    {
        //        this.ShowNotification = false;
        //        this.NotificationTimer.Stop();
        //        this.StateHasChanged();
        //    };

        //    this.NotificationTimer.AutoReset = false;
        //    this.NotificationTimer.Start();

        //}

        //public void HideNotification()
        //{
        //    this.ShowNotification = false;
        //    this.StateHasChanged();
        //}

        //TaskCompletionSource<bool?> ConfirmTaskCompletion;

        //protected string ConfirmText { get; set; }

        //public bool ShowConfirmation { get; set; }

        //public string YesConfirmationText { get; set; } = "Oui";

        //public string NoConfirmationText { get; set; } = "Non";

        //public bool ShowNotification { get; private set; }

        //public string NotificationText { get; set; }

        //public Color NotificationColor { get; set; }

        //public async Task<bool?> AskAsync(string message, string yesText = null, string noText = null)
        //{
        //    this.ConfirmTaskCompletion = new TaskCompletionSource<bool?>();
        //    this.ConfirmText = message;
        //    this.ShowConfirmation = true;
        //    this.YesConfirmationText = yesText ?? "Oui";
        //    this.NoConfirmationText = noText ?? "Non";

        //    this.StateHasChanged();

        //    return await this.ConfirmTaskCompletion.Task;
        //}

        //protected void SetConfirmationResult(bool? result)
        //{
        //    this.ConfirmTaskCompletion.SetResult(result);
        //    this.ShowConfirmation = false;
        //    this.StateHasChanged();
        //}
    }
}
