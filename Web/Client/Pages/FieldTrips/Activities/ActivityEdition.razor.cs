﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api.Activity;
using Common;
using Entities;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Utilities;
using Syncfusion.Blazor.InPlaceEditor;
using Syncfusion.Blazor.Inputs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Client.Pages.FieldTrips.LocatedGameUnits;
using Web.Client.Repositories;
using Web.Client.Shared;
using Web.Client.Utilities;

namespace Web.Client.Pages.FieldTrips.Activities
{
	[Route(CreateUrl)]
	[Route(EditionUrl)]
	public partial class ActivityEdition : FormComponent
	{
		public const string CreateUrl = "/activities/{fieldTripId:guid}/{locatedGameUnitId:guid}/new/{activityType}";
		public const string EditionUrl = "/activities/{fieldTripId:guid}/{locatedGameUnitId:guid}/{activityId:guid}";

		public static string GetCreateUrl(Guid fieldTripId, Guid locatedGameUnitId, string activityType)
		{
			return CreateUrl.Replace("{fieldTripId:guid}", $"{fieldTripId}")
				.Replace("{locatedGameUnitId:guid}", $"{locatedGameUnitId}")
				 .Replace("{activityType}", activityType);
		}

		public static string GetEditionUrl(Guid fieldTripId, Guid locatedGameUnitId, Guid activityId)
		{
			return EditionUrl.Replace("{fieldTripId:guid}", $"{fieldTripId}")
				.Replace("{locatedGameUnitId:guid}", $"{locatedGameUnitId}")
				.Replace("{activityId:guid}", $"{activityId}");
		}

		private readonly List<OptionItem<QuestionActivityType>> QuestionTypeSelectorOptions = typeof(QuestionActivityType).ToOptionItems<QuestionActivityType>().ToList();

		public override async Task OnProcessAsync()
		{
			//await this.SaveAsync();
		}

		[Parameter]
		public Guid FieldTripId { get; set; }
		public FieldTrip FieldTrip { get; set; }

		[Parameter]
		public Guid LocatedGameUnitId { get; set; }
		public LocatedGameUnit LocatedGameUnit { get; set; }

		[Parameter]
		public Guid? ActivityId { get; set; }

		[Parameter]
		public string ActivityType { get; set; }


		[Inject]
		public IJSRuntime JSRuntime { get; set; }

		public LocatedActivity Activity { get; set; }

		public ICreateModel Model { get; set; }

		public bool ClueIsExpanded { get; set; }

		public bool SiteMapIsExpanded { get; set; } = true;

		[Inject]
		public ActivitiesRepository ActivityRepository { get; set; }

		[Inject]
		public LocatedGameUnitsRepository LocatedGameUnitsRepository { get; set; }

		[Inject]
		public FieldTripsRepository FieldTripsRepository { get; set; }

		[Inject]
		public UsersRepository UsersRepository { get; set; }

		public bool CanEdit => this.UsersRepository.CurrentUser.Role == UserRoleNames.Administrator || (this.FieldTrip?.DesignerId == this.UsersRepository.CurrentUser.Id);

		[Inject]
		public NavigationWithHistoryManager NavigationManager { get; set; }

		public bool ShowContextualHelpModal { get; set; }
		public string ContextualHelpIdToShow { get; set; }

		public SfTextBox TxtBox { get; set; }

		public void CloseContextualHelpModal()
		{
			this.ShowContextualHelpModal = false;
			this.ContextualHelpIdToShow = null;
			this.StateHasChanged();
		}

		public void DisplayContextualHelpModal(string contextualHelpId)
		{
			this.ContextualHelpIdToShow = contextualHelpId;
			this.ShowContextualHelpModal = true;
			this.StateHasChanged();
		}

		protected override async Task OnInitializedAsync()
		{
			this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId, false);
			this.LocatedGameUnit = await this.LocatedGameUnitsRepository.GetLocatedGameUnitAsync(this.LocatedGameUnitId);

			if (this.ActivityId.HasValue)
			{
				await this.MapFromId(this.ActivityId.Value);
			}
			else
			{
				this.Activity = ActivityType == "question" ? new QuestionActivity() : new ReadingActivity();
				this.Model = ActivityType == "question" ? new QuestionCreateModel() : new StatementCreateModel();
				this.Model.Title = this.Activity.Name;
			}

			this.Model.Name = this.Activity.Name;
			this.Model.LocatedGameUnitId = this.LocatedGameUnitId;

			await base.OnInitializedAsync();
		}

		public void TextBoxCreated()
		{
			this.TxtBox.FocusIn();
		}

		public async void RemoveAnswer(AnswerUpdateModel answer)
		{
			if (this.Model is QuestionCreateModel question)
			{
				if (!answer.Id.HasValue)
				{
					return;
				}

				await this.ActivityRepository.DeleteAnswer(answer.Id.Value);

				question.Answers.Remove(answer);
				foreach (var item in this.AnswersSelectors)
				{
					item.Value.NeedRefresh = true;
				}

				this.StateHasChanged();
			}
		}

		public Dictionary<int, InlineImageChooser> AnswersSelectors { get; set; } = new();

		protected async Task SaveAsync()
		{
			Console.WriteLine("Saving");

			this.Errors = null;
            this.IsProcessing = true;
            this.StateHasChanged();

			try
			{
                if (this.Model is QuestionUpdateModel questionUpdateModel)
                {
                    Console.WriteLine("update question");
                    this.Activity = await this.ActivityRepository.UpdateQuestionAsync(questionUpdateModel);
                }
                else if (this.Model is QuestionCreateModel questionCreateModel)
                {
                    Console.WriteLine("create question");
                    this.Activity = await this.ActivityRepository.CreateQuestionAsync(questionCreateModel, this.FieldTripId);
                    this.Model = new QuestionUpdateModel(questionCreateModel)
                    {
                        Id = this.Activity.Id
                    };
                }
                else if (this.Model is StatementUpdateModel statementUpdateModel)
                {
                    Console.WriteLine("update statement");
                    this.Activity = await this.ActivityRepository.UpdateStatementAsync(statementUpdateModel);
                }
                else if (this.Model is StatementCreateModel statementCreateModel)
                {
                    Console.WriteLine("create statement");
                    this.Activity = await this.ActivityRepository.CreateStatementAsync(statementCreateModel, this.FieldTripId);
                    this.Model = new StatementUpdateModel(statementCreateModel)
                    {
                        Id = this.Activity.Id
                    };
                }
                else
                {
                    throw new BusinessException("Objet inconnu");
				}

				this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId, false);
				this.NavigationManager.NavigateTo(ActivityEdition.GetEditionUrl(this.FieldTripId, this.LocatedGameUnitId, this.Activity.Id));
			}
			catch (BusinessException ex)
            {
                this.Errors = ex.Messages;

                this.Notifier.Notify(Color.Danger, this.ErrorMessage ?? "Erreur lors de la mise à jour des données");
            }
            finally
            {
                this.IsProcessing = false;
            }
		}

		public SiteMap Sitemap { get; set; }

		public InlineImageChooser ClueImagePicker { get; set; }
		public InlineImageChooser PicturePicker { get; set; }

		private async Task MapFromId(Guid activityId)
		{
			this.Activity = await this.ActivityRepository.GetActivity(activityId);
			this.LocatedGameUnit = await this.LocatedGameUnitsRepository.GetLocatedGameUnitAsync(this.Activity.LocatedGameUnitId);

			if (this.ClueImagePicker != null)
			{
				this.ClueImagePicker.NeedRefresh = true;
			}

			if (this.PicturePicker != null)
			{
				this.PicturePicker.NeedRefresh = true;
			}

			if (this.Activity.ActivityType == Entities.ActivityType.Question)
			{
				var question = await this.ActivityRepository.GetQuestion(activityId) as QuestionActivity;
				this.Activity = question;

				this.Model = null;

				this.Model = new QuestionUpdateModel
				{
					Id = question.Id,
					Name = question.Name,
					ActivityQuestionType = (int)question.QuestionActivityType,
					Title = question.Title ?? question.Name,
					Statement = question.Statement,
					Picture = question.Picture,
					LocatedGameUnitId = question.LocatedGameUnitId,
					TextToDisplayForCorrectAnswer = question.TextToDisplayForCorrectAnswer,
					TextToDisplayForWrongAnswer = question.TextToDisplayForWrongAnswer,
					Gain = question.Gain,
					Answers = question.Answers.Select(a => new AnswerUpdateModel
					{
						Id = a.Id,
						Content = a.Content,
						IsCorrect = a.IsCorrectAnswer
					}).ToList(),
					ClueEnabled = question.ClueEnabled,
					Clue = new ClueUpdateModel
					{
						Id = question.Clue?.Id,
						Content = question.Clue?.Content,
						Cost = question.Clue?.Cost ?? 1,
						Picture = question.Clue?.Picture
					}
				};

				this.ClueIsExpanded = question.ClueEnabled;
			}
			else if (this.Activity.ActivityType == Entities.ActivityType.Statement)
			{
				var statement = await this.ActivityRepository.GetStatement(activityId) as ReadingActivity;
				this.Activity = statement;

				this.Model = new StatementUpdateModel
				{
					Id = statement.Id,
					Name = statement.Name,
					Title = statement.Title,
					Statement = statement.Statement,
					Picture = statement.Picture,

					AllowsAudioRecordings = statement.AllowsAudioRecordings,
					AllowsNoteTaking = statement.AllowsNoteTaking,
					AllowsPhotos = statement.AllowsPhotos,
					ReadingsAreUnlimited = statement.ReadingsAreUnlimited,
				};
			}
			else
			{
				throw new BusinessException("Cette activité est de type inconnu");
			}
		}

		public async Task Refresh(Guid activityId)
		{
			await this.MapFromId(activityId);

			foreach (var item in this.AnswersSelectors)
			{
				item.Value.NeedRefresh = true;
			}

			this.StateHasChanged();
		}

		public async Task UpdateField(string fieldName, object value, int? answerIndex = null)
		{
			var question = this.Model as QuestionUpdateModel;
			var statement = this.Model as StatementUpdateModel;

			switch (fieldName)
			{
				case "name":
					this.Model.Name = (string)value;
					if (string.IsNullOrEmpty(this.Model.Title))
					{
						this.Model.Title = this.Model.Name;
					}
					break;
				case "activity_question_type":
					question.ActivityQuestionType = (int)value;
					break;
				case "activity_title":
					this.Model.Title = (string)value;
					break;
				case "activity_statement":
					this.Model.Statement = (string)value;
					break;
				case "activity_picture":
					this.Model.Picture = (string)value;
					break;
				case "activity_gain":
					question.Gain = (int)value;
					break;
				case "activity_correct_answer":
					question.TextToDisplayForCorrectAnswer = (string)value;
					break;
				case "activity_wrong_answer":
					question.TextToDisplayForWrongAnswer = (string)value;
					break;
				case "clue_cost":
					question.Clue.Cost = (int)value;
					break;
				case "clue_content":
					question.Clue.Content = (string)value;
					break;
				case "clue_picture":
					question.Clue.Picture = (string)value;
					break;
				case "statement_notes":
					statement.AllowsNoteTaking = (bool)value;
					break;
				case "statement_photos":
					statement.AllowsPhotos = (bool)value;
					break;
				case "statement_audios":
					statement.AllowsAudioRecordings = (bool)value;
					break;
				case "statements_unlimited":
					statement.ReadingsAreUnlimited = (bool)value;
					break;

				default:
					break;
			}

            //await this.ProcessAsync();
            await this.SaveAsync();

            this.StateHasChanged();

			await this.JSRuntime.InvokeVoidAsync("gotoLastScroll");
		}

		public async Task ValueChangeHandler(EndEditEventArgs args)
		{
			if (args.Action.Equals("cancel", StringComparison.InvariantCultureIgnoreCase))
			{
				await this.Refresh(this.ActivityId.Value);
			}

			if (args.Action.Equals("submit", StringComparison.InvariantCultureIgnoreCase))
			{
				await this.ProcessAsync();
			}
		}

		public async Task AddAnswer(QuestionCreateModel questionModel)
		{
			if (!this.ActivityId.HasValue)
			{
				Console.WriteLine("Unable to add answer to no question. Activity not loaded");
				return;
			}

			bool answerDefaultRightness = questionModel.ActivityQuestionType == (int)QuestionActivityType.Question || questionModel.Answers.Count == 0;
			
			Guid? newAnswerId = null;
			try
			{
				newAnswerId = await this.ActivityRepository.AddAnswer(this.ActivityId.Value, "", answerDefaultRightness);
			}
			catch (Exception e)
			{
				this.Notifier.Notify(Color.Danger, e.Message);
				return;
			}

			questionModel.Answers.Add(new AnswerUpdateModel
			{
				Id = newAnswerId,
				IsCorrect = answerDefaultRightness
			});

			this.StateHasChanged();
		}

		public async Task EditAnswer(AnswerUpdateModel answerModel, string field, object value)
		{
			if (!answerModel.Id.HasValue)
			{
				Console.WriteLine("Unable to edit answer - no Id");
				return;
			}

			if (this.Model is QuestionCreateModel question)
			{

				var index = question.Answers.IndexOf(answerModel);

				if (field == "img")
				{
					await this.ActivityRepository.UpdateAnswer(answerModel.Id.Value, answerModel.IsCorrect, (string)value);
					question.Answers[index].Content = (string)value;
				}
				else if (field == "correct")
				{
					var prevVal = question.Answers[index].IsCorrect;
					question.Answers[index].IsCorrect = (bool)value;
					if ((QuestionActivityType)question.ActivityQuestionType != QuestionActivityType.Question && question.Answers.All(x => !x.IsCorrect))
					{
						this.ErrorMessage = "Il faut une bonne réponse dans la cadre d'un QCM.";
						this.Errors = new string[] { "Il faut une bonne réponse dans la cadre d'un QCM." };

						question.Answers[index].IsCorrect = prevVal;
						this.StateHasChanged();
						return;
					}

					await this.ActivityRepository.UpdateAnswer(answerModel.Id.Value, (bool)value, answerModel.Content);

					foreach (var item in question.Answers)
					{
						if ((bool)value)
						{
							if (question.Answers.IndexOf(item) != index)
							{
								item.IsCorrect = false;
							}
						}
					}

				}
				else
				{
					Console.WriteLine("Unable to edit answer - unkonw field : " + field);
				}
			}
			else
			{
				Console.WriteLine("Unable to edit answer - Not a question");
			}

			this.StateHasChanged();
		}

		public async Task OnActivityDeleted(Guid activityId)
		{
			if (activityId == this.ActivityId)
			{
				this.NavigationManager.NavigateTo(LocatedGameUnitEdition.GetUrl(this.FieldTripId, this.LocatedGameUnitId));
			}

			await Task.CompletedTask;
		}

		public async Task OnUnitDeleted(Guid unitId)
		{
			if (unitId == this.LocatedGameUnitId)
			{
				this.NavigationManager.NavigateTo(FieldTripEditionPage.GetEditionUrl(this.FieldTripId));
			}

			await Task.CompletedTask;
		}

		protected void NavigateToUnitEditionActivitiesOptions()
		{
			this.NavigationManager.NavigateTo(string.Concat(LocatedGameUnitEdition.GetUrl(this.FieldTripId, this.LocatedGameUnitId), "?activities-options"));
			this.StateHasChanged();
		}

		private void OnMaxUploadedFileSizeReached(int size)
		{
			string sizeToDisplay = "";

			if (size >= 1_000 && size < 1_000_000)
			{
				sizeToDisplay = $"{decimal.Round((decimal)size / 1_000, 1, MidpointRounding.AwayFromZero)} Ko";
			}
			else if (size >= 1_000_000 && size < 1_000_000_000)
			{
				sizeToDisplay = $"{decimal.Round((decimal)size / 1_000_000, 1, MidpointRounding.AwayFromZero)} Mo";
			}
			else
			{
				sizeToDisplay = $"{size} o)";
			}

			this.Notifier.Notify(Color.Warning, $"Attention, la taille de votre fichier est de {sizeToDisplay}");
		}
	}

}
