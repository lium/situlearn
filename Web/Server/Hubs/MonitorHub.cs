﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Server.Hubs
{
    public class MonitorHub : Hub
    {
        public static ConcurrentDictionary<string, (Guid, TimeOnly)> Connections = new ConcurrentDictionary<string, (Guid, TimeOnly)>();

        public void OnConnected(string hubConnectionId, Guid fieldtripId, TimeOnly time)
        {
            Connections.TryAdd(hubConnectionId, (fieldtripId, time));
        }

        public void OnDisconnected(string hubConnectionId)
        {
            Connections.TryRemove(hubConnectionId, out (Guid, TimeOnly) retrievedValue);
        }

        public async Task SendNewPositionAndBatteryLevel(Guid fieldtripId, TimeOnly startTime, Guid playerId, decimal latitude, decimal longitude, decimal? batteryLevel)
        {
            var recipientConnectionIds = Connections.Where(c => c.Value.Item1 == fieldtripId && c.Value.Item2 <= startTime).Select(c => c.Key);
            
            await Clients.Clients(recipientConnectionIds).SendAsync("RefreshPositionAndBatteryLevel", playerId, latitude, longitude, batteryLevel);
        }

        public async Task SendOutOfExplorationAreaAlert(Guid fieldtripId, TimeOnly startTime, Guid playerId, bool isOutOfExplorationArea)
        {
            var recipientConnectionIds = Connections.Where(c => c.Value.Item1 == fieldtripId && c.Value.Item2 <= startTime).Select(c => c.Key);
            
            await Clients.Clients(recipientConnectionIds).SendAsync("RefreshOutOfExplorationAreaAlert", playerId, isOutOfExplorationArea);
        }

        public async Task SendNewPlayerInGame(Guid fieldtripId, TimeOnly startTime, PlayerForMonitorModel player)
        {
            var recipientConnectionIds = Connections.Where(c => c.Value.Item1 == fieldtripId && c.Value.Item2 <= startTime).Select(c => c.Key);
            
            await Clients.Clients(recipientConnectionIds).SendAsync("AddNewPlayer", player);
        }

        public async Task SendStartNewUnit(Guid fieldtripId, TimeOnly startTime, Guid playerId, int playedUnitsNumber, string currentUnitName)
        {
            var recipientConnectionIds = Connections.Where(c => c.Value.Item1 == fieldtripId && c.Value.Item2 <= startTime).Select(c => c.Key);
            
            await Clients.Clients(recipientConnectionIds).SendAsync("RefreshCurrentUnit", playerId, playedUnitsNumber, currentUnitName);
        }

        public async Task SendCurrentUnitIsLocated(Guid fieldtripId, TimeOnly startTime, Guid playerId, int playedUnitsNumber, string currentUnitName, int locationPoints)
        {
            var recipientConnectionIds = Connections.Where(c => c.Value.Item1 == fieldtripId && c.Value.Item2 <= startTime).Select(c => c.Key);
            
            await Clients.Clients(recipientConnectionIds).SendAsync("RefreshCurrentUnitAndAddLocationPoints", playerId, playedUnitsNumber, currentUnitName, locationPoints);
        }

        public async Task SendNewQuestionAnswered(Guid fieldtripId, TimeOnly startTime, Guid playerId, bool isCorrect, int points)
        {
            var recipientConnectionIds = Connections.Where(c => c.Value.Item1 == fieldtripId && c.Value.Item2 <= startTime).Select(c => c.Key);
            
            await Clients.Clients(recipientConnectionIds).SendAsync("RefreshCorrectAnswersRateAndScore", playerId, isCorrect, points);
        }

        public async Task SendIncrementReadingsNumber(Guid fieldtripId, TimeOnly startTime, Guid playerId)
        {
            var recipientConnectionIds = Connections.Where(c => c.Value.Item1 == fieldtripId && c.Value.Item2 <= startTime).Select(c => c.Key);
            
            await Clients.Clients(recipientConnectionIds).SendAsync("IncrementReadingsNumber", playerId);
        }

        public async Task SendEndGame(Guid fieldtripId, TimeOnly startTime, Guid playerId)
        {
            var recipientConnectionIds = Connections.Where(c => c.Value.Item1 == fieldtripId && c.Value.Item2 <= startTime).Select(c => c.Key);
            
            await Clients.Clients(recipientConnectionIds).SendAsync("EndGame", playerId);
        }

        public async Task RefreshPlayer(Guid fieldtripId, TimeOnly startTime, PlayerForMonitorModel player)
        {
            var recipientConnectionIds = Connections.Where(c => c.Value.Item1 == fieldtripId && c.Value.Item2 <= startTime).Select(c => c.Key);
            
            await Clients.Clients(recipientConnectionIds).SendAsync("RefreshReconnectedPlayer", player);
        }
    }
}
