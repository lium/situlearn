﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class JourneyLocatedGameUnitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LocatedGameUnits_Journeys_JourneyId",
                table: "LocatedGameUnits");

            migrationBuilder.DropIndex(
                name: "IX_LocatedGameUnits_JourneyId",
                table: "LocatedGameUnits");

            migrationBuilder.DropColumn(
                name: "JourneyId",
                table: "LocatedGameUnits");

            migrationBuilder.CreateTable(
                name: "JourneyLocatedGameUnit",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    JourneyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    LocatedGameUnitId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JourneyLocatedGameUnit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JourneyLocatedGameUnit_Journeys_JourneyId",
                        column: x => x.JourneyId,
                        principalTable: "Journeys",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_JourneyLocatedGameUnit_LocatedGameUnits_LocatedGameUnitId",
                        column: x => x.LocatedGameUnitId,
                        principalTable: "LocatedGameUnits",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_JourneyLocatedGameUnit_JourneyId",
                table: "JourneyLocatedGameUnit",
                column: "JourneyId");

            migrationBuilder.CreateIndex(
                name: "IX_JourneyLocatedGameUnit_LocatedGameUnitId",
                table: "JourneyLocatedGameUnit",
                column: "LocatedGameUnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JourneyLocatedGameUnit");

            migrationBuilder.AddColumn<Guid>(
                name: "JourneyId",
                table: "LocatedGameUnits",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LocatedGameUnits_JourneyId",
                table: "LocatedGameUnits",
                column: "JourneyId");

            migrationBuilder.AddForeignKey(
                name: "FK_LocatedGameUnits_Journeys_JourneyId",
                table: "LocatedGameUnits",
                column: "JourneyId",
                principalTable: "Journeys",
                principalColumn: "Id");
        }
    }
}
