﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Entities;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.JSInterop;
using Ozytis.Common.Core.ClientApi.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Player.Repositories.IndexedDbManager
{
    public class GamesManager : IndexedDbBaseManager<Game>
    {
		public override string StoreName { get { return "Games"; } }
		public GamesManager(LocalStorageService locaStorageService, IJSRuntime jsRuntime) : base(locaStorageService, jsRuntime, true)
        {

        }

        public override async Task CreateOrUpdateById(string id, Game entity)
        {
            await this.SetItemAsync(GetKey(id, this.EntitiyType), entity, new Dictionary<string, object> { { nameof(Game.FielTripId), entity.FielTripId.ToString() } });
        }
        public override async Task BulkCreateOrUpdateById(Dictionary<Guid, Game> data)
        {
            var transformed = data.ToDictionary(kv => GetKey(kv.Key.ToString(), this.EntitiyType), kv => (object)kv.Value);
            var indexes = data.ToDictionary(kv => GetKey(kv.Key.ToString(), this.EntitiyType), kv => new Dictionary<string, object> { { nameof(Game.FielTripId), kv.Value.FielTripId.ToString() } });
            //Console.WriteLine("Bulk FieldTrip");

            await this.SetBulkItemsAsync(transformed, indexes);
        }
    }
}
