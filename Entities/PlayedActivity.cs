﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    public class PlayedActivity
    {
        public Guid Id { get; set; }

        [ForeignKey(nameof(PlayedUnit))]
        public Guid PlayedUnitId { get; set; }

        public PlayedUnit PlayedUnit { get; set; }

        [ForeignKey(nameof(Activity))]
        public Guid ActivityId { get; set; }

        public LocatedActivity Activity { get; set; }

        [MaxLength(100)]
        public string ActivityName { get; set; }

        public ICollection<PlayedActivityFile> Files { get; set; }

        [MaxLength(Entities.Answer.AnswerMaxLength)]
        public string Answer { get; set; }

        public DateTime AnswerDate { get; set; }

        public int? Points { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? Latitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? Longitude { get; set; }
    }
}