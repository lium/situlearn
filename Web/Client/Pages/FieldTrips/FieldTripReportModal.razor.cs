﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.FieldTrips
{
    public partial class FieldTripReportModal : ComponentBase
    {
        [Parameter]
        public EventCallback OnClose { get; set; }

        [Parameter]
        public FieldTrip FieldTrip { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        public DateTime? ReportFilterStartDate { get; set; }
        public DateTime? ReportFilterStartTime { get; set; }
        public DateTime? ReportFilterEndDate { get; set; }
        public DateTime? ReportFilterEndTime { get; set; }

        public DateTime ReportStartDate { get; set; }
        public DateTime ReportEndDate { get; set; }

        public bool ShowDeadlineAlert { get; set; }

        public IEnumerable<Player> FieldtripPlayers { get; set; }

        public int ParticipantsNumber { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.FieldtripPlayers = await this.FieldTripsRepository.SelectAllPlayersByFieldTripIdAsync(this.FieldTrip.Id);

            await base.OnInitializedAsync();
        }

        protected async Task OnCloseReportModal()
        {
            await this.OnClose.InvokeAsync();

            this.StateHasChanged();
        }

        protected void OnChangeReportFilterStartDate(DateTime? startDate)
        {
            this.ReportFilterStartDate = startDate;

            if (startDate.HasValue && startDate < DateTime.Now.AddMonths(-3))
            {
                this.ShowDeadlineAlert = true;
            }
            else
            {
                this.ShowDeadlineAlert = false;
            }

            if (!this.ReportFilterEndDate.HasValue || this.ReportFilterEndDate.Value < this.ReportFilterStartDate)
            {
                this.ReportFilterEndDate = startDate;
            }

            this.CalculateParticipantsNumber();

            this.StateHasChanged();
        }

        protected void OnChangeReportFilterStartTime(DateTime? endTime)
        {
            this.ReportFilterStartTime = endTime;

            this.CalculateParticipantsNumber();

            this.StateHasChanged();
        }

        protected void OnChangeReportFilterEndDate(DateTime? endDate)
        {
            this.ReportFilterEndDate = endDate;

            this.CalculateParticipantsNumber();

            this.StateHasChanged();
        }

        protected void OnChangeReportFilterEndTime(DateTime? endTime)
        {
            this.ReportFilterEndTime = endTime;

            this.CalculateParticipantsNumber();

            this.StateHasChanged();
        }

        protected void CalculateParticipantsNumber()
        {
            if (this.ReportFilterStartDate.HasValue && this.ReportFilterStartTime.HasValue && this.ReportFilterEndDate.HasValue && this.ReportFilterEndTime.HasValue)
            {
                this.ReportStartDate = new DateTime(this.ReportFilterStartDate.Value.Year, this.ReportFilterStartDate.Value.Month, this.ReportFilterStartDate.Value.Day,
                    this.ReportFilterStartTime.Value.Hour, this.ReportFilterStartTime.Value.Minute, this.ReportFilterStartTime.Value.Second);

                this.ReportEndDate = new DateTime(this.ReportFilterEndDate.Value.Year, this.ReportFilterEndDate.Value.Month, this.ReportFilterEndDate.Value.Day,
                    this.ReportFilterEndTime.Value.Hour, this.ReportFilterEndTime.Value.Minute, this.ReportFilterEndTime.Value.Second);

                this.ParticipantsNumber = this.FieldtripPlayers.Count(p => p.StartPlayedDate >= this.ReportStartDate && p.StartPlayedDate <= this.ReportEndDate);
            }

            this.StateHasChanged();
        }

        protected async Task OnSuccessDownload()
        {
            
            await this.OnCloseReportModal();

            this.StateHasChanged();
        }
    }
}
