﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;
using DataAccess;
using Entities;
using Microsoft.EntityFrameworkCore;
using MimeTypes;
using NetTopologySuite.Geometries;
using Ozytis.Common.Core.Storage;
using Ozytis.Common.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace Business
{
    public class FieldTripsManager : BaseEntityManager<FieldTrip>
    {
        public GamesManager GamesManager { get; set; }

        public FieldTripsManager(DataContext context, IFileSystem fileSystem, GamesManager gamesManager, LocatedGameUnitsManager locatedGameUnitsManager) : base(context)
        {
            this.GamesManager = gamesManager;
            this.LocatedGameUnitsManager = locatedGameUnitsManager;
            this.FileSystem = fileSystem;
        }

        public IFileSystem FileSystem { get; }
        public LocatedGameUnitsManager LocatedGameUnitsManager { get; }

        public async Task<FieldTrip> CreateFieldTripAsync(FieldTrip fieldTrip, ApplicationUser designer)
        {
            ExplorationMap explorationMap = new ExplorationMap();
            explorationMap.Id = Guid.NewGuid();
            explorationMap.IsGeolocatable = true;

            this.DataContext.ExplorationMaps.Add(explorationMap);

            fieldTrip.ExplorationMapId = explorationMap.Id;
            fieldTrip.Id = Guid.NewGuid();
            fieldTrip.DesignerId = designer.Id;
            fieldTrip.Establishment = designer.Establishment;
            fieldTrip.LastUpdateDate = DateTime.UtcNow;
            fieldTrip.Journeys = new List<Journey>();
            fieldTrip.IsVisible = true;

            fieldTrip.HomePageTitle = fieldTrip.Name;
            fieldTrip.EndPageTitle = fieldTrip.Name;

            if (!fieldTrip.Journeys?.Any() ?? false)
            {
                fieldTrip.Journeys.Add(new Journey { Color = ColorJourney.Green, Name = ColorJourney.Green.GetFriendlyName() });
            }

            fieldTrip = this.DataContext.FieldTrips.Add(fieldTrip).Entity;

            await this.SaveChangesAsync();

            return fieldTrip;
        }

        public async Task DeleteFieldTripAsync(Guid fieldTripId, ApplicationUser deleter)
        {
            FieldTrip fieldTrip = await this.SelectAll()
                .Include(f => f.LocatedGameUnits)
                    .ThenInclude(g => g.Activities)
                    .ThenInclude(a => (a as QuestionActivity).Answers)
                .Include(f => f.Journeys)
                    .ThenInclude(j => j.JourneyLocatedGameUnits)
                    .ThenInclude(jlg => jlg.LocatedGameUnit)
                    .ThenInclude(lgu => lgu.Activities)
                    .ThenInclude(a => (a as QuestionActivity).Answers)
                .Include(f => f.Journeys)
                    .ThenInclude(j => j.JourneyLocatedGameUnits)
                    .ThenInclude(jlg => jlg.LocatedGameUnit)
                    .ThenInclude(lgu => lgu.Activities)
                    .ThenInclude(a => (a as QuestionActivity).Clue)
                .Include(f => f.ExplorationMap)
                .FirstOrDefaultAsync(f => f.Id == fieldTripId);

            if (fieldTrip == null)
            {
                return;
            }

            if (deleter.Id != fieldTrip.DesignerId && !deleter.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            using TransactionScope transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var removingFiles = new List<string>();

            try
            {
                await this.GamesManager.DeleteFieldTripGamesAsync(fieldTripId);
                await this.DeleteUserFieldTripsAsync(fieldTripId);
                foreach (var journey in fieldTrip.Journeys)
                {
                    foreach (var journeyLocatedGameUnit in journey.JourneyLocatedGameUnits)
                    {
                        this.DataContext.JourneyLocatedGameUnits.Remove(journeyLocatedGameUnit);
                    }
                }

                if (!string.IsNullOrEmpty(fieldTrip.ExplorationMap.CustomBackgroundMap) && await this.FileSystem.ExistsAsync(fieldTrip.ExplorationMap.CustomBackgroundMap))
                {
                    removingFiles.Add(fieldTrip.ExplorationMap.CustomBackgroundMap);
                }

                if (!string.IsNullOrEmpty(fieldTrip.HomePagePicture) && await this.FileSystem.ExistsAsync(fieldTrip.HomePagePicture))
                {
                    removingFiles.Add(fieldTrip.HomePagePicture);
                }

                if (!string.IsNullOrEmpty(fieldTrip.EndPagePicture) && await this.FileSystem.ExistsAsync(fieldTrip.EndPagePicture))
                {
                    removingFiles.Add(fieldTrip.EndPagePicture);
                }

                foreach (var gameUnit in fieldTrip.LocatedGameUnits)
                {
                    if (!string.IsNullOrEmpty(gameUnit.ConclusionPicture) && await this.FileSystem.ExistsAsync(gameUnit.ConclusionPicture))
                    {
                        removingFiles.Add(gameUnit.ConclusionPicture);
                    }

                    if (!string.IsNullOrEmpty(gameUnit.InformationSheetPicture) && await this.FileSystem.ExistsAsync(gameUnit.InformationSheetPicture))
                    {
                        removingFiles.Add(gameUnit.InformationSheetPicture);
                    }

                    if (!string.IsNullOrEmpty(gameUnit.LocationSupportPicture) && await this.FileSystem.ExistsAsync(gameUnit.LocationSupportPicture))
                    {
                        removingFiles.Add(gameUnit.LocationSupportPicture);
                    }

                    foreach (var activity in gameUnit.Activities)
                    {
                        if (!string.IsNullOrEmpty(activity.Picture) && await this.FileSystem.ExistsAsync(activity.Picture))
                        {
                            removingFiles.Add(activity.Picture);
                        }

                        if (activity.ActivityType == ActivityType.Question)
                        {
                            var question = activity as QuestionActivity;
                            if (!string.IsNullOrEmpty(question?.Clue?.Picture) && await this.FileSystem.ExistsAsync(question.Clue.Picture))
                            {
                                removingFiles.Add(question.Clue.Picture);
                            }


                            foreach (var answer in question.Answers)
                            {
                                if (!string.IsNullOrEmpty(answer.Content) && await this.FileSystem.ExistsAsync(answer.Content))
                                {
                                    removingFiles.Add(answer.Content);
                                }
                            }


                        }
                    }
                }

                this.DataContext.Remove(fieldTrip);
                this.DataContext.Remove(fieldTrip.ExplorationMap);

                await this.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new BusinessException(e.Message);
            }

            transactionScope.Complete();

            foreach (var fileToRemove in removingFiles)
            {
                await this.FileSystem.DeleteAsync(fileToRemove);
            }
        }

        public async Task<FieldTrip> UpdateVisibilityAsync(Guid fieldTripId, ApplicationUser updater)
        {
            FieldTrip fieldTrip = await this.SelectAsync(fieldTripId, f => f.LocatedGameUnits);

            if (fieldTrip == null)
            {
                return fieldTrip;
            }

            if (updater.Id != fieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            fieldTrip.IsVisible = !fieldTrip.IsVisible;
            fieldTrip.LastUpdateDate = DateTime.UtcNow;

            await this.SaveChangesAsync();

            return fieldTrip;
        }

        public async Task<FieldTrip> UpdatePublishStatusAsync(Guid fieldTripId, ApplicationUser updater)
        {
            FieldTrip fieldTrip = await this.SelectAsync(fieldTripId, f => f.LocatedGameUnits);

            if (fieldTrip == null)
            {
                return fieldTrip;
            }

            if (updater.Id != fieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            fieldTrip.IsPublished = !fieldTrip.IsPublished;
            fieldTrip.LastUpdateDate = DateTime.UtcNow;

            await this.SaveChangesAsync();

            return fieldTrip;
        }

        public async Task<LocatedGameUnit> AddLocatedUnitAsync(LocatedGameUnit locatedGameUnit, ApplicationUser updater)
        {
            FieldTrip fieldTrip = await this.SelectAll()
                .Include(f => f.LocatedGameUnits)
                .Include(f => f.Journeys)
                .ThenInclude(j => j.JourneyLocatedGameUnits)
                    .ThenInclude(jlg => jlg.LocatedGameUnit)
                .FirstOrDefaultAsync(f => f.Id == locatedGameUnit.FieldTripId);

            if (fieldTrip == null)
            {
                return null;
            }

            if (updater.Id != fieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            locatedGameUnit.Order = fieldTrip.LocatedGameUnits.Any() ? fieldTrip.LocatedGameUnits.Max(t => t.Order) + 1 : 0;
            locatedGameUnit.Id = Guid.NewGuid();
            locatedGameUnit.ActivationAreaType = ActivationAreaType.Circle;
            locatedGameUnit.LocationSupportTitle = locatedGameUnit.Name;
            locatedGameUnit.ActivationRadius = 10;
            locatedGameUnit.ActivationAreaCenterLatitude = locatedGameUnit.Latitude;
            locatedGameUnit.ActivationAreaCenterLongitude = locatedGameUnit.Longitude;
            locatedGameUnit = this.DataContext.LocatedGameUnits.Add(locatedGameUnit).Entity;
            locatedGameUnit.LocationSupportTitle = locatedGameUnit.Name;
            fieldTrip.LastUpdateDate = DateTime.UtcNow;

            foreach (var journey in fieldTrip.Journeys)
            {
                journey.JourneyLocatedGameUnits.Add(new JourneyLocatedGameUnit
                {
                    JourneyId = journey.Id,
                    LocatedGameUnitId = locatedGameUnit.Id,
                    Order = journey.JourneyLocatedGameUnits.Any() ? journey.JourneyLocatedGameUnits.Max(jlgu => jlgu.Order) + 5 : 0
                });
            }

            await this.SaveChangesAsync();

            return locatedGameUnit;
        }

        public async Task DeleteLocatedUnitAsync(Guid fieldTripId, Guid locatedUnitId, ApplicationUser deleter)
        {
            FieldTrip fieldTrip = await this.SelectAll()
                .Include(ft => ft.LocatedGameUnits)
                .ThenInclude(lgu => lgu.JourneyLocatedGameUnits)
                .FirstOrDefaultAsync(f => f.Id == fieldTripId);

            if (fieldTrip == null)
            {
                return;
            }

            if (deleter.Id != fieldTrip.DesignerId && !deleter.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            var locatedUnit = fieldTrip.LocatedGameUnits.FirstOrDefault(lu => lu.Id == locatedUnitId);

            if (locatedUnit == null)
            {
                return;
            }

            foreach (var link in locatedUnit.JourneyLocatedGameUnits)
            {
                this.DataContext.Remove(link);
            }

            locatedUnit.IsDeleted = true;
            fieldTrip.LastUpdateDate = DateTime.UtcNow;

            await this.SaveChangesAsync();
        }

        public async Task<LocatedGameUnit> UpdateLocatedUnitInfoAsync(LocatedGameUnit locatedGameUnit, ApplicationUser updater)
        {
            FieldTrip fieldTrip = await this.SelectAsync(locatedGameUnit.FieldTripId, ft => ft.LocatedGameUnits);

            if (fieldTrip == null)
            {
                return null;
            }

            if (updater.Id != fieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            var original = fieldTrip.LocatedGameUnits.FirstOrDefault(lu => lu.Id == locatedGameUnit.Id);

            if (original == null)
            {
                return null;
            }

            original.Name = locatedGameUnit.Name;
            original.Latitude = locatedGameUnit.Latitude;
            original.Longitude = locatedGameUnit.Longitude;
            original.ActivationAreaType = locatedGameUnit.ActivationAreaType;
            original.ActivationArea = locatedGameUnit.ActivationArea;

            if (original.ActivationArea != null && original.ActivationArea is Polygon polygon && !polygon.Shell.IsCCW)
            {
                original.ActivationArea = (Polygon)((Geometry)polygon).Reverse();
            }

            original.ActivationRadius = locatedGameUnit.ActivationRadius;
            original.ActivationAreaCenterLatitude = locatedGameUnit.ActivationAreaCenterLatitude;
            original.ActivationAreaCenterLongitude = locatedGameUnit.ActivationAreaCenterLongitude;
            fieldTrip.LastUpdateDate = DateTime.UtcNow;

            await this.SaveChangesAsync();

            return original;

        }

        public async Task UpdateMapPositionAsync(Guid fieldTripId, ExplorationMap explorationMap, ApplicationUser updater)
        {
            FieldTrip fieldTrip = await this.SelectAsync(fieldTripId, ft => ft.ExplorationMap);

            if (fieldTrip == null)
            {
                return;
            }

            if (updater.Id != fieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            if (string.IsNullOrEmpty(explorationMap.CustomBackgroundMap) && !explorationMap.IsGeolocatable)
            {
                throw new BusinessException("La carte n'étant pas géolocalisée vous devez fournir un fond personnalisé");
            }
            fieldTrip.ExplorationMap.CenterLatitude = explorationMap.CenterLatitude;
            fieldTrip.ExplorationMap.CenterLongitude = explorationMap.CenterLongitude;
            fieldTrip.ExplorationMap.Zoom = explorationMap.Zoom;
            fieldTrip.ExplorationMap.IsGeolocatable = explorationMap.IsGeolocatable;
            fieldTrip.ExplorationMap.CustomBackgroundLowerRightLatitude = explorationMap.CustomBackgroundLowerRightLatitude;
            fieldTrip.ExplorationMap.CustomBackgroundLowerRightLongitude = explorationMap.CustomBackgroundLowerRightLongitude;
            fieldTrip.ExplorationMap.SouthWestLatitude = explorationMap.SouthWestLatitude;
            fieldTrip.ExplorationMap.SouthWestLongitude = explorationMap.SouthWestLongitude;
            fieldTrip.ExplorationMap.NorthEastLatitude = explorationMap.NorthEastLatitude;
            fieldTrip.ExplorationMap.NorthEastLongitude = explorationMap.NorthEastLongitude;
            fieldTrip.ExplorationMap.CustomBackgroundOpacity = explorationMap.CustomBackgroundOpacity;
            fieldTrip.ExplorationMap.CustomBackgroundRotation = explorationMap.CustomBackgroundRotation;
            fieldTrip.ExplorationMap.CustomBackgroundUpperLeftLatitude = explorationMap.CustomBackgroundUpperLeftLatitude;
            fieldTrip.ExplorationMap.CustomBackgroundUpperLeftLongitude = explorationMap.CustomBackgroundUpperLeftLongitude;

            fieldTrip.ExplorationMap.ParticipantOutOfZoneAlert = explorationMap.ParticipantOutOfZoneAlert;
            fieldTrip.ExplorationMap.OrganizerOutOfZoneAlert = explorationMap.OrganizerOutOfZoneAlert;
            fieldTrip.ExplorationMap.ExplorationArea = explorationMap.ExplorationArea;
            fieldTrip.ExplorationMap.ExplorationAreaCenterLatitude = explorationMap.ExplorationAreaCenterLatitude;
            fieldTrip.ExplorationMap.ExplorationAreaCenterLongitude = explorationMap.ExplorationAreaCenterLongitude;
            fieldTrip.ExplorationMap.ExplorationAreaRadius = explorationMap.ExplorationAreaRadius;
            fieldTrip.ExplorationMap.ExplorationShape = explorationMap.ExplorationShape;

            fieldTrip.LastUpdateDate = DateTime.UtcNow;

            //s'il existait déjà un fond de carte personnalisé, on supprime dans un premier temps le fichier
            if (!string.IsNullOrEmpty(fieldTrip.ExplorationMap.CustomBackgroundMap))
            {
                await this.FileSystem.DeleteAsync(fieldTrip.ExplorationMap.CustomBackgroundMap);
			}

			//puis on en créé un nouveau avec un nom différent
			fieldTrip.ExplorationMap.CustomBackgroundMap = await this.FileSystem.SetFile(explorationMap.CustomBackgroundMap, $"customaps/{Guid.NewGuid()}");

			await this.SaveChangesAsync();

        }

        public async Task<FieldTrip> UpdateFieldTripAsync(FieldTrip fieldTrip, ApplicationUser updater)
        {
            var original = await this.SelectAsync(fieldTrip.Id);

            if (original == null)
            {
                return null;
            }

            if (updater.Id != original.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            original.Name = fieldTrip.Name;
            original.AllottedTimeDuration = fieldTrip.AllottedTimeDuration;
            original.Description = fieldTrip.Description;
            original.PedagogicalFieldName = fieldTrip.PedagogicalFieldName;
            //original.ReadingsAreVisible = fieldTrip.ReadingsAreVisible;
            //original.ReadingsVisibilityEndDate = fieldTrip.ReadingsVisibilityEndDate;
            //original.ReadingsVisibilityStartDate = fieldTrip.ReadingsVisibilityStartDate;
            original.TimeIsLimited = fieldTrip.TimeIsLimited;
            original.LastUpdateDate = DateTime.UtcNow;
            original.ShowElapsedTime = fieldTrip.ShowElapsedTime;

            original.EndPagePicture = await this.FileSystem.SetFile(fieldTrip.EndPagePicture, $"pages/{fieldTrip.Id}_end");
            original.EndPageText = fieldTrip.EndPageText;
            original.EndPageTitle = fieldTrip.EndPageTitle;

            original.HomePagePicture = await this.FileSystem.SetFile(fieldTrip.HomePagePicture, $"pages/{fieldTrip.Id}_home");
            original.HomePageText = fieldTrip.HomePageText;
            original.HomePageTitle = fieldTrip.HomePageTitle;

            original.AllowTeamNames = fieldTrip.AllowTeamNames;

            await this.SaveChangesAsync();

            return original;
        }

        public async Task<Journey> UpdateJourneyInfoAsync(Journey journey, ApplicationUser updater)
        {
            FieldTrip fieldTrip = await this.SelectAsync(journey.FieldTripId, ft => ft.Journeys);

            if (fieldTrip == null)
            {
                return null;
            }

            if (updater.Id != fieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            var original = fieldTrip.Journeys.FirstOrDefault(j => j.Id == journey.Id);

            if (original == null)
            {
                return original;
            }

            original.Color = journey.Color;
            original.Name = original.Color.GetFriendlyName();
            fieldTrip.LastUpdateDate = DateTime.UtcNow;
            await this.SaveChangesAsync();

            return original;
        }

        public async Task DeleteJourneyAsync(Guid fieldTripId, Guid journeyId, ApplicationUser deleter)
        {
            FieldTrip fieldTrip = await this.SelectAll()
                .Include(f => f.Journeys)
                .ThenInclude(j => j.JourneyLocatedGameUnits)
                    .ThenInclude(jlg => jlg.LocatedGameUnit)
                .FirstOrDefaultAsync(f => f.Id == fieldTripId);

            if (fieldTrip == null)
            {
                return;
            }

            if (deleter.Id != fieldTrip.DesignerId && !deleter.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            if (fieldTrip.Journeys.Count == 1)
            {
                throw new BusinessException("Vous ne pouvez pas supprimer le seul parcours d'une sortie");
            }

            var original = fieldTrip.Journeys.FirstOrDefault(j => j.Id == journeyId);

            if (original == null)
            {
                return;
            }

            var players = this.DataContext.Players.Where(p => p.JourneyId == journeyId).ToArray();
            this.DataContext.Players.RemoveRange(players);

            // set orders right
            var removingOrder = original.Order;
            foreach (var item in fieldTrip.Journeys.Where(j => j.Order > original.Order))
            {
                item.Order -= 1;
            }

            this.DataContext.JourneyLocatedGameUnits.RemoveRange(original.JourneyLocatedGameUnits);
            this.DataContext.Journeys.Remove(original);
            fieldTrip.LastUpdateDate = DateTime.UtcNow;

            await this.SaveChangesAsync();

        }

        public async Task<JourneyLocatedGameUnit> AddLocatedUnitToJourneyAsync(Guid fieldTripId, JourneyLocatedGameUnit journeyLocatedGameUnit, ApplicationUser updater)
        {
            //Console.Write("adding new game unit at position ");
            //Console.WriteLine(journeyLocatedGameUnit.Order);

            FieldTrip fieldTrip = await this.SelectAll()
                .Include(f => f.Journeys)
                .ThenInclude(j => j.JourneyLocatedGameUnits)
                    .ThenInclude(jlg => jlg.LocatedGameUnit)
                .FirstOrDefaultAsync(f => f.Id == fieldTripId);

            if (fieldTrip == null)
            {
                Console.WriteLine("Fieldtrip not found");
                return null;
            }

            Console.WriteLine("Fieldtrip found");

            if (updater.Id != fieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                Console.WriteLine("Not authorized");
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            var journey = fieldTrip.Journeys.FirstOrDefault(j => j.Id == journeyLocatedGameUnit.JourneyId);

            if (journey == null)
            {
                Console.WriteLine("Journey not found");
                return null;
            }

            //Console.Write("Journeyfound : ");
            //Console.Write(journey.JourneyLocatedGameUnits.Count);
            //Console.WriteLine(" unités de jeu");

            // TODO: vérifier si l'unité peut être ajoutée à ce parcours
            if (journey.JourneyLocatedGameUnits.Any(jlg => jlg.LocatedGameUnitId == journeyLocatedGameUnit.LocatedGameUnitId))
            {
                Console.WriteLine("Unit already added");
                return null;
            }

            //int fakeOrder = 0;

            foreach (var unitLink in journey.JourneyLocatedGameUnits.OrderBy(l => l.Order))
            {
                if (unitLink.Order >= journeyLocatedGameUnit.Order)
                {
                    unitLink.Order += 5;
                }
            }

            journeyLocatedGameUnit.Id = Guid.NewGuid();
            journeyLocatedGameUnit.Order = Math.Min(journeyLocatedGameUnit.Order, journey.JourneyLocatedGameUnits.Count * 5);

            journeyLocatedGameUnit = this.DataContext.JourneyLocatedGameUnits.Add(journeyLocatedGameUnit).Entity;
            fieldTrip.LastUpdateDate = DateTime.UtcNow;
            await this.SaveChangesAsync();

            return journeyLocatedGameUnit;
        }

        public async Task UpdateGameUnitOrderAsync(Guid fieldTripId, Guid journeyId, Guid journeyUnitLinkId, int newOrder, ApplicationUser updater)
        {
            FieldTrip fieldTrip = await this.SelectAll().Include(f => f.Journeys)
                 .ThenInclude(j => j.JourneyLocatedGameUnits).ThenInclude(jlg => jlg.LocatedGameUnit)
                 .FirstOrDefaultAsync(f => f.Id == fieldTripId);

            if (fieldTrip == null)
            {
                Console.WriteLine("Fieldtrip not found");
                return;
            }

            Console.WriteLine("Fieldtrip found");

            if (updater.Id != fieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                Console.WriteLine("Not authorized");
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            var journey = fieldTrip.Journeys.FirstOrDefault(j => j.Id == journeyId);

            if (journey == null)
            {
                Console.WriteLine("Journey not found");
                return;
            }

            var link = journey.JourneyLocatedGameUnits.FirstOrDefault(j => j.Id == journeyUnitLinkId);

            if (link == null)
            {
                return;
            }

            //Console.Write("Updating game unit order from ");
            //Console.Write(link.Order);
            //Console.Write(" to ");
            //Console.WriteLine(newOrder);

            if (newOrder > link.Order)
            {
                //Console.WriteLine("link moving forward");
                // moving forward : links between move backward
                foreach (var unitLink in journey.JourneyLocatedGameUnits.Where(u => link.Order < u.Order && u.Order <= newOrder))
                {
                    unitLink.Order -= 5;
                }
            }
            else
            {
                //Console.WriteLine("link moving backward");
                // moving backward : links between move forward
                foreach (var unitLink in journey.JourneyLocatedGameUnits.Where(u => newOrder <= u.Order && u.Order < link.Order))
                {
                    unitLink.Order += 5;
                }
            }

            link.Order = newOrder;

            fieldTrip.LastUpdateDate = DateTime.UtcNow;
            await this.SaveChangesAsync();
        }

        public async Task<Guid> DuplicateAsync(Guid fieldTripId, ApplicationUser copycat)
        {
            var original = await this.DataContext.FieldTrips
                .Include(ft => ft.ExplorationMap)
                .Include(ft => ft.LocatedGameUnits)
                    .ThenInclude(lgu => lgu.Activities)
                .Include(ft => ft.Journeys)
                    .ThenInclude(j => j.JourneyLocatedGameUnits)
                .FirstOrDefaultAsync(ft => ft.Id == fieldTripId);

            if (original == null)
            {
                throw new BusinessException("Sortie inconnue : " + fieldTripId);
            }

            using TransactionScope transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var duplicatedId = Guid.NewGuid();
            var duplicated = new FieldTrip
            {
                Id = duplicatedId,
                Name = original.Name + " (copie)",
                AllottedTimeDuration = original.AllottedTimeDuration,
                ReadingsAreVisible = original.ReadingsAreVisible,
                Description = original.Description,
                //EndPagePicture = await this.FileSystem.Duplicate(original.EndPagePicture, original.EndPagePicture.Replace($"{original.Id}", $"{duplicatedId}")),
                EndPageText = original.EndPageText,
                EndPageTitle = original.EndPageTitle,
                Establishment = original.Establishment,
                FieldTripType = original.FieldTripType,
                //HomePagePicture = await this.FileSystem.Duplicate(original.HomePagePicture, original.HomePagePicture.Replace($"{original.Id}", $"{duplicatedId}")),
                HomePageText = original.HomePageText,
                HomePageTitle = original.HomePageTitle,
                PedagogicalFieldName = original.PedagogicalFieldName,
                ShowElapsedTime = original.ShowElapsedTime,
                TimeIsLimited = original.TimeIsLimited,
                IsDeleted = false,
                IsPublished = false,
                IsVisible = false,
                LastUpdateDate = DateTime.UtcNow,
                //ReadingsVisibilityEndDate = original.ReadingsVisibilityEndDate,
                //ReadingsVisibilityStartDate = original.ReadingsVisibilityStartDate,
                ExplorationMap = new ExplorationMap
                {
                    Id = Guid.NewGuid(),
                    ExplorationArea = original.ExplorationMap.ExplorationArea,
                    ExplorationShape = original.ExplorationMap.ExplorationShape,
                    ExplorationAreaCenterLatitude = original.ExplorationMap.ExplorationAreaCenterLatitude,
                    ExplorationAreaCenterLongitude = original.ExplorationMap.ExplorationAreaCenterLongitude,
                    ExplorationAreaRadius = original.ExplorationMap.ExplorationAreaRadius,
                    OrganizerOutOfZoneAlert = original.ExplorationMap.OrganizerOutOfZoneAlert,
                    ParticipantOutOfZoneAlert = original.ExplorationMap.ParticipantOutOfZoneAlert,
                    CenterLatitude = original.ExplorationMap.CenterLatitude,
                    CenterLongitude = original.ExplorationMap.CenterLongitude,
                    CustomBackgroundLowerRightLatitude = original.ExplorationMap.CustomBackgroundLowerRightLatitude,
                    CustomBackgroundLowerRightLongitude = original.ExplorationMap.CustomBackgroundLowerRightLongitude,
                    //CustomBackgroundMap = ,
                    CustomBackgroundOpacity = original.ExplorationMap.CustomBackgroundOpacity,
                    CustomBackgroundRotation = original.ExplorationMap.CustomBackgroundRotation,
                    CustomBackgroundUpperLeftLatitude = original.ExplorationMap.CustomBackgroundUpperLeftLatitude,
                    CustomBackgroundUpperLeftLongitude = original.ExplorationMap.CustomBackgroundUpperLeftLongitude,
                    IsDeleted = false,
                    IsGeolocatable = original.ExplorationMap.IsGeolocatable,
                    NorthEastLatitude = original.ExplorationMap.NorthEastLatitude,
                    NorthEastLongitude = original.ExplorationMap.NorthEastLongitude,
                    SouthWestLatitude = original.ExplorationMap.SouthWestLatitude,
                    SouthWestLongitude = original.ExplorationMap.SouthWestLongitude,
                    Zoom = original.ExplorationMap.Zoom,
                },
                LocatedGameUnits = new(),
                Journeys = new List<Journey>(),
                DesignerId = copycat.Id
            };
            duplicated = this.DataContext.FieldTrips.Add(duplicated).Entity;
            //await this.SaveChangesAsync();

            if (!string.IsNullOrEmpty(original.HomePagePicture) && await this.FileSystem.ExistsAsync(original.HomePagePicture))
            {
                duplicated.HomePagePicture = await this.FileSystem.Duplicate(original.HomePagePicture, original.HomePagePicture.Replace($"{original.Id}", $"{duplicatedId}"));
            }

            if (!string.IsNullOrEmpty(original.EndPagePicture) && await this.FileSystem.ExistsAsync(original.EndPagePicture))
            {
                duplicated.EndPagePicture = await this.FileSystem.Duplicate(original.EndPagePicture, original.EndPagePicture.Replace($"{original.Id}", $"{duplicatedId}"));
            }

            // clone mapFile if exists
            if (!string.IsNullOrEmpty(original.ExplorationMap.CustomBackgroundMap))
            {
                duplicated.ExplorationMap.CustomBackgroundMap =
                    await this.FileSystem.Duplicate(original.ExplorationMap.CustomBackgroundMap, original.ExplorationMap.CustomBackgroundMap.Replace($"{original.ExplorationMapId}", $"{duplicated.ExplorationMapId}"));
            }

            Dictionary<Guid, Guid> gameUnitsMapping = new();
            foreach (var item in original.LocatedGameUnits.Where(lgu => !lgu.IsDeleted))
            {
                var duplicatedGameUnit = await this.LocatedGameUnitsManager.DuplicateLocatedGameUnitAsync(duplicated.Id, item, false, duplicated);
                gameUnitsMapping.Add(item.Id, duplicatedGameUnit.Id);

                duplicated.LocatedGameUnits.Add(duplicatedGameUnit);
            }

            foreach (var originalJourney in original.Journeys)
            {
                var newJourney = new Journey
                {
                    Id = Guid.NewGuid(),
                    Color = originalJourney.Color,
                    Name = originalJourney.Name,
                    Order = originalJourney.Order,
                    JourneyLocatedGameUnits = new()
                };

                var newLinks = originalJourney.JourneyLocatedGameUnits.Select(originalLink => new JourneyLocatedGameUnit
                {
                    Id = Guid.NewGuid(),
                    JourneyId = newJourney.Id,
                    LocatedGameUnitId = gameUnitsMapping[originalLink.LocatedGameUnitId],
                    Order = originalLink.Order,
                });

                newJourney.JourneyLocatedGameUnits.AddRange(newLinks);

                duplicated.Journeys.Add(newJourney);
            }

            await this.SaveChangesAsync();

            transactionScope.Complete();

            return duplicated.Id;
        }

        public async Task RemoveUnitFromJourneyAsync(Guid fieldTripId, Guid journeyId, Guid journeyUnitLinkId, ApplicationUser deleter)
        {
            FieldTrip fieldTrip = await this.SelectAll().Include(f => f.Journeys)
                .ThenInclude(j => j.JourneyLocatedGameUnits).ThenInclude(jlg => jlg.LocatedGameUnit)
                .FirstOrDefaultAsync(f => f.Id == fieldTripId);

            if (fieldTrip == null)
            {
                Console.WriteLine("Fieldtrip not found");
                return;
            }

            Console.WriteLine("Fieldtrip found");

            if (deleter.Id != fieldTrip.DesignerId && !deleter.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                Console.WriteLine("Not authorized");
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            var journey = fieldTrip.Journeys.FirstOrDefault(j => j.Id == journeyId);

            if (journey == null)
            {
                Console.WriteLine("Journey not found");
                return;
            }

            var link = journey.JourneyLocatedGameUnits.FirstOrDefault(j => j.Id == journeyUnitLinkId);

            if (link == null)
            {
                return;
            }

            this.DataContext.Remove(link);
            journey.JourneyLocatedGameUnits.Remove(link);

            journey.JourneyLocatedGameUnits.Where(jlgu => jlgu.Order > link.Order).ToList().ForEach(unit =>
            {
                unit.Order -= 5;
            });

            fieldTrip.LastUpdateDate = DateTime.UtcNow;
            await this.SaveChangesAsync();
        }

        public async Task AddOrRemoveFromFavoritesAsync(Guid fieldTripId, ApplicationUser updater)
        {
            var existing = this.DataContext.UserFieldTrips.FirstOrDefault(uft => uft.FieldTripId == fieldTripId && uft.UserId == updater.Id);

            if (existing == null)
            {
                var userFieldTrip = new UserFieldTrip()
                {
                    FieldTripId = fieldTripId,
                    UserId = updater.Id,
                    IsFavorite = true
                };

                this.DataContext.UserFieldTrips.Add(userFieldTrip);
            }
            else
            {
                existing.IsFavorite = !existing.IsFavorite;

                if (!existing.IsFavorite && !existing.IsDownloaded)
                {
                    this.DataContext.UserFieldTrips.Remove(existing);
                }
            }

            await this.SaveChangesAsync();
        }

        public async Task<Journey> AddNewJourneyAsync(Guid fieldTripId, ApplicationUser updater)
        {
            FieldTrip fieldTrip = this.DataContext.FieldTrips
                .Include(ft => ft.Journeys)
                    .ThenInclude(j => j.JourneyLocatedGameUnits)
                .FirstOrDefault(ft => ft.Id == fieldTripId);

            if (fieldTrip == null)
            {
                return null;
            }

            if (updater.Id != fieldTrip.DesignerId && !updater.UserRoles.Any(ur => ur.Role.Name == UserRoleNames.Administrator))
            {
                throw new BusinessException("Vous n'avez pas le droit de faire cela");
            }

            ColorJourney[] colors = JourneyColorsHelper.GetAvailableColors();

            if (fieldTrip.Journeys.Count == colors.Length)
            {
                throw new BusinessException("Plus aucune couleur disponible");
            }

            ColorJourney availableColor = ColorJourney.Green;

            while (fieldTrip.Journeys.Any(j => j.Color == availableColor))
            {
                availableColor = (ColorJourney)(((int)availableColor) + 1);
            }

            //Console.WriteLine("Available color " + availableColor);

            Journey journey = new Journey()
            {
                Color = availableColor,
                FieldTripId = fieldTripId,
                Order = fieldTrip.Journeys.Count,
                Id = Guid.NewGuid(),
                Name = availableColor.GetFriendlyName(),
                JourneyLocatedGameUnits = new List<JourneyLocatedGameUnit>()
            };

            var firstJourney = fieldTrip.Journeys.OrderBy(j => j.Order).First();
            foreach (var item in firstJourney.JourneyLocatedGameUnits)
            {
                journey.JourneyLocatedGameUnits.Add(new JourneyLocatedGameUnit
                {
                    JourneyId = journey.Id,
                    LocatedGameUnitId = item.LocatedGameUnitId,
                    Order = item.Order
                    //LocatedGameUnit = item.LocatedGameUnit,
                    //Journey = journey
                });
            }

            journey = this.DataContext.Journeys.Add(journey).Entity;
            fieldTrip.LastUpdateDate = DateTime.UtcNow;

            await this.SaveChangesAsync();

            return journey;
        }

        public async Task<bool> AddOrRemoveFromDownloadsAsync(Guid fieldTripId, ApplicationUser updater)
        {
            var userFieldTrip = this.DataContext.UserFieldTrips.FirstOrDefault(uft => uft.FieldTripId == fieldTripId && uft.UserId == updater.Id);

            if (userFieldTrip == null)
            {
                userFieldTrip = new UserFieldTrip()
                {
                    FieldTripId = fieldTripId,
                    UserId = updater.Id,
                    IsDownloaded = true
                };

                this.DataContext.UserFieldTrips.Add(userFieldTrip);
            }
            else
            {
                userFieldTrip.IsDownloaded = !userFieldTrip.IsDownloaded;

                if (!userFieldTrip.IsFavorite && !userFieldTrip.IsDownloaded)
                {
                    this.DataContext.UserFieldTrips.Remove(userFieldTrip);
                }
            }

            await this.SaveChangesAsync();
            return userFieldTrip.IsDownloaded;
        }

        public IQueryable<FieldTrip> GetAllSavedFieldTrips(string userId)
        {
            var savedFieldTrips = this.DataContext.UserFieldTrips
                .Include(uft => uft.FieldTrip)
                    .ThenInclude(f => f.LocatedGameUnits.Where(lgu => !lgu.IsDeleted))
                    .ThenInclude(g => g.Activities.Where(a => !a.IsDeleted))
                .Include(uft => uft.FieldTrip)
                    .ThenInclude(f => f.Journeys)
                    .ThenInclude(j => j.JourneyLocatedGameUnits)
                    .ThenInclude(jlg => jlg.LocatedGameUnit)
                    .ThenInclude(lgu => lgu.Activities)
                    .ThenInclude(a => (a as QuestionActivity).Answers.OrderBy(a => Guid.NewGuid()))//ordre aléatoire des réponses
                .Include(uft => uft.FieldTrip)
                    .ThenInclude(f => f.Journeys)
                    .ThenInclude(j => j.JourneyLocatedGameUnits)
                    .ThenInclude(jlg => jlg.LocatedGameUnit)
                    .ThenInclude(lgu => lgu.Activities)
                    .ThenInclude(a => (a as QuestionActivity).Clue)
                .Include(uft => uft.FieldTrip)
                    .ThenInclude(f => f.Designer)
                .Include(uft => uft.FieldTrip)
                    .ThenInclude(f => f.ExplorationMap)
                .Where(uft => uft.UserId == userId && uft.IsDownloaded && uft.FieldTrip.IsPublished && !uft.FieldTrip.IsDeleted)
                .Select(uft => uft.FieldTrip);

            return savedFieldTrips;
        }

        public async Task DeleteUserFieldTripsAsync(Guid fieldTripId)
        {
            var userFieldTrips = await this.DataContext.UserFieldTrips.Where(uf => uf.FieldTripId == fieldTripId).ToArrayAsync();
            this.DataContext.UserFieldTrips.RemoveRange(userFieldTrips);
            await this.SaveChangesAsync();
        }
    }
}
