﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Web.Client.Repositories;
using Ozytis.Common.Core.Utilities;
using Api;
using Ozytis.Common.Core.ClientApi;

namespace Web.Client.Security
{
    public class OzytisAuthStateProvider : AuthenticationStateProvider
    {
        public OzytisAuthStateProvider(IServiceProvider services) : base()
        {
            this.Services = services;
        }

        public IServiceProvider Services { get; internal set; }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            UsersRepository usersRepository = this.Services.GetService<UsersRepository>();

            if (usersRepository.CurrentUser == null && !string.IsNullOrEmpty(BaseService.BearerToken))
            {
                await usersRepository.RefreshCurrentUserAsync(true);                
            }

            if (usersRepository.CurrentUser == null)
            {
                return new AuthenticationState(new ClaimsPrincipal());
            }

            var claims = new List<Claim>
            {
                 new Claim(ClaimTypes.Name, $"{usersRepository.CurrentUser.FirstName} {usersRepository.CurrentUser.LastName}"),
                 new Claim(ClaimTypes.Email, usersRepository.CurrentUser.Email),
            };

            if (!string.IsNullOrEmpty(usersRepository.CurrentUser.Role))
            {
                claims.Add(new Claim(ClaimTypes.Role, usersRepository.CurrentUser.Role));
            }

            var identity = new ClaimsIdentity(claims.ToArray(), "Ozytis");

            var user = new ClaimsPrincipal(identity);

            await Task.CompletedTask;

            return new AuthenticationState(user);
        }

        public void NotifyStateChanged(UserModel user)
        {
            this.NotifyAuthenticationStateChanged(this.GetAuthenticationStateAsync());
        }
    }
}
