// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using BlazorPro.BlazorSize;
using Common;
using Common.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.JSInterop;
using Newtonsoft.Json;

using Ozytis.Common.Core.ClientApi;
using Ozytis.Common.Core.ClientApi.Storage;
using Ozytis.Common.Core.GeoJson;
using Ozytis.Common.Core.Logs.NetCore.Client;
using Ozytis.Common.Core.Web.Razor;
using Ozytis.Common.Core.Web.Razor.Utilities;
using Syncfusion.Blazor;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Web.Client.Repositories;
using Web.Client.Security;
using Web.Client.Utilities;

namespace Web.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(SyncfusionKey.Key);

            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            BaseService.BaseUrl = builder.HostEnvironment.BaseAddress;
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Converters = GeoJsonUtils.GetSerializer().Converters
            };

            builder.Services.AddSingleton<AuthenticationStateProvider, Security.OzytisAuthStateProvider>();
            builder.Services.AddSingleton<IAuthorizationPolicyProvider, Security.OzytisAuthPolicyProvider>();
            builder.Services.AddSingleton<OzytisAuthStateProvider>(sp => sp.GetService<AuthenticationStateProvider>() as OzytisAuthStateProvider);

            builder.Services.AddSingleton<LocalStorageService>();
            builder.Services.AddSingleton<GeoLocationService>();
            builder.Services.AddSingleton<IndexDbStorageService>();

            builder.Services.AddSingleton<NavigationWithHistoryManager>();
            builder.Services.AddSingleton<AdditionalAssemblyProvider>();
            builder.Services.AddOzLogsDashboard();

            builder.Services.AddSingleton<ComponentMapping>(new ComponentMapping() {
                { Ozytis.Common.Core.Web.Razor.ArchitectUI.ComponentMappingPart.Menu, typeof(Shared.MainMenu) },
                { Ozytis.Common.Core.Web.Razor.ArchitectUI.ComponentMappingPart.UserBox, typeof(Shared.UserBox) },
                { Ozytis.Common.Core.Web.Razor.ArchitectUI.ComponentMappingPart.Confirm, typeof(Ozytis.Common.Core.Web.Razor.SyncfusionComponents.Confirm) },
            });

            builder.Services.AddAuthorizationCore(options =>
                options.AddPolicy(Policy.UserIsAdministrator, policy => policy.RequireRole(UserRoleNames.Administrator)));

            foreach (Type manager in typeof(UsersRepository).Assembly.GetTypes()
                .Where(t => t.Name.EndsWith("Repository") && !t.IsAbstract))
            {
                builder.Services.AddSingleton(manager);
            }

            builder.Services.AddSingleton<ResizeListener>();
            builder.Services.AddSingleton<IMediaQueryService, MediaQueryService>();
            builder.Services.AddSingleton<DragAndDropService, DragAndDropService>();

            builder.Services.AddSingleton<CultureInfo>(CultureInfo.GetCultureInfo("fr-FR"));
            builder.Services.AddSingleton<TimeZoneInfo>(TimeZoneInfo.FindSystemTimeZoneById("Europe/Paris"));

            builder.Services.AddSyncfusionBlazor(options =>
            {
                options.IgnoreScriptIsolation = true;
            });

            var buildInformation = new BuildInformation
            {
                BuildDate = Assembly.GetAssembly(typeof(Program)).GetCustomAttribute<BuildInformationAttribute>()?.BuildDate,
                Branch = ThisAssembly.Git.Branch,
                Commit = ThisAssembly.Git.Commit
            };
            builder.Services.AddSingleton<BuildInformation>(buildInformation);

            WebAssemblyHost host = builder.Build();

            await CommonScriptLoader.EnsureOzytisCommonIsPresentAsync(host.Services.GetService<IJSRuntime>());

            IndexDbStorageService storageService = host.Services.GetService<IndexDbStorageService>();

            await storageService.InitializeAsync(new StorageOptions
            {
                Description = "SituLearn Editor",
                Database = "SituLearn",
                Store = "offline"
            });

            string token = await host.Services.GetService<LocalStorageService>().GetItemAsync<string>("token");
            BaseService.SetBearerToken(token);

            host.Services.UseOzLogsDashboard();


            DownloadUtilities.Configure(host.Services.GetService<IJSRuntime>(), host.Services.GetService<UsersRepository>());

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            await host.RunAsync();
        }
    }
}
