﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Components;
using Syncfusion.Blazor.RichTextEditor;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;

namespace Web.Client.Shared
{
	public partial class InPlaceRichText : ComponentBase
	{
		private readonly XmlDocument XlmAnalyser = new();

		[Parameter]
		public string Text { get; set; }

		[Parameter]
		public bool Disabled { get; set; } = false;

		[Parameter]
		public EventCallback<string> OnUpdate { get; set; }

		public bool TextIsClearExceptTags { get; set; } = false;

		protected override void OnInitialized()
		{
			base.OnInitialized();

			this.ControlInputEmptyNess(this.Text);
		}

		private void ControlInputEmptyNess(string input)
		{
            if (!string.IsNullOrEmpty(input))
			{
				this.XlmAnalyser.LoadXml($"<text>{input.Replace("<br>", "").Replace("&nbsp;", "&#160;")}</text>");
				this.TextIsClearExceptTags = string.IsNullOrEmpty(this.XlmAnalyser.InnerText);
			}
			else
			{
				this.TextIsClearExceptTags = true;
			}
		}

		public async Task ValueChanged(string newVal)
		{
			if (newVal != this.Text)
			{
                this.ControlInputEmptyNess(newVal);

                await this.OnUpdate.InvokeAsync(newVal);
            }			
		}

		private readonly List<ToolbarItemModel> EditorToolbarItems = new()
		{
			new ToolbarItemModel() { Command = ToolbarCommand.Bold },
			new ToolbarItemModel() { Command = ToolbarCommand.Italic },
			new ToolbarItemModel() { Command = ToolbarCommand.Underline },
			new ToolbarItemModel() { Command = ToolbarCommand.CreateLink }
        };
	}
}
