﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;
using NetTopologySuite.Geometries;
using System;

namespace Api
{
	public class UpdateMapPositionModel
    {
        public Guid FieldTripId { get; set; }

        public decimal CenterLatitude { get; set; }

        public decimal CenterLongitude { get; set; }

        public int Zoom { get; set; }

        public bool MapIsGeolocalized { get; set; }
        public string MapBackground { get; set; }
        public decimal? MapBackgroundLowerRightLatitude { get; set; }
        public decimal? MapBackgroundLowerRightLongitude { get; set; }
        public decimal? MapBackgroundUpperLeftLatitude { get; set; }
        public decimal? MapBackgroundUpperLeftLongitude { get; set; }
        public decimal MapBackgroundOpacity { get; set; }
        public decimal? MapRotation { get; set; }
        public decimal SouthWestLatitude { get; set; }
        public decimal SouthWestLongitude { get; set; }
        public decimal NorthEastLatitude { get; set; }
        public decimal NorthEastLongitude { get; set; }

        public bool AlertUserOutOfExplorationZoneInPlayer { get; set; }
        public bool AlertUserOutOfExplorationZoneInMonitor { get; set; }
        public Geometry ExplorationArea { get; set; }
        public ActivationAreaType ExplorationAreaShape { get; set; }
        public decimal? ExplorationAreaCenterLatitude { get; set; }
        public decimal? ExplorationAreaCenterLongitude { get; set; }
        public decimal ExplorationAreaRadius { get; set; }
        public bool ExplorationZoneIsActivated { get; set; }
    }
}
