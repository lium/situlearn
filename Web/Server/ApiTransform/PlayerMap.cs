﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Web.Server.ApiTransform
{
	public static class PlayerMap
	{
		public static PlayerForMonitorModel ToMonitorApiResult(this Player source)
		{
			int pointsForLocatingUnits = source.PlayedUnits.Sum(u => u.LocationPoints);

			var pointsForQuestionActivities = source.PlayedUnits
				.Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
				.SelectMany(pu => pu.PlayedActivities)
				.Sum(pa => pa.Points ?? 0);

			var score = pointsForLocatingUnits + pointsForQuestionActivities;

			int pointsToWinForLocatingUnits = source.Game.FieldTrip.Journeys.FirstOrDefault(j => j.Id == source.JourneyId).JourneyLocatedGameUnits
				.Select(jlgu => jlgu.LocatedGameUnit)
				.Where(u => !u.IsDeleted)
				.Sum(u => u.LocationGain);

			int pointsToWinForQuestionActivities = source.Game.FieldTrip.Journeys.FirstOrDefault(j => j.Id == source.JourneyId).JourneyLocatedGameUnits
				.Select(jlgu => jlgu.LocatedGameUnit)
				.Where(u => u.Activities != null && u.Activities.Any())
				.SelectMany(u => u.Activities)
				.Where(a => !a.IsDeleted)
				.Sum(a => a is QuestionActivity question ? question.Gain : 0);

			int totalPointsToWin = pointsToWinForLocatingUnits + pointsToWinForQuestionActivities;

			int answeredQuestionsNumber = source.PlayedUnits
				.Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
				.SelectMany(pu => pu.PlayedActivities)
				.Where(pa => pa.Activity is QuestionActivity)
				.Count();

			int correctAnswersNumber = source.PlayedUnits
				.Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
				.SelectMany(pu => pu.PlayedActivities)
				.Where(pa => pa.Points.HasValue && pa.Points.Value >= ((pa.Activity as QuestionActivity).Gain - (pa.Activity as QuestionActivity).Clue.Cost))
				.Count();

			int readingsNumber = source.PlayedUnits
				.Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
				.SelectMany(pu => pu.PlayedActivities)
				.Where(pa => pa.Activity is ReadingActivity)
				.Count();

			bool outOfExplorationAreaAlert = false;

			if (source.CurrentLongitude.HasValue && source.CurrentLatitude.HasValue && source.Game.FieldTrip.ExplorationMap.ExplorationArea != null)
			{
				var point = new NetTopologySuite.Geometries.Point(
					new NetTopologySuite.Geometries.Coordinate((double)source.CurrentLongitude.Value, 
					(double)source.CurrentLatitude.Value)
				);

				if (!source.Game.FieldTrip.ExplorationMap.ExplorationArea.ContainsPoint(point))
				{
					outOfExplorationAreaAlert = true;
				}
			}			

			var result = new PlayerForMonitorModel
			{
				Id = source.Id,
				UserId = source.UserId,
				Name = !string.IsNullOrEmpty(source.TeamName) ? source.TeamName : $"{source.User.FirstName} {source.User.LastName.ToUpperInvariant()}",
				PlayedUnitsNumber = source.EndGameDate.HasValue ? source.PlayedUnits.Count : (source.PlayedUnits.Any() ? source.PlayedUnits.Count - 1 : 0),
				UnitsToPlayNumber = source.Game.FieldTrip.Journeys.FirstOrDefault(j => j.Id == source.JourneyId).JourneyLocatedGameUnits.Count(),
				PhoneNumber = source.User.PhoneNumber,
				CurrentUnitName = source.CurrentUnit?.Name,
				Score = score,
				TotalPointsToWin = totalPointsToWin,
                CorrectAnswersNumber = correctAnswersNumber,
                AnsweredQuestionsNumber = answeredQuestionsNumber,
                ReadingsNumber = readingsNumber,
				CurrentLatitude = source.CurrentLatitude.HasValue ? source.CurrentLatitude.Value : default,
				CurrentLongitude = source.CurrentLongitude.HasValue ? source.CurrentLongitude.Value : default,
				OutOfExplorationAreaAlert = outOfExplorationAreaAlert,
				LowBatteryAlert = source.BatteryLevel.HasValue && source.BatteryLevel.Value < 0.20M
			};

			return result;
		}
	}
}
