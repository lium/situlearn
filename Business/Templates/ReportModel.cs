﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Templates
{
    public class ReportModel
    {
        public Guid Id { get; set; }

        public string BaseUrl { get; set; }

        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        //public string CreationDate { get; set; }
        public string LastUpdateDate { get; set; }

        public string ReportStartDate { get; set; }
        public string ReportEndDate { get; set; }
        public int ParticipantsNumber { get; set; }
        public int ParticipantsCompletedGameNumber { get; set; }
        public int ReadingsNumber { get; set; }
        public string CorrectAnswersAverageRate { get; set; }
        public string AverageScore { get; set; }
        public string AverageDuration { get; set; }

        public List<(string Name, string Date, TimeSpan Duration, decimal CorrectAnswersRate, int Score, (string FileName, string FilePath) TeamPicture)> Participants { get; set; }

        public List<(string ParticipantName, string LocatedGameUnitName, string LocatedActivityName, string Date, List<(string FileName, string FilePath)> Files)> Readings { get; set; }
    }
}
