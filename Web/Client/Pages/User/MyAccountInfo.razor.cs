﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.Components.Forms;
using Ozytis.Common.Core.Utilities;
using System;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.User
{
    public partial class MyAccountInfo : ComponentBase
    {
        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        public string[] Errors { get; set; }

        public bool IsEditing { get; set; }

        public bool IsUpdating { get; set; }

        public UserModel UserModel { get; set; }

        public UserUpdateModel UpdateModel { get; set; }

        protected void ChangeEditingMode()
        {
            this.Errors = null;
            this.ReinitUpdateModel();
            this.IsEditing = !this.IsEditing;
        }

        protected override async Task OnInitializedAsync()
        {
            this.ReinitUpdateModel();

            await base.OnInitializedAsync();
        }

        protected void ReinitUpdateModel()
        {
            this.UserModel = UsersRepository.CurrentUser;

            this.UpdateModel = new UserUpdateModel
            {
                FirstName = this.UserModel.FirstName,
                Language = this.UserModel.Language,
                LastName = this.UserModel.LastName,
                Establishment = this.UserModel.Establishment,
                Role = this.UserModel.Role,
                Email = this.UsersRepository.CurrentUser.Email
            };
        }

        protected async Task SaveAsync()
        {
            if (this.IsUpdating)
            {
                return;
            }

            this.IsUpdating = true;
            this.Errors = null;

            try
            {
                await this.UsersRepository.UpdateCurrentUserAsync(this.UpdateModel);
                this.Notifier.Notify(Color.Success, "Vos informations ont bien été mises à jour");
                this.IsEditing = false;
                this.ReinitUpdateModel();
            }
            catch (BusinessException ex)
            {
                this.Errors = ex.Messages;
            }

            this.IsUpdating = false;
        }

        protected async Task ChangePicture(InputFileChangeEventArgs e)
        {
            var file = await e.File.RequestImageFileAsync("image/jpeg", 512, 512);

            var buffer = new byte[file.Size];
            await file.OpenReadStream().ReadAsync(buffer);
            var imageDataUrl = $"data:image/jpeg;base64,{Convert.ToBase64String(buffer)}";
            this.UpdateModel.Photo = imageDataUrl;
        }
    }
}
