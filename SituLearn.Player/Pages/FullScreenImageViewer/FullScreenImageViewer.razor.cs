﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Components.Web;

namespace SituLearn.Player.Pages.FullScreenImageViewer
{
    public partial class FullScreenImageViewer : ComponentBase
    {
        [Parameter]
        public string PictureUrl { get; set; }

        [Parameter]
        public Action OnClosing { get; set; }

        public double Zoom { get; set; } = 1;

        public List<TouchPoint> TpCache { get; set; } = new();

        public double XOff { get; set; }

        public double YOff { get; set; }

        public (double x, double y) Start { get; set; }

        public bool Panning { get; set; }

        private void OnTouchStart(TouchEventArgs e)
        {
            // If the user makes simultaneous touches, the browser will fire a
            // separate touchstart event for each touch point. Thus if there are
            // three simultaneous touches, the first touchstart event will have
            // targetTouches length of one, the second event will have a length
            // of two, and so on.
            
            // Cache the touch points for later processing of 2-touch pinch/zoom
            if (e.TargetTouches.Length == 2)
            {
                for (var i = 0; i < e.TargetTouches.Length; i++)
                {
                    this.TpCache.Add(e.TargetTouches[i]);
                }
            }

            if (e.TargetTouches.Length == 1 && this.Zoom > 1)
            {
                this.Start = new() { x = e.TargetTouches[0].ClientX - this.XOff, y = e.TargetTouches[0].ClientY - this.YOff };
                this.Panning = true;
            }
        }

        private void OnTouchEnd(TouchEventArgs e)
        {
            if (e.TargetTouches.Length == 1)
            {
                this.Panning = false;
            }
        }

        private void OnTouchMove(TouchEventArgs e)
        {
            // Note: if the user makes more than one "simultaneous" touches, most browsers
            // fire at least one touchmove event and some will fire several touchmoves.
            // Consequently, an application might want to "ignore" some touchmoves.
            
            // Check this event for 2-touch Move/Pinch/Zoom gesture
            this.HandlePinchZoom(e);

            if (e.TargetTouches.Length == 1)
            {
                if (!this.Panning || this.Zoom == 1)
                {
                    return;
                }

                this.XOff = (e.TargetTouches[0].ClientX - this.Start.x);
                this.YOff = (e.TargetTouches[0].ClientY - this.Start.y);
            }            
        }

        private void HandlePinchZoom(TouchEventArgs e)
        {
            if (e.TargetTouches.Length == 2 && e.ChangedTouches.Length == 2)
            {
                var middlePointX = (e.TargetTouches[0].ClientX + e.TargetTouches[1].ClientX) / 2;
                var middlePointY = (e.TargetTouches[0].ClientY + e.TargetTouches[1].ClientY) / 2;

                // take the scale into account with the offset
                var xs = (middlePointX - this.XOff) / this.Zoom;
                var ys = (middlePointY - this.YOff) / this.Zoom;

                // Check if the two target touches are the same ones that started
                // the 2-touch
                var point1 = this.TpCache.FindLastIndex(
                  (tp) => tp.Identifier == e.TargetTouches[0].Identifier
                );

                var point2 = TpCache.FindLastIndex(
                  (tp) => tp.Identifier == e.TargetTouches[1].Identifier
                );

                if (point1 >= 0 && point2 >= 0)
                {
                    var initialDistance = Math.Sqrt(Math.Pow((this.TpCache[point2].ClientX - this.TpCache[point1].ClientX), 2) + Math.Pow((this.TpCache[point2].ClientY - this.TpCache[point1].ClientY), 2));

                    var newDistance = Math.Sqrt(Math.Pow((e.TargetTouches[1].ClientX - e.TargetTouches[0].ClientX), 2) + Math.Pow((e.TargetTouches[1].ClientY - e.TargetTouches[0].ClientY), 2));

                    if (!(this.Zoom == 1 && newDistance < initialDistance))
                    {
                        // get scroll direction & set zoom level
                        this.Zoom = (newDistance > initialDistance) ? (Math.Round(this.Zoom * 1.1, 2, MidpointRounding.AwayFromZero)) : (Math.Round(this.Zoom / 1.1, 2, MidpointRounding.AwayFromZero));

                        // reverse the offset amount with the new scale
                        this.XOff = this.Zoom == 1 ? 0 : middlePointX - xs * this.Zoom;
                        this.YOff = this.Zoom == 1 ? 0 : middlePointY - ys * this.Zoom;
                    }

                    this.StateHasChanged();
                }
                else
                {
                    // empty tpCache
                    this.TpCache = new();
                }
            }            
        }

        private void OnMouseDown(MouseEventArgs e)
        {
            if (this.Zoom > 1)
            {
                this.Start = new() { x = e.ClientX - this.XOff, y = e.ClientY - this.YOff };
                this.Panning = true;
            }            
        }

        private void OnMouseUp(MouseEventArgs e)
        {
            this.Panning = false;
        }

        private void OnMouseMove(MouseEventArgs e)
        {
            if (!this.Panning || this.Zoom == 1)
            {
                return;
            }

            this.XOff = (e.ClientX - this.Start.x);
            this.YOff = (e.ClientY - this.Start.y);
        }

        private void OnMouseWheel(WheelEventArgs e)
        {
            // take the scale into account with the offset
            var xs = (e.ClientX - this.XOff) / this.Zoom;
            var ys = (e.ClientY - this.YOff) / this.Zoom;
            var delta = -e.DeltaY;

            if (!(this.Zoom == 1 && delta < 0))
            {
                // get scroll direction & set zoom level            
                this.Zoom = (delta > 0) ? (Math.Round(this.Zoom * 1.1, 2, MidpointRounding.AwayFromZero)) : (Math.Round(this.Zoom / 1.1, 2, MidpointRounding.AwayFromZero));

                // reverse the offset amount with the new scale
                this.XOff = this.Zoom == 1 ? 0 : e.ClientX - xs * this.Zoom;
                this.YOff = this.Zoom == 1 ? 0 : e.ClientY - ys * this.Zoom;
            }

            this.StateHasChanged();
        }
    }
}
