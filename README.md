Le projet de recherche **[SituLearn](https://situlearn.univ-lemans.fr/index.php/)**, vise à aider les enseignants à enrichir leurs sorties pédagogiques avec plusieurs applications :

<img src="https://situlearn.univ-lemans.fr/wp-content/uploads/2023/06/image.png" width="600">

* **SituLearn-Editor** est un éditeur qui permet aux enseignants de créer des applications mobiles pour des sorties pédagogiques, sans compétences en programmation informatique. Il permet d’intégrer des cartes personnalisées, des fiches d’informations et des activités géolocalisées comme des questions ou des relevées (prise de notes, de photos et d’enregistrement audio).

* **SituLearn-Player** est l’application mobile dédiée aux apprenants qui fonctionne sur smartphone ou tablette, sans nécessiter de connexion internet. Grâce à celle-ci, les apprenants parcourent la carte en se déplaçant physiquement. Ils peuvent ainsi débloquer des informations et jouer aux activités conçues par leurs enseignants.

* **SituLearn-Monitor** permet aux enseignants de suivre la position et la progression de leurs élèves pendant les sorties pédagogiques s’ils ont accès à Internet. Cela permet d’assurer la sécurité ainsi que le bon fonctionnement du jeu. De retour à l’établissement, les enseignants peuvent télécharger les relevés pris par les élèves pendant la sortie pour les utiliser en cours.

Vous pouvez tester les applications sur le notre serveur :
* https://situlearn-editor.univ-lemans.fr/
* https://situlearn-player.univ-lemans.fr/
* https://situlearn-monitor.univ-lemans.fr/

SituLearn s’applique à toutes les matières enseignées qui nécessitent un apprentissage sur le terrain (Histoire, Botanique, EPS, Géologie…) de la maternelle à la formation professionnelle. Les outils ont été co-conçus avec une dizaine d’enseignants et testés avec leurs élèves. Ils sont aussi utilisés, par les acteurs du tourisme, pour créer des visites interactives de musées ou de villes.

Voici l'interface de l'éditeur et quelques exemples de sorties crée par nos enseignants pilotes : \
<img src="https://situlearn.univ-lemans.fr/wp-content/uploads/2023/04/aide_page_UJS2.png" width="600">\
<img src="https://situlearn.univ-lemans.fr/wp-content/uploads/2023/04/aide_question.png" width="228">
<img src="https://situlearn.univ-lemans.fr/wp-content/uploads/2023/06/Screenshot_20220623-144445-scaled.jpg" width="400">


Si le site du projet **[SituLearn](https://situlearn.univ-lemans.fr/index.php/)**, vous trouverez : 
* la présentation du projet de recherche et les articles publiés
* des vidéos tutoriel pour vous aider à prendre en main les applications
* la liste des enseignants pilotes qui ont participé au projet

Voici les retours d'exprience de deux enseignants pilotes : \
<img src="https://situlearn.univ-lemans.fr/wp-content/uploads/2023/06/benoit.png" width="400">
<img src="https://situlearn.univ-lemans.fr/wp-content/uploads/2023/06/Guy.png" width="400">
 
La démarche du **Logiciel Libre** est en totale adéquation avec les principes de la recherche publique. De la même façon que nous devons publier nos articles de recherche en libre accès, il nous semble pertinent de donner accès à nos codes. Bien que contraignante pour le développement, cette démarche offre plusieurs avantages pour la recherche : elle facilite la réutilisation et l’amélioration des logiciels, mais aussi la vérification des résultats scientifiques par des tiers.
 
Dans le domaine de la recherche en EIAH (Environnement Informatique pour l’Apprentissage Humain), le partage des logiciels développés peut aussi bénéficier aux enseignants et aux apprenants. Nous voulons que les résultats de nos recherches, financées par l’argent public (Agence Nationale de la Recherche pour SituLearn), servent au public ! Le fait de déposer nos applications sur la **Forge de l’Éducation nationale** nous permet de partager ses ressources plus facilement avec les acteurs de l’éducation. Nous souhaitons que ces logiciels, dont nous avons testé l’efficacité sur quelques enseignants pilotes, puissent bénéficier au plus grand nombre.

Ce projet est porté par la Laboratoire d’Informatique de l’Université du Mans (LIUM) et financé par l'Agence Nationale de la Recherche (Projet-ANR-20-CE38-0012)\
<img src="https://situlearn.univ-lemans.fr/wp-content/uploads/2022/01/anr-1.png" width="200">
<img src="https://situlearn.univ-lemans.fr/wp-content/uploads/2023/05/Logo-LIUM_Couleurs_WEB-1.png" width="200">