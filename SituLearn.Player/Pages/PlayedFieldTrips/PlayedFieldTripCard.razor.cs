// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Entities;
using Microsoft.AspNetCore.Components;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using SituLearn.Player.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Player.Pages.PlayedFieldTrips
{
	public partial class PlayedFieldTripCard : ComponentBase
	{
		[Parameter]
		public FieldTripMadeHeaderModel FieldTripMade { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        public string BasePictureUrl { get; set; }

		public bool ShowEnrichedMap { get; set; }

		public bool ShowInformationSheet { get; set; }

		public LocatedGameUnit UnitInformationSheetToShow { get; set; }

		public bool ShowPlayedReadingActivity { get; set; }

		public PlayedActivity PlayedReadingActivityToShow { get; set; }

		public bool ShowReadingActivitiesList { get; set; }

		public bool ShowInformationSheetsList { get; set; }

		public List<LocatedGameUnit> PlayedUnitsWithInformationSheet { get; set; }

        public bool ShowShareReportOptions { get; set; }
        
        public bool IsSharing { get; set; }

        public bool ShowShareReportEmailEditionModal { get; set; }

        public bool IsSharingToAnotherEmail { get; set; }

        public string ShareReportEmail { get; set; }

        protected override async Task OnInitializedAsync()
		{
			this.PlayedUnitsWithInformationSheet = this.FieldTripMade.PlayedUnits.Where(u => u.InformationSheetEnabled).ToList();

            await base.OnInitializedAsync();
        }

        public IEnumerable<PlayedActivity> StatementsWithContent()
		{
			IEnumerable<PlayedActivity> ligetimateStatements = this.FieldTripMade.PlayedReadingActivities?.Where(pra => !string.IsNullOrEmpty(pra.Answer) || (pra.Files?.Any() ?? false)) ?? Array.Empty<PlayedActivity>();
			return ligetimateStatements;
		}

		protected override void OnInitialized()
		{
			this.BasePictureUrl = "";
		}

		public void SwitchReadingsListDisplay()
		{
			this.ShowReadingActivitiesList = !this.ShowReadingActivitiesList;
			this.StateHasChanged();
		}

		public void SwitchInformationSheetsListDisplay()
		{
			this.ShowInformationSheetsList = !this.ShowInformationSheetsList;
			this.StateHasChanged();
        }

        public void SwitchShareReportOptionsDisplay()
        {
            this.ShowShareReportOptions = !this.ShowShareReportOptions;
            this.StateHasChanged();
        }

        public async Task DisplayInformationSheetModal(Guid unitId)
		{
			if (unitId == Guid.Empty)
			{
				return;
			}

			this.UnitInformationSheetToShow = this.FieldTripMade.PlayedUnits.FirstOrDefault(u => u.Id == unitId);

			if (this.UnitInformationSheetToShow != null)
			{
				this.ShowInformationSheet = true;
				this.StateHasChanged();
			}

			await Task.CompletedTask;
		}

		public void CloseInformationSheetModal()
		{
			this.ShowInformationSheet = false;
			this.UnitInformationSheetToShow = null;
			this.StateHasChanged();
		}

		public void DisplayPlayedReadingActivityModal(Guid PlayedReadingActivityId)
		{
			if (PlayedReadingActivityId == Guid.Empty)
			{
				return;
			}

			this.PlayedReadingActivityToShow = this.FieldTripMade.PlayedReadingActivities
				.FirstOrDefault(pa => pa.Id == PlayedReadingActivityId);

			if (this.PlayedReadingActivityToShow != null)
			{
				this.ShowPlayedReadingActivity = true;
			}
		}

		public void ClosePlayedReadingActivityModal()
		{
			this.ShowPlayedReadingActivity = false;
			this.PlayedReadingActivityToShow = null;
			this.StateHasChanged();
        }

        public async Task DisplayShareReportEmailEditionModal()
        {
            this.ShowShareReportEmailEditionModal = true;
            this.StateHasChanged();

            await Task.CompletedTask;
        }

        public void CloseShareReportEmailEditionModal()
        {
			this.ShowShareReportEmailEditionModal = false;
            this.StateHasChanged();
        }

        public async Task ValidateShareReportToAnotherEmailAsync()
        {
			if (this.IsSharingToAnotherEmail)
			{
                return;
			}

            this.IsSharingToAnotherEmail = true;
            this.StateHasChanged();

            if (string.IsNullOrEmpty(this.ShareReportEmail))
            {
                this.Notifier.Notify(Color.Danger, $"Vous devez renseigner l'adresse e-mail du destinataire");
                this.IsSharingToAnotherEmail = false;
                return;
            }

            try
            {
                await this.GamesRepository.ShareReportAsync(this.FieldTripMade.GameId, this.ShareReportEmail);
                this.Notifier.Notify(Color.Success, "Votre demande de partage de compte-rendu de la sortie a bien �t� prise en compte : le destinataire va maintenant recevoir un message contenant le lien lui permettant de le t�l�charger");
            }
            catch (Exception e)
            {
                this.Notifier.Notify(Color.Danger, $"Erreur lors de la demande de partage de relev� : {e.Message}");
                throw;
            }
            finally
            {
                this.IsSharingToAnotherEmail = false;
                this.StateHasChanged();
            }

            this.ShowShareReportEmailEditionModal = false;
            this.ShareReportEmail = null;
            this.StateHasChanged();
        }

        protected async Task ShareReportAsync()
        {
            if (this.IsSharing)
            {
                return;
            }

            this.IsSharing = true;
            this.StateHasChanged();

            try
            {
				await this.GamesRepository.ShareReportAsync(this.FieldTripMade.GameId);
				this.Notifier.Notify(Color.Success, "Votre demande de partage de compte-rendu de la sortie a bien �t� prise en compte : veuillez maintenant consulter votre messagerie afin de retrouver le lien vous permettant de le t�l�charger.");
            }
            catch (Exception e)
            {
                this.Notifier.Notify(Color.Danger, $"Erreur lors de la demande de partage de relev� : {e.Message}");
                throw;
            }
            finally
            {
                this.IsSharing = false;
                this.StateHasChanged();
            }

        }
    }
}