﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api.PedagogicalFields;
using ClientApi;

using Entities;
using Ozytis.Common.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Web.Client.Repositories
{
    public class PedagogicalFieldsRepository
    {
        private readonly PedagogicalFieldsService pedagogicalFieldsService = new();

        public async Task<IEnumerable<PedagogicalField>> GetAllPedagogicalFieldsAsync(Expression<Func<IQueryable<PedagogicalField>, IQueryable<PedagogicalField>>> query = null)
        {
            var fields = await this.pedagogicalFieldsService.GetAllPedagogicalFieldsAsync(query);
            return fields;
        }

        public async Task<PedagogicalField> CreatePedagocicalField(PedagogicalFieldCreateModel model)
        {
            var result = await this.pedagogicalFieldsService.CreatePedagogicalFieldAsync(model);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }

            return result.Data;
        }

        public async Task<PedagogicalField> UpdatePedagocicalField(PedagogicalFieldUpdateModel model)
        {
            var result = await this.pedagogicalFieldsService.UpdatePedagogicalFieldAsync(model);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }

            return result.Data;
        }

        public async Task DeletePedagogicalField(int fieldId)
        {
            await this.pedagogicalFieldsService.DeletePedagogicalFieldAsync(fieldId);
        }

        public async Task<PedagogicalField> GetField(int fieldId)
        {
            return await this.pedagogicalFieldsService.GetPedagocialFieldAsync(fieldId);
        }
    }
}
