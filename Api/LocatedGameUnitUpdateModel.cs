﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api
{
	public class LocatedGameUnitUpdateModel
	{
		public string Name { get; set; }

		public LocationAidType LocationAidType { get; set; }

		public LocationValidationType LocationValidationType { get; set; }

		public int LocationGain { get; set; }

        [MaxLength(250, ErrorMessage = "Le titre de la fiche d'information ne doit pas dépasser {1} caractères")]
        public string InformationSheetTitle { get; set; }

        [MaxLength(1000, ErrorMessage = "La description de la fiche d'information comporte trop de caractères")]
        public string InformationSheetDescription { get; set; }

        public string InformationSheetPicture { get; set; }

        [MaxLength(250, ErrorMessage = "Le titre de la conclusion de l'unité de jeu ne doit pas dépasser {1} caractères")]
        public string ConclusionTitle { get; set; }

        [MaxLength(1000, ErrorMessage = "Le texte de la conclusion de l'unité de jeu comporte trop de caractères")]
        public string ConclusionText { get; set; }

        public string ConclusionPicture { get; set; }

        [MaxLength(250, ErrorMessage = "Le titre de l'aide à la localisation ne doit pas dépasser {1} caractères")]
        public string LocationSupportTitle { get; set; }

        [MaxLength(1000, ErrorMessage = "La description de l'aide à la localisation comporte trop de caractères")]
        public string LocationSupportDescription { get; set; }

        public string LocationSupportPicture { get; set; }
		
        public bool InformationSheetEnabled { get; set; }
        
        public bool ActivitiesEnabled { get; set; }
        
        public bool ConclusionEnabled { get; set; }

	}
}
