﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using SituLearn.Monitor.Pages.FieldTrips;
using SituLearn.Monitor.Repositories;
using System;
using System.Threading.Tasks;

namespace SituLearn.Monitor.Pages.Supervision
{
    [Route(Url)]
    [Authorize]

    public partial class TimeSelectSupervisionPage : ComponentBase
	{
		public const string Url = "selection-heure-suivi/{fieldTripId:guid}";

		public static string GetUrl(Guid fieldTripId)
		{
			return Url.Replace("{fieldTripId:guid}", fieldTripId.ToString());
		}

		[Parameter]
		public Guid FieldTripId { get; set; }

		[Inject]
		public NavigationManager NavigationManager { get; set; }

        [Parameter]
        [SupplyParameterFromQuery(Name = "backUrl")]
		public string BackUrl { get; set; }

		[Inject]
		public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        public FieldTrip FieldTrip { get; set; }

		public bool NotFound { get; set; }

        public TimeOnly TimeSelected { get; set; }

        protected override async Task OnInitializedAsync()
		{
			this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId);

			if (this.FieldTrip == null)
			{
				this.NotFound = true;
			}

			this.TimeSelected = TimeOnly.FromTimeSpan(DateTime.UtcNow.ToLocalTime().TimeOfDay);

            await base.OnInitializedAsync();
		}

		protected void GoBack()
		{
			var backUrl = this.BackUrl ?? FieldTripsPage.Url;

            this.NavigationManager.NavigateTo(backUrl);
        }

        public async Task StartSupervisionAsync()
        {
			await this.GamesRepository.InitializeParticipationsSupervisionAsync();
            this.GamesRepository.StartSupervisionAsync(this.FieldTrip, this.TimeSelected);
            //this.NavigationManager.NavigateTo(ParticipationsSupervisionPage.GetUrl(this.TimeSelected));
            this.NavigationManager.NavigateTo(ParticipationsSupervisionPage.Url);
        }

    }
}
