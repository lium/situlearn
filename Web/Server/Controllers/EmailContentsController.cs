﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Business;
using Common;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ozytis.Common.Core.Web.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Web.Server.Controllers
{
    [Authorize(Roles = UserRoleNames.Administrator)]
    [Route("api/email-contents")]
    public class EmailContentsController : ControllerBase
    {
        public EmailContentsController(EmailContentsManager emailContentsManager)
        {
            this.EmailContentsManager = emailContentsManager;
        }

        public EmailContentsManager EmailContentsManager { get; set; }

        [HttpGet]
        [WebApiQueryable2(nameof(EmailContent))]
        public async Task<IEnumerable<EmailContent>> GetAllEmailContentsAsync()
        {
            EmailContent[] results = await this.EmailContentsManager.SelectAll()
                .ApplyQueryV2Options(this.Request).ToArrayAsync();

            return results;
        }

        [HttpGet("{emailContentId:int}")]
        public async Task<EmailContent> GetEmailContentAsync(int emailContentId)
        {
            EmailContent result = await this.EmailContentsManager.GetEmailContentAsync(emailContentId);

            return result;
        }

        [HttpPut]
        [HandleBusinessException, ValidateModel]
        public async Task<EmailContent> UpdateEmailContentAsync([FromBody] EmailContentUpdateModel model)
        {
            EmailContent result = await this.EmailContentsManager.UpdateEmailAsync(new EmailContent
            {
                Id = model.Id,
                Name = model.Name,
                Subject = model.Subject,
                Content = model.Content,
                Variables = model.Variables,
            });

            return result;
        }
    }
}
