// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Api.Extensions;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using Ozytis.Common.Core.Web.Razor.Utilities;
using SituLearn.Monitor.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Monitor.Pages.Supervision
{
    [Route(Url)]
    [Authorize]

    public partial class ParticipationsSupervisionPage : ComponentBase, IAsyncDisposable
    {
        public const string Url = "/suivi-participations";

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [Inject]
        public IConfiguration Configuration { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        public TimeOnly TimeSelected { get; set; }

        public FieldTrip FieldTrip { get; set; }

        public bool DisplayMap { get; set; }

        public List<PlayerForMonitorModel> Players { get; set; }

        private HubConnection HubConnection { get; set; }

        public string HubConnectionId { get; set; }

        public string HubConnectionIdToDelete { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.TimeSelected = this.GamesRepository.CurrentTimeSelected;

            this.FieldTrip = this.GamesRepository.CurrentFieldTrip;

            this.DisplayMap = this.FieldTrip.ExplorationMap.IsGeolocatable;

            this.Players = (await this.GamesRepository.GetAllCurrentPlayers()).ToList();

            await this.InitializeHubConnection();

            this.FlagLatePlayers();

            this.GamesRepository.ConnectivityService.OnConnectionStateChanged += this.Connectivity_ConnectivityChanged;

            await base.OnInitializedAsync();
        }

        private async Task InitializeHubConnection()
        {
            this.HubConnection = new HubConnectionBuilder()
                .WithUrl(this.Configuration["ServerUrl"] + "monitorhub")
                .WithAutomaticReconnect()
                .Build();

            this.HubConnection.On<Guid, decimal, decimal, decimal?>("RefreshPositionAndBatteryLevel", (playerId, latitude, longitude, batteryLevel) =>
            {
                var player = this.Players.FirstOrDefault(p => p.Id == playerId);
                player.CurrentLatitude = latitude;
                player.CurrentLongitude = longitude;
                player.LowBatteryAlert = batteryLevel.HasValue && batteryLevel.Value < 0.20M;

                this.StateHasChanged();
            });

            this.HubConnection.On<Guid, bool>("RefreshOutOfExplorationAreaAlert", (playerId, isOutOfExplorationArea) =>
            {
                var player = this.Players.FirstOrDefault(p => p.Id == playerId);
                player.OutOfExplorationAreaAlert = isOutOfExplorationArea;

                this.StateHasChanged();
            });

            this.HubConnection.On<PlayerForMonitorModel>("AddNewPlayer", async (player) =>
            {
                //Si un utilisateur a recommenc� une sortie,
                if (this.Players.Any(p => p.UserId == player.UserId))
                {
                    //on attribue au nouveau Player la couleur du pr�c�dent Player
                    var playerToUpdate = this.Players.FirstOrDefault(p => p.UserId == player.UserId);
                    player.Color = playerToUpdate?.Color;

                    //on supprime dans le cache la couleur du pr�c�dent Player
                    await this.GamesRepository.RemovePlayerColorFromCacheAsync(playerToUpdate.Id);

                    //on ajoute dans le cache la couleur du nouveau Player
                    await this.GamesRepository.AddNewPlayerColorToCacheAsync(player.Id, player.Color);

                    //on supprime l'ancien Player de la liste des Players affich�s
                    this.Players.Remove(playerToUpdate);
                }

                this.Players = this.Players.Concat(new List<PlayerForMonitorModel> { player }).OrderBy(p => p.Name).ToList();
                this.FlagLatePlayers();
                this.StateHasChanged();
            });

            this.HubConnection.On<Guid, int, string>("RefreshCurrentUnit", (playerId, playedUnitsNumber, currentUnitName) =>
            {
                var player = this.Players.FirstOrDefault(p => p.Id == playerId);
                player.PlayedUnitsNumber = playedUnitsNumber;
                player.CurrentUnitName = currentUnitName;
                this.FlagLatePlayers();

                this.StateHasChanged();
            });

            this.HubConnection.On<Guid, int, string, int>("RefreshCurrentUnitAndAddLocationPoints", (playerId, playedUnitsNumber, currentUnitName, locationPoints) =>
            {
                var player = this.Players.FirstOrDefault(p => p.Id == playerId);
                player.PlayedUnitsNumber = playedUnitsNumber;
                player.CurrentUnitName = currentUnitName;
                player.Score += locationPoints;

                this.FlagLatePlayers();

                this.StateHasChanged();
            });

            this.HubConnection.On<Guid, bool, int>("RefreshCorrectAnswersRateAndScore", (playerId, isCorrect, points) =>
            {
                var player = this.Players.FirstOrDefault(p => p.Id == playerId);
                player.AnsweredQuestionsNumber++;
                player.Score += points;

                if (isCorrect)
                {
                    player.CorrectAnswersNumber++;
                }

                this.StateHasChanged();
            });

            this.HubConnection.On<Guid>("IncrementReadingsNumber", (playerId) =>
            {
                var player = this.Players.FirstOrDefault(p => p.Id == playerId);
                player.ReadingsNumber++;

                this.StateHasChanged();
            });

            this.HubConnection.On<Guid>("EndGame", (playerId) =>
            {
                var player = this.Players.FirstOrDefault(p => p.Id == playerId);
                player.PlayedUnitsNumber++;
                player.CurrentUnitName = null;
                this.FlagLatePlayers();
                this.StateHasChanged();
            });

            this.HubConnection.On<PlayerForMonitorModel>("RefreshReconnectedPlayer", player =>
            {
                //Si le joueur n'avait jamais �t� connect� avant,
                if (!this.Players.Any(p => p.Id == player.Id))
                {
                    //on le rajoute � la liste
                    this.Players = this.Players.Concat(new List<PlayerForMonitorModel> { player }).OrderBy(p => player.Name).ToList();
                }
                else//sinon on actualise ses donn�es
                {
                    var playerToRefresh = this.Players.FirstOrDefault(p => p.Id == player.Id);

                    playerToRefresh.Name = player.Name;
                    playerToRefresh.PlayedUnitsNumber = player.PlayedUnitsNumber;
                    playerToRefresh.UnitsToPlayNumber = player.UnitsToPlayNumber;
                    playerToRefresh.CurrentUnitName = player.CurrentUnitName;
                    playerToRefresh.Score = player.Score;
                    playerToRefresh.TotalPointsToWin = player.TotalPointsToWin;
                    playerToRefresh.CorrectAnswersNumber = player.CorrectAnswersNumber;
                    playerToRefresh.AnsweredQuestionsNumber = player.AnsweredQuestionsNumber;
                    playerToRefresh.ReadingsNumber = player.ReadingsNumber;
                    playerToRefresh.CurrentLatitude = player.CurrentLatitude;
                    playerToRefresh.CurrentLongitude = player.CurrentLongitude;
                    playerToRefresh.OutOfExplorationAreaAlert = player.OutOfExplorationAreaAlert;
                    playerToRefresh.LowBatteryAlert = player.LowBatteryAlert;
                }
                
                this.FlagLatePlayers();
                this.StateHasChanged();
            });

            this.HubConnection.Reconnecting += error =>
            {
                if (!string.IsNullOrEmpty(this.HubConnectionId))
                {
                    this.HubConnectionIdToDelete = this.HubConnectionId;
                    this.HubConnectionId = null;
                }

                this.Notifier.Notify(color: Ozytis.Common.Core.Web.Razor.Color.Warning, "La connexion a �t� perdue : veuillez r�tablir votre connexion internet", 5000);
                this.StateHasChanged();
                
                return Task.CompletedTask;
            };

            this.HubConnection.Reconnected += async (connectionId) =>
            {
                this.HubConnectionId = connectionId;

                this.Notifier.Notify(color: Ozytis.Common.Core.Web.Razor.Color.Success, "La connexion a �t� r�tablie", 5000);
                await this.RefreshData();

                await this.HubConnection.InvokeAsync("OnConnected", this.HubConnection.ConnectionId, this.FieldTrip.Id, this.TimeSelected);

                await this.HubConnection.InvokeAsync("OnDisconnected", this.HubConnectionIdToDelete);
                this.HubConnectionIdToDelete = null;

                this.StateHasChanged();
            };

            this.HubConnection.Closed += error =>
            {
                if (error != null)
                {
                    this.Notifier.Notify(color: Ozytis.Common.Core.Web.Razor.Color.Danger, "La connexion n'a pas pu �tre r�tablie : veuillez r�tablir votre connexion internet", 5000);
                }

                return Task.CompletedTask;
            };

            await this.HubConnection.StartAsync();

            await this.HubConnection.InvokeAsync("OnConnected", this.HubConnection.ConnectionId, this.FieldTrip.Id, this.TimeSelected);

            this.HubConnectionId = this.HubConnection.ConnectionId;
        }

        private async void Connectivity_ConnectivityChanged(object sender, bool isConnected)
        {
            if (isConnected && this.HubConnection.State == HubConnectionState.Disconnected)
            {
                await this.InitializeHubConnection();

                await this.HubConnection.InvokeAsync("OnDisconnected", this.HubConnectionIdToDelete);
                this.HubConnectionIdToDelete = null;

                this.Notifier.Notify(color: Ozytis.Common.Core.Web.Razor.Color.Success, "La connexion a �t� r�tablie", 10000);

                await this.RefreshData();
            }
        }

        private async Task RefreshData()
        {
            var hiddenPlayers = this.Players.Where(p => p.IsHidden).Select(p => p.Id);

            this.Players = (await this.GamesRepository.GetAllCurrentPlayers()).ToList();

            foreach (var player in this.Players)
            {
                if (hiddenPlayers.Contains(player.Id))
                {
                    player.IsHidden = true;
                }
            }

            this.FlagLatePlayers();
        }

        protected void GoBack()
        {
            this.NavigationManager.NavigateTo(TimeSelectSupervisionPage.GetUrl(this.FieldTrip.Id));
        }

        protected void SwitchMapOrListDisplay()
        {
            this.DisplayMap = !this.DisplayMap;

            this.StateHasChanged();
        }

        public void OnPlayerHiddenChange(Guid id)
        {
            var playerToChange = this.Players.FirstOrDefault(p => p.Id == id);
            playerToChange.IsHidden = !playerToChange.IsHidden;

            if (playerToChange.IsHidden && playerToChange.LateAlert)
            {
                playerToChange.LateAlert = false;
            }

            this.FlagLatePlayers();

            this.StateHasChanged();
        }

        public void FlagLatePlayers()
        {
            PlayerForMonitorModel[] players = this.Players.Where(p => !p.IsHidden).OrderBy(p => p.GetProgressionRatio()).ToArray();
            int lateCount = players.Length / 4;

            if (lateCount < 1)
            {
                return;
            }

            while (lateCount > 0 && players[lateCount - 1].GetProgressionRatio() == players[lateCount].GetProgressionRatio())
            {
                lateCount--;
            }

            int i = 0;
            for (; i < lateCount; i++)
            {
                players[i].LateAlert = true;
            }

            for (; i < players.Length; i++)
            {
                players[i].LateAlert = false;
            }
        }

        public async ValueTask DisposeAsync()
        {
            if (this.HubConnection is not null)
            {
                await this.HubConnection.InvokeAsync("OnDisconnected", this.HubConnection.ConnectionId);
                await this.HubConnection.DisposeAsync();
            }
        }
    }
}