﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;
using Entities;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.Extensions.Configuration;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using Ozytis.Common.Core.Web.Razor.Leaflet;
using Ozytis.Common.Core.Web.Razor.Utilities.GeoLocation;
using SituLearn.Player.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components.Web;

namespace SituLearn.Player.Pages.FieldTrips
{
    [Route(Url)]
    [Route(Url2)]
    [Route(ResumeUrl)]
    public partial class LocatedGameUnitPage : ComponentBase, IDisposable
    {
        public const string Url = "/unite";
        public const string Url2 = "/unite/{unitId:guid}";
        public const string ResumeUrl = "/unite/reprendre";

        public static string GetUrl(Guid locatedGameUnitId)
        {
            return Url2.Replace("{unitId:guid}", locatedGameUnitId.ToString());
        }

        [Parameter]
        public Guid? UnitId { get; set; }

        [SupplyParameterFromQuery]
        public string BackUrl { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public IConfiguration Configuration { get; set; }

        [Inject]
        public IConnectivityService ConnectivityService { get; set; }

        [Inject]
        public IJSRuntime JSRuntime { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [CascadingParameter]
        public AppLayout Layout { get; set; }

        public bool TestEnv => this.Layout.TestEnv;

        public LocatedGameUnit Unit { get; set; }

        public Journey Journey { get; set; }

        public FieldTrip FieldTrip { get; set; }

        public string BasePictureUrl { get; set; }

        public bool HasReachedPoint { get; private set; }

        public LocatedActivity CurrentActivity { get; set; }

        public bool ClueIsUsed { get; set; }

        public bool DisplayClue { get; set; }

        public Guid AnswerIdSelected { get; set; }

        public string AnswerText { get; set; }

        public bool? AnswerIsCorrect { get; set; }

        public int TotalPoints { get; set; }

        public bool TotalPointsIncreased { get; set; }

        public bool TotalPointsDecreased { get; set; }

        public List<PlayedActivityFile> PlayedActivityFiles { get; set; } = new();

        public bool DisplayEndScreenReadingActivity { get; set; }

        public bool ShowEnrichedMap { get; set; }

        public bool ShowUnitInfo { get; set; }

        public bool ShowUnitConclusion { get; set; }

        public int? DistanceFromUnit { get; set; }

        public bool RadarDistanceIncreased { get; set; }

        public bool RadarDistanceDecreased { get; set; }

        public bool IsSubmitting { get; set; }

        public bool IsPlayingNextActivity { get; set; }

        public LeafletMap LeafletMap { get; set; }

        public LeafletCoordinates MapCenter { get; set; }

        public bool Rerender { get; set; } = false;

        private System.Timers.Timer Timer;

        public TimeSpan ElapsedTime { get; set; }

        public System.Timers.Timer ElapsedTimeTimer { get; set; }

        public bool DisplayTimeAlert { get; set; }

        public bool DisplayOutOfExplorationAreaAlert { get; set; }

        public bool DisplayPictureInFullScreen { get; set; }

        public string PictureUrlToDisplayInFullScreen { get; set; }

        public bool DisplayAudioRecorder { get; set; }

        public bool IsRecordingAudio { get; set; }

        public bool AudioRecordingIsFinished { get; set; }

        public byte[] AudioRecordingData { get; set; }

        public string AudioRecordingMimeType { get; set; }

        public bool IsReadingCode { get; set; }

        [JSInvokable(nameof(OnAudioRecordingAvailable))]
        public Task OnAudioRecordingAvailable(byte[] bytes, string mimeType)
        {
            this.AudioRecordingData = bytes;
            this.AudioRecordingMimeType = mimeType;

            this.StateHasChanged();

            return Task.CompletedTask;
        }

        protected override async Task OnInitializedAsync()
        {
            //Console.WriteLine("Initialisation page unité de jeu");

            this.BasePictureUrl = "";

            if (this.GamesRepository.CurrentPlayer == null)
            {
                this.Notifier.Notify(Color.Info, "Pas de partie en cours");
                this.NavigationManager.NavigateTo(FieldTripsPage.Url);
                return;
            }
            this.GamesRepository.CurrentPlayer.PlayerState = Common.PlayerState.Playing;
			this.FieldTrip = this.GamesRepository.CurrentFieldTrip;
			this.Journey = this.FieldTrip.Journeys.FirstOrDefault(j => j.Id == this.GamesRepository.CurrentPlayer.JourneyId);
			if (this.UnitId.HasValue)
            {
                this.Unit = this.Journey.JourneyLocatedGameUnits
                    .FirstOrDefault(jlgu => jlgu.LocatedGameUnitId == this.UnitId)?.LocatedGameUnit;
            }
            else
            {
                var unitId = this.GamesRepository.CurrentPlayer.CurrentUnitId;
                this.Unit = this.FieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == unitId);
            }

            if (this.Unit == null)
            {
                this.Notifier.Notify(Color.Dark, "Unité de jeu actuelle non trouvée");

                if (!this.TestEnv)
                {
                    this.NavigationManager.NavigateTo(FieldTripsPage.Url);
                }

                return;
            }



			this.HasReachedPoint = this.GamesRepository.CurrentPlayer.CurrentUnitReachedDate.HasValue;

            this.TotalPoints = this.GamesRepository.CalculatePlayerTotalPoints();

            this.ShowUnitInfo = this.HasReachedPoint && this.Unit.InformationSheetEnabled;

            this.GamesRepository.OnUnitReached += async (sender, e) => await this.GamesRepository_OnUnitReached(sender, e);

            try
            {
                var elapsedTime = await this.GamesRepository.GetElapsedTimeAsync(this.GamesRepository.CurrentPlayer.Game.FielTripId);

                if(elapsedTime == default)
                {
                    this.ElapsedTime = TimeSpan.FromMinutes(0);
                }
                else
                {
                    this.ElapsedTime = elapsedTime;
                }
            }
            catch
            {
                this.ElapsedTime = TimeSpan.FromMinutes(0);
            }

            if (this.NavigationManager.Uri.EndsWith(ResumeUrl))
            {
                await this.ResumeActivityAsync();
            }


            this.GamesRepository.OnRadarDistanceFromUnitHaveChanged += this.OnRadarDistanceFromUnitHaveChanged;

            if (this.Unit.LocationAidType == LocationAidType.GpsRadar)
            {
                if(this.GamesRepository.CurrentPlayer.CurrentLatitude.HasValue && this.GamesRepository.CurrentPlayer.CurrentLongitude.HasValue)
                {
                    this.CalculateDistanceFromUnit(this.GamesRepository.CurrentPlayer.CurrentLatitude.Value, this.GamesRepository.CurrentPlayer.CurrentLongitude.Value);
                }
            }

            this.MapCenter = new LeafletCoordinates
            {
                Latitude = this.Unit.Latitude,
                Longitude = this.Unit.Longitude,
            };


            this.ElapsedTimeTimer = new System.Timers.Timer(60000);
            this.ElapsedTimeTimer.Elapsed += async (sender, e) => await this.ElapsedTimeTimer_OnTimeElapsed(sender, e);

            this.ElapsedTimeTimer.AutoReset = true;
            this.ElapsedTimeTimer.Enabled = true;

            this.GamesRepository.OnExplorationAreaLeft += this.OnExplorationAreaLeft;

            await this.JSRuntime.InvokeAsync<object>(
                identifier: "registerForAudioRecordingAvailability",
                DotNetObjectReference.Create(this),
                nameof(OnAudioRecordingAvailable)
            );

            this.StateHasChanged();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if ((firstRender || this.Rerender) && this.Unit != null && this.Unit.LocationAidType != LocationAidType.GpsRadar && !this.HasReachedPoint)
            {
                this.Timer = new System.Timers.Timer(1000);
                this.Timer.Elapsed += async (sender, args) =>
                {
                    if (this.FieldTrip.ExplorationMap.NorthEastLatitude.HasValue)
                    {
                        var bounds = new Bounds
                        {
                            NorthEast = new()
                            {
                                Latitude = this.FieldTrip.ExplorationMap.NorthEastLatitude.Value,
                                Longitude = this.FieldTrip.ExplorationMap.NorthEastLongitude.Value,
                            },
                            SouthWest = new()
                            {
                                Longitude = this.FieldTrip.ExplorationMap.SouthWestLongitude.Value,
                                Latitude = this.FieldTrip.ExplorationMap.SouthWestLatitude.Value,
                            }
                        };

                        await this.LeafletMap.FitBoundsAsync(bounds);
                        await this.LeafletMap.SetMaxBoundsAsync(bounds);
                        await this.LeafletMap.RefreshAsync();
                    }
                };

                this.Timer.AutoReset = false;
                this.Timer.Enabled = true;

                this.Rerender = false;
            }

            //Si l'unité de jeu a déjà été localisée à l'affichage de la page de l'unité de jeu
            if (firstRender && this.HasReachedPoint)
            {
                var playedActivities = this.GamesRepository.CurrentPlayer.PlayedUnits.FirstOrDefault(pu => pu.UnitId == this.GamesRepository.CurrentPlayer.CurrentUnitId).PlayedActivities;

                //et si aucune activité n'a encore été terminée
                if (playedActivities == null || playedActivities.Count() <= 1)
                {
                    //this.Notifier.Notify(Color.Success, "Bravo vous êtes au bon endroit !");

                    if (!this.ShowUnitInfo)
                    {
                        await this.PlayNextActivityAsync();
                    }

                    this.TotalPointsIncreased = true;
                    this.StateHasChanged();

                    await Task.Delay(2000);

                    this.TotalPointsIncreased = false;
                    this.StateHasChanged();
                }
            }

            await Task.CompletedTask;
        }

        private async Task GamesRepository_OnUnitReached(object sender, LocatedGameUnit e)
        {
            if (!this.HasReachedPoint)
            {
                //this.Notifier.Notify(Color.Success, "Bravo vous êtes au bon endroit !");
                this.HasReachedPoint = true;

                this.ShowUnitInfo = e.InformationSheetEnabled;

                if (!this.ShowUnitInfo)
                {
                    await this.PlayNextActivityAsync();
                }

                this.TotalPoints = this.GamesRepository.CalculatePlayerTotalPoints();
                this.TotalPointsIncreased = true;

                this.StateHasChanged();

                await Task.Delay(2000);

                this.TotalPointsIncreased = false;

                this.StateHasChanged();
            }
        }

        private void OnRadarDistanceFromUnitHaveChanged(object sender, GeoLocationResult e)
        {
            this.CalculateDistanceFromUnit(e.Location.Coords.Latitude, e.Location.Coords.Longitude);
            this.StateHasChanged();
        }

        protected void CalculateDistanceFromUnit(decimal currentLatitude, decimal currentLongitude)
        {
            var newDistance = this.GamesRepository.CalculateDistanceBetweenTwoPoints(currentLatitude, currentLongitude, this.Unit.Latitude, this.Unit.Longitude);

            if (this.DistanceFromUnit != null && newDistance < this.DistanceFromUnit)
            {
                this.RadarDistanceDecreased = true;
                this.RadarDistanceIncreased = false;
            }
            else
            {
                this.RadarDistanceDecreased = false;
                this.RadarDistanceIncreased = true;
            }

            this.DistanceFromUnit = newDistance;
        }

        protected async Task UnlockPositionAsync()
        {
            if (this.IsSubmitting)
            {
                return;
            }

            this.IsSubmitting = true;

            this.StateHasChanged();

            await this.GamesRepository.UnlockPositionAsync();
            //this.Notifier.Notify(Color.Success, "La destination a été débloquée");
            this.HasReachedPoint = true;
            var currentUnit = this.GamesRepository.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == this.GamesRepository.CurrentPlayer.CurrentUnitId);
            this.ShowUnitInfo = currentUnit.InformationSheetEnabled;

            if (!this.ShowUnitInfo)
            {
                await this.PlayNextActivityAsync();
            }

            if (this.Unit.LocationValidationType == LocationValidationType.Manual)
            {
                this.TotalPoints = this.GamesRepository.CalculatePlayerTotalPoints();
                this.TotalPointsIncreased = true;

                this.StateHasChanged();

                await Task.Delay(2000);

                this.TotalPointsIncreased = false;
            }

            this.IsSubmitting = false;

            this.StateHasChanged();
        }

        public void Dispose()
        {
            this.GamesRepository.OnUnitReached -= async (sender, e) => await this.GamesRepository_OnUnitReached(sender, e);
            this.GamesRepository.OnRadarDistanceFromUnitHaveChanged -= this.OnRadarDistanceFromUnitHaveChanged;
            this.ElapsedTimeTimer.Elapsed -= async (sender, e) => await this.ElapsedTimeTimer_OnTimeElapsed(sender, e);
            this.ElapsedTimeTimer.Enabled = false;
            this.GamesRepository.OnExplorationAreaLeft -= this.OnExplorationAreaLeft;
        }

        protected async Task OnCodeReadAsync(string value)
        {
            if (this.IsReadingCode)
            {
                return;
            }

            this.IsReadingCode = true;

            bool result = await this.GamesRepository.CheckCodeValidationAsync(value);

            if (result)
            {
                //this.Notifier.Notify(Color.Success, "La destination a été débloquée");
                this.HasReachedPoint = true;
            }
            else
            {
                this.Notifier.Notify(Color.Danger, "Ce code n'est pas le bon");
            }

            this.IsReadingCode = false;
        }

        protected async Task OnClickPlayNextActivityAsync()
        {
            if (this.IsPlayingNextActivity)
            {
                return;
            }

            this.IsPlayingNextActivity = true;

            this.StateHasChanged();

            await this.PlayNextActivityAsync();

            this.IsPlayingNextActivity = false;

            this.ClueIsUsed = false;

            this.StateHasChanged();
        }

        protected async Task PlayNextActivityAsync()
        {
            this.CurrentActivity = await this.GamesRepository.PlayNextActivityAsync();

            if (this.CurrentActivity == null)
            {
                var unit = this.GamesRepository.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == this.GamesRepository.CurrentPlayer.CurrentUnitId);
                this.ShowUnitConclusion = unit.ConclusionEnabled;
                if (!this.ShowUnitConclusion)
                {
                    await this.PlayNextUnitAsync();
                }
            }

            if (this.ShowUnitInfo)
            {
                this.ShowUnitInfo = false;
            }

            this.AnswerIdSelected = Guid.Empty;
            this.AnswerText = null;
            this.AnswerIsCorrect = null;
            this.PlayedActivityFiles = new();
            this.DisplayEndScreenReadingActivity = false;
        }

        protected async Task ResumeActivityAsync()
        {
            this.CurrentActivity = await this.GamesRepository.ResumeActivity();
            if (this.CurrentActivity == null)
            {
                var unit = this.GamesRepository.CurrentFieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == this.GamesRepository.CurrentPlayer.CurrentUnitId);

                this.ShowUnitConclusion = unit.ConclusionEnabled;

                if (!this.ShowUnitConclusion)
                {
                    await this.PlayNextUnitAsync();
                }
            }

            if (this.ShowUnitInfo)
            {
                this.ShowUnitInfo = false;
            }

            this.AnswerIdSelected = Guid.Empty;
            this.AnswerText = null;
            this.AnswerIsCorrect = null;
            this.PlayedActivityFiles = new();
            this.DisplayEndScreenReadingActivity = false;

            this.StateHasChanged();
        }

        protected async Task OnClickPlayNextUnitAsync()
        {
            if (this.IsSubmitting)
            {
                return;
            }

            this.IsSubmitting = true;

            this.StateHasChanged();

            await this.PlayNextUnitAsync();

            this.IsSubmitting = false;

            this.StateHasChanged();
        }

        protected async Task PlayNextUnitAsync()
        {
            var currentPlayer = this.GamesRepository.CurrentPlayer;

            if (this.FieldTrip.FieldTripType == Common.FieldTripType.TreasureHunt)
            {
                var unitId = await this.GamesRepository.PlayNextUnitAsync(currentPlayer.GameId, currentPlayer.Id);
                this.Unit = unitId.HasValue ? this.FieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == unitId): null;
                this.HasReachedPoint = false;
                this.Rerender = true;

                if (this.ShowUnitConclusion)
                {
                    this.ShowUnitConclusion = false;
                }

                if (this.Unit == null)
                {
                    // pas de nouvelle unité la partie est terminée
                    this.NavigationManager.NavigateTo(EndGamePage.GetUrl(this.GamesRepository.CurrentPlayer.Id));
                    return;
                }

                if (this.Unit.LocationAidType == LocationAidType.GpsRadar && this.GamesRepository.CurrentPlayer.CurrentLatitude.HasValue && this.GamesRepository.CurrentPlayer.CurrentLongitude.HasValue)
                {
                    this.CalculateDistanceFromUnit(this.GamesRepository.CurrentPlayer.CurrentLatitude.Value, this.GamesRepository.CurrentPlayer.CurrentLongitude.Value);
                }
            }
            else
            {
                bool gameOver = await this.GamesRepository.EndUnitAsync();

                if (gameOver)
                {
                    this.NavigationManager.NavigateTo(EndGamePage.GetUrl(this.GamesRepository.CurrentPlayer.Id));
                }
                else
                {
                    this.NavigationManager.ResetTo(InteractiveWalkOrActivitiesHubPage.Url);
                }                
            }            
        }

        protected async Task ValidateClueIsUsed()
        {
            //Player has already paid for it
            if (this.ClueIsUsed)
            {
                this.DisplayClue = false;
                this.StateHasChanged();
            }
            else
            {
                var question = this.CurrentActivity as QuestionActivity;

                this.ClueIsUsed = true;
                this.Notifier.Notify(Color.Danger, $"Vous avez utilisé l'indice : cela vous coûte {question.Clue.Cost} points");

                this.TotalPoints = await this.GamesRepository.RemoveClueCostToCurrentActivityPoints(this.CurrentActivity.Id, question.Clue.Cost);

                this.TotalPointsDecreased = true;

                this.StateHasChanged();

                await Task.Delay(2000);

                this.TotalPointsDecreased = false;

                this.StateHasChanged();
            }

        }

        protected void UpdateAnswerSelection(Guid answerIdSelected)
        {
            if (this.AnswerIsCorrect.HasValue)
            {
                return;
            }

            if (this.AnswerIdSelected == answerIdSelected)
            {
                this.AnswerIdSelected = Guid.Empty;
            }
            else
            {
                this.AnswerIdSelected = answerIdSelected;
            }

            this.StateHasChanged();
        }

        protected async Task ValidateAnswerAsync(QuestionActivity question)
        {
            if (this.IsSubmitting)
            {
                return;
            }

            this.IsSubmitting = true;

            this.StateHasChanged();

            if ((string.IsNullOrEmpty(this.AnswerText) && this.AnswerIdSelected == Guid.Empty) || this.DisplayClue)
            {
                return;
            }

            this.AnswerIsCorrect = await this.GamesRepository.ValidateAnswerAsync(question, this.AnswerText, this.AnswerIdSelected);

            if (this.AnswerIsCorrect.Value)
            {
                this.TotalPoints = this.GamesRepository.CalculatePlayerTotalPoints();
                this.TotalPointsIncreased = true;

                this.StateHasChanged();

                await Task.Delay(2000);

                this.TotalPointsIncreased = false;
            }

            this.IsSubmitting = false;

            this.StateHasChanged();
        }

        protected async Task AddPhotoToCurrentReadingActivity(InputFileChangeEventArgs e)
        {
            string mimeType;

            if (e.File.Name.EndsWith(".jpeg") || e.File.Name.EndsWith(".jpg"))
            {
                mimeType = "image/jpeg";
            }
            else if (e.File.Name.EndsWith(".png"))
            {
                mimeType = "image/png";
            }
            else
            {
                this.Notifier.Notify(Color.Danger, "Vous ne pouvez enregistrer que des photos");

                this.StateHasChanged();

                return;
            }

            var file = await e.File.RequestImageFileAsync(mimeType, 512, 512);

            var buffer = new byte[file.Size];
            await file.OpenReadStream(maxAllowedSize: 50000000).ReadAsync(buffer);
            var dataUrl = "data:" + mimeType + $";base64,{Convert.ToBase64String(buffer)}";

            PlayedActivityFile playedActivityFile = new PlayedActivityFile
            {
                Id = Guid.NewGuid(),
                MimeType = mimeType,
                Url = dataUrl
            };

            this.PlayedActivityFiles.Add(playedActivityFile);

            this.StateHasChanged();
        }

        protected async Task AddAudioRecordingToCurrentReadingActivity()
        {
            if (this.AudioRecordingData != null)
            {
                var dataUrl = $"data:{this.AudioRecordingMimeType};base64,{Convert.ToBase64String(this.AudioRecordingData)}";

                PlayedActivityFile playedActivityFile = new PlayedActivityFile
                {
                    Id = Guid.NewGuid(),
                    MimeType = this.AudioRecordingMimeType,
                    Url = dataUrl
                };

                this.PlayedActivityFiles.Add(playedActivityFile);

                this.IsRecordingAudio = false;
                this.AudioRecordingIsFinished = false;
                this.DisplayAudioRecorder = false;
                this.AudioRecordingData = null;
                this.AudioRecordingMimeType = null;
                this.StateHasChanged();
            }
            else
            {
                await this.CancelAudioRecording();
            }
        }

        protected async Task DisplayAudioRecordingModal()
        {
            try
            {
                bool authorizeRecording = await this.JSRuntime.InvokeAsync<bool>("authorizeAudioRecording");
                
                if (authorizeRecording)
                {
                    this.DisplayAudioRecorder = true;
                    this.StateHasChanged();
                }
                else
                {
                    this.Notifier.Notify(color: Color.Danger, "Vous n'avez pas autorisé l'application à utiliser votre micro");
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                this.Notifier.Notify(color: Color.Danger, "Vous n'avez pas autorisé l'application à utiliser votre micro");
            }
        }

        protected async Task CancelAudioRecording()
        {
            await this.StopAudioRecording();

            this.IsRecordingAudio = false;
            this.AudioRecordingIsFinished = false;
            this.DisplayAudioRecorder = false;
            this.AudioRecordingData = null;
            this.AudioRecordingMimeType = null;
            this.StateHasChanged();
        }

        protected async Task StartAudioRecording()
        {            
            this.IsRecordingAudio = true;
            this.StateHasChanged();
            
            await this.JSRuntime.InvokeVoidAsync("startAudioRecording");
        }

        protected async Task StopAudioRecording()
        {            
            this.IsRecordingAudio = false;
            this.AudioRecordingIsFinished = true;
            this.StateHasChanged();

            await this.JSRuntime.InvokeVoidAsync("stopAudioRecording");
        }

        protected async Task RegisterReadingAsync(ReadingActivity reading)
        {
            if (this.IsSubmitting)
            {
                return;
            }

            this.IsSubmitting = true;

            this.StateHasChanged();

            await this.GamesRepository.RegisterReadingAsync(reading, this.AnswerText, this.PlayedActivityFiles);

            this.DisplayEndScreenReadingActivity = true;
            this.PlayedActivityFiles = new();

            this.IsSubmitting = false;

            this.StateHasChanged();
        }

        protected async Task AddNewReadingAsync(ReadingActivity reading)
        {
            if (this.IsSubmitting)
            {
                return;
            }

            this.IsSubmitting = true;

            this.StateHasChanged();

            await this.GamesRepository.AddNewReadingAsync(reading);

            this.DisplayEndScreenReadingActivity = false;
            this.AnswerText = null;
            this.PlayedActivityFiles = new();

            this.IsSubmitting = false;

            this.StateHasChanged();
        }

        protected void CloseDisplayTimeAlert(int remainingTime)
        {
            this.DisplayTimeAlert = false;
            this.StateHasChanged();

            if (remainingTime == 0)
            {
                this.NavigationManager.NavigateTo(EndGamePage.GetUrl(this.GamesRepository.CurrentPlayer.Id));
            }
        }

        private async Task ElapsedTimeTimer_OnTimeElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.ElapsedTime = this.ElapsedTime + TimeSpan.FromMinutes(1);

            await this.GamesRepository.StoreElapsedTimeAsync(this.GamesRepository.CurrentPlayer.Game.FielTripId, this.ElapsedTime);
            if ((int)this.ElapsedTime.TotalMinutes == this.FieldTrip.AllottedTimeDuration - 1)
            {
                this.ElapsedTimeTimer.AutoReset = false;
            }

            if (this.FieldTrip.TimeIsLimited)
            {
                var remainingTime = this.FieldTrip.AllottedTimeDuration - this.ElapsedTime.TotalMinutes;

                switch(remainingTime)
                {
                    case < 1 and >= 0:
                        if (this.IsRecordingAudio)
                        {
                            await this.StopAudioRecording();
                            this.DisplayAudioRecorder = false;
                        }

                        this.DisplayTimeAlert = true;
                        this.ElapsedTimeTimer.Enabled = false;
                        await this.GamesRepository.StopGameAsync();
                        break;
                    case < 6 and >= 5:
                    case < 11 and >= 10:
                        this.DisplayTimeAlert = true;
                        break;

                    default:
                        this.DisplayTimeAlert = false;
                        break;
                }
            }

            this.StateHasChanged();

            await Task.CompletedTask;
        }

        private void OnExplorationAreaLeft(object sender, GeoLocationCoordinates e)
        {
            this.DisplayOutOfExplorationAreaAlert = true;
            this.StateHasChanged();
        }

        public async Task OnKeyDown(KeyboardEventArgs args)
        {
            if (args.Key == "Enter")
            {
                await this.JSRuntime.InvokeVoidAsync("window.blurActiveElement");
            }
        }

        public void DisplayFullScreenPicture(string pictureUrl)
        {
            this.DisplayPictureInFullScreen = true;
            this.PictureUrlToDisplayInFullScreen = pictureUrl;
            this.StateHasChanged();
        }

        public void CloseDisplayPictureInFullScreen()
        {
            this.DisplayPictureInFullScreen = false;
            this.PictureUrlToDisplayInFullScreen = null;
            this.StateHasChanged();
        }
    }
}
