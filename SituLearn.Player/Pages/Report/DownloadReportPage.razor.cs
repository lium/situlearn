﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Player.Pages.Report
{
    [Route(DownloadReportPage.Url)]
    [AllowAnonymous]
    public partial class DownloadReportPage : ComponentBase
    {
        public DownloadReportPage()
        {

        }

        public const string Url = "/download-report";

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        private IJSRuntime JSRuntime { get; set; }

        [Inject]
        public IConfiguration Configuration { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [Parameter]
        public string Path { get; set; }

        public string[] Errors { get; set; }

        public bool IsProcessing { get; set; }

        public bool IsCompleted { get; set; }

        public bool IsSuccess { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var uri = this.NavigationManager.ToAbsoluteUri(NavigationManager.Uri);
            var queryParameters = QueryHelpers.ParseQuery(uri.Query);

            if (queryParameters.TryGetValue("path", out var path))
            {
                this.Path = path;
            }

            await base.OnInitializedAsync();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender && !this.IsProcessing && !this.IsCompleted)
            {
                this.IsProcessing = true;
                this.StateHasChanged();

                DotNetObjectReference<DownloadReportPage> dotNetReference = DotNetObjectReference.Create(this);

                await this.JSRuntime.InvokeVoidAsync("queryAndSaveAsFile", $"{this.Configuration["ServerUrl"]}api/games/report?path={Uri.EscapeDataString(this.Path)}", true, dotNetReference);
            }            

            await base.OnAfterRenderAsync(firstRender);
        }

        [JSInvokable]
        public void HandleSendingSuccess()
        {
            Notifier.Notify(Color.Success, "Le fichier a bien été téléchargé.");
            this.IsProcessing = false;
            this.IsCompleted = true;
            this.IsSuccess = true;
            this.StateHasChanged();
        }

        [JSInvokable]
        public void HandleSendingFail(string[] err)
        {
            this.Notifier.Notify(Color.Danger, $"Une erreur s'est produite : {string.Join(" ", err)}" );
            this.IsProcessing = false;
            this.IsCompleted = true;
            this.StateHasChanged();
        }
    }
}
