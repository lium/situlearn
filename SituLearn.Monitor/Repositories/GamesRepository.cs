﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using ClientApi;
using Entities;
using Ozytis.Common.Core.ClientApi;
using Ozytis.Common.Core.Web.Razor.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Monitor.Repositories
{
    public class GamesRepository
    {
        private const string PlayersColorDateKey = "PlayersColorDate";
        private const string ColorByPlayerIdStartKey = "color-player-";

        private readonly GamesService gamesService = new();

        public GamesRepository(LocalStorageService localStorageService, IConnectivityService connectivityService)
        {
            this.LocalStorageService = localStorageService;
            this.ConnectivityService = connectivityService;
        }

        public FieldTrip CurrentFieldTrip { get; set; }

        public TimeOnly CurrentTimeSelected { get; set; }

        public LocalStorageService LocalStorageService { get; }

        public IConnectivityService ConnectivityService { get; }

        public List<string> Colors { get; set; } = new List<string>() { "#00b4ff", "#b493c9", "#ffb0ff", "#ffad91", "#ffd79e", "#b9ffa6", "#ffffaa", "#96c189", "#9ec5c4", "#bcffff", "#007aff", "#743f9d", "#ff63ff", "#ff613d", "#ffb74d", "#ffff5c", "#6aff54", "#469639", "#4a9796", "#70ffff", "#004ddb", "#42066f", "#d218d2", "#d91305", "#d2890e", "#d2d115", "#1fcb15", "#0f6909", "#106767", "#1ed2d1", "#002d78", "#2a0d42", "#731e72", "#7d1c0b", "#734d14", "#73721b", "#226b19", "#153e0e", "#153939", "#247272", "#57d2ff", "#d2bedf", "#ffd1ff", "#ffcfbd", "#ffe7c5", "#ffffcd", "#d8ffcb", "#bbd7b2", "#c6dddc", "#d9ffff", "#0096ff", "#9469b3", "#ff8dff", "#ff8966", "#ffc776", "#ffff85", "#97ff80", "#70ac61", "#76aead", "#9affff", "#0061ff", "#531087", "#ff1bff", "#ff250f", "#ffa711", "#ffff18", "#00ff00", "#008000", "#008080", "#21ffff", "#003bac", "#360b58", "#a11ea1", "#aa1b0a", "#a26a14", "#a1a01b", "#24991a", "#14530d", "#155050", "#25a1a0", "#001f45", "#1f0c2d", "#481947", "#52180a", "#483112", "#484717", "#1b3f14", "#132a0d", "#122424", "#1d4747", "#ffb593", "#ffea98", "#e7e7e7", "#ff7440", "#ffdd4a", "#c4c4c4", "#d63d07", "#ccac13", "#8e8e8e", "#79290e", "#736118", "#4a4a4a", "#ffd4be", "#fff1bc", "#f3f3f3", "#ff966a", "#ffe474", "#d7d7d7", "$ff4d0e", "#fcd502", "#acacac", "#a6330c", "#9f8618", "#6d6d6d", "#4e1e0c", "#4a3f15", "#282828", "#d2efff", "#f1eaf5", "#fff1ff", "#fff0ea", "#fff8ed", "#fffff0", "#f4ffef", "#eaeddc", "#eef5f4", "#f4ffff", "#000d19", "#14061a", "#211020", "#2b1105", "#21180a", "#211f0e", "#0e1809", "#0c1707", "#070f0f", "#12201f", "#fff2eb", "#fff8de", "#ffffff", "#271306", "#251f0e", "#000000" };

        public List<string> UsedColors { get; set; } = new();

        public async Task InitializeParticipationsSupervisionAsync()
        {
            string date = await this.LocalStorageService.GetItemAsync<string>(PlayersColorDateKey);

            if (date == null || DateOnly.Parse(date) != DateOnly.FromDateTime(DateTime.UtcNow))
            {
                await this.LocalStorageService.SetItemAsync(PlayersColorDateKey, DateOnly.FromDateTime(DateTime.UtcNow).ToString());
            }

            if (date != null && DateOnly.Parse(date) != DateOnly.FromDateTime(DateTime.UtcNow))
            {
                var playersColorKeysToRemove = await this.LocalStorageService.GetKeysByPartialKeyAsync(ColorByPlayerIdStartKey);

                foreach (var key in playersColorKeysToRemove)
                {
                    await this.LocalStorageService.RemoveItemAsync(key);
                }
            }

            this.UsedColors = new List<string>();
        }

        public void StartSupervisionAsync(FieldTrip fieldTrip, TimeOnly timeSelected)
        {
            this.CurrentFieldTrip = fieldTrip;
            this.CurrentTimeSelected = timeSelected;
        }

        public async Task<IEnumerable<PlayerForMonitorModel>> GetAllCurrentPlayers()
        {
            //IEnumerable<PlayerForMonitorModel> players = new List<PlayerForMonitorModel>()
            //         {
            //             new PlayerForMonitorModel()
            //             {
			         //        //Id = Guid.NewGuid(),
			         //        Id = Guid.Parse("e2f1b59d-a735-43a9-a68b-b7aab3ec7615"),
            //                 Name = "Alerte dernier",
			         //        //Color = "#05bef6",
			         //        PlayedUnitsNumber = 1,
            //                 UnitsToPlayNumber = 9,
            //                 PhoneNumber = "06 07 08 09 10",
            //                 CurrentUnitName = "Belvédère d'Ozytis",
            //                 Score = 5,
            //                 TotalPointsToWin = 76,
            //                 ReadingsNumber = 2,
            //                 CurrentLatitude = 48.06738M,
            //                 CurrentLongitude = -0.7621M,
            //                 OutOfExplorationAreaAlert = true,
            //                 LateAlert = false,
            //                 LowBatteryAlert = true,
            //                 IsHidden = false
            //             },
            //             new PlayerForMonitorModel()
            //             {
			         //        //Id = Guid.NewGuid(),
			         //        Id = Guid.Parse("00a01c28-87d0-4e70-8d2b-86f143528d6a"),
            //                 Name = "Avant dernier alerte",
			         //        //Color = "#f68a05",
			         //        PlayedUnitsNumber = 2,
            //                 UnitsToPlayNumber = 9,
            //                 PhoneNumber = "06 97 98 99 90",
            //                 CurrentUnitName = "Domino’s Pizza",
            //                 Score = 43,
            //                 TotalPointsToWin = 76,
            //                 ReadingsNumber = 6,
            //                 CurrentLatitude = 48.06873M,
            //                 CurrentLongitude = -0.76147M,
            //                 OutOfExplorationAreaAlert = false,
            //                 LateAlert = true,
            //                 LowBatteryAlert = false,
            //                 IsHidden = false
            //             },
            //             new PlayerForMonitorModel()
            //             {
			         //        //Id = Guid.NewGuid(),
			         //        Id = Guid.Parse("fb1c81c9-8c34-4672-b389-de7c671eb53d"),
            //                 Name = "Masqué padalerte",
			         //        //Color = "#f60549",
			         //        PlayedUnitsNumber = 1,
            //                 UnitsToPlayNumber = 9,
            //                 PhoneNumber = "",
            //                 CurrentUnitName = "Coiff&Co",
            //                 Score = 12,
            //                 TotalPointsToWin = 76,
            //                 ReadingsNumber = 3,
            //                 CurrentLatitude = 48.06816M,
            //                 CurrentLongitude = -0.76128M,
            //                 OutOfExplorationAreaAlert = true,
            //                 LateAlert = false,
            //                 LowBatteryAlert = false,
            //                 IsHidden = true,

            //             },
            //             new PlayerForMonitorModel()
            //            {
            //                Id = Guid.Parse("de35f23d-b635-49cf-9932-0d817043bf75"),
            //                Name = "Padalerte Exaequo",
            //                PlayedUnitsNumber = 3,
            //                UnitsToPlayNumber = 9,
            //                PhoneNumber = "06 06 06 06 06",
            //                CurrentUnitName = "Pharmacie Saint-Michel",
            //                Score = 35,
            //                TotalPointsToWin = 76,
            //                ReadingsNumber = 4,
            //                CurrentLatitude = 48.06842M,
            //                CurrentLongitude = -0.76131M,
            //                OutOfExplorationAreaAlert = false,
            //                LateAlert = false,
            //                LowBatteryAlert = true,
            //                IsHidden = false
            //            },
            //            new PlayerForMonitorModel()
            //            {
            //                Id = Guid.Parse("de12f23d-b635-49cf-9932-0d817043bf75"),
            //                Name = "Onsen fiche",
            //                PlayedUnitsNumber = 3,
            //                UnitsToPlayNumber = 9,
            //                PhoneNumber = "06 06 06 06 06",
            //                CurrentUnitName = "Pharmacie Saint-Michel",
            //                Score = 35,
            //                TotalPointsToWin = 76,
            //                ReadingsNumber = 4,
            //                CurrentLatitude = 48.06843M,
            //                CurrentLongitude = -0.76132M,
            //                OutOfExplorationAreaAlert = false,
            //                LateAlert = false,
            //                LowBatteryAlert = false,
            //                IsHidden = false
            //            },
            //            new PlayerForMonitorModel()
            //            {
            //                Id = Guid.Parse("de98f23d-b635-49cf-9932-0d817043bf75"),
            //                Name = "Onsen fiche 2",
            //                PlayedUnitsNumber = 3,
            //                UnitsToPlayNumber = 9,
            //                PhoneNumber = "06 06 06 06 06",
            //                CurrentUnitName = "Pharmacie Saint-Michel",
            //                Score = 35,
            //                TotalPointsToWin = 76,
            //                ReadingsNumber = 4,
            //                CurrentLatitude = 48.06842M,
            //                CurrentLongitude = -0.76131M,
            //                OutOfExplorationAreaAlert = false,
            //                LateAlert = false,
            //                LowBatteryAlert = false,
            //                IsHidden = false
            //            },
            //                                    new PlayerForMonitorModel()
            //            {
            //                Id = Guid.Parse("de77f23d-b635-49cf-9932-0d817043bf75"),
            //                Name = "Onsen fich 3",
            //                PlayedUnitsNumber = 3,
            //                UnitsToPlayNumber = 9,
            //                PhoneNumber = "06 06 06 06 06",
            //                CurrentUnitName = "Pharmacie Saint-Michel",
            //                Score = 35,
            //                TotalPointsToWin = 76,
            //                ReadingsNumber = 4,
            //                CurrentLatitude = 48.06842M,
            //                CurrentLongitude = -0.76131M,
            //                OutOfExplorationAreaAlert = false,
            //                LateAlert = false,
            //                LowBatteryAlert = false,
            //                IsHidden = false
            //            },
            //            new PlayerForMonitorModel()
            //            {
            //                Id = Guid.Parse("de12f24d-b635-49cf-9932-0d817043bf75"),
            //                Name = "Onsen fiche 4",
            //                PlayedUnitsNumber = 5,
            //                UnitsToPlayNumber = 9,
            //                PhoneNumber = "06 06 06 06 06",
            //                CurrentUnitName = "Pharmacie Saint-Michel",
            //                Score = 35,
            //                TotalPointsToWin = 76,
            //                ReadingsNumber = 4,
            //                CurrentLatitude = 48.06842M,
            //                CurrentLongitude = -0.76131M,
            //                OutOfExplorationAreaAlert = false,
            //                LateAlert = false,
            //                LowBatteryAlert = true,
            //                IsHidden = false
            //            },
            //            new PlayerForMonitorModel()
            //            {
            //                Id = Guid.Parse("de12f23d-b635-49cf-9932-0d817043bf75"),
            //                Name = "Onsen fiche 5",
            //                PlayedUnitsNumber = 7,
            //                UnitsToPlayNumber = 9,
            //                PhoneNumber = "06 06 06 06 06",
            //                CurrentUnitName = "Pharmacie Saint-Michel",
            //                Score = 35,
            //                TotalPointsToWin = 76,
            //                ReadingsNumber = 4,
            //                CurrentLatitude = 48.06842M,
            //                CurrentLongitude = -0.76131M,
            //                OutOfExplorationAreaAlert = false,
            //                LateAlert = false,
            //                LowBatteryAlert = false,
            //                IsHidden = false
            //            }

            //}
            //.OrderBy(p => p.Name)
            //.ToList();

            DateTime startDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, this.CurrentTimeSelected.Hour, this.CurrentTimeSelected.Minute, this.CurrentTimeSelected.Second);

            IEnumerable<PlayerForMonitorModel> players = await this.gamesService.GetAllPlayersForMonitorAsync(this.CurrentFieldTrip.Id, startDateTime);

            //players = players.Concat(await this.gamesService.GetAllPlayersForMonitorAsync(this.CurrentFieldTrip.Id, startDateTime));

            players = await this.AssignColorsToPlayers(players);

            //TODO : traitement alerte "en retard"

            return players;
        }

        public async Task<IEnumerable<PlayerForMonitorModel>> AssignColorsToPlayers(IEnumerable<PlayerForMonitorModel> players)
        {
            //vérification si des joueurs ont déjà une couleur affectée dans le cache
            var existingPlayersColorKeys = await this.LocalStorageService.GetKeysByPartialKeyAsync(ColorByPlayerIdStartKey);

            if (existingPlayersColorKeys != null)
            {
                //si oui, on réattribue aux joueurs présents dans le suivi la même couleur
                foreach (var key in existingPlayersColorKeys)
                {
                    string color = await this.LocalStorageService.GetItemAsync<string>(key);

                    Guid playerId = Guid.Parse(key.Replace(ColorByPlayerIdStartKey, ""));
                    PlayerForMonitorModel player = players.FirstOrDefault(p => p.Id == playerId);

                    if (player != null)
                    {
                        player.Color = color;
                    }

                    //si des couleurs sont affectées dans le cache (y compris à des joueurs non présents dans le suivi), on les considère comme déjà utilisées
                    this.UsedColors.Add(color);
                }
            }

            var playersWhitoutColor = players.Where(p => string.IsNullOrEmpty(p.Color));

            foreach (var player in playersWhitoutColor)
            {
                player.Color = this.Colors.FirstOrDefault(c => !this.UsedColors.Contains(c));
                this.UsedColors.Add(player.Color);
                await this.LocalStorageService.SetItemAsync($"{ColorByPlayerIdStartKey}{player.Id}", player.Color);
            }

            return players;
        }

        public async Task RemovePlayerColorFromCacheAsync(Guid playerId)
        {
            await this.LocalStorageService.RemoveItemAsync($"{ColorByPlayerIdStartKey}{playerId}");
        }

        public async Task AddNewPlayerColorToCacheAsync(Guid playerId, string color)
        {
            await this.LocalStorageService.SetItemAsync($"{ColorByPlayerIdStartKey}{playerId}", color);
        }

        public decimal? CalculateCorrectAnswersRate(int correctAnswersNumber, int answeredQuestionsNumber)
        {
            decimal? result = answeredQuestionsNumber == 0 ? null : Math.Round((decimal)correctAnswersNumber / (decimal)answeredQuestionsNumber * 100, 0);

            return result;
        }
    }
}
