﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Common;

using NetTopologySuite.Geometries;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    public class LocatedGameUnit : Abstract.SoftDelete
	{
        public Guid Id { get; set; }

        public FieldTrip FieldTrip { get; set; }

        [MaxLength(250)]
        public string Name { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal Latitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal Longitude { get; set; }

        public Geometry ActivationArea { get; set; }

        public ActivationAreaType ActivationAreaType { get; set; }

        public int Order { get; set; }

        public LocationAidType LocationAidType { get; set; }

        public LocationValidationType LocationValidationType { get; set; }

        public int LocationGain { get; set; } = 5;

        [MaxLength(250)]
        [Description("Information : Titre")]
        public string InformationSheetTitle { get; set; }

        [MaxLength(1000)]
        [Description("Information : Description")]
        public string InformationSheetDescription { get; set; }

        [MaxLength(500)]
        [Description("Information : Illustration")]
        public string InformationSheetPicture { get; set; }

        [MaxLength(250)]
        [Description("Conclusion : Titre")]
        public string ConclusionTitle { get; set; }

        [MaxLength(1000)]
        [Description("Conclusion : Description")]
        public string ConclusionText { get; set; }

        [MaxLength(500)]
        [Description("Conclusion : Illustration")]
        public string ConclusionPicture { get; set; }

        [ForeignKey("FieldTrip")]
        public Guid FieldTripId { get; set; }

        [InverseProperty("LocatedGameUnit")]
        public List<LocatedActivity> Activities { get; set; }

        public List<JourneyLocatedGameUnit> JourneyLocatedGameUnits { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? ActivationAreaCenterLatitude { get; set; }

        [Column(TypeName = "decimal(8,5)")]
        public decimal? ActivationAreaCenterLongitude { get; set; }

        [Column(TypeName = "decimal(7,1)")]
        public decimal? ActivationRadius { get; set; }

        [MaxLength(250)]
        [Description("Aide à la localisation : Titre")]
        public string LocationSupportTitle { get; set; }

        [MaxLength(1000)]
        [Description("Aide à la localisation : Description")]
        public string LocationSupportDescription { get; set; }

        [MaxLength(500)]
        [Description("Aide à la localisation : Illustration")]
        public string LocationSupportPicture { get; set; }

        public bool InformationSheetEnabled { get; set; }
        public bool ActivitiesEnabled { get; set; }
        public bool ConclusionEnabled { get; set; }
    }
}