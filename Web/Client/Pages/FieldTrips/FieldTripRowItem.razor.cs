// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Entities;
using Microsoft.IdentityModel.Tokens;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Utilities;
using System;
using System.Linq;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.FieldTrips
{
    public partial class FieldTripRowItem : ComponentBase
    {
        [Parameter]
        public FieldTrip Trip { get; set; }

        [Parameter]
        public bool IsAdmin { get; set; }

        [Parameter]
        public UserModel CurrentUser { get; set; }

        [Parameter]
        public EventCallback OnDeleted { get; set; }

        [Parameter]
        public EventCallback<FieldTrip> OnUpdate { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public IJSRuntime JsRuntime { get; set; }

        [Inject]
        public TimeZoneInfo TimeZone { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [CascadingParameter]
        public IConfirmProvider Confirm { get; set; }

        public bool IsExpanded { get; set; } = true;

        public bool IsDeleting { get; set; }

        public bool ShowReportModal { get; set; }

        public bool IsUncomplete => string.IsNullOrEmpty(this.Trip?.HomePageTitle) || string.IsNullOrEmpty(this.Trip?.HomePageText)
            || string.IsNullOrEmpty(this.Trip?.EndPageTitle) || string.IsNullOrEmpty(this.Trip?.EndPageText)
            || (!this.Trip?.LocatedGameUnits?.Any() ?? true);

        public string PlayTestUrl => $"{NavigationManager.BaseUri}jouer/{Base64UrlEncoder.Encode(this.Trip.Id.ToByteArray())}";

        public async Task PlayInNewTab()
		{
            await this.JsRuntime.InvokeVoidAsync("open", PlayTestUrl, "_blank");
		}

        protected async Task DeleteAsync()
        {
            if (this.IsDeleting || !await Confirm.AskAsync($"Etes-vous s�r de vouloir supprimer la sortie {this.Trip.Name} ? Cette action est irreversible"))
            {
                return;
            }

            this.IsDeleting = true;
            this.StateHasChanged();

            try
            {
                await this.FieldTripsRepository.DeleteFieldTripAsync(this.Trip.Id);
                this.Notifier.Notify(Color.Success, $"La sortie {this.Trip.Name} a bien �t� supprim�e");
                this.IsDeleting = false;
                this.StateHasChanged();
                await this.OnDeleted.InvokeAsync();

            }
            catch (BusinessException ex)
            {
                this.Notifier.Notify(Color.Danger, ex.Message);
                this.IsDeleting = false;
                this.StateHasChanged();
            }
        }

        public bool IsUpdatingVisibility { get; set; }

        protected async Task UpdateVisibilityAsync()
        {
            if (this.IsUpdatingVisibility)
            {
                return;
            }

            this.IsUpdatingVisibility = true;
            this.StateHasChanged();

            try
            {
                var result = await this.FieldTripsRepository.UpdateVisibilityAsync(this.Trip.Id);
                await this.OnUpdate.InvokeAsync(result);
            }
            catch (BusinessException ex)
            {
                this.Notifier.Notify(Color.Danger, ex.Message);
            }


            this.IsUpdatingVisibility = false;
            this.StateHasChanged();
        }

        public bool IsUpdatingPublishStatus { get; set; }

        protected async Task UpdatePublishStatusAsync()
        {
            if (this.IsUpdatingPublishStatus)
            {
                return;
            }

            this.IsUpdatingPublishStatus = true;
            this.StateHasChanged();

            try
            {
                var result = await this.FieldTripsRepository.UpdatePublishStatusAsync(this.Trip.Id);
                await this.OnUpdate.InvokeAsync(result);
            }
            catch (BusinessException ex)
            {
                this.Notifier.Notify(Color.Danger, ex.Message);
            }


            this.IsUpdatingPublishStatus = false;
            this.StateHasChanged();
        }
    }
}