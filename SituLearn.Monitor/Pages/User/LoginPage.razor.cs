﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Ozytis.Common.Core.Web.Razor.Utilities;
using SituLearn.Monitor.Repositories;
using System.Threading.Tasks;

namespace SituLearn.Monitor.Pages.User
{
    [AllowAnonymous]
    [Route(LoginPage.Url)]
    public partial class LoginPage : ComponentBase
    {
        public const string Url = "/login";

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        public IConfiguration Configuration { get; set; }

        [Parameter]
        [SupplyParameterFromQuery(Name = "email")]
        public string Email { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.ServerUrl = this.Configuration["ServerUrl"];

            await base.OnInitializedAsync();
        }

        public void NavigateTo(string to)
        {
            this.NavigationManager.NavigateTo(to);
        }

        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public string[] Errors { get; set; }

        public bool IsSubmitting { get; set; }

        public string ServerUrl { get; set; }

        public async Task SubmitLoginAsync()
        {
            if (this.IsSubmitting)
            {
                return;
            }

            this.Errors = null;
            this.IsSubmitting = true;

            this.StateHasChanged();

            try
            {
                await this.UsersRepository.LoginAsync(this.Email, this.Password, this.RememberMe);
                this.NavigateTo(SituLearn.Monitor.Pages.FieldTrips.FieldTripsPage.Url);
            }
            catch (Ozytis.Common.Core.Utilities.BusinessException ex)
            {
                this.Errors = ex.Messages ?? new[] { ex.Message };
            }

            this.IsSubmitting = false;

            this.StateHasChanged();
        }
    }
}
