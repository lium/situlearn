﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using ClientApi;
using Common;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Web.Client.Repositories
{
    public class FieldTripsRepository
    {
        private readonly FieldTripsService fieldTripsService = new();

        public async Task<IEnumerable<FieldTrip>> GetAllFieldTripsAsync(Expression<Func<IQueryable<FieldTrip>, IQueryable<FieldTrip>>> query)
        {
            return await this.fieldTripsService.GetAllFieldTripsAsync(query);
        }

        public async Task<FieldTrip> CreateFieldTripAsync(FieldTripCreationModel model)
        {
            var result = await this.fieldTripsService.CreateFieldTripAsync(model);

            result.CheckIfSuccess();

            return result.Data;
        }

        public async Task<IEnumerable<string>> SelectAllEstablishmentsAsync()
        {
            return await this.fieldTripsService.SelectAllEstablishmentsAsync();
        }

        public async Task<IEnumerable<ApplicationUser>> SelectAllDesignersAsync()
        {
            return await this.fieldTripsService.SelectAllDesignersAsync();
        }

        public async Task<IEnumerable<string>> SelectAllPedagogicFieldsAsync()
        {
            return await this.fieldTripsService.SelectAllPedagogicFieldsAsync();
        }

        public async Task<IEnumerable<FieldTripType>> SelectAllTypesAsync()
        {
            return await this.fieldTripsService.SelectAllTypesAsync();
        }

        public async Task<int> GetPublishedFieldTripsCountAsync()
        {
            return await this.fieldTripsService.GetPublishedFieldTripsCountAsync(true);
        }

        public async Task RemoveUnitFromJourneyAsync(Guid fieldTripId, Guid journeyId, Guid journeyUnitLinkId)
        {
            var result = await this.fieldTripsService.RemoveUnitFromJourneyAsync(fieldTripId, journeyId, journeyUnitLinkId);
            result.CheckIfSuccess();
        }

        public async Task<Journey> AddNewJourneyAsync(Guid fieldTripId)
        {
            var result = await this.fieldTripsService.AddNewJourneyAsync(fieldTripId);
            result.CheckIfSuccess();

            return result.Data;
        }

        public async Task<FieldTrip> GetFieldTripAsync(Guid fieldTripId, bool expandUrl = true, bool minimizeForPlayer = false)
        {
            return await this.fieldTripsService.GetFieldTripAsync(fieldTripId, expandUrl, minimizeForPlayer);
        }

        public async Task DeleteFieldTripAsync(Guid fieldTripId)
        {
            var result = await this.fieldTripsService.DeleteFieldTripAsync(fieldTripId);

            result.CheckIfSuccess();
        }

        public async Task<Journey> UpdateJourneyInfoAsync(Guid fieldTripId, JourneyInfoUpdateModel model)
        {
            var result = await this.fieldTripsService.UpdateJourneyInfoAsync(fieldTripId, model);

            result.CheckIfSuccess();
            return result.Data;
        }

        public async Task DeleteLocatedUnitAsync(Guid fieldTripId, Guid locatedUnitId)
        {
            var result = await this.fieldTripsService.DeleteLocatedUnitAsync(fieldTripId, locatedUnitId);

            result.CheckIfSuccess();
        }

        public async Task<FieldTrip> UpdateVisibilityAsync(Guid fieldTripId)
        {
            var result = await this.fieldTripsService.UpdateVisibilityAsync(fieldTripId);

            result.CheckIfSuccess();

            return result.Data;
        }

        public async Task<LocatedGameUnit> UpdateLocatedUnitInfoAsync(LocatedUnitUpdateInfoModel model)
        {
            var result = await this.fieldTripsService.UpdateLocatedUnitInfoAsync(model.FieldTripId, model.Id, model);

            result.CheckIfSuccess();

            return result.Data;
        }

        public async Task DeleteJourneyAsync(Guid fieldTripId, Guid journeyId)
        {
            var result = await this.fieldTripsService.DeleteJourneyAsync(fieldTripId, journeyId);

            result.CheckIfSuccess();
        }

        public async Task<FieldTrip> UpdatePublishStatusAsync(Guid fieldTripId)
        {
            var result = await this.fieldTripsService.UpdatePublishStatusAsync(fieldTripId);

            result.CheckIfSuccess();

            return result.Data;
        }

        public async Task<LocatedGameUnit> AddLocatedUnitAsync(LocatedUnitCreationModel locatedUnitCreationModel)
        {
            var result = await this.fieldTripsService.AddLocatedUnitAsync(locatedUnitCreationModel.FieldTripId, locatedUnitCreationModel);

            result.CheckIfSuccess();

            return result.Data;
        }

        public async Task UpdateMapPositionAsync(UpdateMapPositionModel updateMapPositionModel)
        {
            var result = await this.fieldTripsService.UpdateMapPositionAsync(updateMapPositionModel.FieldTripId, updateMapPositionModel);
            result.CheckIfSuccess();
        }

        public async Task AddLocatedUnitToJourneyAsync(Guid fieldTripId, Guid journeyId, Guid locatedGameUnitId, int? order = null)
        {
            var result = await this.fieldTripsService.AddLocatedUnitToJourneyAsync(fieldTripId, journeyId, new LocatedGameUnitJourneyCreationModel
            {
                JourneyId = journeyId,
                LocatedGameUnitId = locatedGameUnitId,
                Order = order
            });

            result.CheckIfSuccess();
        }

        public async Task UpdateFieldTripAsync(FieldTripUpdateModel updateModel)
        {
            var result = await this.fieldTripsService.UpdateFieldTripAsync(updateModel);
            result.CheckIfSuccess();
        }

        public async Task UpdateGameUnitOrderAsync(Guid fieldTripId, Guid journeyId, Guid linkId, int newOrder)
        {
            var result = await this.fieldTripsService.UpdateGameUnitOrderAsync(fieldTripId, journeyId, new DraggableObjectOrderUpdateModel
            {
                LinkId = linkId,
                NewOrder = newOrder
            });
            result.CheckIfSuccess();
        }

        public async Task<Dictionary<Guid, string>> GetFieldTripIdsAndNamesAsync()
        {
            return await this.fieldTripsService.GetFieldTripIdsAndNamesAsync();
        }

  //      public async Task RemoveStatements(Guid fieldtripId, long from, long to)
		//{
  //          var result = await this.fieldTripsService.RemoveStatementsAsync(fieldtripId, from, to);

  //          result.CheckIfSuccess();
  //      }

        public async Task<Guid> DuplicateFieldTrip(Guid fieldTripId)
		{
            return await this.fieldTripsService.DuplicateFieldTripAsync(fieldTripId);
		}

        public async Task<FieldTripTestingModel> TestFieldTrip(Guid fieldTripId)
		{
            return await this.fieldTripsService.TestFieldTripAsync(fieldTripId);
		}

        public async Task<IEnumerable<Player>> SelectAllPlayersByFieldTripIdAsync(Guid fieldTripId)
        {
            return await this.fieldTripsService.SelectAllPlayersByFieldTripIdAsync(fieldTripId);
        }
    }
}
