﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Ozytis.Common.Core.Web.Razor.Utilities;
using SituLearn.Monitor.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SituLearn.Monitor.Pages.Supervision
{
    public partial class SupervisionList : ComponentBase
    {
        [Parameter]
        public List<PlayerForMonitorModel> Players { get; set; }

        [Parameter]
        public Action<Guid> OnPlayerHiddenChange { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        public IConfiguration Configuration { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        public string BasePictureUrl { get; set; }

        public string Filter { get; set; }

        public bool DisplayHiddenPlayers { get; set; }

        public IEnumerable<PlayerForMonitorModel> PlayersToDisplay { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.BasePictureUrl = this.Configuration["ServerUrl"] + "files?path=";

            this.PlayersToDisplay = this.Players.Where(p => p.IsHidden == this.DisplayHiddenPlayers);

            await base.OnInitializedAsync();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender && this.Players != null && this.Players.Any(p => string.IsNullOrEmpty(p.Color)))
            {
                this.Players = (await this.GamesRepository.AssignColorsToPlayers(this.Players)).ToList();

                this.PlayersToDisplay = this.Players.Where(p => p.IsHidden == this.DisplayHiddenPlayers);

                this.StateHasChanged();
            }

            await base.OnAfterRenderAsync(firstRender);
        }

        public override async Task SetParametersAsync(ParameterView parameters)
        {
            bool needsRefresh = parameters.GetValueOrDefault<List<PlayerForMonitorModel>>(nameof(Players)).Count != this.Players?.Count;

            await base.SetParametersAsync(parameters);

            if (needsRefresh)
            {
                this.PlayersToDisplay = this.Players.Where(p => p.IsHidden == this.DisplayHiddenPlayers);
            }
        }

        public void OnChangeDisplayHiddenPlayers()
        {
            this.DisplayHiddenPlayers = !this.DisplayHiddenPlayers;

            if (!this.DisplayHiddenPlayers)
            {
                this.PlayersToDisplay = this.Players
                    .Where(p => !p.IsHidden)
                    .OrderBy(p => p.Name);
            }
            else
            {
                this.PlayersToDisplay = this.Players;
            }

            if (!string.IsNullOrEmpty(this.Filter))
            {
                this.PlayersToDisplay = this.PlayersToDisplay
                    .Where(p => p.Name.Contains(this.Filter, StringComparison.InvariantCultureIgnoreCase))
                    .OrderBy(p => p.Name);
            }
        }

        public void ChangePlayerHidden(Guid id)
        {
            this.OnPlayerHiddenChange.Invoke(id);
        }

        public void SwitchDescriptionDisplay(Guid id)
        {
            var playerToSwitchDescription = this.Players.FirstOrDefault(p => p.Id == id);
            playerToSwitchDescription.ShowDescription = !playerToSwitchDescription.ShowDescription;

            this.StateHasChanged();
        }
    }
}
