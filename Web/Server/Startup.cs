// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Business;
using Common;
using Common.Security;
using DataAccess;
using Entities;
using Hangfire;
using HereApi;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using NetTopologySuite;
using Ozytis.Common.Core.Emails.Core;
using Ozytis.Common.Core.Emails.NetCore;
using Ozytis.Common.Core.GeoJson;
using Ozytis.Common.Core.Identity;
using Ozytis.Common.Core.Logs.NetCore;
using Ozytis.Common.Core.RazorTemplating;
using Ozytis.Common.Core.Storage;
using Ozytis.Common.Core.Web.WebApi;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Web.Server.Hubs;

namespace Web.Server
{
    public class Startup
    {
        public const string ConnectionStringName = "DefaultConnection";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(options =>
            {
                options
                   .UseSqlServer(
                       Configuration.GetConnectionString(Startup.ConnectionStringName),
                       o =>
                       {
                           o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery);
                       })
                    .EnableSensitiveDataLogging(true);
            }, ServiceLifetime.Scoped);

            services.AddControllersWithViews().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Serialize;
                options.SerializerSettings.MaxDepth = 30;
                options.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
                options.SerializerSettings.TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All;

                foreach (var converter in GeoJsonUtils.GetSerializer().Converters)
                {
                    options.SerializerSettings.Converters.Add(converter);
                }
            });

            this.ConfigureSecurity(services);

            services.AddSingleton<IConfiguration>((sp) => Configuration);

            services.AddMvc(config =>
            {
                config.Filters.Add(new HandleBusinessExceptionAttribute());
            })
            .AddMvcOptions(options =>
            {
                options.MaxValidationDepth = 999;
            })
            .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull;
            })
            .AddOzytisMails(new MailConfiguration
            {
                DefaultSenderEmail = this.Configuration["Data:Emails:DefaultSenderEmail"],
                DefaultSenderName = this.Configuration["Data:Emails:DefaultSenderName"],
                HostName = this.Configuration["Data:Emails:SmtpHost"],
                Password = this.Configuration["Data:Emails:SmtpPassword"],
                UserName = this.Configuration["Data:Emails:SmtpUser"],
                Port = int.Parse(this.Configuration["Data:Emails:SmtpPort"]),
                DataBaseConnectionString = this.Configuration.GetConnectionString(Startup.ConnectionStringName),
                UseSsl = false,
                MailDashBoardAllowedRole = UserRoleNames.Administrator,
                UseApi = this.Configuration.GetValue<bool>("Data:Emails:UseApi"),
                ApiPassword = this.Configuration["Data:Emails:ApiPassword"]
            }).AddOzytisLogsDashBoard(new OzLogDashboardConfiguration
            {
                UseSsl = true,
                DashBoardAllowedRole = UserRoleNames.Administrator,
                DashBoardPath = "/logs",
                AuthenticationPath = "/login",
                LocalStorageAuthenticationTokenName = "token",
                CssPath = "/css/all.min.css"
            });

            services.AddSingleton<IFileSystem, LocalFileSystemV2>((sp) =>
            {
                var env = sp.GetService<IWebHostEnvironment>();
                return new LocalFileSystemV2(env.ContentRootPath, "files") { Name = "local", AllowWebDownload = true };
            });

            services.AddRazorPages();

            foreach (var manager in typeof(Business.ForbiddenTokenManager).Assembly.GetTypes().Where(t => t.Name.EndsWith("Manager")))
            {
                services.AddScoped(manager);
            }


            services.Configure<MvcOptions>(options =>
            {
                if (bool.Parse(Configuration["Network:RequireHttps"] ?? "false"))
                {
                    options.Filters.Add(new RequireHttpsAttribute());
                }
            });

            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" });
            });

            services.AddSingleton(NtsGeometryServices.Instance);

            services
              .AddHangfire((config) =>
              {
                  config.UseSqlServerStorage(this.Configuration.GetConnectionString(Startup.ConnectionStringName));
              })
              .AddHangfireServer((serviceProvider, options) =>
              {
                  // TODO configurer si n�cessaire
              });

            services.AddSingleton<GeocodingService>();

            DynamicRazorEngine dynamicRazorEngine = new DynamicRazorEngine();
            dynamicRazorEngine.AddMetadataReference(typeof(IdentityUser).Assembly);
            dynamicRazorEngine.AddMetadataReference(typeof(Entities.ApplicationUser).Assembly);

            Assembly businessAssembly = typeof(Business.UsersManager).Assembly;
            dynamicRazorEngine.LoadAssembly(businessAssembly);
            services.AddSingleton<DynamicRazorEngine>(dynamicRazorEngine);

            services.AddScoped<Setup>();

            services.AddSignalR();
            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                   new[] { "application/octet-stream" });
            });
        }

        public void ConfigureSecurity(IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireDigit = false;
            })
               .AddRoles<IdentityRole>()
               .AddDefaultTokenProviders()
               .AddErrorDescriber<FrenchIdentityErrorDescriber>()
               .AddEntityFrameworkStores<DataContext>();

            this.ConfigureAuthentication(services);

            var policies = Policy.GetRolePolicies();

            services.AddAuthorization(options =>
            {
                foreach (var customPolicy in policies)
                {
                    options.AddPolicy(customPolicy.Key, policy =>
                        policy.RequireRole(customPolicy.Value)
                    );
                }
            });
        }

        public void ConfigureAuthentication(IServiceCollection services)
        {
            byte[] key = Encoding.ASCII.GetBytes(this.Configuration["Authentication:AuthenticationTokenSecretSigningKey"]);

            TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            services.AddSingleton<TokenValidationParameters>(tokenValidationParameters);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = tokenValidationParameters;

                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = ctx =>
                    {
                        Console.WriteLine("Authentication failed");

                        if (ctx.Request.Path.StartsWithSegments("/api", StringComparison.InvariantCultureIgnoreCase))
                        {
                            ctx.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                        else
                        {
                            ctx.Response.Redirect("/login");
                        }

                        return Task.CompletedTask;
                    },

                    OnForbidden = ctx =>
                    {
                        if (ctx.Request.Path.StartsWithSegments("/api", StringComparison.InvariantCultureIgnoreCase))
                        {
                            ctx.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                        else
                        {
                            ctx.Response.Redirect("/login");
                        }

                        return Task.FromResult(0);
                    },

                    OnTokenValidated = async context =>
                    {
                        string token = (context.SecurityToken as JwtSecurityToken).RawData;

                        if (await context.HttpContext.RequestServices.GetService<ForbiddenTokenManager>().IsForbiddenAsync(token))
                        {
                            context.Fail("D�connect� depuis le site");
                        }
                    },
                    OnMessageReceived = async context =>
                    {
                        if (!context.Request.Query.ContainsKey("access_token"))
                        {
                            return;
                        }

                        var token = context.Request.Query["access_token"];

                        if (string.IsNullOrWhiteSpace(token))
                        {
                            context.Fail("Token vide");
                            return;
                        }

                        context.Token = token;
                        await Task.CompletedTask;
                    }
                };
            })
            .AddCookie();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, Setup setup, IServiceProvider serviceProvider)
        {
            app.UseResponseCompression();
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                ServeUnknownFileTypes = true
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseHangfireDashboard(options: new DashboardOptions()
            {
                Authorization = new[] {
                    new BasicAuthAuthorizationFilter {
                        User = Configuration["Hangfire:User"],
                        Password = Configuration["Hangfire:Password"]
                    }
                }
            });

            app.UseOzytisLogs();
            app.UseOzytisMails();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapHub<MonitorHub>("/monitorhub");
                endpoints.MapFallbackToPage("/_Host");
            });

            setup.Init();

            RecurringJob.AddOrUpdate(() => serviceProvider.GetService<GamesManager>().DeleteLastWeekReadingsToShareAsync(), Cron.Daily(2));
            RecurringJob.AddOrUpdate(() => serviceProvider.GetService<ActivitiesManager>().RemoveReadingFilesOlderThanThreeMonthsAsync(), Cron.Daily(3));
        }
    }
}
