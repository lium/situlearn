﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class JourneyLocatedGameUnitOrderMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JourneyLocatedGameUnit_Journeys_JourneyId",
                table: "JourneyLocatedGameUnit");

            migrationBuilder.DropForeignKey(
                name: "FK_JourneyLocatedGameUnit_LocatedGameUnits_LocatedGameUnitId",
                table: "JourneyLocatedGameUnit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JourneyLocatedGameUnit",
                table: "JourneyLocatedGameUnit");

            migrationBuilder.RenameTable(
                name: "JourneyLocatedGameUnit",
                newName: "JourneyLocatedGameUnits");

            migrationBuilder.RenameIndex(
                name: "IX_JourneyLocatedGameUnit_LocatedGameUnitId",
                table: "JourneyLocatedGameUnits",
                newName: "IX_JourneyLocatedGameUnits_LocatedGameUnitId");

            migrationBuilder.RenameIndex(
                name: "IX_JourneyLocatedGameUnit_JourneyId",
                table: "JourneyLocatedGameUnits",
                newName: "IX_JourneyLocatedGameUnits_JourneyId");

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "JourneyLocatedGameUnits",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_JourneyLocatedGameUnits",
                table: "JourneyLocatedGameUnits",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_JourneyLocatedGameUnits_Journeys_JourneyId",
                table: "JourneyLocatedGameUnits",
                column: "JourneyId",
                principalTable: "Journeys",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_JourneyLocatedGameUnits_LocatedGameUnits_LocatedGameUnitId",
                table: "JourneyLocatedGameUnits",
                column: "LocatedGameUnitId",
                principalTable: "LocatedGameUnits",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JourneyLocatedGameUnits_Journeys_JourneyId",
                table: "JourneyLocatedGameUnits");

            migrationBuilder.DropForeignKey(
                name: "FK_JourneyLocatedGameUnits_LocatedGameUnits_LocatedGameUnitId",
                table: "JourneyLocatedGameUnits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JourneyLocatedGameUnits",
                table: "JourneyLocatedGameUnits");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "JourneyLocatedGameUnits");

            migrationBuilder.RenameTable(
                name: "JourneyLocatedGameUnits",
                newName: "JourneyLocatedGameUnit");

            migrationBuilder.RenameIndex(
                name: "IX_JourneyLocatedGameUnits_LocatedGameUnitId",
                table: "JourneyLocatedGameUnit",
                newName: "IX_JourneyLocatedGameUnit_LocatedGameUnitId");

            migrationBuilder.RenameIndex(
                name: "IX_JourneyLocatedGameUnits_JourneyId",
                table: "JourneyLocatedGameUnit",
                newName: "IX_JourneyLocatedGameUnit_JourneyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JourneyLocatedGameUnit",
                table: "JourneyLocatedGameUnit",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_JourneyLocatedGameUnit_Journeys_JourneyId",
                table: "JourneyLocatedGameUnit",
                column: "JourneyId",
                principalTable: "Journeys",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_JourneyLocatedGameUnit_LocatedGameUnits_LocatedGameUnitId",
                table: "JourneyLocatedGameUnit",
                column: "LocatedGameUnitId",
                principalTable: "LocatedGameUnits",
                principalColumn: "Id");
        }
    }
}
