﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Business.Templates;
using Common;
using DataAccess;
using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MimeKit;
using Ozytis.Common.Core.Emails.Core;
using Ozytis.Common.Core.Managers;
using Ozytis.Common.Core.RazorTemplating;
using Ozytis.Common.Core.Storage;
using Ozytis.Common.Core.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Business
{
    public class GamesManager : BaseEntityManager<Game>
	{
		public GamesManager(DataContext context, IFileSystem fileSystem, IConfiguration configuration, IServiceProvider serviceProvider) : base(context)
		{
			this.FileSystem = fileSystem;
			this.Configuration = configuration;
            this.ServiceProvider = serviceProvider;
        }

		public IFileSystem FileSystem { get; }

		public IConfiguration Configuration { get; }
        public IServiceProvider ServiceProvider { get; private set; }

        public IQueryable<Game> GetAllPlayerGames(string playerId)
		{
			return this.SelectAll().Where(g => g.Players.Any(p => p.UserId == playerId));
		}

		public async Task UpdateGamesAsync(Game[] games, ApplicationUser player)
		{
			foreach (var game in games)
			{
				await this.UpdateGameAsync(game, player);
			}
		}

		public async Task UpdateGameAsync(Game game, ApplicationUser user)
		{
			var remotePlayer = game.Players?.FirstOrDefault(p => p.UserId == user.Id);

			if (remotePlayer == null)
			{
				// l'utilisateur n'a pas à mettre à jour cette partie
				return;
			}

			var fieldTrip = this.DataContext.FieldTrips.FirstOrDefault(f => f.Id == game.FielTripId);
			if (fieldTrip == null)
			{
				// removed
				return;
			}

			var existingGame = await this.SelectAsync(game.Id);

			using TransactionScope transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

			if (existingGame == null)
			{
				// la partie n'existe pas encore, on la crée
				existingGame = this.DataContext.Games.Add(new()
				{
					Id = game.Id,
					EndDate = game.EndDate,
					FielTripId = game.FielTripId,
					FieldTripName = game.FieldTripName,
					StartDate = game.StartDate,
					LastUpdateDate = DateTime.UtcNow,
				}).Entity;
			}
			else
			{
				existingGame.StartDate = game.StartDate;
				existingGame.EndDate = game.EndDate;
                existingGame.FieldTripName = game.FieldTripName;
            }

			await this.SaveChangesAsync();

			// on regarde si on a déjà une partie liée à cejoueur
			Player player = await this.SelectAllPlayers()
				.Include(p => p.PlayedUnits)
					.ThenInclude(p => p.PlayedActivities)
					.ThenInclude(pa => pa.Files)
				.FirstOrDefaultAsync(p => p.GameId == existingGame.Id && p.UserId == user.Id);

            bool createdPlayerIsTemporary = player != null && player.StartPlayedDate == default;

			//si le player avait été créé temporairement pour permettre la création d'une playedUnit et/ou d'une playedActivity après le rétablissement de la connexion internet
			//avant que le Player ait été créé, on le met alors à jour
			if (createdPlayerIsTemporary)
			{
				player.CurrentUnitActivationHasBeenUnlocked = remotePlayer.CurrentUnitActivationHasBeenUnlocked;
				player.CurrentUnitId = remotePlayer.CurrentUnitId;
				player.CurrentUnitReachedDate = remotePlayer.CurrentUnitReachedDate;
                player.EndGameDate = remotePlayer.EndGameDate;
                player.GameId = game.Id;
                player.Id = remotePlayer.Id;
                player.JourneyId = remotePlayer.JourneyId;
                player.PlayerState = remotePlayer.PlayerState;
                player.StartCurrentUnitDate = remotePlayer.StartCurrentUnitDate;
                player.StartPlayedDate = remotePlayer.StartPlayedDate;
                player.UserId = user.Id;
                player.CurrentLatitude = remotePlayer.CurrentLatitude;
                player.CurrentLongitude = remotePlayer.CurrentLongitude;
                player.TeamName = remotePlayer.TeamName;

                if (!string.IsNullOrEmpty(remotePlayer.TeamPicture) && remotePlayer.TeamPicture.StartsWith("data:"))
                {
                    player.TeamPicture = await this.FileSystem.SetFile(remotePlayer.TeamPicture, $"games/{player.Id}-team");
                }

                existingGame.LastUpdateDate = DateTime.UtcNow;

                await this.SaveChangesAsync();
            }
			else
			{
                bool noChange = player != null && player.StartPlayedDate == remotePlayer.StartPlayedDate && player.EndGameDate == remotePlayer.EndGameDate
                && player.PlayedUnits.Count == remotePlayer.PlayedUnits?.Count
                && player.PlayedUnits.SelectMany(pu => pu.PlayedActivities ?? new List<PlayedActivity>()).Count() == remotePlayer.PlayedUnits?.SelectMany(pu => pu.PlayedActivities ?? new List<PlayedActivity>()).Count();

                // si aucune partie n'existe, on la crée ou si une partie existe déjà et qu'elle a changé, on supprime les infos existantes puis on la crée
                if (!noChange)
                {
                    if (player != null)
                    {
                        await this.DeletePlayerAsync(player, false);
                    }

                    var dbPlayer = new Player
                    {
                        CurrentUnitActivationHasBeenUnlocked = remotePlayer.CurrentUnitActivationHasBeenUnlocked,
                        CurrentUnitId = remotePlayer.CurrentUnitId,
                        CurrentUnitReachedDate = remotePlayer.CurrentUnitReachedDate,
                        EndGameDate = remotePlayer.EndGameDate,
                        GameId = game.Id,
                        Id = remotePlayer.Id,
                        JourneyId = remotePlayer.JourneyId,
                        PlayerState = remotePlayer.PlayerState,
                        StartCurrentUnitDate = remotePlayer.StartCurrentUnitDate,
                        StartPlayedDate = remotePlayer.StartPlayedDate,
                        UserId = user.Id,
                        PlayedUnits = new List<PlayedUnit>(),
                        CurrentLatitude = remotePlayer.CurrentLatitude,
                        CurrentLongitude = remotePlayer.CurrentLongitude,
                        TeamName = remotePlayer.TeamName
                    };

                    if (!string.IsNullOrEmpty(remotePlayer.TeamPicture) && remotePlayer.TeamPicture.StartsWith("data:"))
                    {
                        dbPlayer.TeamPicture = await this.FileSystem.SetFile(remotePlayer.TeamPicture, $"games/{dbPlayer.Id}-team");
                    }

                    existingGame.LastUpdateDate = DateTime.UtcNow;

                    if (remotePlayer.PlayedUnits != null)
                    {
                        foreach (PlayedUnit playedUnit in remotePlayer.PlayedUnits)
                        {
                            PlayedUnit dbPlayedUnit = new PlayedUnit()
                            {
                                Id = playedUnit.Id,
                                LocalisationReachDate = playedUnit.LocalisationReachDate,
                                PlayedActivities = new List<PlayedActivity>(),
                                PlayerId = dbPlayer.Id,
                                UnitId = playedUnit.UnitId,
                                HaveLocatedUnit = playedUnit.HaveLocatedUnit,
								LocationPoints = playedUnit.LocationPoints
                            };

                            if (playedUnit.PlayedActivities != null)
                            {
                                foreach (PlayedActivity playedActivity in playedUnit.PlayedActivities)
                                {
                                    PlayedActivity dbPlayedActivity = new()
                                    {
                                        ActivityId = playedActivity.ActivityId,
										ActivityName = playedActivity.ActivityName,
                                        Answer = playedActivity.Answer,
                                        AnswerDate = playedActivity.AnswerDate,
                                        Files = new List<PlayedActivityFile>(),
                                        Id = playedActivity.Id,
                                        PlayedUnitId = playedActivity.PlayedUnitId,
                                        Points = playedActivity.Points,
                                        Latitude = playedActivity.Latitude,
                                        Longitude = playedActivity.Longitude
                                    };


                                    if (playedActivity.Files != null)
                                    {
                                        foreach (var file in playedActivity.Files)
                                        {
                                            PlayedActivityFile dbPlayedActivityFile = new()
                                            {
                                                Id = file.Id,
                                                MimeType = file.MimeType,
                                                PlayedActivityId = playedActivity.Id,
                                                Url = file.Url,
                                            };

                                            if (dbPlayedActivityFile.Url != null && dbPlayedActivityFile.Url.StartsWith("data:"))
                                            {
                                                dbPlayedActivityFile.Url = await this.FileSystem.SetFile(dbPlayedActivityFile.Url, $"games/{file.Id}-file");
                                            }

                                            dbPlayedActivity.Files.Add(dbPlayedActivityFile);
                                        }
                                    }

                                    dbPlayedUnit.PlayedActivities.Add(dbPlayedActivity);
                                }
                            }

                            dbPlayer.PlayedUnits.Add(dbPlayedUnit);
                        }
                    }

                    this.DataContext.Players.Add(dbPlayer);

                    await this.SaveChangesAsync();
                }
            }            

			transactionScope.Complete();
		}

		public IQueryable<Player> SelectAllPlayers(params Expression<Func<Player, object>>[] includes)
		{
			return this.DataContext.Players.LoadIncludes(includes);
		}

		public async Task DeletePlayerAsync(Guid playerId)
		{
			var player = await this.SelectAllPlayers()
				.Include(p => p.PlayedUnits)
					.ThenInclude(pu => pu.PlayedActivities)
						.ThenInclude(pa => pa.Files)
				.FirstOrDefaultAsync(p => p.Id == playerId);

			if (player == null)
			{
				return;
			}

			await this.DeletePlayerAsync(player);
		}

		private async Task DeletePlayerAsync(Player player, bool deleteGameAfter = true)
        {
            //si le player existant avait une image de groupe, on la supprime
            if (!string.IsNullOrEmpty(player.TeamPicture))
            {
                await this.FileSystem.DeleteAsync(player.TeamPicture);
            }

            foreach (PlayedUnit playedUnit in player.PlayedUnits)
			{
				foreach (var activities in playedUnit.PlayedActivities)
				{
					foreach (var file in activities.Files)
					{
                        if (!string.IsNullOrEmpty(file.Url) && await this.FileSystem.ExistsAsync(file.Url))
						{
							await this.FileSystem.DeleteAsync(file.Url);
						}

						this.DataContext.PlayedActivityFiles.Remove(file);
					}

					this.DataContext.PlayedActivities.Remove(activities);
				}

				this.DataContext.PlayedUnits.Remove(playedUnit);
			}

			this.DataContext.Players.Remove(player);

            if (deleteGameAfter)
            {
				//suppression du game après avoir supprimé le player
				var gameToDelete = await this.SelectAll()
					.FirstOrDefaultAsync(g => g.Id == player.GameId);

				this.DataContext.Games.Remove(gameToDelete);
			}            

            await this.SaveChangesAsync();
		}

		public async Task UpdatePlayerPositionAsync(Guid playerId, decimal latitude, decimal longitude, decimal? batteryLevel, DateTime lastUpdatedPositionDate)
		{
			Player player = await this.SelectAllPlayers().FirstOrDefaultAsync(p => p.Id == playerId);

			if (player == null)
			{
				return;
			}

			player.CurrentLongitude = longitude;
			player.CurrentLatitude = latitude;
			player.BatteryLevel = batteryLevel ?? player.BatteryLevel;
			player.LastUpdatedPositionDate = lastUpdatedPositionDate;

			await this.SaveChangesAsync();
		}

		public async Task DeleteFieldTripGamesAsync(Guid fieldTripId)
		{
			var games = await this.SelectAll()
				.Include(g => g.Players)
					.ThenInclude(p => p.PlayedUnits)
						.ThenInclude(pu => pu.PlayedActivities)
							.ThenInclude(pa => pa.Files)
				.Where(g => g.FielTripId == fieldTripId)
				.ToListAsync();

			foreach (var game in games)
			{
				foreach (Player player in game.Players)
				{
					await this.DeletePlayerAsync(player);
				}
			}

			this.DataContext.Games.RemoveRange(games);

			await this.SaveChangesAsync();
		}

		public async Task ShareReadingAsync(Guid readingId, ApplicationUser player)
		{
			var reading = await this.SelectAll()
				.Include(g => g.Players)
					.ThenInclude(p => p.PlayedUnits)
						.ThenInclude(pu => pu.PlayedActivities)
							.ThenInclude(pa => pa.Files)
				.Include(g => g.Players)
					.ThenInclude(p => p.PlayedUnits)
						.ThenInclude(pu => pu.PlayedActivities)
							.ThenInclude(pa => pa.Activity)
				.SelectMany(g => g.Players)
				.Where(p => p.UserId == player.Id)
				.SelectMany(p => p.PlayedUnits)
				.SelectMany(pu => pu.PlayedActivities)
				.FirstOrDefaultAsync(pa => pa.Id == readingId);

			if (reading == null)
			{
				throw new BusinessException("Le relevé recherché n'a pas été trouvé");
			}

			MimeMessage email = new MimeMessage();

			email.From.Add(new MailboxAddress(this.Configuration["Data:Emails:DefaultSenderName"], this.Configuration["Data:Emails:DefaultSenderEmail"]));

			email.To.Add(new MailboxAddress($"{player.FirstName} {player.LastName}", player.Email));

			email.Subject = $"SITULEARN - Relevé {reading.Activity.Title} du {reading.AnswerDate.ToLocalTime().ToString("dd/MM/yyyy à HH:mm")}";

			string notesMessage = "";

			if (!string.IsNullOrEmpty(reading.Answer))
			{
				notesMessage = @$"Votre prise de notes est la suivante :
                    
                        '{reading.Answer}'
                    
                    ";
			}

			string attachmentsMessage = "";

			if (reading.Files.Any())
			{
				attachmentsMessage = (@$"Veuillez trouver ci-joints les enregistrements que vous avez effectués.
                    
                    ");
			}

			string message = string.Concat(notesMessage, attachmentsMessage);

			var builder = new BodyBuilder();

			builder.TextBody = @$"Bonjour {player.FirstName} {player.LastName},

                Vous avez demandé à partager votre relevé {reading.Activity.Title} du {reading.AnswerDate.ToLocalTime().ToString("dd/MM/yyyy à HH:mm")}.

                {message}
                Cordialement,

                L'équipe SITULEARN";

			foreach (var file in reading.Files)
			{
				if (this.FileSystem.Exists(file.Url))
				{
					var mimeType = this.FileSystem.GetContentType(file.Url);

					var type = mimeType.Split("/").FirstOrDefault();
					var extension = mimeType.Split("/").LastOrDefault();

					string filename = Path.GetFileName(file.Url);

					if (!filename.Contains("."))
					{
						filename = string.Concat(filename, $".{extension}");
					}

					var data = await this.FileSystem.ReadAllBytesAsync(file.Url);

					builder.Attachments.Add(filename, data, new ContentType(type, extension));
				}
			}

			email.Body = builder.ToMessageBody();

			await email.SendWithSmtpAsync();
        }

        public async Task ShareReportAsync(Guid gameId, string emailAdress, ApplicationUser user)
        {
            var game = await this.SelectAll()
                .Include(g => g.FieldTrip)
                .Include(g => g.Players)
                    .ThenInclude(p => p.User)
                .Include(g => g.Players)
                    .ThenInclude(p => p.PlayedUnits)
                        .ThenInclude(pu => pu.Unit)
                .Include(g => g.Players)
                    .ThenInclude(p => p.PlayedUnits)
                        .ThenInclude(pu => pu.PlayedActivities)
                            .ThenInclude(pa => pa.Files)
                .Include(g => g.Players)
                    .ThenInclude(p => p.PlayedUnits)
                        .ThenInclude(pu => pu.PlayedActivities)
                            .ThenInclude(pa => pa.Activity)
                .FirstOrDefaultAsync(g => g.Id == gameId);

            var readings = game.Players
                .SelectMany(p => p.PlayedUnits)
                .OrderBy(pu => pu.Unit.Name)
                .SelectMany(pu => pu.PlayedActivities)
                .Where(pa => pa.Activity.ActivityType == ActivityType.Statement)
                .ToList();

            var player = game.Players.FirstOrDefault();

            string playerName = game.FieldTrip.AllowTeamNames && !string.IsNullOrEmpty(player.TeamName) ? player.TeamName : $"{player.User.FirstName} {player.User.LastName}";

            int noteIndex = 1;

            List<(string FileName, string FilePath)> files = new List<(string FileName, string FilePath)>();

            foreach (var reading in readings)
            {
                (List<(string FileName, string FilePath)> files, int index) readingFilesInfos = this.GetReadingFilesInfos(reading, noteIndex, playerName);

                noteIndex = readingFilesInfos.index;

                files = (files.Concat(readingFilesInfos.files)).ToList();
            }

            string path = $"readings-to-share/{this.GetNameWithoutSpecialCharacters(game.FieldTrip.Name)}_{this.GetNameWithoutSpecialCharacters(playerName)}_{DateTime.UtcNow.ToString("dd MM yyyy hh mm")}.zip";

            //Création dossier .zip
            using (MemoryStream ms = new MemoryStream())
            {
                using (var archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    try
                    {
                        //création des fichiers de relevé
                        foreach (var file in files)
                        {
                            //si le fichier n'a pas été supprimé (date de moins de 3 mois)
                            if (!string.IsNullOrEmpty(file.FilePath))
                            {
                                var zipEntry = archive.CreateEntry($"Relevés/{file.FileName}");

                                byte[] fileContent;

                                if (file.FilePath.Contains("games/"))
                                {
                                    fileContent = await this.FileSystem.ReadAllBytesAsync(file.FilePath);
                                }
                                else//s'il s'agit d'une note, on créée un fichier texte
                                {
                                    fileContent = Encoding.UTF8.GetBytes(file.FilePath);
                                }

                                if (fileContent != null)
                                {
                                    using (var originalFileStream = new MemoryStream(fileContent))
                                    {
                                        using (var zipEntryStream = zipEntry.Open())
                                        {
                                            originalFileStream.CopyTo(zipEntryStream);
                                        }
                                    }
                                }
                            }
                        }

                        //création des images de groupe
                        if (game.FieldTrip.AllowTeamNames && !string.IsNullOrEmpty(player.TeamPicture))
                        {
                            string fileNameGuid = player.Id.ToString().Substring(0, player.Id.ToString().IndexOf("-"));
                            string fileNameExtension = player.TeamPicture.Contains(".") ? player.TeamPicture.Substring(player.TeamPicture.IndexOf(".")) : "";

                            string teamPictureFileName = $"{this.GetNameWithoutSpecialCharacters(player.TeamName)}_{fileNameGuid}{fileNameExtension}";

                            var zipEntry = archive.CreateEntry($"Image de groupe/{teamPictureFileName}");

                            byte[] pictureContent = await this.FileSystem.ReadAllBytesAsync(player.TeamPicture);

                            if (pictureContent != null)
                            {
                                using (var originalFileStream = new MemoryStream(pictureContent))
                                {
                                    using (var zipEntryStream = zipEntry.Open())
                                    {
                                        originalFileStream.CopyTo(zipEntryStream);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw new BusinessException(e.Message);
                    }
                }

                ms.Seek(0, SeekOrigin.Begin);
                byte[] content = ms.ToArray();

                await this.FileSystem.WriteAllBytesAsync(path, content, "application/zip");
            }

            //envoi du mail contenant le lien de partage
            MimeMessage email = new MimeMessage();

            email.From.Add(new MailboxAddress(this.Configuration["Data:Emails:DefaultSenderName"], this.Configuration["Data:Emails:DefaultSenderEmail"]));

            if (string.IsNullOrEmpty(emailAdress))
            {
                email.To.Add(new MailboxAddress($"{user.FirstName} {user.LastName}", user.Email));
            }
            else
            {
                email.To.Add(new MailboxAddress(emailAdress, emailAdress));
            }
            
            email.Subject = $"SITULEARN - Partage des relevés de la sortie {game.FieldTrip.Name} du {player.StartPlayedDate.ToLocalTime().ToString("dd/MM/yyyy à HH:mm")}";

            var builder = new BodyBuilder();
            
            builder.TextBody = @$"Bonjour,

Voici un lien pour télécharger les relevés réalisés lors de la sortie {game.FieldTrip.Name} le {player.StartPlayedDate.ToLocalTime().ToString("dd/MM/yyyy à HH:mm")} :
{$"{this.Configuration["Data:SituLearn.Player:applicationUrl"]}download-report?path={Uri.EscapeDataString(path)}"}
                
Celui-ci est actif pendant une semaine.
                
Cordialement,

L'équipe SITULEARN";

            email.Body = builder.ToMessageBody();

            await email.SendWithSmtpAsync();
        }

        /// <summary>
        /// Return a dictionary of playedActivityId with its purgedValue. (true if no statements)
        /// </summary>
        /// <param name="playedActivityIds"></param>
        /// <returns></returns>
        public async Task<Dictionary<Guid, bool>> GetPlayedActivityIdIsPurgeDictionary(List<Guid> playedActivityIds)
		{
			//Console.WriteLine("##########");
			//Console.WriteLine(JsonConvert.SerializeObject(playedActivityIds));
			

			return await this.DataContext.PlayedActivities
				.Include(pa => pa.Files)
				.Where(x => playedActivityIds.Contains(x.Id))
				.ToDictionaryAsync(
					x => x.Id,
					x => string.IsNullOrEmpty(x.Answer) && !x.Files.Any() && x.Latitude == null && x.Longitude == null
				);
        }

        public async Task UpdatePlayedUnitAsync(PlayedUnit playedUnit, Guid gameId, Guid fielTripId, Guid journeyId, DateTime startCurrentUnitDate, bool currentUnitActivationHasBeenUnlocked, ApplicationUser user)
        {
            Player player = await this.SelectAllPlayers(p => p.PlayedUnits).FirstOrDefaultAsync(p => p.Id == playedUnit.PlayerId);

            if (player == null)
            {
                //Si player inexistant (en cas d'absence de connexion internet), créer player et Game temporairement en attendant méthode création player et Game rejouée au rétablissement de la connexion
                Game game = new Game()
				{
					Id = gameId,
					FielTripId = fielTripId,
					StartDate = default,
                    Players = new List<Player>()
				};

				player = new Player()
				{
					Id = playedUnit.PlayerId,
					GameId = game.Id,
					JourneyId = journeyId,
					UserId = user.Id,
					StartPlayedDate = default,
                    PlayerState = Common.PlayerState.Playing,
                    PlayedUnits = new List<PlayedUnit>()
                };

				game.Players.Add(player);

				await this.UpdateGameAsync(game, user);
            }

			//MAJ des données du Player
			player.CurrentUnitId = playedUnit.UnitId;
			player.CurrentUnitReachedDate = playedUnit.LocalisationReachDate;
			player.StartCurrentUnitDate = startCurrentUnitDate;
			player.CurrentUnitActivationHasBeenUnlocked = currentUnitActivationHasBeenUnlocked;
			Game existingGame = await this.SelectAsync(gameId);
			existingGame.LastUpdateDate = DateTime.UtcNow;
			PlayedUnit existingPlayedUnit = player.PlayedUnits.FirstOrDefault(pu => pu.Id == playedUnit.Id);

            if (existingPlayedUnit == null)
            {
                this.DataContext.PlayedUnits.Add(playedUnit);

                await this.SaveChangesAsync();
            }
            else
            {
                existingPlayedUnit.LocalisationReachDate = playedUnit.LocalisationReachDate;
                existingPlayedUnit.HaveLocatedUnit = playedUnit.HaveLocatedUnit;
                existingPlayedUnit.LocationPoints = playedUnit.LocationPoints;

                await this.SaveChangesAsync();
            }

		}

        public async Task UpdatePlayedActivityAsync(PlayedActivity playedActivity, Guid playerId, Guid unitId, Guid gameId, Guid fielTripId, Guid journeyId, ApplicationUser user)
        {
            PlayedUnit playedUnit = await this.DataContext.PlayedUnits.Include(pu => pu.PlayedActivities).FirstOrDefaultAsync(pu => pu.Id == playedActivity.PlayedUnitId);

            if (playedUnit == null)
            {
                //Si playedUnit inexistant (en cas d'absence de connexion internet), créer playedUnit temporairement en attendant méthode création playedUnit rejouée au rétablissement de la connexion
                playedUnit = new PlayedUnit()
				{
					Id = playedActivity.PlayedUnitId,
					PlayerId = playerId,
					UnitId = unitId,
                    PlayedActivities = new List<PlayedActivity>()
                };

				await this.UpdatePlayedUnitAsync(playedUnit, gameId, fielTripId, journeyId, default, false, user);
            }

            if (playedActivity.Files != null)
            {
                foreach (var file in playedActivity.Files)
                {
					string fileName = $"games/{file.Id}-file";

					if (!string.IsNullOrEmpty(file.Url) && file.Url.StartsWith("data:"))
                    {
                        if (file.Url.Contains("audio/webm;codecs=opus"))
                        {
						    fileName += ".weba";
                        }
                        else if (file.Url.Contains("audio/mp4"))
                        {
                            fileName += ".mp4";
                        }
					}

                    file.Url = await this.FileSystem.SetFile(file.Url, fileName);
                }
            }

            PlayedActivity existingPlayedActivity = playedUnit.PlayedActivities.FirstOrDefault(pa => pa.Id == playedActivity.Id);

            if (existingPlayedActivity == null)
            {
                this.DataContext.PlayedActivities.Add(playedActivity);
            }
            else
            {
                existingPlayedActivity.Answer = playedActivity.Answer;
                existingPlayedActivity.AnswerDate = playedActivity.AnswerDate;
                existingPlayedActivity.Latitude = playedActivity.Latitude;
                existingPlayedActivity.Longitude = playedActivity.Longitude;
                existingPlayedActivity.Points = playedActivity.Points;
                existingPlayedActivity.Files = playedActivity.Files;
            }
			Game game = await this.SelectAsync(gameId);
			game.LastUpdateDate = DateTime.UtcNow;
            await this.SaveChangesAsync();
        }

        public async Task EndGameAsync(Guid gameId, Guid playerId, DateTime endGameDate)
        {
            Game existingGame = await this.SelectAsync(gameId);
			existingGame.EndDate = endGameDate;
			existingGame.LastUpdateDate = DateTime.UtcNow;

            Player existingPlayer = await this.SelectAllPlayers().FirstOrDefaultAsync(p => p.Id == playerId);
            existingPlayer.EndGameDate = endGameDate;
			existingPlayer.PlayerState = PlayerState.Complete;
			existingPlayer.CurrentUnitId = null;
			existingPlayer.CurrentUnitReachedDate = null;
			existingPlayer.CurrentUnitActivationHasBeenUnlocked = false;
			existingPlayer.StartCurrentUnitDate = null;

            await this.SaveChangesAsync();
        }

        public async Task<byte[]> ExportReportByFieldTripIdForAPeriodAsync(Guid fieldTripId, DateTime startDateSelected, DateTime endDateSelected, FieldTrip fieldTrip)
        {
            var players = await this.SelectAll()
				.Include(g => g.Players)
                    .ThenInclude(p => p.PlayedUnits)
                        .ThenInclude(pu => pu.Unit)
                .Include(g => g.Players)
                    .ThenInclude(p => p.PlayedUnits)
                        .ThenInclude(pu => pu.PlayedActivities)
                            .ThenInclude(pa => pa.Activity)
                .Include(g => g.Players)
                    .ThenInclude(p => p.PlayedUnits)
                        .ThenInclude(pu => pu.PlayedActivities)
                            .ThenInclude(pa => pa.Files)
                .Include(g => g.Players)
                    .ThenInclude(p => p.User)
                .Where(g => g.FielTripId == fieldTripId && g.StartDate >= startDateSelected && g.StartDate <= endDateSelected)
				.OrderBy(g => g.StartDate)
                .SelectMany(g => g.Players)
                .OrderBy(p => fieldTrip.AllowTeamNames && !string.IsNullOrEmpty(p.TeamName) ? p.TeamName : p.User.FirstName)
                    .ThenBy(p => p.User.LastName)
				.ToArrayAsync();

            var questionsNumber = fieldTrip.LocatedGameUnits.SelectMany(u => u.Activities).Count(a => a.ActivityType == ActivityType.Question);
            
            List<(string Name, string Date, TimeSpan Duration, decimal CorrectAnswersRate, int Score, (string FileName, string FilePath) TeamPicture)> participants = new();
            List<(string ParticipantName, string LocatedGameUnitName, string LocatedActivityName, string Date, List<(string FileName, string FilePath)> Files)> readings = new();
            int noteIndex = 1;

            //récupération des données de participation pour chaque participant
            foreach (var player in players)
			{
                //constitution des statistiques individuelles
                string playerName = fieldTrip.AllowTeamNames && !string.IsNullOrEmpty(player.TeamName) ? player.TeamName : $"{player.User.FirstName} {player.User.LastName}";

                var playedActivitiesQuestion = player.PlayedUnits
					.SelectMany(p => p.PlayedActivities)
					.Where(pa => pa.Activity.ActivityType == ActivityType.Question);

				decimal correctAnswersRate = default(decimal);

                if (playedActivitiesQuestion.Any())
				{
                    correctAnswersRate = (decimal)playedActivitiesQuestion.Count(q => q.Points.HasValue && q.Points.Value > 0) / (decimal)questionsNumber * 100;
                }				

				int locationGain = player.PlayedUnits.Sum(pu => pu.LocationPoints);

				int questionGain = player.PlayedUnits
						.Where(pu => pu.PlayedActivities != null && pu.PlayedActivities.Any())
						.SelectMany(pu => pu.PlayedActivities)
						.Sum(pa => pa.Points ?? 0);

                string teamPictureFileName = null;

                if (fieldTrip.AllowTeamNames && !string.IsNullOrEmpty(player.TeamPicture))
                {
                    string fileNameGuid = player.Id.ToString().Substring(0, player.Id.ToString().IndexOf("-"));
                    string fileNameExtension = player.TeamPicture.Contains(".") ? player.TeamPicture.Substring(player.TeamPicture.IndexOf(".")) : "";

                    teamPictureFileName = $"{this.GetNameWithoutSpecialCharacters(player.TeamName)}_{fileNameGuid}{fileNameExtension}";
                }

                var participant =
                (
                    $"{playerName}",
                    player.StartPlayedDate.ToString("dd/MM/yyyy"),
                    player.EndGameDate.HasValue ? player.EndGameDate.Value - player.StartPlayedDate : default,
                    correctAnswersRate,
                    locationGain + questionGain,
                    (teamPictureFileName, player.TeamPicture)
                );

				participants.Add(participant);

                //constitution de la liste de relevés
                var playedActivitiesReading = player.PlayedUnits
                    .OrderBy(pu => pu.Unit.Name)
                    .SelectMany(p => p.PlayedActivities)
                    .Where(pa => pa.Activity.ActivityType == ActivityType.Statement);

                foreach (var playedActivityReading in playedActivitiesReading)
                {
                    (List<(string FileName, string FilePath)> files, int index) readingFilesInfos = this.GetReadingFilesInfos(playedActivityReading, noteIndex, playerName);

                    noteIndex = readingFilesInfos.index;

                    var reading =
                        (
                            $"{playerName}",
                            playedActivityReading.PlayedUnit.Unit.Name,
                            playedActivityReading.Activity.Name,
                            playedActivityReading.AnswerDate.ToString("dd/MM/yyyy HH:mm:ss"),
                            readingFilesInfos.files.ToList()
                        );

                    readings.Add(reading);
                }
            }

            //constitution des statistiques globales
            var correctAnswersAverageRate = participants.Any() ? participants.Select(p => p.CorrectAnswersRate).Average() : 0;
            var averageScore = participants.Any() ? participants.Select(p => p.Score).Average() : 0;
            var averageDuration = participants.Any(p => p.Duration != default) ? participants.Where(p => p.Duration != default).Select(p => p.Duration.TotalMilliseconds).Average() : 0;

            var readingsNumber = players
                .SelectMany(p => p.PlayedUnits)
                .SelectMany(p => p.PlayedActivities)
                .Where(pa => pa.Activity.ActivityType == ActivityType.Statement)
                .Count();

            ReportModel model = new ReportModel()
			{
				Id = fieldTripId,
                BaseUrl = this.Configuration["Data:Network:BaseUrl"],
                Name = fieldTrip.Name,
				Type = fieldTrip.FieldTripType.GetFriendlyName(),
				Description = fieldTrip.Description,
				//CreationDate = "",
				LastUpdateDate = fieldTrip.LastUpdateDate.ToString("dd/MM/yyyy HH:mm:ss"),
				ReportStartDate = startDateSelected.ToString("dd/MM/yyyy HH:mm:ss"),
                ReportEndDate = endDateSelected.ToString("dd/MM/yyyy HH:mm:ss"),
				ParticipantsNumber = players.Count(),
				ParticipantsCompletedGameNumber = players.Count(g => g.EndGameDate.HasValue),
                ReadingsNumber = readingsNumber,
                CorrectAnswersAverageRate = String.Format("{0:0.0#}", correctAnswersAverageRate),
				AverageScore = String.Format("{0:0.0#}", averageScore),
				AverageDuration = TimeSpan.FromMilliseconds(averageDuration).ToString(@"hh\:mm\:ss"),
				Participants = participants.ToList(),
				Readings = readings.ToList()
            };

            var dynamicRazorEngine = this.ServiceProvider.GetService<DynamicRazorEngine>();

            string html = "";
            html = await dynamicRazorEngine.GetHtmlAsync("Business,Business.Templates.Report.cshtml", model);

            using (MemoryStream ms = new MemoryStream())
            {
                using (var archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    var fileInArchive = archive.CreateEntry($"{fieldTrip.Name}.html", CompressionLevel.Optimal);
                    using var entryStream = fileInArchive.Open();
                    using var fileToCompressStream = new MemoryStream(Encoding.UTF8.GetBytes(html));
                    fileToCompressStream.CopyTo(entryStream);
                    fileToCompressStream.Close();
                    entryStream.Close();

                    try
                    {
						//création des fichiers de relevé
                        var readingFiles = model.Readings.SelectMany(r => r.Files);

                        foreach (var file in readingFiles)
                        {
                            //si le fichier n'a pas été supprimé (date de moins de 3 mois)
                            if (!string.IsNullOrEmpty(file.FilePath))
                            {
                                var zipEntry = archive.CreateEntry($"Relevés/{file.FileName}");

                                byte[] fileContent;

                                if (file.FilePath.Contains("games/"))
                                {
                                    fileContent = await this.FileSystem.ReadAllBytesAsync(file.FilePath);
                                }
                                else//s'il s'agit d'une note, on créée un fichier texte
                                {
                                    fileContent = Encoding.UTF8.GetBytes(file.FilePath);
                                }

                                if (fileContent != null)
                                {
                                    using (var originalFileStream = new MemoryStream(fileContent))
                                    {
                                        using (var zipEntryStream = zipEntry.Open())
                                        {
                                            originalFileStream.CopyTo(zipEntryStream);
                                        }
                                    }
                                }
                            }                            
                        }

                        //création des images de groupe
                        if (fieldTrip.AllowTeamNames)
                        {
                            var teamPictures = model.Participants.Select(p => p.TeamPicture).Where(tp => !string.IsNullOrEmpty(tp.FilePath));

                            foreach (var teamPicture in teamPictures)
                            {
                                var zipEntry = archive.CreateEntry($"Images de groupe/{teamPicture.FileName}");

                                byte[] content = await this.FileSystem.ReadAllBytesAsync(teamPicture.FilePath);

                                if (content != null)
                                {
                                    using (var originalFileStream = new MemoryStream(content))
                                    {
                                        using (var zipEntryStream = zipEntry.Open())
                                        {
                                            originalFileStream.CopyTo(zipEntryStream);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw new BusinessException(e.Message);
                    }
                }

                ms.Seek(0, SeekOrigin.Begin);
                return ms.ToArray();
            }
        }

        private (List<(string FileName, string FilePath)> files, int index) GetReadingFilesInfos(PlayedActivity playedActivityReading, int noteIndex, string playerName)
        {
            var files = new List<(string FileName, string FilePath)>();

            string activityName = this.GetNameWithoutSpecialCharacters(playedActivityReading.Activity.Name);

            //si le relevé contient une note, on prépare le fichier texte à créer
            if (!string.IsNullOrEmpty(playedActivityReading.Answer))
            {
                var file =
                    (
                        $"{activityName}/{this.GetNameWithoutSpecialCharacters(playerName)}_note_{noteIndex.ToString().PadLeft(4, '0')}.txt",
                        playedActivityReading.Answer
                    );

                files.Add(file);

                noteIndex++;
            }

            foreach (var readingFile in playedActivityReading.Files)
            {
                string fileName = null;
                string typeFile = readingFile.MimeType == "image/jpeg" ? "photo" : (readingFile.MimeType.Contains("audio/") ? "audio" : "");

                //si le fichier n'a pas été supprimé (date de moins de 3 mois)
                if (!string.IsNullOrEmpty(readingFile.Url))
                {
                    string playedActivityFileName = readingFile.Url.Replace("games/", "").Replace("-file", "");
                    string fileNameGuid = playedActivityFileName.Substring(0, playedActivityFileName.IndexOf("-"));
                    string fileNameExtension = playedActivityFileName.Contains(".") ? playedActivityFileName.Substring(playedActivityFileName.IndexOf(".")) : "";

                    fileName = $"{activityName}/{this.GetNameWithoutSpecialCharacters(playerName)}_{typeFile}_{fileNameGuid}{fileNameExtension}";
                }
                else
                {
                    fileName = $"{activityName}/{this.GetNameWithoutSpecialCharacters(playerName)}_{typeFile}_(fichier supprimé)";
                }                

                var file =
                    (
                        fileName,
                        readingFile.Url
                    );

                files.Add(file);
            }

            return (files, noteIndex);
        }

        public string GetNameWithoutSpecialCharacters(string text)
        {
            return text.Replace(" ", "_").Replace(".", "_").Replace("\\", "_").Replace("/", "_").Replace(":", "_").Replace("*", "_").Replace("?", "_").Replace("\"", "_")
                .Replace("<", "_").Replace(">", "_").Replace("|", "_");
        }

        public async Task DeleteLastWeekReadingsToShareAsync()
        {
            //string dateToDelete = DateTime.UtcNow.AddDays(-7).ToString("dd MM yyyy");
            string folderPath = "readings-to-share";

            var files = await this.FileSystem.GetFiles(folderPath);
            //var filesToDelete = files.Where(f => f.Contains(dateToDelete)).ToList();

            //foreach (var file in filesToDelete)
            //{
            //    await this.FileSystem.DeleteAsync($"{folderPath}/{file}");
            //}

            foreach (var file in files) {
                var creationDate = await this.FileSystem.GetModificationDateAsync($"{folderPath}/{file}");

                if (creationDate.Value.Date < DateTime.UtcNow.AddDays(-7).Date)
                {
                    await this.FileSystem.DeleteAsync($"{folderPath}/{file}");
                }
            }
        }

        public async Task DeleteMyGamesAsync(string userId)
        {
            var playersToDelete = await this.SelectAllPlayers()
                .Include(p => p.PlayedUnits)
                    .ThenInclude(pu => pu.PlayedActivities)
                        .ThenInclude(pa => pa.Files)
                .Where(p => p.UserId == userId)
                .ToListAsync();

            if (playersToDelete == null)
            {
                return;
            }

            foreach (var player in playersToDelete)
            {
                await this.DeletePlayerAsync(player);
            }
        }

		public async Task<IEnumerable<Player>> GetAllPlayersStartedGameAfterSupervisionStartTimeAsync(Guid fieldTripId, DateTime startTime)
		{
            var players = await this.SelectAllPlayers()
				.Include(p => p.User)
				.Include(p => p.Game)
                    .ThenInclude(g => g.FieldTrip)
                    .ThenInclude(f => f.Journeys)
					.ThenInclude(j => j.JourneyLocatedGameUnits)
					.ThenInclude(jlg => jlg.LocatedGameUnit)
					.ThenInclude(lgu => lgu.Activities)
                .Include(p => p.Game)
                    .ThenInclude(g => g.FieldTrip)
                    .ThenInclude(f => f.Journeys)
                    .ThenInclude(j => j.JourneyLocatedGameUnits)
                    .ThenInclude(jlg => jlg.LocatedGameUnit)
                    .ThenInclude(lgu => lgu.Activities)
                    .ThenInclude(a => (a as QuestionActivity).Clue)
                .Include(p => p.Game)
					.ThenInclude(g => g.FieldTrip)
					.ThenInclude(f => f.ExplorationMap)
				.Include(p => p.PlayedUnits)
                    .ThenInclude(pu => pu.PlayedActivities)
					.ThenInclude(pa => pa.Activity)
				.Include(p => p.CurrentUnit)
				.Where(p => p.Game.FielTripId == fieldTripId && p.StartPlayedDate >= startTime)
                .ToListAsync();

            return players;
		}
	}


}
