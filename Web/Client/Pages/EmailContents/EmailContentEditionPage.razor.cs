﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Common.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.EmailContents
{
    [Route(Url)]
    [Authorize(Policy = Policy.UserIsAdministrator)]
    public partial class EmailContentEditionPage : FormComponent
    {
        public const string Url = "/email-content/{id}";

        public static string GetUrl(string id, string backUrl = null)
        {
            var url = Url.Replace("{id}", id.ToString());

            if (!string.IsNullOrEmpty(backUrl))
            {
                url += $"?backUrl={Uri.EscapeDataString(backUrl)}";
            }

            return url;
        }

        [Parameter]
        public string Id { get; set; }

        [Inject]
        public EmailContentsRepository EmailContentsRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        public EmailContentUpdateModel Model { get; set; }

        public QuillEditor.QuillEditorVariableBlock[] Variables
        {
            get
            {
                try
                {
                    var result = JsonConvert.DeserializeObject<QuillEditor.QuillEditorVariableBlock[]>(this.Model.Variables);
                    return result;
                }
                catch
                {
                    return Array.Empty<QuillEditor.QuillEditorVariableBlock>();
                }
            }
        }

        protected override async Task OnInitializedAsync()
        {
            if (!int.TryParse(this.Id, out int emailContentId))
            {
                this.Notifier.Notify(Color.Danger, "Identifiant de contenu d'email invalide.");
                await base.OnInitializedAsync();
                return;
            }

            var existing = await this.EmailContentsRepository.GetEmailContent(emailContentId);

            this.Model = new();

            if (existing != null)
            {
                this.Model.Id = existing.Id;
                this.Model.Name = existing.Name;
                this.Model.Subject = existing.Subject;
                this.Model.Content = existing.Content;
                this.Model.Variables = existing.Variables;
            }

            await base.OnInitializedAsync();
        }

        public override async Task OnProcessAsync()
        {
            try
            {
                await this.EmailContentsRepository.UpdateEmailContent(this.Model);
                this.Notifier.Notify(Color.Primary, "Modification effectuée.", 1500, async () => { this.NavigationManager.NavigateTo(EmailContentsPage.Url); await Task.CompletedTask; });
            }
            catch (Exception e)
            {
                this.Notifier.Notify(Color.Danger, "Une erreur est survenue. \n" + e.Message);
                throw;
            }
        }
    }
}
