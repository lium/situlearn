// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Common;
using Entities;
using NetTopologySuite.Geometries;
using Ozytis.Common.Core.GeoJson;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.Razor.Leaflet;
using Syncfusion.Blazor.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Client.Repositories;
using FontAwesomeIcons = Ozytis.Common.Core.Web.Razor.FontAwesomeIcons;

namespace Web.Client.Pages.FieldTrips
{
	[Route(Url)]
	public partial class FieldTripMapEditionPage : ComponentBase
	{
		public const string Url = "/fieldtrip/{fieldTripId:guid}/map-edition";

		public string AddMarkerState = FontAwesomeIcons.MapMarkerPlus;

		public LocatedUnitCreationModel CreatedPoint { get; set; }

		public bool EditActivationArea { get; set; }

		public LocatedGameUnit EditedUnit { get; set; }

		public FieldTrip FieldTrip { get; set; }

		[Parameter]
		public Guid FieldTripId { get; set; }

		[Inject]
		public NavigationWithHistoryManager NavigationManager { get; set; }

		[Inject]
		public FieldTripsRepository FieldTripsRepository { get; set; }

		[Inject]
		public UsersRepository UsersRepository { get; set; }

		[Inject]
		public LocationRepository LocationRepository { get; set; }

		[CascadingParameter]
		public INotifier Notifier { get; set; }

        [CascadingParameter]
        public IConfirmProvider Confirm { get; set; }

        public bool IsSavingPosition { get; set; }

		public bool IsSearchingLocation { get; set; }

		public IEnumerable<GeoLocalisationResult> LocalisationResults { get; set; }

		public string LocalisationText { get; set; }

		public LeafletMap Map { get; set; }

		public string MapBackground { get; set; }

		public decimal? MapBackgroundLowerRightLatitude { get; set; }

		public decimal? MapBackgroundLowerRightLongitude { get; set; }

		public decimal MapBackgroundOpacity { get; set; } = 0.7m;

		public decimal MapRotation { get; set; }

		public decimal? MapBackgroundUpperLeftLatitude { get; set; }

		public decimal? MapBackgroundUpperLeftLongitude { get; set; }

		public LeafletCoordinates MapCenter { get; set; }

		public bool MapIsGeolocalized { get; set; }

		public string NextMapClick { get; set; }

		public bool ShowLocationResults { get; set; }

		public bool ShowContextualHelpModal { get; set; }

		public string ContextualHelpIdToShow { get; set; }

		public bool AddMarkerInProgress { get; set; }

		public bool ShowLargeMap { get; set; } = false;

		public bool PrintOptionsExportOnly { get; set; }

		public LeafletCoordinates[] MapBackgroundBounds
		{
			get
			{
				return new LeafletCoordinates[]
				{
					new()
					{
						Latitude = this.MapBackgroundUpperLeftLatitude ?? 0,
						Longitude = this.MapBackgroundUpperLeftLongitude ?? 0
					},
					new()
					{
						Latitude = this.MapBackgroundLowerRightLatitude ?? 0,
						Longitude = this.MapBackgroundLowerRightLongitude ?? 0
					},
				};
			}
		}

		public static string GetUrl(Guid fieldTripId)
		{
			return Url.Replace("{fieldTripId:guid}", fieldTripId.ToString());
		}

		public const string AddMarkerAction = "AddMarkerAction";
		public const string AddZoneAction = "AddZoneAction";
		protected async Task AddMarkerAsync()
		{
			if (this.NextMapClick != AddMarkerAction)
			{
				await this.Map.SetMapStyleAsync(new { Cursor = "crosshair" });
				this.NextMapClick = AddMarkerAction;

				this.AddMarkerInProgress = true;
			}

			this.StateHasChanged();
		}

		public ActivationAreaType? CurrentExplorationZoneAddition { get; set; }
		protected async Task AddExplorationZoneAsync(ActivationAreaType explorationZoneType)
		{
			if (this.ExplorationArea == null)
			{
				this.AddMarkerState = FontAwesomeIcons.Hourglass;
				await this.Map.SetMapStyleAsync(new { Cursor = "crosshair" });
				this.NextMapClick = AddZoneAction;
				this.CurrentExplorationZoneAddition = explorationZoneType;

				this.StateHasChanged();
			}
		}

		protected async Task ChangePoiActivationTypeAsync(ActivationAreaType newType, LocatedGameUnit unit)
		{
			var mapBounds = await this.Map.GetBoundsAsync();

			var latRatio = (mapBounds.NorthEast.Latitude - mapBounds.SouthWest.Latitude) / 10;
			var lngRatio = (mapBounds.NorthEast.Longitude - mapBounds.SouthWest.Longitude) / 10;

			var up = new Coordinate((double)(unit.Longitude + lngRatio), (double)(unit.Latitude + latRatio));
			var down = new Coordinate((double)(unit.Longitude + lngRatio), (double)(unit.Latitude - latRatio));
			var down2 = new Coordinate((double)(unit.Longitude - lngRatio), (double)(unit.Latitude - latRatio));
			var up2 = new Coordinate((double)(unit.Longitude - lngRatio), (double)(unit.Latitude + latRatio));

			switch (newType)
			{
				case ActivationAreaType.None:
					unit.ActivationAreaType = ActivationAreaType.None;
					break;

				case ActivationAreaType.Circle:

					var polygon = await this.Map.TransformCircleToPolygonAsync(new LeafletCoordinates
					{
						Latitude = unit.Latitude,
						Longitude = unit.Longitude
					}, 10);

					this.SetCircleActivationArea(new CircleData
					{
						Center = new LeafletCoordinates
						{
							Latitude = unit.Latitude,
							Longitude = unit.Longitude
						},
						Polygon = polygon,
						Radius = 10
					}, unit);

					break;

				case ActivationAreaType.Rectangle:

					unit.ActivationArea = new Polygon(new LinearRing(new[] { up, down, down2, up2, up }));
					unit.ActivationAreaType = ActivationAreaType.Rectangle;
					break;

				case ActivationAreaType.Polygon:
					unit.ActivationArea = new Polygon(new LinearRing(new[] { up, down, down2, up2, up }));
					unit.ActivationAreaType = ActivationAreaType.Polygon;
					break;

				default:
					break;
			}

			this.StateHasChanged();
		}

		public void EditExplorationArea(string type, object data)
		{
			if (!this.ExplorationZoneInProgress)
			{
				return;
			}

			switch (this.ExplorationAreaShape)
			{
				case ActivationAreaType.Circle:
					var leafletCircle = data as CircleData;

					this.ExplorationAreaCenterLatitude = leafletCircle.Center.Latitude;
					this.ExplorationAreaCenterLongitude = leafletCircle.Center.Longitude;
					this.ExplorationAreaRadius = leafletCircle.Radius;
					break;
				case ActivationAreaType.Rectangle:
				case ActivationAreaType.Polygon:
					this.ExplorationArea = data as Geometry;
					break;
				case ActivationAreaType.None:
				default:
					break;
			}

			this.StateHasChanged();
		}

		protected void EditActivationAreaShape(string type, object data)
		{
			if (this.ExplorationZoneInProgress)
			{
				return;
			}

			switch (this.EditedUnit.ActivationAreaType)
			{
				case ActivationAreaType.Circle:
					var leafletCircle = data as CircleData;

					this.EditedUnit.ActivationAreaCenterLatitude = leafletCircle.Center.Latitude;
					this.EditedUnit.ActivationAreaCenterLongitude = leafletCircle.Center.Longitude;
					this.EditedUnit.ActivationRadius = leafletCircle.Radius;
					break;
				case ActivationAreaType.Rectangle:
				case ActivationAreaType.Polygon:
					this.EditedUnit.ActivationArea = data as Geometry;
					break;
				case ActivationAreaType.None:
				default:
					break;
			}

			this.StateHasChanged();
		}

		public InlineImageChooser BackgroundChooser { get; set; }

		protected async Task MapBackgroundHasChanged(string url)
		{
			if (string.IsNullOrEmpty(this.MapBackground))
			{
				this.MapRotation = 0;
			}

			this.MapBackground = url;

			if (BackgroundChooser.ImageWidth > 0 && BackgroundChooser.ImageHeight > 0)
			{
				var bounds = await this.Map.GetBoundsAsync();
				var widthHeightRatio = (double)BackgroundChooser.ImageWidth / BackgroundChooser.ImageHeight;

				// see https://gis.stackexchange.com/questions/422321/longitude-to-metres-ratio-given-a-latitude-for-google-maps-static-api-stitching

				var degreesPerMeterAtEquator = 360 / (2 * Math.PI * 6378137); // earth radius
				var metresAtEquatorPerTilePx = 156543.03392 / Math.Pow(2, await Map.GetCurrentZoomAsync());

				var latIncrement = degreesPerMeterAtEquator * Math.Cos((double)bounds.NorthEast.Latitude * Math.PI / 180) * metresAtEquatorPerTilePx * 700;
				var lonIncrement = degreesPerMeterAtEquator * metresAtEquatorPerTilePx * 700 * widthHeightRatio;

				this.MapBackgroundUpperLeftLatitude = bounds.NorthEast.Latitude;
				this.MapBackgroundUpperLeftLongitude = bounds.SouthWest.Longitude;

				this.MapBackgroundLowerRightLatitude = bounds.NorthEast.Latitude - (decimal)latIncrement;
				this.MapBackgroundLowerRightLongitude = bounds.SouthWest.Longitude + (decimal)lonIncrement;
			}
			else if (true || !this.MapBackgroundLowerRightLatitude.HasValue)
			{
				var bounds = await this.Map.GetBoundsAsync();

				Console.WriteLine("Custom map " + GeoJsonUtils.Serialize(bounds));

				this.MapBackgroundUpperLeftLatitude = bounds.NorthEast.Latitude;
				this.MapBackgroundUpperLeftLongitude = bounds.NorthEast.Longitude;
				this.MapBackgroundLowerRightLatitude = bounds.SouthWest.Latitude;
				this.MapBackgroundLowerRightLongitude = bounds.SouthWest.Longitude;
			}

			await this.Map.RefreshAsync();
			await this.Map.LoadObjectsAsync();

			this.StateHasChanged();
		}

		protected override async Task OnInitializedAsync()
		{
			await this.RefreshAsync();

			this.ExplorationAreaEnabled = this.FieldTrip.ExplorationMap.OrganizerOutOfZoneAlert
				|| this.FieldTrip.ExplorationMap.ParticipantOutOfZoneAlert
				|| this.FieldTrip.ExplorationMap.ExplorationArea != null;

			this.AlertZoneOutUsersPlayer = this.FieldTrip?.ExplorationMap?.ParticipantOutOfZoneAlert ?? false;
			this.AlertZoneOutUsersMonitor = this.FieldTrip?.ExplorationMap?.OrganizerOutOfZoneAlert ?? false;

			this.ExplorationArea = this.FieldTrip?.ExplorationMap?.ExplorationArea;
			this.ExplorationAreaCenterLatitude = this.FieldTrip?.ExplorationMap?.ExplorationAreaCenterLatitude;
			this.ExplorationAreaCenterLongitude = this.FieldTrip?.ExplorationMap?.ExplorationAreaCenterLongitude;
			this.ExplorationAreaRadius = this.FieldTrip?.ExplorationMap?.ExplorationAreaRadius ?? 0;

			this.PoiEditionInProgress = true;
			this.ExplorationAreaShape = this.FieldTrip?.ExplorationMap?.ExplorationShape ?? ActivationAreaType.None;
			this.ExplorationAreaEnabled = this.ExplorationArea != null;

			if (ExplorationAreaTab != null && this.ExplorationAreaEnabled)
			{
				this.ExplorationAreaTab.NavStyle = null;
			}
		}

		public void RemoveExplorationArea()
		{
			this.ExplorationArea = null;
			this.ExplorationAreaShape = ActivationAreaType.None;
			this.StateHasChanged();
		}

		public Tab ExplorationAreaTab { get; set; }

		public const string ExplorationTabName = "Zone d'exploration";
		public const string PoiTabName = "Points d'int�r�t";
		public void ToggleTabs(Tab activeTab)
		{
			if (activeTab.Title == ExplorationTabName)
			{
				this.ExplorationZoneInProgress = true;
				this.PoiEditionInProgress = false;
				this.EditedUnit = null;
			}
			else if (activeTab.Title == PoiTabName)
			{
				this.ExplorationZoneInProgress = false;
				this.PoiEditionInProgress = true;
			}
			else
			{
				this.ExplorationZoneInProgress = false;
				this.PoiEditionInProgress = false;
				this.EditedUnit = null;
			}

			this.StateHasChanged();
		}
		public bool ExplorationZoneInProgress { get; set; }
		public bool PoiEditionInProgress { get; set; }

		public bool AlertZoneOutUsersPlayer { get; set; }
		public bool AlertZoneOutUsersMonitor { get; set; }

		protected async Task OnMapClickAsync(MouseEventData data)
		{
			switch (this.NextMapClick)
			{
				case AddMarkerAction:

					this.CreatedPoint = new LocatedUnitCreationModel
					{
						Latitude = data.Coordinates.Latitude,
						Longitude = data.Coordinates.Longitude
					};

					break;

				case AddZoneAction:
					Polygon polygon = null;
					switch (this.CurrentExplorationZoneAddition)
					{
						case ActivationAreaType.Circle:
							polygon = await this.Map.TransformCircleToPolygonAsync(new LeafletCoordinates
							{
								Latitude = data.Coordinates.Latitude,
								Longitude = data.Coordinates.Longitude
							}, 10);
							break;
						case ActivationAreaType.Rectangle:
						case ActivationAreaType.Polygon:
							decimal lngRatio = 0.0005m, latRatio = 0.0005m;

							var up = new Coordinate((double)(data.Coordinates.Longitude + lngRatio), (double)(data.Coordinates.Latitude + latRatio));
							var up2 = new Coordinate((double)(data.Coordinates.Longitude - lngRatio), (double)(data.Coordinates.Latitude + latRatio));
							var down2 = new Coordinate((double)(data.Coordinates.Longitude - lngRatio), (double)(data.Coordinates.Latitude - latRatio));
							var down = new Coordinate((double)(data.Coordinates.Longitude + lngRatio), (double)(data.Coordinates.Latitude - latRatio));

							polygon = new Polygon(new LinearRing(new[] { up, up2, down2, down, up })); ;
							break;
						default:
							break;
					}
					this.ExplorationAreaRadius = 10;
					this.ExplorationAreaCenterLatitude = data.Coordinates.Latitude;
					this.ExplorationAreaCenterLongitude = data.Coordinates.Longitude;
					this.ExplorationArea = polygon;
					this.ExplorationAreaShape = this.CurrentExplorationZoneAddition.Value;
					break;

				default:
					break;
			}

			this.AddMarkerState = FontAwesomeIcons.MapMarkerPlus;
			await this.Map.SetMapStyleAsync(new { Cursor = "grab" });
			this.NextMapClick = null;
			this.CurrentExplorationZoneAddition = null;
			this.AddMarkerInProgress = false;

			this.StateHasChanged();
		}

		protected async Task OnPointCancelledAsync()
		{
			await this.Map.SetMapStyleAsync(new { Cursor = "grab" });
			this.CreatedPoint = null;
            this.StateHasChanged();
		}

		protected async Task OnPointCreatedAsync()
		{
			await this.Map.SetMapStyleAsync(new { Cursor = "grab" });
			await this.RefreshLocatedGameUnitsAsync();
			this.CreatedPoint = null;
            this.StateHasChanged();
		}


		protected async Task RefreshLocatedGameUnitsAsync()
		{
            var fieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId, false);
			this.FieldTrip.LocatedGameUnits = fieldTrip.LocatedGameUnits;
			this.StateHasChanged();
        }

		protected async Task RefreshAsync()
		{
			this.EditedUnit = null;

			this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId, false);

			this.MapIsGeolocalized = this.FieldTrip.ExplorationMap.IsGeolocatable;

			if (this.FieldTrip?.ExplorationMap?.CenterLatitude != null)
			{
				this.MapCenter = new LeafletCoordinates
				{
					Latitude = this.FieldTrip.ExplorationMap.CenterLatitude.Value,
					Longitude = this.FieldTrip.ExplorationMap.CenterLongitude.Value,
				};
			}

			this.MapBackground = this.FieldTrip.ExplorationMap.CustomBackgroundMap;
			this.MapBackgroundUpperLeftLatitude = this.FieldTrip.ExplorationMap.CustomBackgroundUpperLeftLatitude;
			this.MapBackgroundUpperLeftLongitude = this.FieldTrip.ExplorationMap.CustomBackgroundUpperLeftLongitude;
			this.MapBackgroundLowerRightLatitude = this.FieldTrip.ExplorationMap.CustomBackgroundLowerRightLatitude;
			this.MapBackgroundLowerRightLongitude = this.FieldTrip.ExplorationMap.CustomBackgroundLowerRightLongitude;
			this.MapBackgroundOpacity = this.FieldTrip.ExplorationMap.CustomBackgroundOpacity ?? 1m;
			this.MapRotation = this.FieldTrip.ExplorationMap.CustomBackgroundRotation ?? 1m;

			this.StateHasChanged();
		}

		public bool CanEdit => this.UsersRepository.CurrentUser.Role == UserRoleNames.Administrator || (this.FieldTrip?.DesignerId == this.UsersRepository.CurrentUser.Id);

		public void RotateMapBackground(int angle)
		{
			this.MapRotation += angle;
			this.StateHasChanged();
		}

		protected void ChangeMapBackgroundSize(int sizeAdjustement)
		{
			var latitudeChange = this.MapBackgroundUpperLeftLatitude - this.MapBackgroundLowerRightLatitude;
			var latitudeRatio = latitudeChange / 100 * sizeAdjustement;

			var longitudeChange = this.MapBackgroundUpperLeftLongitude - this.MapBackgroundLowerRightLongitude;
			var longitudeRatio = longitudeChange / 100 * sizeAdjustement;

			this.MapBackgroundLowerRightLatitude -= latitudeRatio;
			this.MapBackgroundLowerRightLongitude -= longitudeRatio;


			this.StateHasChanged();
		}

		protected void MoveMapBackgroundY(int adjustment)
		{
			var difference = this.MapBackgroundUpperLeftLatitude - this.MapBackgroundLowerRightLatitude;
			var ratio = difference / 200 * adjustment;

			Console.WriteLine("ratio /" + ratio);

			this.MapBackgroundLowerRightLatitude += ratio;
			this.MapBackgroundUpperLeftLatitude += ratio;

			this.StateHasChanged();
		}

		protected void MoveMapBackgroundX(int adjustment)
		{
			var difference = this.MapBackgroundLowerRightLongitude - this.MapBackgroundUpperLeftLongitude;
			var ratio = difference / 200 * adjustment;

			this.MapBackgroundLowerRightLongitude += ratio;
			this.MapBackgroundUpperLeftLongitude += ratio;

			this.StateHasChanged();
		}

		protected async Task SaveMapPositionAsync()
		{
			if (this.IsSavingPosition)
			{
				return;
			}

			this.IsSavingPosition = true;

			this.StateHasChanged();

			try
			{
				var center = await this.Map.GetCurrentCenterAsync();
				var zoom = await this.Map.GetCurrentZoomAsync();
				var bounds = await this.Map.GetBoundsAsync();

				await this.FieldTripsRepository.UpdateMapPositionAsync(new UpdateMapPositionModel
				{
					FieldTripId = this.FieldTripId,
					CenterLatitude = center.Latitude,
					CenterLongitude = center.Longitude,
					Zoom = zoom,
					MapIsGeolocalized = this.MapIsGeolocalized,
					MapBackground = this.MapBackground,
					MapBackgroundLowerRightLatitude = this.MapBackgroundLowerRightLatitude,
					MapBackgroundLowerRightLongitude = this.MapBackgroundLowerRightLongitude,
					MapBackgroundUpperLeftLatitude = this.MapBackgroundUpperLeftLatitude,
					MapBackgroundUpperLeftLongitude = this.MapBackgroundUpperLeftLongitude,
					MapBackgroundOpacity = this.MapBackgroundOpacity,
					MapRotation = this.MapRotation,
					SouthWestLatitude = bounds.SouthWest.Latitude,
					SouthWestLongitude = bounds.SouthWest.Longitude,
					NorthEastLatitude = bounds.NorthEast.Latitude,
					NorthEastLongitude = bounds.NorthEast.Longitude,
					AlertUserOutOfExplorationZoneInMonitor = this.AlertZoneOutUsersMonitor,
					AlertUserOutOfExplorationZoneInPlayer = this.AlertZoneOutUsersPlayer,
					ExplorationArea = this.ExplorationArea,
					ExplorationAreaCenterLatitude = this.ExplorationAreaCenterLatitude,
					ExplorationAreaCenterLongitude = this.ExplorationAreaCenterLongitude,
					ExplorationAreaRadius = this.ExplorationAreaRadius,
					ExplorationAreaShape = this.ExplorationAreaShape,
					ExplorationZoneIsActivated = this.ExplorationAreaEnabled
				});

				this.NavigationManager.NavigateTo(FieldTripEditionPage.GetEditionUrl(this.FieldTripId));
			}
			catch (BusinessException ex)
			{
				Console.WriteLine(ex.Message);

				this.Notifier.Notify(Color.Danger, string.Join(Environment.NewLine, ex.Messages));
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(e.Message);
				Console.Error.WriteLine(e.StackTrace);
			}

			this.IsSavingPosition = false;
			this.StateHasChanged();
		}

		protected async Task SearchLocationAsync()
		{
			if (this.IsSearchingLocation || string.IsNullOrEmpty(this.LocalisationText))
			{
				return;
			}

			this.IsSearchingLocation = true;
			this.LocalisationResults = null;
			this.ShowLocationResults = false;
			this.StateHasChanged();

			this.LocalisationResults = await this.LocationRepository.GetGeoLocalisationResultsAsync(this.LocalisationText);
			this.ShowLocationResults = true;

			if (this.LocalisationResults.Count() == 1)
			{
				await this.SetMapCenterAsync(this.LocalisationResults.First().Latitude, this.LocalisationResults.First().Longitude);
			}

			this.IsSearchingLocation = false;

			this.StateHasChanged();
		}

		protected async Task SetMapCenterAsync(decimal latitude, decimal longitude)
		{
			this.MapCenter = new LeafletCoordinates
			{
				Latitude = latitude,
				Longitude = longitude
			};

			await Task.CompletedTask;

			this.StateHasChanged();

			await Task.CompletedTask;
		}

		protected async Task UpdatePoiOrderAsync(LocatedGameUnit target, object droppedItem)
		{
			Console.WriteLine(droppedItem.GetType().Name);

			if (droppedItem is LocatedGameUnit unit)
			{
				var orderedList = this.FieldTrip.LocatedGameUnits.OrderBy(o => o.Order).ToList();

				var targetIndex = orderedList.IndexOf(target);
				var unitIndex = orderedList.IndexOf(unit);
				orderedList.RemoveAt(unitIndex);

				orderedList.Insert(targetIndex, unit);

				for (int index = 0; index < this.FieldTrip.LocatedGameUnits.Count; index++)
				{
					orderedList[index].Order = index;
				}

				this.FieldTrip.LocatedGameUnits = orderedList;

				this.StateHasChanged();
			}

			await Task.CompletedTask;
		}

		protected async Task UpdatePoiPositionAsync(LocatedGameUnit unit, LeafletCoordinates coordinates)
		{
			this.EditedUnit.Latitude = coordinates.Latitude;
			this.EditedUnit.Longitude = coordinates.Longitude;

			await Task.CompletedTask;

			this.StateHasChanged();
		}

		private void SetCircleActivationArea(CircleData leafletCircle, LocatedGameUnit unit)
		{
			try
			{
				unit.ActivationArea = leafletCircle.Polygon;
				unit.ActivationAreaCenterLatitude = leafletCircle.Center.Latitude;
				unit.ActivationAreaCenterLongitude = leafletCircle.Center.Longitude;
				unit.ActivationRadius = leafletCircle.Radius;
				unit.ActivationAreaType = ActivationAreaType.Circle;
				this.EditActivationArea = true;
				this.StateHasChanged();

				Console.WriteLine("SetCircleActivationArea set");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}

		public void DisplayContextualHelpModal(string contextualHelpId)
		{
			this.ContextualHelpIdToShow = contextualHelpId;
			this.ShowContextualHelpModal = true;
			this.StateHasChanged();
		}

		public void CloseContextualHelpModal()
		{
			this.ShowContextualHelpModal = false;
			this.ContextualHelpIdToShow = null;
			this.StateHasChanged();
		}

		public bool ExplorationAreaEnabled { get; set; }
		public Geometry ExplorationArea { get; set; }
		public ActivationAreaType ExplorationAreaShape { get; set; }
		public decimal? ExplorationAreaCenterLatitude { get; set; }
		public decimal? ExplorationAreaCenterLongitude { get; set; }
		public decimal ExplorationAreaRadius { get; set; }

		public Tabs Tabs { get; set; }
		public void ToggleExplorationAreaActivation(bool val)
		{
			this.ExplorationAreaEnabled = !this.ExplorationAreaEnabled;

			if (val)
			{
				this.Tabs.SetActiveTabByName(ExplorationTabName);
				this.ExplorationAreaTab.NavStyle = null;
			}
			else
			{
				this.Tabs.SetActiveTabByName(PoiTabName);
				this.ExplorationAreaTab.NavStyle = "display: none;";
			}

			this.StateHasChanged();
		}

        private async void onChange(Microsoft.AspNetCore.Components.ChangeEventArgs args)
        {
			if (!this.MapIsGeolocalized && this.FieldTrip.LocatedGameUnits.Any(u => u.LocationAidType == LocationAidType.MapWithGps || u.LocationAidType == LocationAidType.GpsRadar || u.LocationValidationType == LocationValidationType.GpsLocation))
			{
				var POIsWithGPS = this.FieldTrip.LocatedGameUnits
					.Where(u => u.LocationAidType == LocationAidType.MapWithGps || u.LocationAidType == LocationAidType.GpsRadar || u.LocationValidationType == LocationValidationType.GpsLocation)
					.Select(u => u.Name);


                if (!await Confirm.AskAsync($"Cette sortie contient des POI dont le mode d'orientation est \"Carte avec GPS\" ou \"Radar GPS\", ou dont la validation est \"Proximit� GPS\" ({ string.Join(", ", POIsWithGPS) }) : �tes-vous s�r de vouloir rendre la carte non g�olocalisable malgr� tout ?"))
				{
					this.MapIsGeolocalized = true;
				}				
			}

			if (!this.MapIsGeolocalized && this.ExplorationAreaEnabled)
			{
				this.ToggleExplorationAreaActivation(false);
            }
        }
    }
}