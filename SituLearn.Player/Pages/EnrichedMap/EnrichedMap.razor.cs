// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Entities;
using SituLearn.Player.Repositories;
using System;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Web.Razor.Leaflet;
using System.Text.Json;
using System.Linq;
using SituLearn.Player.Pages.FieldTrips;

namespace SituLearn.Player.Pages.EnrichedMap
{
    public partial class EnrichedMap : ComponentBase
    {
        [Parameter]
        public FieldTrip FieldTrip { get; set; }

        [Parameter]
        public bool ShowEnrichedMap { get; set; }

        [Parameter]
        public Action OnClosing { get; set; }

        [Parameter]
        public bool IsAlwaysVisible { get; set; }

        [Parameter]
        public string MapClass { get; set; }

        [Parameter]
        public bool DisableInteractions { get; set; } = false;

        [Parameter]
        public bool TrackUser { get; set; } = false;

        [CascadingParameter]
        public AppLayout Confirm { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        [Inject]
        public IConfiguration Configuration { get; set; }

        [Inject]
        private IJSRuntime JSRuntime { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        public string BasePictureUrl { get; set; }

        private CustomEventInterop Interop { get; set; }

        public LeafletMap Map { get; set; }

        public bool ShowInformationSheet { get; set; }

        public LocatedGameUnit UnitInformationSheetToShow { get; set; }

        public bool ShowPlayedReadingActivity { get; set; }

        public PlayedActivity PlayedReadingActivityToShow { get; set; }

        public Entities.Player PlayerToDisplay { get; set; }

        public bool Rerender { get; set; } = true;

        public LeafletCoordinates MapCenter { get; set; }

        private System.Timers.Timer Timer;

        public bool IsStartingUnit { get; set; }

        public bool GameIsInProgress { get; set; }
        public Journey CurrentJourney { get; set; }

        protected override async Task OnInitializedAsync()
        {
            //this.PlayerToDisplay = this.GamesRepository.CachedGames.Values.FirstOrDefault(g => g.FielTripId == this.FieldTrip.Id)
            //    .Players.FirstOrDefault(p => p.UserId == this.UsersRepository.CurrentUser?.Id);

            if (this.GamesRepository.IsOnDiscoveryMode)
            {
                (_, this.PlayerToDisplay) = (this.GamesRepository.CurrentGame, this.GamesRepository.CurrentPlayer);
            }
            else
            {
                (_, this.PlayerToDisplay) = await this.GamesRepository.GetPlayerForUserByFieldTripIdAsync(this.FieldTrip.Id, this.UsersRepository.CurrentUser.Id);
            }
            
            this.GameIsInProgress = this.GamesRepository.CurrentPlayer != null && this.GamesRepository.CurrentPlayer.Id == this.PlayerToDisplay.Id && this.GamesRepository.CurrentPlayer.PlayerState == Common.PlayerState.Playing;

            if (this.GamesRepository.CurrentPlayer != null && this.FieldTrip != null && this.FieldTrip.Journeys != null)
            {
                this.CurrentJourney = this.FieldTrip.Journeys.FirstOrDefault(j => j.Id == this.GamesRepository.CurrentPlayer.JourneyId);
            }

            if (this.GameIsInProgress)
            {
                this.BasePictureUrl = "";
            }
            else
            {
                this.BasePictureUrl = this.Configuration["ServerUrl"] + "files?path=";
            }

            if (this.IsAlwaysVisible)
            {
                this.ShowEnrichedMap = true;
            }
            
            await base.OnInitializedAsync();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender)
            {
                return;
            }

            await CommonScriptLoader.EnsureOzytisCommonIsPresentAsync(this.JSRuntime);

            this.Interop = new(JSRuntime);

            await this.Interop.SetupCustomEventCallback(args => DisplayInformationSheetModal(args), "display-information-sheet", "{\"Coordinates\": { \"Latitude\": {0}, \"Longitude\": {1} }, \"UnitId\": \"{2}\" }", new string[] { "lat", "lng", "unitId" });
            await this.Interop.SetupCustomEventCallback(args => DisplayPlayedReadingActivityModal(args), "display-played-reading-activity", "{\"Coordinates\": { \"Latitude\": {0}, \"Longitude\": {1} }, \"PlayedReadingActivityId\": \"{2}\" }", new string[] { "lat", "lng", "playedReadingActivityId" });
            await this.Interop.SetupCustomEventCallback(args => PlayUnitAsync(args), "play-unit", "{\"Coordinates\": { \"Latitude\": {0}, \"Longitude\": {1} }, \"UnitId\": \"{2}\" }", new string[] { "lat", "lng", "unitId" });
        }

        protected override async Task OnParametersSetAsync()
        {
            if (this.Rerender && this.FieldTrip != null)
            {
                if (this.FieldTrip.ExplorationMap?.CenterLatitude != null)
                {
                    this.MapCenter = new LeafletCoordinates
                    {
                        Latitude = this.FieldTrip.ExplorationMap.CenterLatitude.Value,
                        Longitude = this.FieldTrip.ExplorationMap.CenterLongitude.Value,
                    };
                }

                this.StateHasChanged();

                this.Timer = new System.Timers.Timer(1000);
                this.Timer.Elapsed += async (sender, args) =>
                {
                    if (this.FieldTrip.ExplorationMap.NorthEastLatitude.HasValue)
                    {
                        var bounds = new Bounds
                        {
                            NorthEast = new()
                            {
                                Latitude = this.FieldTrip.ExplorationMap.NorthEastLatitude.Value,
                                Longitude = this.FieldTrip.ExplorationMap.NorthEastLongitude.Value,
                            },
                            SouthWest = new()
                            {
                                Longitude = this.FieldTrip.ExplorationMap.SouthWestLongitude.Value,
                                Latitude = this.FieldTrip.ExplorationMap.SouthWestLatitude.Value,
                            }
                        };

                        await this.Map.FitBoundsAsync(bounds);
                        await this.Map.SetMaxBoundsAsync(bounds);
                        await this.Map.RefreshAsync();
                    }
                };

                this.Timer.AutoReset = false;
                this.Timer.Enabled = true;

                this.Rerender = false;
            }

            await Task.CompletedTask;
        }

        public async Task DisplayInformationSheetModal(object args)
        {
            DisplayInformationSheetEventArgs mapArgs = JsonSerializer.Deserialize<DisplayInformationSheetEventArgs>(args.ToString());

            if (!mapArgs.UnitId.HasValue)
            {
                return;
            }

            if (mapArgs != null)
            {
                this.UnitInformationSheetToShow = this.FieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == mapArgs.UnitId);

                if (this.UnitInformationSheetToShow != null)
                {
                    this.ShowInformationSheet = true;
                    this.StateHasChanged();
                }
            }

            await Task.CompletedTask;
        }

        public void CloseInformationSheetModal()
        {
            this.ShowInformationSheet = false;
            this.UnitInformationSheetToShow = null;
            this.StateHasChanged();
        }

        public async Task DisplayPlayedReadingActivityModal(object args)
        {
            DisplayPlayedReadingActivityEventArgs mapArgs = JsonSerializer.Deserialize<DisplayPlayedReadingActivityEventArgs>(args.ToString());

            if (!mapArgs.PlayedReadingActivityId.HasValue)
            {
                return;
            }

            if (mapArgs != null)
            {
                this.PlayedReadingActivityToShow = this.PlayerToDisplay.PlayedUnits
                    .Where(pu => pu.LocalisationReachDate.HasValue && pu.PlayedActivities != null)
                    .SelectMany(pu => pu.PlayedActivities)
                    .FirstOrDefault(pa => pa.Id == mapArgs.PlayedReadingActivityId);

                if (this.PlayedReadingActivityToShow != null)
                {
                    this.ShowPlayedReadingActivity = true;

                    this.StateHasChanged();
                }
            }

            await Task.CompletedTask;
        }

        public void ClosePlayedReadingActivityModal()
        {
            this.ShowPlayedReadingActivity = false;
            this.PlayedReadingActivityToShow = null;
            this.StateHasChanged();
        }

        private async Task PlayUnitAsync(object args)
        {
            PlayUnitEventArgs mapArgs = JsonSerializer.Deserialize<PlayUnitEventArgs>(args.ToString());

            if (!mapArgs.UnitId.HasValue)
            {
                return;
            }

            if (mapArgs != null)
            {
                var unit = this.FieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == mapArgs.UnitId);

                var answer = await Confirm.AskAsync($"Souhaitez vous explorer l'unit� de jeu {unit.Name} ?", "Oui", "Non");

                if (answer.HasValue && answer.Value)
                {
                    this.IsStartingUnit = true;
                    this.StateHasChanged();

                    await this.GamesRepository.StartUnitAsync(unit);
                    this.NavigationManager.NavigateTo(LocatedGameUnitPage.GetUrl(unit.Id));

                    this.IsStartingUnit = false;
                    this.StateHasChanged();
                }
            }
        }

    }
}

public class DisplayInformationSheetEventArgs : MapEventArgs
{
    public Guid? UnitId { get; set; }
}

public class DisplayPlayedReadingActivityEventArgs : MapEventArgs
{
    public Guid? PlayedReadingActivityId { get; set; }
}

public class PlayUnitEventArgs : MapEventArgs
{
    public Guid? UnitId { get; set; }
}