﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Entities;
using Ozytis.Common.Core.Utilities;
using System;
using System.Linq;
using System.Threading.Tasks;
using Web.Client.Pages.FieldTrips.Activities;
using Web.Client.Pages.FieldTrips.LocatedGameUnits;
using Web.Client.Repositories;

namespace Web.Client.Shared
{
	public partial class SiteMap : ComponentBase
    {
        [Parameter]
        public FieldTrip FieldTrip { get; set; }

        [Parameter]
        public Type CurrentPageType { get; set; }

        [Parameter]
        public Guid? CurrentItemId { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

		[Inject]
		public FieldTripsRepository FieldTripsRepository { get; set; }

		[Inject]
		public ActivitiesRepository ActivitiesRepository { get; set; }

		[Parameter]
        public Func<Guid, Task> RefreshActivity { get; set; }

        [Parameter]
        public Func<Guid, Task> RefreshGameUnit { get; set; }

		[Parameter]
		public Func<Guid, Task> OnUnitDeleted { get; set; }

		[Parameter]
        public Func<Guid, Task> OnActivityDeleted { get; set; }

        [CascadingParameter]
        public ComponentBase ParentComponent { get; set; }

		[CascadingParameter]
		public INotifier Notifier { get; set; }

		[CascadingParameter]
		public IConfirmProvider Confirm { get; set; }

		public bool IsDeleting { get; set; }

		public async Task NextActivity(Guid gameUnitId, Guid activityId)
        {
            this.NavigationManager.NavigateTo(ActivityEdition.GetEditionUrl(this.FieldTrip.Id, gameUnitId, activityId));
            await this.RefreshActivity?.Invoke(activityId);
        }

        public async Task NextGameUnit(Guid gameUnitId)
        {
            this.NavigationManager.NavigateTo(LocatedGameUnitEdition.GetUrl(this.FieldTrip.Id, gameUnitId));
            await this.RefreshGameUnit?.Invoke(gameUnitId);
		}

		protected async Task DeleteUnitAsync(LocatedGameUnit unit)
		{
			if (this.IsDeleting || !await Confirm.AskAsync($"Etes-vous sûr de vouloir supprimer le point {unit.Name} ? Cette action est irreversible"))
			{
				return;
			}

			this.IsDeleting = true;
			this.StateHasChanged();

			try
			{
				await this.FieldTripsRepository.DeleteLocatedUnitAsync(unit.FieldTripId, unit.Id);

				if (this.OnUnitDeleted != null)
				{
					await this.OnUnitDeleted.Invoke(unit.Id);
				}

				this.FieldTrip.LocatedGameUnits.Remove(unit);
			}
			catch (BusinessException ex)
			{
				this.Notifier.Notify(Color.Danger, ex.Message);
			}

			this.IsDeleting = false;
			this.StateHasChanged();
		}

		public async Task DeleteActivityAsync(LocatedActivity activity, Guid unitId)
		{
			if (this.IsDeleting || !await Confirm.AskAsync($"Etes-vous sûr de vouloir supprimer l'activité {activity.Name} ? Cette action est irreversible"))
			{
				return;
			}

			this.IsDeleting = true;
			this.StateHasChanged();

			try
			{
				await this.ActivitiesRepository.DeleteAsync(activity);

				if (this.OnActivityDeleted != null)
                {
                    await this.OnActivityDeleted.Invoke(activity.Id);
                }
				
				this.FieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == unitId).Activities.Remove(activity);
			}
			catch (BusinessException ex)
			{
				this.Notifier.Notify(Color.Danger, ex.Message);
			}

			this.IsDeleting = false;
			this.StateHasChanged();
		}
	}
}
