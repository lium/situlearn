// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using Web.Client.Repositories;
using Entities;
using Api;
using Common;
using Ozytis.Common.Core.Utilities;

namespace Web.Client.Pages.FieldTrips
{
    public partial class PoiEdition : ComponentBase
    {
        [Parameter]
        public LocatedGameUnit Unit { get; set; }

        [Parameter]
        public EventCallback<LocatedGameUnit> OnUpdated { get; set; }

        [Parameter]
        public EventCallback OnCancel { get; set; }

        [Parameter]
        public EventCallback<ActivationAreaType> OnActivationAreaTypeChange { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [Parameter]
        public bool MapIsGeolocalized { get; set; }

        public string[] Errors { get; set; }

        public bool IsProcessing { get; set; }

        public LocatedUnitUpdateInfoModel Model { get; set; } = new();

        public bool IsInitialized { get; set; }

        protected override void OnInitialized()
        {
            this.Model.FieldTripId = this.Unit.FieldTripId;
            this.Model.Latitude = this.Unit.Latitude;
            this.Model.Longitude = this.Unit.Longitude;
            this.Model.Name = this.Unit.Name;
            this.Model.Id = this.Unit.Id;
            this.Model.ActivationArea = this.Unit.ActivationArea;
            this.Model.ActivationAreaType = this.Unit.ActivationAreaType;
            this.Model.ActivationAreaCenterLatitude = this.Unit.ActivationAreaCenterLatitude;
            this.Model.ActivationAreaCenterLongitude = this.Unit.ActivationAreaCenterLongitude;
            this.Model.ActivationRadius = this.Unit.ActivationRadius;

            this.IsInitialized = true;
            this.StateHasChanged();
        }

        protected async Task OnAreaActivationTypeChangedAsync(ActivationAreaType newType)
        {
            if (this.Model.ActivationAreaType != newType)
            {
                this.Model.ActivationAreaType = newType;
                await this.OnActivationAreaTypeChange.InvokeAsync(newType);
                this.StateHasChanged();
            }
        }

        protected override void OnParametersSet()
        {
            if (this.IsInitialized)
            {
                this.Model.Longitude = this.Unit.Longitude;
                this.Model.Latitude = this.Unit.Latitude;
                this.Model.ActivationArea = this.Unit.ActivationArea;
                this.Model.ActivationAreaType = this.Unit.ActivationAreaType;
                this.Model.ActivationRadius = this.Unit.ActivationRadius;
                this.Model.ActivationAreaCenterLongitude = this.Unit.ActivationAreaCenterLongitude;
                this.Model.ActivationAreaCenterLatitude = this.Unit.ActivationAreaCenterLatitude;
                this.StateHasChanged();
            }
        }

        protected async Task ProcessAsync()
        {
            if (this.IsProcessing)
            {
                return;
            }

            this.IsProcessing = true;
            this.StateHasChanged();

            try
            {
                var unit = await this.FieldTripsRepository.UpdateLocatedUnitInfoAsync(this.Model);
                await this.OnUpdated.InvokeAsync(unit);
            }
            catch (BusinessException ex)
            {
                this.Notifier.Notify(Color.Danger, ex.Message ?? String.Join(", ", ex.Messages));
                this.Errors = ex.Messages;
            }

            this.IsProcessing = false;
            this.StateHasChanged();
        }
    }
}