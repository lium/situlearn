﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class SmsManager
    {
        public SmsManager(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "Utilisera des paramètres d'instance plus tard")]
        public async Task SendAsync(string number, string message)
        {
            var cleanedSpecificMessage = SmsManager.ClearStringForSMS(message);

            var uriEscapedMessage = Uri.EscapeDataString(cleanedSpecificMessage.Trim());

            var url = "https://sendoz.azurewebsites.net/api/sms/publicsms";

            var model = new PublicSendSmsModel
            {
                Text = cleanedSpecificMessage.Trim(),
                ApiToken = Guid.Parse(Configuration["Data:SendOzToken"]),
                Priority = 1,
                PhoneNumbers = new SmsNumberModel[]
                {
                        new SmsNumberModel
                        {
                            Number = number
                        }
                }
            };

            try
            {
                string json = JsonConvert.SerializeObject(model);

                using HttpClient client = new HttpClient();
                var response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));

                Console.WriteLine("sms envoyé " + json);
                Console.WriteLine($"Réponse {response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            catch (WebException e)
            {
                Console.WriteLine("Erreur dans l'envoi de SMS : {0}", e);
                throw;
            }
        }

        public static string ClearStringForSMS(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return text;
            }

            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);

                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public class PublicSendSmsModel
        {
            public SmsNumberModel[] PhoneNumbers { get; set; }

            public Guid ApiToken { get; set; }

            public string Text { get; set; }

            public int Priority { get; set; }
        }

        public class SmsNumberModel
        {
            public string Number { get; set; }

            public string Name { get; set; }
        }
    }
}
