﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

let mediaRecorder;
let chunks = [];
let data;
let audioStream;
let canvas;
let audioCtx;
let canvasCtx;
let mimeType;

async function authorizeAudioRecording() {
    if (navigator.mediaDevices.getUserMedia) {
        let response = false;

        const constraints = { audio: true };

        let onSuccess = async function (stream) {
            audioStream = stream;
            response = true;
            mediaRecorder = new MediaRecorder(audioStream);

            mediaRecorder.onstop = async function (e) {
                mimeType = chunks[0].type;
                const blob = new Blob(chunks, { 'type': chunks[0].type });

                chunks = [];

                data = new Uint8Array(await blob.arrayBuffer());

                const serviceWorkerActivatedEvent = new Event("audioRecordingAvailable");
                self.dispatchEvent(serviceWorkerActivatedEvent);

                audioStream.getAudioTracks()[0].stop();
            }

            mediaRecorder.ondataavailable = function (e) {
                chunks.push(e.data);
            }
        }

        let onError = function (err) {
            response = false;
        }

        await navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onError);
        return response;

    } else {
        return false;
    }
}

async function startAudioRecording() {
    mediaRecorder.start();
    canvas = document.querySelector('.visualizer');
    canvasCtx = canvas.getContext("2d");

    visualize(audioStream);
}

async function stopAudioRecording() {
    if (mediaRecorder.state != "inactive") {
        mediaRecorder.stop();
    } else {
        audioStream.getAudioTracks()[0].stop();
    }
    
}

function visualize(stream) {
    if (!audioCtx) {
        audioCtx = new AudioContext();
    }

    const source = audioCtx.createMediaStreamSource(stream);

    const analyser = audioCtx.createAnalyser();
    analyser.fftSize = 2048;
    const bufferLength = analyser.frequencyBinCount;
    const dataArray = new Uint8Array(bufferLength);

    source.connect(analyser);

    draw();

    function draw() {
        const WIDTH = canvas.width
        const HEIGHT = canvas.height;

        requestAnimationFrame(draw);

        analyser.getByteTimeDomainData(dataArray);

        canvasCtx.fillStyle = 'rgb(16, 81, 117)';
        canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);

        canvasCtx.lineWidth = 2;
        canvasCtx.strokeStyle = 'rgb(28, 204, 198)';

        canvasCtx.beginPath();

        let sliceWidth = WIDTH * 1.0 / bufferLength;
        let x = 0;


        for (let i = 0; i < bufferLength; i++) {

            let v = dataArray[i] / 128.0;
            let y = v * HEIGHT / 2;

            if (i === 0) {
                canvasCtx.moveTo(x, y);
            } else {
                canvasCtx.lineTo(x, y);
            }

            x += sliceWidth;
        }

        canvasCtx.lineTo(canvas.width, canvas.height / 2);
        canvasCtx.stroke();

    }
}

window.registerForAudioRecordingAvailability = (caller, methodName) => {
    window.addEventListener('audioRecordingAvailable', event => {
        caller.invokeMethodAsync(methodName, data, mimeType);
    });
};