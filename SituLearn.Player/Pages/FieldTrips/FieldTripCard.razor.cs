// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;

using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using SituLearn.Player.Repositories;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace SituLearn.Player.Pages.FieldTrips
{
	public partial class FieldTripCard : ComponentBase
	{
		[Parameter]
		public FieldTripHeaderModel FieldTrip { get; set; }

		[Parameter]
		public EventCallback OnChanged { get; set; }

        [Parameter]
        public bool IsOnDiscoveryMode { get; set; }

        [CascadingParameter]
		public INotifier Notifier { get; set; }
		[CascadingParameter]
		public AppLayout Confirm { get; set; }

		[Inject]
		public GamesRepository GamesRepository { get; set; }

		[Inject]
		public IConfiguration Configuration { get; set; }

		[Inject]
		public NavigationManager NavigationManager { get; set; }

		[Inject]
		public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public UsersRepository UsersRepository { get; set; }

        public string BasePictureUrl { get; set; }

		public bool IsProcessing { get; set; }

		public int ShowResume { get; set; }

		public bool IsDownloadingFieldTrip { get; set; }

		public IConnectivityService ConnectivityService { get; }

		public bool OffLineNotDownloaded { get; private set; }

		public bool OffLineAddDownload { get; private set; }

		public bool OffLineSupDownload { get; private set; }

		public bool OffLineFavorites { get; private set; }

        [Parameter]
		public EventCallback<Guid> OnDownloaded { get; set; }

        protected override void OnInitialized()
		{
			this.BasePictureUrl = "";
		}

		protected async Task AddOrRemoveFromFavoritesAsync()
		{
			if (this.IsProcessing)
			{
				return;
			}

            if (!this.FieldTripsRepository.ConnectivityService.IsOnline)
            {
                this.OffLineFavorites = true;
                return;
            }

            this.IsProcessing = true;
			this.StateHasChanged();
			try
			{
				await this.FieldTripsRepository.AddOrRemoveFromFavoritesAsync(this.FieldTrip.Id);
				this.FieldTrip.IsFavorite = !this.FieldTrip.IsFavorite;

				await this.OnChanged.InvokeAsync();
			}
			catch (BusinessException)
			{
				this.Notifier.Notify(Color.Danger, "Erreur lors de la mise � jour des donn�es");
			}

            this.IsProcessing = false;
			this.StateHasChanged();
		}

		//V�rifie le nombre de parcours et redirige vers la page adapt�e
		public async Task StartFieldTripAsync()
		{
			if (!this.IsOnDiscoveryMode)
			{
                //var existingPlayer = this.GamesRepository.CachedGames?.Values.FirstOrDefault(g => g.FielTripId == this.FieldTrip.Id)?.Players.FirstOrDefault(p => p.UserId == this.UsersRepository.CurrentUser.Id);
                var (game, existingPlayer) = await this.GamesRepository.GetPlayerForUserByFieldTripIdAsync(this.FieldTrip.Id, this.UsersRepository.CurrentUser.Id);

                //Si le joueur n'est pas connect� et n'a pas t�l�charg� la sortie
                if (!this.FieldTripsRepository.ConnectivityService.IsOnline && !this.FieldTrip.IsDownloaded)
                {
                    this.OffLineNotDownloaded = true;
                    return;
                }

                //Si le joueur a d�j� une partie en cours sur cette sortie
                if (existingPlayer != null && (existingPlayer.PlayerState == Common.PlayerState.Playing || existingPlayer.PlayerState == Common.PlayerState.Cancelled))
                {
                    var answer = await Confirm.AskAsync("Voulez vous reprendre votre partie ou recommencer (en perdant points, relev�s etc.) ?", "Reprendre", "Recommencer");

                    if (answer.HasValue)
                    {
                        if (answer.Value) // reprendre
                        {
                            this.ResumeJourney(game, existingPlayer);
                            return;
                        }
                        else //recommencer
                        {
                            await this.GamesRepository.DeleteElapsedTimeAsync(this.FieldTrip.Id);
                        }
                    }
                    else //Modale ferm�e sans r�ponse
                    {
                        return;
                    }
                }

                //Si le joueur a d�j� une partie termin�e sur cette sortie
                if (existingPlayer != null && existingPlayer.PlayerState == Common.PlayerState.Complete)
                {
                    var answer = await Confirm.AskAsync("Vous avez d�j� effectu� cette sortie : voulez-vous recommencer une partie (en perdant points, relev�s etc. de la pr�c�dente partie) ?");

                    if (!answer.HasValue || !answer.Value)
                    {
                        return;
                    }
                }
            }			

			this.NavigateToFieldTrip();
		}

		protected void ValidateModalOffLineNotDownloaded()
		{
			this.OffLineNotDownloaded = !this.OffLineNotDownloaded;
			this.StateHasChanged();
		}

		private async void ResumeJourney(Entities.Game game, Entities.Player playerToResume)
		{
			await this.GamesRepository.ResumePlayer(game, playerToResume);

			if (this.GamesRepository.CurrentPlayer.CurrentUnitId.HasValue)
			{
				this.NavigationManager.NavigateTo(LocatedGameUnitPage.ResumeUrl);
			}
			else if (this.FieldTrip.Type == Common.FieldTripType.TreasureHunt)
			{
				this.NavigationManager.NavigateTo(EndGamePage.GetUrl(this.GamesRepository.CurrentPlayer.Id));
			}
			else
			{
				this.NavigationManager.NavigateTo(InteractiveWalkOrActivitiesHubPage.ResumeUrl);
			}
		}

		private void NavigateToFieldTrip()
		{
			if (this.FieldTrip.AllowTeamNames && !this.IsOnDiscoveryMode)
			{
				this.NavigationManager.NavigateTo(TeamCreationPage.GetUrl(this.FieldTrip.Id, this.FieldTrip.JourneysCounter, this.FieldTrip.JourneyIdFirst));
			}
			else if (this.FieldTrip.JourneysCounter == 1 && this.FieldTrip.JourneyIdFirst.HasValue)
			{
				this.NavigationManager.NavigateTo(JourneyPage.GetUrl(this.FieldTrip.Id, this.FieldTrip.JourneyIdFirst.Value));
			}
			else
			{
				this.NavigationManager.NavigateTo(FieldTripPage.GetUrl(this.FieldTrip.Id));
			}
		}

		public void SwitchDescriptionDisplay()
		{
            this.FieldTrip.ShowDescription = !this.FieldTrip.ShowDescription;
			this.StateHasChanged();
        }

		protected async Task DownloadFieltripAsync()
		{
			//Si le joueur n'est pas connect� et la sortie n'est pas t�l�charg�e
			if (!this.FieldTripsRepository.ConnectivityService.IsOnline && !this.FieldTrip.IsDownloaded)
			{
				this.OffLineAddDownload = true;
				return;
			}
			else if (!this.FieldTripsRepository.ConnectivityService.IsOnline && this.FieldTrip.IsDownloaded)
			{
				this.OffLineSupDownload = true;
				return;
			}

			if (this.IsDownloadingFieldTrip)
			{
				return;
			}

			this.IsDownloadingFieldTrip = true;
			this.StateHasChanged();

			string confirmText = this.FieldTrip.IsDownloaded ?
				$"�tes-vous s�r de vouloir supprimer les donn�es de la sortie \"{this.FieldTrip.Name}\" de vos t�l�chargements ?"
				: $"Voulez-vous t�l�charger les donn�es de la sortie \"{this.FieldTrip.Name}\" ?";

			var confirmation = await Confirm.AskAsync(confirmText);

			if (confirmation.HasValue && confirmation.Value)
			{
				try
				{
					await this.FieldTripsRepository.AddOrRemoveFromDownloadsAsync(this.FieldTrip.Id);
					this.FieldTrip.IsDownloaded = !this.FieldTrip.IsDownloaded;

					string succesMessage = this.FieldTrip.IsDownloaded ?
						$"Les donn�es de la sortie \"{this.FieldTrip.Name}\" ont bien �t� t�l�charg�es"
						: $"Les donn�es de la sortie \"{this.FieldTrip.Name}\" ont bien �t� supprim�es de vos t�l�chargements";

					this.Notifier.Notify(Color.Success, succesMessage);

					if (this.FieldTrip.IsDownloaded)
					{
						await this.OnDownloaded.InvokeAsync(this.FieldTrip.Id);
					}

					await this.OnChanged.InvokeAsync();
				}
				catch (Exception e)
				{
					string errorMessage = !this.FieldTrip.IsDownloaded ?
						$"Erreur lors du t�l�chargement des donn�es : {e.Message}"
						: $"Erreur lors de la suppression des donn�es : {e.Message}";

					this.Notifier.Notify(Color.Danger, errorMessage);
				}
			}

			
			this.IsDownloadingFieldTrip = false;
			this.StateHasChanged();
		}

		protected void ValidateModalOffLineAddDownload()
		{
			this.OffLineAddDownload = !this.OffLineAddDownload;
			this.StateHasChanged();
		}

		protected void ValidateModalOffLineSupDownload()
		{
			this.OffLineSupDownload = !this.OffLineSupDownload;
			this.StateHasChanged();
        }

        protected void ValidateModalOffLineFavorites()
        {
            this.OffLineFavorites = !this.OffLineFavorites;
            this.StateHasChanged();
        }
    }
}