// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SituLearn.Player.Repositories;
using Entities;
using Ozytis.Common.Core.Web.Razor.Interfaces;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.WebUtilities;
using System.Collections.Generic;

namespace SituLearn.Player.Pages.FieldTrips
{
    [Route(Url)]
    public partial class TeamCreationPage : ComponentBase
    {
        public const string Url = "/groupe/{fieldTripId:guid}/{journeysCounter:int}/{journeyIdFirst:guid?}";

        public static string GetUrl(Guid fieldTripId, int journeysCounter, Guid? journeyIdFirst)
        {
            return Url.Replace("{fieldTripId:guid}", fieldTripId.ToString()).Replace("{journeysCounter:int}", journeysCounter.ToString()).Replace("{journeyIdFirst:guid?}", journeyIdFirst.ToString());
        }

        [Parameter]
        public Guid FieldTripId { get; set; }

        [Parameter]
        public int JourneysCounter { get; set; }

        [Parameter]
        public Guid? JourneyIdFirst { get; set; }

        [SupplyParameterFromQuery]
        public string BackUrl { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [CascadingParameter]
        public AppLayout Layout { get; set; }

        public bool NotFound { get; set; }

        public FieldTrip FieldTrip { get; set; }

        public bool IsSubmitting { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId);

            if (this.FieldTrip == null)
            {
                this.NotFound = true;
            }

            this.StateHasChanged();
        }

        private void GoBack()
        {
            this.NavigationManager.NavigateTo(this.BackUrl ?? FieldTrips.FieldTripsPage.Url);
            this.GamesRepository.TeamName = null;
            this.GamesRepository.TeamPicture = null;
        }

        private void Validate()
        {
            if (this.JourneysCounter == 1 && this.JourneyIdFirst.HasValue)
            {
                this.NavigationManager.NavigateTo(JourneyPage.GetUrl(this.FieldTrip.Id, this.JourneyIdFirst.Value));
            }
            else
            {
                string url = QueryHelpers.AddQueryString(FieldTripPage.GetUrl(this.FieldTrip.Id), new Dictionary<string, string> { { "backUrl", this.NavigationManager.Uri } });
                this.NavigationManager.NavigateTo(url);
            }
        }

        protected async Task OnChangeTeamPicture(InputFileChangeEventArgs e)
        {
            string mimeType;

            if (e.File.Name.EndsWith(".jpeg") || e.File.Name.EndsWith(".jpg"))
            {
                mimeType = "image/jpeg";
            }
            else if (e.File.Name.EndsWith(".png"))
            {
                mimeType = "image/png";
            }
            else
            {
                this.Notifier.Notify(Color.Danger, "Vous ne pouvez enregistrer que des photos");

                this.StateHasChanged();

                return;
            }

            var file = await e.File.RequestImageFileAsync(mimeType, 512, 512);

            var buffer = new byte[file.Size];
            await file.OpenReadStream(maxAllowedSize: 50000000).ReadAsync(buffer);
            var dataUrl = "data:" + mimeType + $";base64,{Convert.ToBase64String(buffer)}";

            this.GamesRepository.TeamPicture = dataUrl;

            this.StateHasChanged();
        }

        protected async Task OnDeleteTeamPicture()
        {
            var confirmation = await this.Layout.AskAsync("�tes-vous s�r de vouloir supprimer votre photo ?");

            if (confirmation.HasValue && confirmation.Value)
            {
                this.GamesRepository.TeamPicture = null;
                this.StateHasChanged();
            }            
        }

    }
}