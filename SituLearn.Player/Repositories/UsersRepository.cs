﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using ClientApi;
using Entities;
using Ozytis.Common.Core.Utilities;
using SituLearn.Player.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace SituLearn.Player.Repositories
{
    public class UsersRepository
    {
        private const string ConnectAsTokenKey = "ctoken";
        private const string TokenKey = "token";
        private const string CurrentUserKey = "user";
        private readonly AccountsService accountsService = new AccountsService();

        public UsersRepository(OzytisAuthStateProvider stateProvider, LocalStorageService localStorageService)
        {
            this.StateProvider = stateProvider;
            this.LocalStorageService = localStorageService;
        }

        public event EventHandler OnCurrentUserUpdated;

        public async Task<UserModel> GetUserAsync(string userId)
        {
            UserModel result = await this.accountsService.GetUserAsync(userId);

            return result;
        }

        public UserModel CurrentUser { get; set; }

        public LocalStorageService LocalStorageService { get; }

        public OzytisAuthStateProvider StateProvider { get; }

        public Dictionary<string, UserModel> Users { get; set; } = new();

        public async Task InitializeAsync()
        {
            this.CurrentUser = await this.LocalStorageService.GetItemAsync<UserModel>(CurrentUserKey);
        }

        public async Task CancelConnectAs()
        {
            await this.LocalStorageService.SetItemAsync(UsersRepository.TokenKey, await this.LocalStorageService.GetItemAsync<string>(UsersRepository.ConnectAsTokenKey));
            await this.LocalStorageService.RemoveItemAsync(UsersRepository.ConnectAsTokenKey);

            BaseService.SetBearerToken(await this.LocalStorageService.GetItemAsync<string>(UsersRepository.TokenKey));
            await this.RefreshCurrentUserAsync();
            this.StateProvider.NotifyStateChanged(this.CurrentUser);
            this.OnCurrentUserUpdated?.Invoke(this, new EventArgs());
        }

        public async Task ConfirmEmailChangeAsync(EmailUpdateConfirmationModel model)
        {
            var result = await this.accountsService.ChangeEmailAsync(model);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }
        }

        public async Task ConfirmPhoneNumberChangeAsync(PhoneNumberConfirmationModel model)
        {
            OperationResult<object> result = await this.accountsService.ConfirmPhoneNumberChangeAsync(model);
            result.CheckIfSuccess();
        }


        public async Task LogInAuto(LoginResult loginResult)
        {
            this.CurrentUser = loginResult.User;

            await this.LocalStorageService.SetItemAsync(TokenKey, loginResult.AccessToken);
            await this.LocalStorageService.SetItemAsync(CurrentUserKey, loginResult.User);

            BaseService.SetBearerToken(loginResult.AccessToken);

            this.StateProvider.NotifyStateChanged(this.CurrentUser);
        }

        public async Task ClearLogin()
		{
            await this.LocalStorageService.RemoveItemAsync(TokenKey);
            await this.LocalStorageService.RemoveItemAsync(CurrentUserKey);

            BaseService.SetBearerToken(null);

            this.StateProvider.NotifyStateChanged(null);
        }

        public async Task ConnectAs(string userId)
        {
            await this.LocalStorageService.SetItemAsync(UsersRepository.ConnectAsTokenKey, await this.LocalStorageService.GetItemAsync<string>(UsersRepository.TokenKey));

            OperationResult<LoginResult> result = await this.accountsService.LoginAsAsync(new ConnectAsModel
            {
                UserId = userId
            });

            await this.LocalStorageService.SetItemAsync(UsersRepository.TokenKey, result.Data.AccessToken);

            BaseService.SetBearerToken(result.Data.AccessToken);

            await this.RefreshCurrentUserAsync();
            this.StateProvider.NotifyStateChanged(this.CurrentUser);
            this.OnCurrentUserUpdated?.Invoke(this, new EventArgs());
        }

        public async Task<TempTokenModel> GetTempTokenAsync()
        {
            return await this.accountsService.GetTempTokenAsync();
        }

        public async Task<UserModel> GetUserByEmailAsync(string email)
        {
            UserModel result = await this.accountsService.GetUserByEmailAsync(email);

            return result;
        }

        public async Task<bool> IsUserConnectedAsAsync()
        {
            return !string.IsNullOrEmpty(await this.LocalStorageService.GetItemAsync<string>(UsersRepository.ConnectAsTokenKey));
        }


        public async Task LoginAsync(string login, string password, bool rememberMe)
        {
            this.CurrentUser = null;

            OperationResult<LoginResult> result = await this.accountsService.LoginAsync(new LoginModel
            {
                Password = password,
                StayConnected = rememberMe,
                UserName = login
            }, true);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }

            this.CurrentUser = result.Data.User;
            
            await this.LocalStorageService.SetItemAsync(TokenKey, result.Data.AccessToken);
            await this.LocalStorageService.SetItemAsync(CurrentUserKey, result.Data.User);


            BaseService.SetBearerToken(result.Data.AccessToken);

            this.StateProvider.NotifyStateChanged(this.CurrentUser);
        }

        public async Task LogoutAsync()
        {
            BaseService.SetBearerToken(null);
            await this.LocalStorageService.RemoveItemAsync(TokenKey);
            await this.LocalStorageService.RemoveItemAsync(CurrentUserKey);
            this.CurrentUser = null;
            await this.accountsService.LogOutAsync();
            this.StateProvider.NotifyStateChanged(null);
        }

        public async Task RefreshCurrentUserAsync(bool emitEvent = false)
        {
            this.CurrentUser = await this.accountsService.GetCurrentUserAsync();

            if (emitEvent)
            {
                this.OnCurrentUserUpdated?.Invoke(this, new EventArgs());
            }
        }

        public async Task CreateUserAsync(UserCreationModel model)
        {
            OperationResult<object> result = await this.accountsService.CreateUserAsync(model);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }
        }

        public async Task RegisterAsync(UserCreationModel model)
        {
            OperationResult<object> result = await this.accountsService.RegisterAsync(model);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }
        }

        public async Task RequestEmailChangeAsync(EmailUpdateModel model)
        {
            OperationResult<object> result = await this.accountsService.RequestEmailChangeAsync(model);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }
        }

        public async Task<bool> RequestPhoneNumberChangeAsync(PhoneNumberUpdateModel model)
        {
            OperationResult<bool> result = await this.accountsService.RequestPhoneNumberChangeAsync(model);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }

            await this.RefreshCurrentUserAsync();

            return result.Data;
        }

        public async Task ResetPasswordAsync(string email, string token, string newPassword)
        {
            OperationResult<object> result = await this.accountsService.PasswordResetAsync(new PasswordResetModel
            {
                Email = email,
                Password = newPassword,
                Token = token
            });

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }
        }

        public async Task SendPasswordRecoveryAsync(string email)
        {
            OperationResult<SendPasswordIsLostResult> result = await this.accountsService.SendPasswordIsLostAsync(new SendPasswordIsLostModel { Email = email }, true);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }
        }

        public async Task UpdateCurrentUserAsync(UserUpdateModel updateModel)
        {
            OperationResult<UserModel> result = await this.accountsService.UpdateMyAccountAsync(updateModel);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }
            else
            {
                this.CurrentUser = result.Data;
                this.OnCurrentUserUpdated?.Invoke(this, new EventArgs());
            }
        }

        public async Task UpdatePasswordAsync(PasswordUpdateModel model)
        {
            OperationResult<object> result = await this.accountsService.UpdateMyPasswordAsync(model);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }
        }

        public async Task<IEnumerable<UserModel>> GetAllUsersAsync(Expression<Func<IQueryable<ApplicationUser>, IQueryable<ApplicationUser>>> query)
        {
            return await this.accountsService.GetAllUsersAsync(query);
        }

        public async Task UpdateUserAsync(UserUpdateModel updateModel)
        {
            OperationResult<UserModel> result = await this.accountsService.UpdateUserAsync(updateModel);

            if (!result.Success)
            {
                throw new BusinessException(result.Errors);
            }
        }

        public async Task DeleteUserAsync(string userId)
        {
            try
            {
                var result = await this.accountsService.DeleteUserAsync(userId);

                if (!result.Success)
                {
                    throw new BusinessException(result.Errors);
                }
            }
            catch (Exception e)
            {
                throw new BusinessException(e.Message);
            }
        }

        public async Task ResendInvitationalMail(string userEmail)
        {
            try
            {
                await this.accountsService.ResendInvitationalEmailAsync(WebUtility.UrlEncode(userEmail));
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}