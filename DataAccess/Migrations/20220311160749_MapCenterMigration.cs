﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class MapCenterMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "CustomBackgroundUpperLeftLongitude",
                table: "ExplorationMaps",
                type: "decimal(8,5)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomBackgroundUpperLeftLatitude",
                table: "ExplorationMaps",
                type: "decimal(8,5)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomBackgroundRotation",
                table: "ExplorationMaps",
                type: "decimal(5,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomBackgroundLowerRightLongitude",
                table: "ExplorationMaps",
                type: "decimal(8,5)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomBackgroundLowerRightLatitude",
                table: "ExplorationMaps",
                type: "decimal(8,5)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,5)");

            migrationBuilder.AddColumn<decimal>(
                name: "CenterLatitude",
                table: "ExplorationMaps",
                type: "decimal(8,5)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "CenterLongitude",
                table: "ExplorationMaps",
                type: "decimal(8,5)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Zoom",
                table: "ExplorationMaps",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CenterLatitude",
                table: "ExplorationMaps");

            migrationBuilder.DropColumn(
                name: "CenterLongitude",
                table: "ExplorationMaps");

            migrationBuilder.DropColumn(
                name: "Zoom",
                table: "ExplorationMaps");

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomBackgroundUpperLeftLongitude",
                table: "ExplorationMaps",
                type: "decimal(8,5)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,5)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomBackgroundUpperLeftLatitude",
                table: "ExplorationMaps",
                type: "decimal(8,5)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,5)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomBackgroundRotation",
                table: "ExplorationMaps",
                type: "decimal(5,2)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomBackgroundLowerRightLongitude",
                table: "ExplorationMaps",
                type: "decimal(8,5)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,5)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomBackgroundLowerRightLatitude",
                table: "ExplorationMaps",
                type: "decimal(8,5)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(8,5)",
                oldNullable: true);
        }
    }
}
