// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SituLearn.Player.Repositories;
using Entities;
using Microsoft.Extensions.Configuration;
using Ozytis.Common.Core.Utilities;
using Ozytis.Common.Core.Web.Razor.Interfaces;

namespace SituLearn.Player.Pages.FieldTrips
{
    [Route(Url)]
    public partial class JourneyPage : ComponentBase
    {
        public const string Url = "/jouer/{fieldTripId:guid}/{journeyId:guid}";

        public static string GetUrl(Guid fieldTripId, Guid journeyId)
        {
            return Url.Replace("{fieldTripId:guid}", fieldTripId.ToString()).Replace("{journeyId:guid}", journeyId.ToString());
        }

        [Parameter]
        public Guid FieldTripId { get; set; }

        [Parameter]
        public Guid JourneyId { get; set; }

        [SupplyParameterFromQuery]
        public string BackUrl { get; set; }

        public Journey Journey { get; set; }

        public bool ShowNotFound { get; set; }

        [Inject]
        public FieldTripsRepository FieldTripsRepository { get; set; }

        [Inject]
        public NavigationWithHistoryManager NavigationManager { get; set; }

        [Inject]
        public GamesRepository GamesRepository { get; set; }

        [Inject]
        public IConnectivityService ConnectivityService { get; set; }

        [CascadingParameter]
        public INotifier Notifier { get; set; }

        [CascadingParameter]
        public IConfirmProvider Confirm { get; set; }

        [CascadingParameter]
        public AppLayout Layout { get; set; }

        public bool TestEnv => this.Layout.TestEnv;

        public string BasePictureUrl { get; set; }

        public FieldTrip FieldTrip { get; set; }

        public bool IsSubmitting { get; set; }

        public bool DisplayPictureInFullScreen { get; set; }

        public string PictureUrlToDisplayInFullScreen { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.BasePictureUrl = "";

            this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId);

            this.Journey = this.FieldTrip?.Journeys.FirstOrDefault(f => f.Id == this.JourneyId);

            if (this.Journey == null)
            {
                this.Notifier.Notify(Color.Dark, "Ce parcours n'a pas �t� trouv�");

                this.ShowNotFound = true;
                this.StateHasChanged();
                return;
            }

            this.StateHasChanged();
        }

        protected async Task PlayAsync()
        {
            if (this.IsSubmitting)
            {
                return;
            }

            this.IsSubmitting = true;

            this.StateHasChanged();

            try
            {
                var game = await this.GamesRepository.StartGameAsync(this.FieldTrip, this.Journey);

                if (game == null)
                {
                    // pas de nouvelle unit� la partie est termin�e
                    this.NavigationManager.NavigateTo(EndGamePage.GetUrl(this.GamesRepository.CurrentPlayer.Id));
                    return;
                }

                if (this.FieldTrip.FieldTripType == Common.FieldTripType.TreasureHunt)
                {
                    //Console.WriteLine("Lancement partie : navigation vers page unit� de jeu");
                    this.NavigationManager.NavigateTo(LocatedGameUnitPage.Url);
                }
                else
                {
                    //Console.WriteLine("Lancement partie : navigation vers page balade interactive / hub d'activit�s");
                    this.NavigationManager.NavigateTo(InteractiveWalkOrActivitiesHubPage.Url);
                }                
            }
            catch (BusinessException ex)
            {
                this.Notifier.Notify(Color.Danger, ex.Message ?? String.Join(", ", ex.Messages));
            }
            catch (Exception ex)
            {
                Console.WriteLine("unknown error");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

            //Console.WriteLine("Lancement partie termin�");

            this.IsSubmitting = false;

            //this.StateHasChanged();

            return;
        }

        protected async Task ContinueToPlayAsync()
        {
            try
            {
                var unitId = this.GamesRepository.CurrentPlayer.CurrentUnitId;
                var unit = this.FieldTrip.LocatedGameUnits.FirstOrDefault(u => u.Id == unitId);
                if (unit == null)
                {
                    // pas de nouvelle unit� la partie est termin�e
                    this.NavigationManager.NavigateTo(EndGamePage.GetUrl(this.GamesRepository.CurrentPlayer.Id));
                    return;
                }

                await Task.CompletedTask;

                this.NavigationManager.NavigateTo(LocatedGameUnitPage.Url);
            }
            catch (BusinessException ex)
            {
                this.Notifier.Notify(Color.Danger, ex.Message ?? String.Join(", ", ex.Messages));
            }

            return;
        }

        public void DisplayFullScreenPicture(string pictureUrl)
        {
            this.DisplayPictureInFullScreen = true;
            this.PictureUrlToDisplayInFullScreen = pictureUrl;
            this.StateHasChanged();
        }

        public void CloseDisplayPictureInFullScreen()
        {
            this.DisplayPictureInFullScreen = false;
            this.PictureUrlToDisplayInFullScreen = null;
            this.StateHasChanged();
        }
    }
}