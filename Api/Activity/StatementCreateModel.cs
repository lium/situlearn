﻿// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Université du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI Sébastien GEORGE propriété Le Mans Université
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Activity
{
    public class StatementCreateModel : ICreateModel
    {
        #region interface implementation
        public Guid LocatedGameUnitId { get; set; }

        [MaxLength(100, ErrorMessage = "Nom limité à 100 caractères")]
        [Required(ErrorMessage = "Nom obligatoire")]
        public string Name { get; set; }

        [MaxLength(250, ErrorMessage = "Titre limité à 250 caractères")]
        public string Title { get; set; }

        [MaxLength(500, ErrorMessage = "L'énoncé comporte trop de caractères")]
        public string Statement { get; set; }

        public string Picture { get; set; }
        #endregion

        public bool AllowsNoteTaking { get; set; }

        public bool AllowsPhotos { get; set; }

        public bool AllowsAudioRecordings { get; set; }

        public bool ReadingsAreUnlimited { get; set; }
    }
}
