// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Http;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;
using SituLearn.Player;
using SituLearn.Player.Repositories;
using  static  Ozytis . Common . Core . Web . Razor . Utilities . BlazorHelper ;
using BlazorPro.BlazorSize;
using Ozytis.Common.Core.Web.Razor;
using Ozytis.Common.Core.Web.Razor.Layout;
using Ozytis.Common.Core.Web.Razor.Leaflet;
using Ozytis.Common.Core.Web.Razor.Utilities;
using Ozytis.Common.Core.Api;
using FontAwesomeIcons =  Ozytis . Common . Core . Web . Razor . FontAwesomeIcons ;
using Entities;
using Api;
using Syncfusion.Blazor.Inputs;
using Common;

namespace SituLearn.Player.Shared
{
    public partial class Modal : ComponentBase
    {
        [Parameter]
        public RenderFragment Content { get; set; }

        [Parameter]
        public Action OnClosing { get; set; }

        [Parameter]
        public string ActionText { get; set; } = "Action";

        [Parameter]
        public bool DisplayCancelButton { get; set; }

        [Parameter]
        public Action OnCancel { get; set; }
    }
}