// SituLearn is a set of tools to create fun and interactive educational field trips. It was developed as a research project of LIUM (Laboratoire d'Informatique de l'Universit� du Mans) in France.
//
// For more information, tutorials and to test a version of the SituLearn tools on our server, please go to https://situlearn.univ-lemans.fr/
//
// Contact : iza.marfisi@univ-lemans.fr
//
// Copyright (C) 2023 Iza MARFISI S�bastien GEORGE propri�t� Le Mans Universit�
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

using Api;
using Common;
using Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using Ozytis.Common.Core.Utilities;
using Syncfusion.Blazor.BarcodeGenerator;
using Syncfusion.Blazor.Calendars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Client.Repositories;

namespace Web.Client.Pages.FieldTrips
{
	[Route(CreationUrl)]
	[Route(EditionUrl)]
	public partial class FieldTripEditionPage : ComponentBase
	{
		public const string CreationUrl = "/fieldtrip/creation";
		public const string EditionUrl = "/fieldtrip/{fieldTripId:guid}";

		public static string GetEditionUrl(Guid fieldTripId)
		{
			return EditionUrl.Replace("{fieldTripId:guid}", fieldTripId.ToString());
		}

		[CascadingParameter]
		public INotifier Notifier { get; set; }

		[Parameter]
		public Guid? FieldTripId { get; set; }

		[Inject]
		public PedagogicalFieldsRepository PedagogicalFieldsRepository { get; set; }

		[Inject]
		public FieldTripsRepository FieldTripsRepository { get; set; }

		[Inject]
		public NavigationWithHistoryManager NavigationManager { get; set; }

		[Inject]
		public UsersRepository UsersRepository { get; set; }
		[CascadingParameter]
		public IConfirmProvider Confirm { get; set; }

		public bool CanEdit => this.UsersRepository.CurrentUser.Role == UserRoleNames.Administrator || (this.FieldTrip?.DesignerId == this.UsersRepository.CurrentUser.Id) || this.NavigationManager.Uri.EndsWith("creation");

		public bool IsUncomplete => string.IsNullOrEmpty(this.FieldTrip?.HomePageTitle) || string.IsNullOrEmpty(this.FieldTrip?.HomePageText)
			|| string.IsNullOrEmpty(this.FieldTrip?.EndPageTitle) || string.IsNullOrEmpty(this.FieldTrip?.EndPageText)
			|| (!this.FieldTrip?.LocatedGameUnits?.Any() ?? true);

		public FieldTripCreationModel Model { get; set; } = new();

		public IEnumerable<string> Errors { get; set; } = Array.Empty<string>();

		public bool IsSaving { get; set; }

		public FieldTrip FieldTrip { get; set; }

		public bool ActivitiesAreExpanded { get; set; } = true;

		public bool SiteMapIsExpanded { get; set; } = true;

		public bool GeneralIsExpanded { get; set; } = true;

		public bool OptionsAreExpanded { get; set; } = false;

		public bool ConceptorIsExpanded { get; set; } = true;

		public FieldTripMap Map { get; set; }

		public bool ShowContextualHelpModal { get; set; }

		public string ContextualHelpIdToShow { get; set; }

        public bool IsDuplicating { get; set; }

		[Inject]
		public IJSRuntime JSRuntime { get; set; }

		public async Task UpdateField(string fieldName, string value)
		{
			switch (fieldName)
			{
				case "name":
					this.Model.Name = value;
					break;
				case "home_title":
					(this.Model as FieldTripUpdateModel).HomePageTitle = value;
					break;
				case "home_text":
					(this.Model as FieldTripUpdateModel).HomePageText = value;
					break;
				case "home_picture":
					(this.Model as FieldTripUpdateModel).HomePagePicture = value;
					break;
				case "end_title":
					(this.Model as FieldTripUpdateModel).EndPageTitle = value;
					break;
				case "end_text":
					(this.Model as FieldTripUpdateModel).EndPageText = value;
					break;
				case "end_picture":
					(this.Model as FieldTripUpdateModel).EndPagePicture = value;
					break;
				default:
					return;
			}

            if (this.FieldTripId.HasValue)
			{
                await this.SaveAsync();
            }                

			this.FieldTrip.Name = this.Model.Name;
			this.StateHasChanged();
		}

		protected override async Task OnInitializedAsync()
		{
			await this.ReloadDataAsync();
		}

		public bool IsUpdatingPublishedState { get; set; }
		protected async Task UpdatePublishedStateAsync()
		{
			if (this.IsUpdatingPublishedState)
			{
				return;
			}

			this.IsUpdatingPublishedState = true;
			this.StateHasChanged();

			try
			{
				var result = await this.FieldTripsRepository.UpdatePublishStatusAsync(this.FieldTrip.Id);
				this.FieldTrip.IsPublished = result.IsPublished;
			}
			catch (BusinessException ex)
			{
				this.Notifier.Notify(Color.Danger, ex.Message);
			}

			this.IsUpdatingPublishedState = false;
			this.StateHasChanged();
		}

		public bool IsUpdatingVisibility { get; set; }
		protected async Task UpdateVisibilityAsync()
		{
			if (this.IsUpdatingVisibility)
			{
				return;
			}

			this.IsUpdatingVisibility = true;
			this.StateHasChanged();

			try
			{
				var result = await this.FieldTripsRepository.UpdateVisibilityAsync(this.FieldTrip.Id);
				this.FieldTrip.IsVisible = result.IsVisible;
			}
			catch (BusinessException ex)
			{
				this.Notifier.Notify(Color.Danger, ex.Message);
			}

			this.IsUpdatingVisibility = false;
			this.StateHasChanged();
		}

		protected async Task ReloadDataAsync()
		{
			if (this.FieldTripId.HasValue)
			{
				this.FieldTrip = await this.FieldTripsRepository.GetFieldTripAsync(this.FieldTripId.Value, false);

				this.Model = new FieldTripUpdateModel
				{
					AllottedTimeDuration = this.FieldTrip.AllottedTimeDuration,
					Id = this.FieldTrip.Id,
					Description = this.FieldTrip.Description,
					EndPagePicture = this.FieldTrip.EndPagePicture,
					EndPageText = this.FieldTrip.EndPageText,
					EndPageTitle = this.FieldTrip.EndPageTitle,
					FieldTripType = this.FieldTrip.FieldTripType,
					HomePagePicture = this.FieldTrip.HomePagePicture,
					HomePageText = this.FieldTrip.HomePageText,
					HomePageTitle = this.FieldTrip.HomePageTitle,
					IsPublished = this.FieldTrip.IsPublished,
					IsVisible = this.FieldTrip.IsVisible,
					Name = this.FieldTrip.Name,
					PedagogicalFieldName = this.FieldTrip.PedagogicalFieldName,
					//ReadingsAreVisible = this.FieldTrip.ReadingsAreVisible,
					//ReadingsVisibilityEndDate = this.FieldTrip.ReadingsVisibilityEndDate,
					//ReadingsVisibilityStartDate = this.FieldTrip.ReadingsVisibilityStartDate,
					TimeIsLimited = this.FieldTrip.TimeIsLimited,
					ShowChrono = this.FieldTrip.ShowElapsedTime,
					AllowTeamNames = this.FieldTrip.AllowTeamNames
				};

				this.StateHasChanged();
			}
		}

		protected async Task SaveAsync(bool isAutomatic = true)
		{
			Console.WriteLine("Saving");

			if (this.IsSaving)
			{
				return;
			}

			this.IsSaving = true;
			this.Errors = Array.Empty<string>();
			this.StateHasChanged();

			try
			{
				if (this.Model is FieldTripUpdateModel updateModel)
				{
					await this.FieldTripsRepository.UpdateFieldTripAsync(updateModel);
				}
				else
				{
					FieldTrip fieldTrip = await this.FieldTripsRepository.CreateFieldTripAsync(this.Model);

					if (fieldTrip == null)
					{
						throw new BusinessException("Erreur inconnue lors de la cr�ation");
					}

					this.FieldTripId = fieldTrip.Id;

					await this.ReloadDataAsync();

					this.NavigationManager.NavigateTo(GetEditionUrl(fieldTrip.Id));
				}
			}
			catch (BusinessException ex)
			{
				this.Errors = ex.Messages;
				this.Notifier.Notify(Color.Danger, "une erreur est survenue");
			}


			if (!this.Errors.Any() && !isAutomatic)
			{
				this.Notifier.Notify(Color.Success, "Modification enregistr�e");
			}

			this.IsSaving = false;
			this.StateHasChanged();
		}

		protected async Task OnLocatedGameUnitDropOnJourneyAsync(object item)
		{
			if (item is LocatedGameUnit unit)
			{
				await Confirm.AskAsync(unit.Name);
			}
		}

		public void DisplayContextualHelpModal(string contextualHelpId)
		{
			this.ContextualHelpIdToShow = contextualHelpId;
			this.ShowContextualHelpModal = true;
			this.StateHasChanged();
		}

		public void CloseContextualHelpModal()
		{
			this.ShowContextualHelpModal = false;
			this.ContextualHelpIdToShow = null;
			this.StateHasChanged();
		}

		public SfQRCodeGenerator QrCode { get; set; }

		public void ExportQrCode()
		{
			this.QrCode.Export($"sortie-{this.FieldTrip.Name}", BarcodeExportType.PNG);
		}

		[Inject]
		public IConfiguration Configuration { get; set; }

		//public async Task RemoveStatements()
		//{
		//	if (this.Model is FieldTripUpdateModel updateModel)
		//	{
		//		var isConfirmed = await Confirm.AskAsync($"�tes-vous s�r de supprimer tous les relev�s pour cette sortie ?");

		//		if (isConfirmed)
		//		{
		//			await this.FieldTripsRepository.RemoveStatements(this.FieldTripId.Value, DateTime.MinValue.Ticks, DateTime.MaxValue.Ticks);
		//		}
		//	}
		//}

		public async Task Duplicate()
        {
            if (this.IsDuplicating)
            {
                return;
            }

            this.IsDuplicating = true;
            this.StateHasChanged();

			try
			{
				if (this.FieldTripId.HasValue)
				{
                    var newTripId = await this.FieldTripsRepository.DuplicateFieldTrip(this.FieldTripId.Value);
                    this.NavigationManager.NavigateTo(FieldTripEditionPage.GetEditionUrl(newTripId), true);
                    this.Notifier.Notify(Color.Success, "La sortie a bien �t� dupliqu�e");
                }
				else
				{
                    Console.Write("Fieldtrip duplication : no Id");
                }                    
            }
			catch (BusinessException ex)
			{
                this.Notifier.Notify(Color.Danger, $"Une erreur est survenue : { string.Join(", ", ex.Messages)}");
			}
			finally
			{
                this.IsDuplicating = false;
                this.StateHasChanged();
            }
		}

		public async Task OnUnitDeleted(Guid unitId)
		{
			await this.ReloadDataAsync();
		}

		private async void OnVisibilityChange(Syncfusion.Blazor.Buttons.ChangeEventArgs<bool> args)
		{
			await this.UpdateVisibilityAsync();
		}

		private async void OnPublishedStateChange(Syncfusion.Blazor.Buttons.ChangeEventArgs<bool> args)
		{
			await this.UpdatePublishedStateAsync();
		}

		protected async Task CopyPersonalLink(string link)
		{
			await this.JSRuntime.InvokeVoidAsync("copyToClipBoard", link);

			this.Notifier.Notify(Color.Info, "Le lien a �t� copi� dans votre presse papier");
		}

		private async Task OnOptionsChange(Microsoft.AspNetCore.Components.ChangeEventArgs args)
		{
			if (this.FieldTripId.HasValue)
			{
				await this.SaveAsync();
			}
		}

		//private async Task OnReadingsVisibilityDateChange(ChangedEventArgs<DateTime?> args)
		//{
		//	if (this.FieldTripId.HasValue)
		//	{
		//		await this.SaveAsync();
		//	}
		//}

		private void OnMaxUploadedFileSizeReached(int size)
		{
			string sizeToDisplay = "";

			if (size >= 1_000 && size < 1_000_000)
			{
				sizeToDisplay = $"{decimal.Round((decimal)size / 1_000, 1, MidpointRounding.AwayFromZero)} Ko";
			}
			else if (size >= 1_000_000 && size < 1_000_000_000)
			{
				sizeToDisplay = $"{decimal.Round((decimal)size / 1_000_000, 1, MidpointRounding.AwayFromZero)} Mo";
			}
			else
			{
				sizeToDisplay = $"{size} o)";
			}

			this.Notifier.Notify(Color.Warning, $"Attention, la taille de votre fichier est de {sizeToDisplay}");
		}
	}
}